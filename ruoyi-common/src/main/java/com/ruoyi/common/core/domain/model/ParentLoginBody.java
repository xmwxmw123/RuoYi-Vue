package com.ruoyi.common.core.domain.model;

/**
 * 家长端登录对象
 * 
 * @author ruoyi
 */
public class ParentLoginBody extends LoginBody
{
    //部门id
    private Long deptId;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }
}
