package com.ruoyi.common.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class DateArrayTypeHandler extends BaseTypeHandler<Date[]> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Date[] parameter, JdbcType jdbcType) throws SQLException {
        // 将Date[]数组转换为合适的类型，例如字符串、数组等
        // 然后将转换结果设置到PreparedStatement中
        // 以下是一个示例，将Date[]数组转换为以逗号分隔的字符串存储
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < parameter.length; index++) {
            if (index > 0) {
                sb.append(",");
            }
            sb.append(parameter[index].getTime());
        }
        ps.setString(i, sb.toString());
    }

    @Override
    public Date[] getNullableResult(ResultSet rs, String columnName) throws SQLException {
        // 从ResultSet中获取存储的值，并将其转换为Date[]数组
        // 这里与setNonNullParameter方法中的转换方式相对应
        String value = rs.getString(columnName);
        if (value != null && !value.isEmpty()) {
            String[] parts = value.split(",");
            Date[] dates = new Date[parts.length];
            for (int index = 0; index < parts.length; index++) {
                dates[index] = new Date(Long.parseLong(parts[index]));
            }
            return dates;
        }
        return null;
    }

    @Override
    public Date[] getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        // 类似于getNullableResult(ResultSet rs, String columnName)方法
        // 请根据需要进行实现
        return null;
    }

    @Override
    public Date[] getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        // 类似于getNullableResult(ResultSet rs, String columnName)方法
        // 请根据需要进行实现
        return null;
    }
}
