import store from '@/store'
import config from '@/config'
import { getAccessToken, getRefreshToken, getTenantId, setToken } from '@/utils/auth'
import errorCode from '@/utils/errorCode'
import { toast, showConfirm, tansParams } from '@/utils/common'
import {refreshToken} from "@/api/login";

let timeout = 10000
const baseUrl = config.baseUrl
uni.setStorageSync('code','1')

// 需要忽略的提示。忽略后，自动 Promise.reject('error')
const ignoreMsgs = [
  "无效的刷新令牌", // 刷新令牌被删除时，不用提示
  "刷新令牌已过期" // 使用刷新令牌，刷新获取新的访问令牌时，结果因为过期失败，此时需要忽略。否则，会导致继续 401，无法跳转到登出界面
]

// 是否显示重新登录
export let isRelogin = { show: false };
// Axios 无感知刷新令牌，参考 https://www.dashingdog.cn/article/11 与 https://segmentfault.com/a/1190000020210980 实现
// 请求队列
let requestList = []
// 是否正在刷新中
let isRefreshToken = false



const request = config => {
	let tenantId='1'
	//是否有code
	if(uni.getStorageSync('code')){
	
		 tenantId= uni.getStorageSync('code')
	}
  // 是否需要设置 token
  const isToken = (config.headers || {}).isToken === false
  config.header = config.header || {}
  if (getAccessToken() && !isToken) {
    config.header['Authorization'] = 'Bearer ' + getAccessToken()
  }
  // 设置租户 TODO 芋艿：强制 1 先
  config.header['tenant-id'] = tenantId;
  // get请求映射params参数
  if (config.params) {
    let url = config.url + '?' + tansParams(config.params)
    url = url.slice(0, -1)
    config.url = url
  }
  return new Promise((resolve, reject) => {
	
	    if(config.url!='/login'&&config.url!='/captchaImage' ){
	        // #ifdef APP-PLUS
	        plus.nativeUI.showWaiting('加载中....', '');
	        //#endif		
		}
	
    uni.request({
        method: config.method || 'get',
        timeout: config.timeout ||  timeout,
        url: config.baseUrl || baseUrl + config.url,
        data: config.data,
        // header: config.header,
        header: config.header,
        dataType: 'json'
      }).then(response => {
		if(config.url!='/login'&&config.url!='/captchaImage' ){
		  // #ifdef APP-PLUS
		  plus.nativeUI.closeWaiting();
		   //#endif 
		}
			
			
		 
			 
		  
		 
		
        let [error, res] = response
        if (error) {
          toast('后端接口连接异常')
          reject('后端接口连接异常')
          return
        }
        const code = res.data.code || 200
        const msg = errorCode[code] || res.data.msg || errorCode['default']
        if (code === 401) {
		 // 	if (!isRefreshToken) {
			//       isRefreshToken = true;
			//       // 1. 如果获取不到刷新令牌，则只能执行登出操作
			//       if (!getRefreshToken()) {
			//         return handleAuthorized();
			//       }
			//       // 2. 进行刷新访问令牌
			//       try {
			//         const refreshTokenRes =  refreshToken();
			//         // 2.1 刷新成功，则回放队列的请求 + 当前请求
			//         setToken(refreshTokenRes.data)
			//         requestList.forEach(cb => cb())
			//         return service(res.config)
			//       } catch (e) {// 为什么需要 catch 异常呢？刷新失败时，请求因为 Promise.reject 触发异常。
			//         // 2.2 刷新失败，只回放队列的请求
			//         requestList.forEach(cb => cb())
			//         // 提示是否要登出。即不回放当前请求！不然会形成递归
			//         return handleAuthorized();
			//       } finally {
			//         requestList = []
			//         isRefreshToken = false
			//       }
			//     } else {
			//       // 添加到队列，等待刷新获取到新的令牌
			//       return new Promise(resolve => {
			//         requestList.push(() => {
			//           res.config.headers['Authorization'] = 'Bearer ' + getAccessToken() // 让每个请求携带自定义token 请根据实际情况自行修改
			//           resolve(service(res.config))
			//         })
			//       })
			//     }
			
			
          showConfirm('登录状态已过期，您可以继续留在该页面，或者重新登录?').then(res => {
            if (res.confirm) {
              store.dispatch('LogOut').then(res => {
                uni.reLaunch({ url: '/pages/login' })
              })
            }
          })
          reject('无效的会话，或者会话已过期，请重新登录。')
        } else if (code === 500) {
          toast(msg)
          reject('500')
        } else if (code !== 200) {
          toast(msg)
          reject(code)
        }
		
        resolve(res.data)
      })
      .catch(error => {
		  
		  if(config.url!='/login'&&config.url!='/captchaImage' ){
		  // #ifdef APP-PLUS
		  plus.nativeUI.closeWaiting();
		   //#endif 
		  }
		
		 			 
		 
        let { message } = error
        if (message === 'Network Error') {
          message = '后端接口连接异常'
        } else if (message.includes('timeout')) {
          message = '系统接口请求超时'
        } else if (message.includes('Request failed with status code')) {
          message = '系统接口' + message.substr(message.length - 3) + '异常'
        }
		 
        toast(message)
		
        reject(error)
      })
  })
}

export default request
