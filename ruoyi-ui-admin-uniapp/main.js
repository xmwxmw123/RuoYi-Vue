import Vue from 'vue'
import App from './App'
import store from './store' // store
import plugins from './plugins' // plugins
import './permission' // permission
import func from './utils/tx_func.js'

Vue.use(plugins)
Vue.prototype.$getH5=func.getH5

Vue.config.productionTip = false
Vue.prototype.$store = store
App.mpType = 'app'
import uView from '@/uni_modules/uview-ui'
import {DICT_TYPE, getDictDataLabel, getDictDatas, getDictDatas2} from "@/utils/dict";
Vue.use(uView)
Vue.prototype.DICT_TYPE = DICT_TYPE



const app = new Vue({
  ...App
})

app.$mount()
