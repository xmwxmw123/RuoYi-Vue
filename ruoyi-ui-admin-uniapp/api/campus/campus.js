import request from '@/utils/request'

// 获取活动管理
export function getActivity(query) {
  return request({
    url: '/clubActivity/clubActivity/list',
	method: 'get',
	params: query
  })
}
// 获取社团列表
export function getCampus(query) {
  return request({
    url: '/club/club/list',
	method: 'get',
	params: query
  })
}
// 删除社团列表
export function deleteArr(query) {
  return request({
    url: '/club/club/'+query,
	method: 'delete',
  })
}
// 删除活动管理
export function deleteActivityArr(query) {
  return request({
    url: '/clubActivity/clubActivity/'+query,
	method: 'delete',
  })
}

//新增活动
export function addCampus(data) {
  return request({
    url: '/clubActivity/clubActivity',
	method: 'post',
	data: data
  })
}
//新增活动
export function editCampus(data) {
  return request({
    url: '/clubActivity/clubActivity',
	method: 'put',
	data: data
  })
}
//新增社团
export function addClub(data) {
  return request({
    url: '/club/club',
	method: 'post',
	data: data
  })
}
//添加学生
export function addStudnetS(data) {
  return request({
    url: '/club/club/addStu',
	method: 'post',
	data: data
  })
}


// 查看社团总人数
export function getStudent(query) {
  return request({
    url: '/club/club/stuList',
	method: 'get',
	params: query
  })
}

//变更责任
export function setHead(data) {
  return request({
    url: '/club/club/updateHead',
	method: 'put',
	data: data
  })
}
//删除
export function deleteNum(data) {
  return request({
    url: '/club/club/deleteStu',
	method: 'DELETE',
	data: data
  })
}


// 获取年级
export function getGradeName(query) {
  return request({
    url: '/grade/grade/query',
	method: 'get',
	params: query
  })
}
//获取班级
export function getClassName(query) {
  return request({
    url: '/class/class/findList',
	method: 'get',
	params: query
  })
}
//获取所选班级年级下的学生
export function getClassAndGrade(query) {
  return request({
    url: '/register/register/appQuery',
	method: 'get',
	params: query
  })
}

// 查询社团管理详细
export function getClub(id) {
  return request({
    url: '/club/club/' + id,
    method: 'get'
  })
}
export function queryStudentList(query) {
  return request({
    url: '/register/register/adoptGradeStudentIdarrQueryList',
    method: 'get',
    params: query
  })
}
export function newClubsAdded(data) {
  return request({
    url: '/club/club',
	method: 'post',
	data: data
  })
}
export function editClubs(id) {
  return request({
    url: '/club/club/'+id,
	method: 'get',
  })
}
export function updateClub(data) {
  return request({
    url: '/club/club',
    method: 'put',
    data: data
  })
}
























