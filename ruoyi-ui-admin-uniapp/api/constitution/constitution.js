import request from '@/utils/request'

// 全部数据
export function listQualityEvaluation(query) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/list',
    method: 'get',
    params: query
  })
}


// 批量删除
// 删除综合素质评价
export function delQualityEvaluation(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/' + data,
    method: 'delete'
  })
}
//获取批量新增时的班级
export function  getStudentClass() {
  return request({
    url: '/class/class/findListNotNull',
    'method': 'get',
  })
}

//单个新增
export function singleAdd(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation',
    method: 'post',
    data:data
  })
}
//批量新增
export function batchAdd(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/batchAdd',
    method: 'post',
    data:data
  })
}

// 修改综合素质评价
export function updateQualityEvaluation(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation',
    method: 'put',
    data: data
  })
}


//获取当前学期
export function  getNowSemesters(query) {
  return request({
    url: '/semesters/semesters/query',
   method: 'get',
   params: query
  })
}







