import request from '@/utils/request'

// 获取级别信息
export function getGrade(query) {
  return request({
    url: '/grade/grade/query',
	method: 'get',
	params: query
  })
}

// 通过级别获取班级
export function getClass(id) {
  return request({
    url: '/class/class/queryRankSList?id='+id,
	method: 'get',
  })
}
// 查询当前年级班级下的人
export function getListData(query) {
  return request({
    url: '/educationEvaluation/educationEvaluation/list',
	method: 'get',
	params: query
  })
}


// 发奖章
export function  IssueMedals(data) {
  return request({
    url: '/educationEvaluation/educationEvaluation/batch',
    'method': 'PUT',
	 'data': data
  })
}












