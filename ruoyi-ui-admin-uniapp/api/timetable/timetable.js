import request from '@/utils/request'

// 获取课程
export function getTimeTable(query) {
  return request({
    url: '/timetable/timetable/getCpTimetableList',
	method: 'get',
	params: query
  })
}







