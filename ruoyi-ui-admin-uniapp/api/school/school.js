import request from '@/utils/request'

// 数据总览
export function getListData() {
    return request({
      url: '/campus/campus/list',
    })
}
// 新增
export function AddData(data) {
    return request({
      url: '/campus/campus',
      method: 'post',
      data: data
    })
}
// 修改
export function setData(data) {
    return request({
      url: '/campus/campus',
      method: 'PUT',
      data: data
    })
}
// 上传图片
export function getImgApi(data) {
    return request({
      url: '/campus/campus/picture',
      method: 'post',
      data: data
    })
}

