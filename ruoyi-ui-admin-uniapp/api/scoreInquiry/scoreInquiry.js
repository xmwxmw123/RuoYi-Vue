import request from '@/utils/request'

// 获取成绩信息
export function  scoreInquiry(query) {
  return request({
    url: '/scoreInquiry/scoreInquiry/list',
	method: 'get',
	params: query
  })
}

// 获取学科类别
export function getSubject() {
  return request({
    url: '/subject/subject/list',
    method: 'get'
  })
}

//获取场地详细信息
export function  getSiteData(data) {
  return request({
    url: '/court/calendar/courtCalendarList',
    'method': 'post',
	 'data': data
  })
}





