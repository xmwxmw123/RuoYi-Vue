import request from '@/utils/request'

// 查询教师任课设置列表
export function listTimetableSet(query) {
  return request({
    url: '/timetableSet/timetableSet/list',
    method: 'get',
    params: query
  })
}

//修改排课
export function arrangeCourseList(id) {
  return request({
    url: '/timetableSet/timetableSet/arrangeCourseList?id=' + id,
    method: 'get',
  })
}

// 提交教师任课设置列表
export function arrangeCourse(data) {
  return request({
    url: '/timetableSet/timetableSet/arrangeCourse',
    method: 'post',
    data: data
  })
}

// 查询教师任课设置详细
export function getTimetableSet(id) {
  return request({
    url: '/timetableSet/timetableSet/' + id,
    method: 'get'
  })
}

// 新增教师任课设置
export function addTimetableSet(data) {
  return request({
    url: '/timetableSet/timetableSet',
    method: 'post',
    data: data
  })
}

// 修改教师任课设置
export function updateTimetableSet(data) {
  return request({
    url: '/timetableSet/timetableSet',
    method: 'put',
    data: data
  })
}

// 删除教师任课设置
export function delTimetableSet(id) {
  return request({
    url: '/timetableSet/timetableSet/' + id,
    method: 'delete'
  })
}
