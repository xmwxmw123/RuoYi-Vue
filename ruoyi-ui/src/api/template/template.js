import request from '@/utils/request'

// 查询评价模板列表
export function listTemplate(query) {
  return request({
    url: '/template/template/list',
    method: 'get',
    params: query
  })
}

// 查询评价模板详细
export function getTemplate(id) {
  return request({
    url: '/template/template/' + id,
    method: 'get'
  })
}

// 新增评价模板
export function addTemplate(data) {
  return request({
    url: '/template/template',
    method: 'post',
    data: data
  })
}

// 修改评价模板
export function updateTemplate(data) {
  return request({
    url: '/template/template',
    method: 'put',
    data: data
  })
}
// 是否启用
export function updateEnable(id) {
  return request({
    url: '/template/template/updateEnable?id=' + id,
    method: 'put',
  })
}

// 删除评价模板
export function delTemplate(id) {
  return request({
    url: '/template/template/' + id,
    method: 'delete'
  })
}

//录入完成
export function complete(id) {
  return request({
    url: 'template/template/complete?id=' + id,
    method: 'put'
  })
}

//二级指标保存接口
export function twoIndexS(data) {
  return request({
    url: 'template/template/twoIndex',
    method: 'put',
    data: data
  })
}

//记录要点新增
export function records(data) {
  return request({
    url: 'template/template/record',
    method: 'put',
    data: data
  })
}

//记录要点删除
export function deleteRecord(data) {
  return request({
    url: '/template/template/deleteRecord',
    method: 'delete',
    data: data
  })
}
