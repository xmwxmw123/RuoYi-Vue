import request from '@/utils/request'

// 查询社团管理列表
export function listClub(query) {
  return request({
    url: '/club/club/list',
    method: 'get',
    params: query
  })
}

// 查询社团管理详细
export function getClub(id) {
  return request({
    url: '/club/club/' + id,
    method: 'get'
  })
}

// 新增社团管理
export function addClub(data) {
  return request({
    url: '/club/club',
    method: 'post',
    data: data
  })
}

// 修改社团管理
export function updateClub(data) {
  return request({
    url: '/club/club',
    method: 'put',
    data: data
  })
}

// 删除社团管理
export function delClub(id) {
  return request({
    url: '/club/club/' + id,
    method: 'delete'
  })
}

// 社团添加学生
export function addStu(data) {
  return request({
    url: '/club/club/addClubPeople',
    method: 'post',
    data: data
  })
}

// 查询社团学生
export function stuList(query) {
  return request({
    url: '/club/club/stuList',
    method: 'get',
    params: query
  })
}

// 社团删除学生
export function deleteStu(data) {
  return request({
    url: '/club/club/deleteStu',
    method: 'delete',
    data: data
  })
}

//社团设置负责人和解除负责人
export function updateHead(data) {
  return request({
    url: '/club/club/updateHead',
    method: 'put',
    data: data
  })
}

//查询社团list接口
export function queryList(query) {
  return request({
    url: '/club/club/queryList',
    method: 'get',
    params: query
  })
}

export function queryStudentList(query) {
  return request({
    url: '/register/register/adoptGradeStudentIdarrQueryList',
    method: 'get',
    params: query
  })
}


