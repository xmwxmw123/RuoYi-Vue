import request from '@/utils/request'

// 查询社团活动列表
export function listClubActivity(query) {
  return request({
    url: '/clubActivity/clubActivity/list',
    method: 'get',
    params: query
  })
}

// 查询社团活动详细
export function getClubActivity(id) {
  return request({
    url: '/clubActivity/clubActivity/' + id,
    method: 'get'
  })
}

// 新增社团活动
export function addClubActivity(data) {
  return request({
    url: '/clubActivity/clubActivity',
    method: 'post',
    data: data
  })
}

// 修改社团活动
export function updateClubActivity(data) {
  return request({
    url: '/clubActivity/clubActivity',
    method: 'put',
    data: data
  })
}

// 删除社团活动
export function delClubActivity(id) {
  return request({
    url: '/clubActivity/clubActivity/' + id,
    method: 'delete'
  })
}
