import request from '@/utils/request'

// 查询考试管理列表
export function listExaminationManagement(query) {
  return request({
    url: '/examinationManagement/examinationManagement/list',
    method: 'get',
    params: query
  })
}



// 查询考试管理详细
export function getExaminationManagement(id) {
  return request({
    url: '/examinationManagement/examinationManagement/' + id,
    method: 'get'
  })
}

// 新增考试管理
export function addExaminationManagement(data) {
  return request({
    url: '/examinationManagement/examinationManagement',
    method: 'post',
    data: data
  })
}

// 修改满分
export function setNum(data) {
  return request({
    url: '/examinationManagement/examinationManagement/fullMark',
    method: 'PUT',
    data: data
  })
}

// 修改考试管理
export function updateExaminationManagement(data) {
  return request({
    url: '/examinationManagement/examinationManagement',
    method: 'put',
    data: data
  })
}

// 删除考试管理
export function delExaminationManagement(id) {
  return request({
    url: '/examinationManagement/examinationManagement/' + id,
    method: 'delete'
  })
}

// 查询成绩录入
export function getScoreEntry(id) {
  return request({
    url: 'examinationManagement/examinationManagement/scoreEntry/'+id,
    method: 'get',

  })
}
// 查询成绩录入详细
export function getScoreEntryList(query) {
  return request({
    url: 'examinationManagement/examinationManagement/scoreEntry/list',
    method: 'get',
     params: query
  })
}
//开会录入
export function getEntering(query) {
  return request({
    url: 'scoreInquiry/scoreInquiry/add',
    method: 'get',
     params: query
  })
}
//保存开始录入
export function scoreInquiryUpdate(data) {
  return request({
    url: 'scoreInquiry/scoreInquiry/update',
    method: 'put',
    data:data
  })
}
//结束录入
export function endEntry(id) {
  return request({
    url: 'scoreInquiry/scoreInquiry/updateStatus?id='+id,
    method: 'put',

  })
}
