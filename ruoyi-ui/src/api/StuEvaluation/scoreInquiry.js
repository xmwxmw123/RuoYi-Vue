import request from '@/utils/request'

// 查询成绩查询(用于存放各个班级里学生的成绩)列表
export function listScoreInquiry(query) {
  return request({
    url: '/scoreInquiry/scoreInquiry/list',
    method: 'get',
    params: query
  })
}

// 查询成绩查询(用于存放各个班级里学生的成绩)详细
export function getScoreInquiry(id) {
  return request({
    url: '/scoreInquiry/scoreInquiry/' + id,
    method: 'get'
  })
}

// 新增成绩查询(用于存放各个班级里学生的成绩)
export function addScoreInquiry(data) {
  return request({
    url: '/scoreInquiry/scoreInquiry',
    method: 'post',
    data: data
  })
}

// 修改成绩查询(用于存放各个班级里学生的成绩)
export function updateScoreInquiry(data) {
  return request({
    url: '/scoreInquiry/scoreInquiry',
    method: 'put',
    data: data
  })
}

// 删除成绩查询(用于存放各个班级里学生的成绩)
export function delScoreInquiry(id) {
  return request({
    url: '/scoreInquiry/scoreInquiry/' + id,
    method: 'delete'
  })
}
