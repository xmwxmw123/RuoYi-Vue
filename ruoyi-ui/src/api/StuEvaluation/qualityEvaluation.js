import request from '@/utils/request'

// 查询综合素质评价列表
export function listQualityEvaluation(query) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/list',
    method: 'get',
    params: query
  })
}

// 查询综合素质评价详细
export function getQualityEvaluation(id) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/' + id,
    method: 'get'
  })
}

// 新增综合素质评价
export function addQualityEvaluation(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation',
    method: 'post',
    data: data
  })
}

// 修改综合素质评价
export function updateQualityEvaluation(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation',
    method: 'put',
    data: data
  })
}

// 删除综合素质评价
export function delQualityEvaluation(id) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/' + id,
    method: 'delete'
  })
}
//单个新增
export function singleAdd(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation',
    method: 'post',
    data:data
  })
}
//批量新增
export function batchAdd(data) {
  return request({
    url: '/qualityEvaluation/qualityEvaluation/batchAdd',
    method: 'post',
    data:data
  })
}