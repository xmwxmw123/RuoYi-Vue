import request from '@/utils/request'

// 查询体质测评列表
export function listGradeAssessment(query) {
  return request({
    url: '/gradeAssessment/gradeAssessment/list',
    method: 'get',
    params: query
  })
}

// 查询体质测评详细
export function getGradeAssessment(id) {
  return request({
    url: '/gradeAssessment/gradeAssessment/' + id,
    method: 'get'
  })
}

// 新增体质测评
export function addGradeAssessment(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment',
    method: 'post',
    data: data
  })
}

// 修改体质测评
export function updateGradeAssessment(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment',
    method: 'put',
    data: data
  })
}

// 删除体质测评
export function delGradeAssessment(id) {
  return request({
    url: '/gradeAssessment/gradeAssessment/' + id,
    method: 'delete'
  })
}

// 批量新增体质测评
export function gradeBatchAdd(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment/batchAdd',
    method: 'post',
    data: data
  })
}