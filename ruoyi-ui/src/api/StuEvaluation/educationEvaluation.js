import request from '@/utils/request'

// 查询五育评价列表
export function listEducationEvaluation(query) {
  return request({
    url: '/educationEvaluation/educationEvaluation/list',
    method: 'get',
    params: query
  })
}

// 查询五育评价详细
export function getEducationEvaluation(id) {
  return request({
    url: '/educationEvaluation/educationEvaluation/' + id,
    method: 'get'
  })
}

// 新增五育评价
export function addEducationEvaluation(data) {
  return request({
    url: '/educationEvaluation/educationEvaluation',
    method: 'post',
    data: data
  })
}

// 修改五育评价
export function updateEducationEvaluation(data) {
  return request({
    url: '/educationEvaluation/educationEvaluation',
    method: 'put',
    data: data
  })
}

// 删除五育评价
export function delEducationEvaluation(id) {
  return request({
    url: '/educationEvaluation/educationEvaluation/' + id,
    method: 'delete'
  })
}

//批量发奖章
export function batchEducationEvaluation(data) {
  return request({
    url: 'educationEvaluation/educationEvaluation/batch',
    method: 'put',
    data: data
  })
}