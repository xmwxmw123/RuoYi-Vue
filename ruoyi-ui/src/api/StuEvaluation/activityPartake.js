import request from '@/utils/request'

// 查询活动参与情况列表
export function listActivityPartake(query) {
  return request({
    url: '/activityPartake/activityPartake/list',
    method: 'get',
    params: query
  })
}

// 查询活动参与情况详细
export function getActivityPartake(id) {
  return request({
    url: '/activityPartake/activityPartake/' + id,
    method: 'get'
  })
}

// 新增活动参与情况
export function addActivityPartake(data) {
  return request({
    url: '/activityPartake/activityPartake',
    method: 'post',
    data: data
  })
}

// 修改活动参与情况
export function updateActivityPartake(data) {
  return request({
    url: '/activityPartake/activityPartake',
    method: 'put',
    data: data
  })
}

// 删除活动参与情况
export function delActivityPartake(id) {
  return request({
    url: '/activityPartake/activityPartake/' + id,
    method: 'delete'
  })
}

//上传图片
export function uploadWorks(data) {
  return request({
    url: '/activityPartake/activityPartake/uploadWorks',
    method: 'put',
    data: data
  })
}
//发放奖章
export function distributeMedal(data) {
  return request({
    url: '/activityPartake/activityPartake/distributeMedal',
    method: 'put',
    data: data
  })
}