import request from '@/utils/request'

// 查询奖章记录列表
export function listMedalRecord(query) {
  return request({
    url: '/medalRecord/medalRecord/list',
    method: 'get',
    params: query
  })
}

// 查询奖章记录详细
export function getMedalRecord(id) {
  return request({
    url: '/medalRecord/medalRecord/' + id,
    method: 'get'
  })
}

// 新增奖章记录
export function addMedalRecord(data) {
  return request({
    url: '/medalRecord/medalRecord',
    method: 'post',
    data: data
  })
}

// 修改奖章记录
export function updateMedalRecord(data) {
  return request({
    url: '/medalRecord/medalRecord',
    method: 'put',
    data: data
  })
}

// 删除奖章记录
export function delMedalRecord(id) {
  return request({
    url: '/medalRecord/medalRecord/' + id,
    method: 'delete'
  })
}

//撤销奖章
export function revocation(id) {
  return request({
    url: 'medalRecord/medalRecord/revocation?id=' + id,
    method: 'put'
  })
}