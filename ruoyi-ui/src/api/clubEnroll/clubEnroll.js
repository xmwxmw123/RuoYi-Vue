import request from '@/utils/request'

// 查询社团报名列表
export function listClubEnroll(query) {
  return request({
    url: '/clubEnroll/clubEnroll/list',
    method: 'get',
    params: query
  })
}

// 查询社团报名详细
export function getClubEnroll(id) {
  return request({
    url: '/clubEnroll/clubEnroll/' + id,
    method: 'get'
  })
}

// 新增社团报名
export function addClubEnroll(data) {
  return request({
    url: '/clubEnroll/clubEnroll',
    method: 'post',
    data: data
  })
}

// 修改社团报名
export function updateClubEnroll(data) {
  return request({
    url: '/clubEnroll/clubEnroll',
    method: 'put',
    data: data
  })
}

// 删除社团报名
export function delClubEnroll(id) {
  return request({
    url: '/clubEnroll/clubEnroll/' + id,
    method: 'delete'
  })
}
// 查询社团报名列表
export function  modifyState(query) {
  return request({
    url: '/clubEnroll/clubEnroll/enrollExamine',
    method: 'get',
    params: query
  })
}


