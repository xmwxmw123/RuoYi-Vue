import request from '@/utils/request'

// 查询家长账号列表
export function listParent(query) {
  return request({
    url: '/parent/parent/list',
    method: 'get',
    params: query
  })
}

// 查询家长账号详细
export function getParent(id) {
  return request({
    url: '/parent/parent/' + id,
    method: 'get'
  })
}

// 新增家长账号
export function addParent(data) {
  return request({
    url: '/parent/parent',
    method: 'post',
    data: data
  })
}

// 修改家长账号
export function updateParent(data) {
  return request({
    url: '/parent/parent',
    method: 'put',
    data: data
  })
}

// 删除家长账号
export function delParent(id) {
  return request({
    url: '/parent/parent/' + id,
    method: 'delete'
  })
}

//账号是否可用
export function usableStatus(id) {
  return request({
    url: '/parent/parent/usableStatus?id=' + id,
    method: 'put',
  })
}
// 查询家长账号详细
export function adjustingStudentStatus(query) {
  return request({
    url: '/parent/parent/examine',
    method: 'get',
	params: query
  })
} 
