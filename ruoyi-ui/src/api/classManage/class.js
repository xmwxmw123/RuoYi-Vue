import request from '@/utils/request'

// 查询设置班级列表
export function listClass(query) {
  return request({
    url: '/class/class/list',
    method: 'get',
    params: query
  })
}

// 查询设置班级详细
export function getClass(id) {
  return request({
    url: '/class/class/' + id,
    method: 'get'
  })
}

// 新增设置班级
export function addClass(data) {
  return request({
    url: '/class/class',
    method: 'post',
    data: data
  })
}

// 修改设置班级
export function updateClass(data) {
  return request({
    url: '/class/class',
    method: 'put',
    data: data
  })
}

// 删除设置班级 
export function delClass(id) {
  return request({
    url: '/class/class/' + id,
    method: 'delete'
  })
}

// 班级list接口 （需要传级别id）
export function queryRankSList(query) {
  return request({
    url: '/class/class/queryRankSList',
    method: 'get',
    params: query
  })
}
// 班级list接口 （可不带参数）
export function findList(query) {
  return request({
    url: '/class/class/findList',
    method: 'get',
    params: query
  })
}
// 班级list接口 （可不带参数）
export function findListNotNull(query) {
  return request({
    url: '/class/class/findListNotNull',
    method: 'get',
    params: query
  })
}
