import request from '@/utils/request'

// 查询设置学期列表
export function listSemesters(query) {
  return request({
    url: '/semesters/semesters/list',
    method: 'get',
    params: query
  })
}

// 查询设置学期详细
export function getSemesters(id) {
  return request({
    url: '/semesters/semesters/' + id,
    method: 'get'
  })
}

// 新增设置学期
export function addSemesters(data) {
  return request({
    url: '/semesters/semesters',
    method: 'post',
    data: data
  })
}

// 修改设置学期
export function updateSemesters(data) {
  return request({
    url: '/semesters/semesters',
    method: 'put',
    data: data
  })
}

// 删除设置学期
export function delSemesters(id) {
  return request({
    url: '/semesters/semesters/' + id,
    method: 'delete'
  })
}

//学期list接口
export function SemestersListQuery(query) {
  return request({
    url: 'semesters/semesters/query',
    method: 'get',
    params: query
  })
}

//设置当前学期接口
export function SelectNowSemesters(id) {
  return request({
    url: 'semesters/semesters/updatesemester?id=' + id,
    method: 'post',
  })
}