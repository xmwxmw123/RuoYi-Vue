import request from '@/utils/request'

// 查询设置级别列表
export function listGrade(query) {
  return request({
    url: '/grade/grade/list',
    method: 'get',
    params: query
  })
}

// 查询设置级别详细
export function getGrade(id) {
  return request({
    url: '/grade/grade/' + id,
    method: 'get'
  })
}

// 新增设置级别
export function addGrade(data) {
  return request({
    url: '/grade/grade',
    method: 'post',
    data: data
  })
}

// 修改设置级别
export function updateGrade(data) {
  return request({
    url: '/grade/grade',
    method: 'put',
    data: data
  })
}

// 删除设置级别
export function delGrade(id) {
  return request({
    url: '/grade/grade/' + id,
    method: 'delete'
  })
}
// 升级当前年级
export function upgrade(data) {
  return request({
    url: '/grade/grade/upgrade',
    method: 'post',
    data:data
  })
}
// 级别list接口
export function gradeQuery(query) {
  return request({
    url: '/grade/grade/query',
    method: 'get',
    params: query
  })
}
