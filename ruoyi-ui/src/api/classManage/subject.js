import request from '@/utils/request'

// 查询设置学科分页
export function listSubjectPage(query) {
  return request({
    url: '/subject/subject/list',
    method: 'get',
    params: query
  })
}

// 查询设置学科列表列表
export function listSubject(query) {
  return request({
    url: '/subject/subject/query',
    method: 'get',
    params: query
  })
}

// 查询设置学科详细
export function getSubject(id) {
  return request({
    url: '/subject/subject/' + id,
    method: 'get'
  })
}

// 新增设置学科
export function addSubject(data) {
  return request({
    url: '/subject/subject',
    method: 'post',
    data: data
  })
}

// 修改设置学科
export function updateSubject(data) {
  return request({
    url: '/subject/subject',
    method: 'put',
    data: data
  })
}

// 删除设置学科
export function delSubject(id) {
  return request({
    url: '/subject/subject/' + id,
    method: 'delete'
  })
}
