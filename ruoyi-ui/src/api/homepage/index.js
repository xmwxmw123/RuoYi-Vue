import request from '@/utils/request'

// 数据总览
export function HomePageData(data) {
    return request({
      url: '/homepage/HomePageData',
      method: 'post',
      data: data
    })
}
// 综合素质评价
export function HomePageMedal(data) {
    return request({
      url: 'homepage/HomePageMedal',
      method: 'post',
      data: data
    })
}
// 学业测评
export function HomePageAssessment(data) {
    return request({
      url: '/homepage/HomePageAssessment',
      method: 'post',
      data: data
    })
}

//体质测评
export function HomePageFitness(data) {
  return request({
    url: '/homepage/HomePageFitness',
    method: 'post',
    data: data
  })
}
//年级和级别,班级
export function homepage(query) {
  return request({
    url: '/homepage',
    method: 'get',
    params: query
  })
}