import request from '@/utils/request'

// 查询课表查询列表
export function listTimetable(query) {
  return request({
    url: '/timetable/timetable/list',
    method: 'get',
    params: query
  })
}

// 查询课表查询详细
export function getTimetable(id) {
  return request({
    url: '/timetable/timetable/' + id,
    method: 'get'
  })
}

// 新增课表查询
export function addTimetable(data) {
  return request({
    url: '/timetable/timetable',
    method: 'post',
    data: data
  })
}

// 修改课表查询
export function updateTimetable(data) {
  return request({
    url: '/timetable/timetable',
    method: 'put',
    data: data
  })
}

// 删除课表查询
export function delTimetable(id) {
  return request({
    url: '/timetable/timetable/' + id,
    method: 'delete'
  })
}
