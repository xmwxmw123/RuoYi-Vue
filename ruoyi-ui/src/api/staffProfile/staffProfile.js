import request from '@/utils/request'

// 查询教工档案列表
export function listStaffProfile(query) {
  return request({
    url: '/staffProfile/staffProfile/list',
    method: 'get',
    params: query
  })
}
// 查询教工档案列表
export function listStaffProfileAll() {
  return request({
    url: '/staffProfile/staffProfile/query',
    method: 'get',
  })
}


// 查询教工档案详细
export function getStaffProfile(id) {
  return request({
    url: '/staffProfile/staffProfile/' + id,
    method: 'get'
  })
}

// 新增教工档案
export function addStaffProfile(data) {
  return request({
    url: '/staffProfile/staffProfile',
    method: 'post',
    data: data
  })
}

// 修改教工档案
export function updateStaffProfile(data) {
  return request({
    url: '/staffProfile/staffProfile',
    method: 'put',
    data: data
  })
}

// 删除教工档案
export function delStaffProfile(id) {
  return request({
    url: '/staffProfile/staffProfile/' + id,
    method: 'delete'
  })
}

// 教工档案list接口
export function selectById(query) {
  return request({
    url: '/staffProfile/staffProfile/selectById',
    method: 'get',
    params: query
  })
}
