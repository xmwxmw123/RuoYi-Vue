import request from '@/utils/request'

// 查询学籍列表
export function listRegister(query) {
  return request({
    url: '/register/register/list',
    method: 'get',
    params: query
  })
}

// 查询学籍详细
export function getRegister(id) {
  return request({
    url: '/register/register/' + id,
    method: 'get'
  })
}

// 新增学籍
export function addRegister(data) {
  return request({
    url: '/register/register',
    method: 'post',
    data: data
  })
}

// 修改学籍
export function updateRegister(data) {
  return request({
    url: '/register/register',
    method: 'put',
    data: data
  })
}

// 删除学籍
export function delRegister(id) {
  return request({
    url: '/register/register/' + id,
    method: 'delete'
  })
}

// 转班
export function changeClass(data) {
  return request({
    url: '/register/register/changeClass',
    method: 'put',
    data: data,
  })
}
// 休学
export function furlough(data) {
  return request({
    url: 'register/register/furlough',
    method: 'put',
    data: data,
  })
}
// 复学
export function reentry(data) {
  return request({
    url: 'register/register/reentry',
    method: 'put',
    data: data,
  })
}

// 查询学籍列表  可以用学生姓名 年级  班级名查询
export function registerQuery(query) {
  return request({
    url: '/register/register/query',
    method: 'get',
    params: query
  })
}