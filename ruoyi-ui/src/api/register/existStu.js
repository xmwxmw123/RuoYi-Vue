import request from '@/utils/request'

// 查询在校学生列表
export function listExistStu(query) {
  return request({
    url: '/existStu/existStu/list',
    method: 'get',
    params: query
  })
}

// 查询在校学生详细
export function getExistStu(id) {
  return request({
    url: '/existStu/existStu/' + id,
    method: 'get'
  })
}

// 新增在校学生
export function addExistStu(data) {
  return request({
    url: '/existStu/existStu',
    method: 'post',
    data: data
  })
}

// 修改在校学生
export function updateExistStu(data) {
  return request({
    url: '/existStu/existStu',
    method: 'put',
    data: data
  })
}

// 删除在校学生
export function delExistStu(id) {
  return request({
    url: '/existStu/existStu/' + id,
    method: 'delete'
  })
}
