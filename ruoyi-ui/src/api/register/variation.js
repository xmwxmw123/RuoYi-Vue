import request from '@/utils/request'

// 查询变动学生列表
export function listVariation(query) {
  return request({
    url: '/variation/variation/list',
    method: 'get',
    params: query
  })
}

// 查询变动学生详细
export function getVariation(id) {
  return request({
    url: '/variation/variation/' + id,
    method: 'get'
  })
}

// 新增变动学生
export function addVariation(data) {
  return request({
    url: '/variation/variation',
    method: 'post',
    data: data
  })
}

// 修改变动学生
export function updateVariation(data) {
  return request({
    url: '/variation/variation',
    method: 'put',
    data: data
  })
}

// 删除变动学生
export function delVariation(id) {
  return request({
    url: '/variation/variation/' + id,
    method: 'delete'
  })
}
