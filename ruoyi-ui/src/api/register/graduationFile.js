import request from '@/utils/request'

// 查询毕业学生档案列表
export function listGraduationFile(query) {
  return request({
    url: '/graduationFile/graduationFile/list',
    method: 'get',
    params: query
  })
}

// 查询毕业学生档案详细
export function getGraduationFile(id) {
  return request({
    url: '/graduationFile/graduationFile/' + id,
    method: 'get'
  })
}

// 新增毕业学生档案
export function addGraduationFile(data) {
  return request({
    url: '/graduationFile/graduationFile',
    method: 'post',
    data: data
  })
}

// 修改毕业学生档案
export function updateGraduationFile(data) {
  return request({
    url: '/graduationFile/graduationFile',
    method: 'put',
    data: data
  })
}

// 删除毕业学生档案
export function delGraduationFile(id) {
  return request({
    url: '/graduationFile/graduationFile/' + id,
    method: 'delete'
  })
}
