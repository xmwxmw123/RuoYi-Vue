package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpParent;

import java.util.List;

/**
 * 家长账号Service接口
 * 
 * @author ruoyi
 * @date 2023-09-27
 */
public interface ICpParentService 
{
    /**
     * 查询家长账号
     * 
     * @param id 家长账号主键
     * @return 家长账号
     */
    public CpParent selectCpParentById(Long id);

    /**
     * 查询家长账号列表
     * 
     * @param cpParent 家长账号
     * @return 家长账号集合
     */
    public List<CpParent> selectCpParentList(CpParent cpParent);

    /**
     * 新增家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    public int insertCpParent(CpParent cpParent);

    /**
     * 修改家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    public int updateCpParent(CpParent cpParent);

    /**
     * 批量删除家长账号
     * 
     * @param ids 需要删除的家长账号主键集合
     * @return 结果
     */
    public int deleteCpParentByIds(Long[] ids);

    /**
     * 删除家长账号信息
     * 
     * @param id 家长账号主键
     * @return 结果
     */
    public int deleteCpParentById(Long id);

    /**
     * 修改家长账号状态
     * @param id
     * @return
     */
    int updateUsableStatus(Long id);

    /**
     * 查询家长账号信息
     * @param username 登录账号
     * @param password 登录密码
     * @param deptId 部门id
     * @return 家长信息
     */
    CpParent selectCpParentInfo(String username, String password, Long deptId);

    /**
     * 家长账号注册（家长端）
     * @param name 用户姓名
     * @param username 登录账号
     * @param password 登录密码
     * @param deptId 部门id
     */
    void cpParentRegister(String name, String username, String password, Long deptId);

    /**
     * 绑定学生(家长APP端)
     * @param cpParent 参数（只需要传入id，bdxs字段）
     */
    void bindingStudent(CpParent cpParent);

    /**
     * 审核绑定学生
     * @param id 家长绑定学生表主键
     * @param type  1-通过, 2-未通过
     */
    void examine(Long id, String type);

    /**
     * 我的资料修改（家长APP端）
     * @param cpParent 参数
     */
    void updMyProfile(CpParent cpParent);
}
