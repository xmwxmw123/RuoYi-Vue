package com.ruoyi.campus.service;


import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportSemesters;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设置学期Service接口
 * 
 * @author ruoyi
 * @date 2023-07-27
 */
public interface ICpSemestersService 
{
    /**
     * 查询设置学期
     * 
     * @param id 设置学期主键
     * @return 设置学期
     */
    public CpSemesters selectCpSemestersById(Long id);

    /**
     * 查询设置学期列表
     * 
     * @param cpSemesters 设置学期
     * @return 设置学期集合
     */
    public List<CpSemesters> selectCpSemestersList(CpSemesters cpSemesters);

    /**
     * 新增设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    public int insertCpSemesters(CpSemesters cpSemesters);

    /**
     * 修改设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    public int updateCpSemesters(CpSemesters cpSemesters);

    /**
     * 批量删除设置学期
     * 
     * @param ids 需要删除的设置学期主键集合
     * @return 结果
     */
    public int deleteCpSemestersByIds(Long[] ids);

    /**
     * 删除设置学期信息
     * 
     * @param id 设置学期主键
     * @return 结果
     */
    public int deleteCpSemestersById(Long id);

    /* 查询学期 */
    List<CpSemesters> query(CpSemesters cpSemesters);

    /* 设置为当前学期*/
    int setCurrentSemester(Long id, Long deptId);

    /**
     *  导出
     * @param exportSemesters
     * @return
     */
        List<ExportSemesters> ExportSelectCpSemestersList(ExportSemesters exportSemesters);

}
