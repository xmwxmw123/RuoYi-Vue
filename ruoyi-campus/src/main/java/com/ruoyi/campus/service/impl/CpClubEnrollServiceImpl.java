package com.ruoyi.campus.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.ruoyi.campus.domain.CpClub;
import com.ruoyi.campus.mapper.CpClubMapper;
import com.ruoyi.campus.service.ICpClubService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.campus.mapper.CpClubEnrollMapper;
import com.ruoyi.campus.domain.CpClubEnroll;
import com.ruoyi.campus.service.ICpClubEnrollService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 社团报名Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
@Service
public class CpClubEnrollServiceImpl implements ICpClubEnrollService 
{
    @Resource
    private CpClubEnrollMapper cpClubEnrollMapper;

    @Resource
    private ICpClubService cpClubService;

    @Resource
    private CpClubMapper cpClubMapper;

    /**
     * 查询社团报名
     * 
     * @param id 社团报名主键
     * @return 社团报名
     */
    @Override
    public CpClubEnroll selectCpClubEnrollById(Long id)
    {
        return cpClubEnrollMapper.selectCpClubEnrollById(id);
    }

    /**
     * 查询社团报名列表
     * 
     * @param cpClubEnroll 社团报名
     * @return 社团报名
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpClubEnroll> selectCpClubEnrollList(CpClubEnroll cpClubEnroll)
    {
        return cpClubEnrollMapper.selectCpClubEnrollList(cpClubEnroll);
    }

    /**
     * 新增社团报名
     * @param cpClubEnroll 报名信息
     * @param status 审核状态
     * @param source 来源
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertCpClubEnroll(CpClubEnroll cpClubEnroll, Long status, Long source)
    {
        if (Objects.isNull(cpClubEnroll.getRegisterId()) || Objects.isNull(cpClubEnroll.getClubId()) || StringUtils.isBlank(cpClubEnroll.getClubName())) {
            throw new GlobalException("参数错误");
        }
        CpClub club = cpClubService.selectCpClubById(cpClubEnroll.getClubId());
        if (Objects.isNull(club)) {
            throw new GlobalException("当前社团不存在");
        }
        //判断社团相关校验并添加社团学生
        this.handleCpClubInfo(club, cpClubEnroll.getRegisterId(), source.equals(1L));
        cpClubEnroll.setUserId(club.getUserId());
        cpClubEnroll.setDeptId(club.getDeptId());
        cpClubEnroll.setCreateTime(DateUtils.getNowDate());
        cpClubEnroll.setStatus(status);
        cpClubEnroll.setSource(source);
        return cpClubEnrollMapper.insertCpClubEnroll(cpClubEnroll);
    }

    /**
     * 判断社团相关校验并添加社团学生
     * @param club 社团信息
     * @param registerId 学生id
     * @param type 是否为后台报名
     */
    private void handleCpClubInfo(CpClub club, Long registerId, boolean type) {
        if (StringUtils.isNotEmpty(club.getClubStudent())) {
            if (Arrays.stream(club.getClubStudent().split(",")).anyMatch(e -> Long.valueOf(e).equals(registerId))) {
                throw new GlobalException("当前学生已报名");
            }
            club.setClubStudent(club.getClubStudent() + "," + registerId);
        } else {
            club.setClubStudent(registerId.toString());
        }
        //判断是否超过参与人数
        if (club.getClubStudent().split(",").length > club.getAttendPeopleNum()) {
            throw new GlobalException("当前参与人数已满");
        }
        //如果是后台添加则无需审核直接通过更新
        if (type) {
            cpClubMapper.updateCpClub(club);
        }
    }

    /**
     * 修改社团报名
     * 
     * @param cpClubEnroll 社团报名
     * @return 结果
     */
    @Override
    public int updateCpClubEnroll(CpClubEnroll cpClubEnroll)
    {
        return cpClubEnrollMapper.updateCpClubEnroll(cpClubEnroll);
    }

    /**
     * 批量删除社团报名
     * 
     * @param ids 需要删除的社团报名主键
     * @return 结果
     */
    @Override
    public int deleteCpClubEnrollByIds(Long[] ids)
    {
        for (Long id : ids) {
            this.deleteCpClubEnrollById(id);
        }
        return 1;
    }

    /**
     * 删除社团报名信息
     * 
     * @param id 社团报名主键
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteCpClubEnrollById(Long id)
    {
        CpClubEnroll cpClubEnroll = this.selectCpClubEnrollById(id);
        if (Objects.isNull(cpClubEnroll)) {
            throw new GlobalException("当前报名记录不存在");
        }
        CpClub club = cpClubService.selectCpClubById(cpClubEnroll.getClubId());
        if (Objects.isNull(club)) {
            throw new GlobalException("当前社团不存在");
        }
        if (StringUtils.isNotEmpty(club.getClubStudent())) {
            StringBuilder sb = new StringBuilder();
            String[] clubStudents = club.getClubStudent().split(",");
            for (String registerId : clubStudents) {
                if (registerId.equals(cpClubEnroll.getRegisterId().toString())) {
                    continue;
                }
                sb.append(registerId).append(",");
            }
            if (sb.length() != 0) {
                sb.deleteCharAt(sb.length() - 1);
                club.setClubStudent(sb.toString());
            } else {
                club.setClubStudent(StringUtils.EMPTY);
            }
            cpClubMapper.updateCpClub(club);
        }
        return cpClubEnrollMapper.deleteCpClubEnrollById(id);
    }

    /**
     * 后台报名审核
     * @param ids 报名审核id  多个以逗号分割
     * @param status 状态(0-待审核 1-通过 2-驳回)
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void enrollExamine(String ids, Long status) {
        String[] idsArr = ids.split(",");
        for (String s : idsArr) {
            CpClubEnroll temp = this.selectCpClubEnrollById(Long.valueOf(s));
            if (Objects.isNull(temp)) {
                throw new GlobalException("当前报名记录不存在");
            }
            if (temp.getStatus() != 0) {
                throw new GlobalException("当前报名记录已审核");
            }
            temp.setStatus(status);
            CpClub club = cpClubService.selectCpClubById(temp.getClubId());
            if (Objects.isNull(club)) {
                throw new GlobalException("当前社团不存在");
            }
            //更新审核状态
            this.updateCpClubEnroll(temp);
            //审核通过才添加学生
            if (status == 1) {
                //判断社团相关校验并添加社团学生
                this.handleCpClubInfo(club, temp.getRegisterId(), true);
            }
        }
    }
}
