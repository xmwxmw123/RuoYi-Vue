package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpCampus;
import com.ruoyi.campus.mapper.CpCampusMapper;
import com.ruoyi.campus.service.ICpCampusService;
import com.ruoyi.common.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 设置校园Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-08
 */
@Service
public class CpCampusServiceImpl implements ICpCampusService 
{
    @Resource
    private CpCampusMapper cpCampusMapper;

    /**
     * 查询设置校园
     * 
     * @param id 设置校园主键
     * @return 设置校园
     */
    @Override
    public CpCampus selectCpCampusById(Long id)
    {
        return cpCampusMapper.selectCpCampusById(id);
    }

    /**
     * 查询设置校园列表
     * 
     * @param cpCampus 设置校园
     * @return 设置校园
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpCampus> selectCpCampusList(CpCampus cpCampus)
    {
        return cpCampusMapper.selectCpCampusList(cpCampus);
    }

    /**
     * 查询设置校园列表(家长端APP)
     */
    @Override
    public List<CpCampus> parentAppQuyCpCampusList(Long deptId) {
        return cpCampusMapper.parentAppQuyCpCampusList(deptId);
    }

    /**
     * 新增设置校园
     * 
     * @param cpCampus 设置校园
     * @return 结果
     */
    @Override
    public int insertCpCampus(CpCampus cpCampus)
    {
        return cpCampusMapper.insertCpCampus(cpCampus);
    }

    /**
     * 修改设置校园
     * 
     * @param cpCampus 设置校园
     * @return 结果
     */
    @Override
    public int updateCpCampus(CpCampus cpCampus)
    {
        return cpCampusMapper.updateCpCampus(cpCampus);
    }

    /**
     * 批量删除设置校园
     * 
     * @param ids 需要删除的设置校园主键
     * @return 结果
     */
    @Override
    public int deleteCpCampusByIds(Long[] ids)
    {
        return cpCampusMapper.deleteCpCampusByIds(ids);
    }

    /**
     * 删除设置校园信息
     * 
     * @param id 设置校园主键
     * @return 结果
     */
    @Override
    public int deleteCpCampusById(Long id)
    {
        return cpCampusMapper.deleteCpCampusById(id);
    }
}
