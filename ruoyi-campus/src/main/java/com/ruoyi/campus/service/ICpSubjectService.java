package com.ruoyi.campus.service;

import java.util.List;
import com.ruoyi.campus.domain.CpSubject;

/**
 * 设置学科Service接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface ICpSubjectService 
{
    /**
     * 查询设置学科
     * 
     * @param id 设置学科主键
     * @return 设置学科
     */
    public CpSubject selectCpSubjectById(Long id);

    /**
     * 查询设置学科列表
     * 
     * @param cpSubject 设置学科
     * @return 设置学科集合
     */
    public List<CpSubject> selectCpSubjectList(CpSubject cpSubject);

    /**
     * 新增设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    public int insertCpSubject(CpSubject cpSubject);

    /**
     * 修改设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    public int updateCpSubject(CpSubject cpSubject);

    /**
     * 批量删除设置学科
     * 
     * @param ids 需要删除的设置学科主键集合
     * @return 结果
     */
    public int deleteCpSubjectByIds(Long[] ids);

    /**
     * 删除设置学科信息
     * 
     * @param id 设置学科主键
     * @return 结果
     */
    public int deleteCpSubjectById(Long id);
}
