package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpTimetable;
import com.ruoyi.campus.mapper.*;
import com.ruoyi.campus.service.ICpTimetableService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 课表查询Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class CpTimetableServiceImpl implements ICpTimetableService 
{
    @Resource
    private CpTimetableMapper cpTimetableMapper;

    @Resource
    private CpTimetableSetMapper cpTimetableSetMapper;

    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private CpGradeMapper cpGradeMapper;
    @Resource
    private CpClassMapper cpClassMapper;

    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;

    @Resource
    private CpSubjectMapper cpSubjectMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;


    /**
     * 查询课表查询
     * 
     * @param id 课表查询主键
     * @return 课表查询
     */
    @Override
    public CpTimetable selectCpTimetableById(Long id)
    {
        return cpTimetableMapper.selectCpTimetableById(id);
    }

    /**
     * 查询课表查询列表
     * 
     * @param cpTimetable 课表查询
     * @return 课表查询
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpTimetable> selectCpTimetableList(CpTimetable cpTimetable)
    {
        return cpTimetableMapper.selectCpTimetableList(cpTimetable);
    }

    /**
     * 新增课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    @Override
    public int insertCpTimetable(CpTimetable cpTimetable)
    {
        cpTimetable.setCreateTime(DateUtils.getNowDate());
        return cpTimetableMapper.insertCpTimetable(cpTimetable);
    }

    /**
     * 修改课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    @Override
    public int updateCpTimetable(CpTimetable cpTimetable)
    {
        cpTimetable.setUpdateTime(DateUtils.getNowDate());
        return cpTimetableMapper.updateCpTimetable(cpTimetable);
    }

    /**
     * 批量删除课表查询
     * 
     * @param ids 需要删除的课表查询主键
     * @return 结果
     */
    @Override
    public int deleteCpTimetableByIds(Long[] ids)
    {
        return cpTimetableMapper.deleteCpTimetableByIds(ids);
    }

    /**
     * 删除课表查询信息
     * 
     * @param id 课表查询主键
     * @return 结果
     */
    @Override
    public int deleteCpTimetableById(Long id)
    {
        return cpTimetableMapper.deleteCpTimetableById(id);
    }

    /**
     * 导出
     * @param cpTimetable
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpTimetable> export(CpTimetable cpTimetable) {
        List<CpTimetable> cpTimetables = cpTimetableMapper.selectCpTimetableList(cpTimetable);
        if(cpTimetables != null){
            for (CpTimetable timetable : cpTimetables) {
                //年级
                if(timetable.getGrade() != null){
                    String label = dictDataMapper.selectDictLabel("grade_list", timetable.getGrade());
                    timetable.setGrade(label);
                }
                //星期
                if(timetable.getWeek() != null){
                    String label = dictDataMapper.selectDictLabel("week_type", timetable.getWeek());
                    timetable.setWeek(label);
                }
                //节数
                if(timetable.getPitchNumber() != null){
                    String label = dictDataMapper.selectDictLabel("pitch_number", timetable.getPitchNumber());
                    timetable.setPitchNumber(label);
                }
            }
        }
        return cpTimetables;
    }
}
