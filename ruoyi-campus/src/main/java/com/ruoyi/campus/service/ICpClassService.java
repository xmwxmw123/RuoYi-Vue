package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.exportVo.ExportClass;
import org.apache.ibatis.annotations.Param;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 设置班级Service接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface ICpClassService 
{
    /**
     * 查询设置班级
     * 
     * @param id 设置班级主键
     * @return 设置班级
     */
    public CpClass selectCpClassById(Long id);

    /**
     * 查询设置班级列表
     * 
     * @param cpClass 设置班级
     * @return 设置班级集合
     */
    public List<CpClass> selectCpClassList(CpClass cpClass);

    /**
     * 新增设置班级
     * 
     * @param cpClass 设置班级
     * @return 结果
     */
    public int insertCpClass(CpClass cpClass);

    /**
     * 修改设置班级
     * 
     * @param cpClass 设置班级
     * @return 结果
     */
    public int updateCpClass(CpClass cpClass);

    /**
     * 批量删除设置班级
     * 
     * @param ids 需要删除的设置班级主键集合
     * @return 结果
     */
    public int deleteCpClassByIds(Long[] ids);

    /**
     * 删除设置班级信息
     * 
     * @param id 设置班级主键
     * @return 结果
     */
    public int deleteCpClassById(Long id);



    /**
     *  根据id查询年级
     */
    List<CpClass> queryList(Long id);

    /**
     *  查询班级列表
     */
    List<CpClass> findList( Long id, Long deptId);


    /**
     * 导出班级列表
     */
    List<ExportClass> findCpClassList(@Param("exportClass") ExportClass exportClass) throws ParseException;


    /**
     * 查询不为空的班级
     */
    List<Map<String,String>> findListNotNull(CpClass cpClass);
}
