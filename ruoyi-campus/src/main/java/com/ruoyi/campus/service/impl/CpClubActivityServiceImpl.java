package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpClubActivity;
import com.ruoyi.campus.mapper.CpClubActivityMapper;
import com.ruoyi.campus.mapper.CpClubPlanMapper;
import com.ruoyi.campus.service.ICpClubActivityService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 社团活动Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@Service
public class CpClubActivityServiceImpl implements ICpClubActivityService 
{
    @Resource
    private CpClubActivityMapper cpClubActivityMapper;

    /**
     * 查询社团活动
     * 
     * @param id 社团活动主键
     * @return 社团活动
     */
    @Override
    public CpClubActivity selectCpClubActivityById(Long id)
    {
        return cpClubActivityMapper.selectCpClubActivityById(id);
    }

    /**
     * 查询社团活动列表
     * 
     * @param cpClubActivity 社团活动
     * @return 社团活动
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpClubActivity> selectCpClubActivityList(CpClubActivity cpClubActivity)
    {
        return cpClubActivityMapper.selectCpClubActivityList(cpClubActivity);
    }

    /**
     * 查询校园社团活动列表(家长端APP)
     * @return 社团活动集合
     */
    @Override
    public List<CpClubActivity> parentAppClublist(CpClubActivity cpClubActivity) {
        return cpClubActivityMapper.parentAppClublist(cpClubActivity);
    }

    /**
     * 新增社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    @Override
    public int insertCpClubActivity(CpClubActivity cpClubActivity)
    {
        cpClubActivity.setCreateTime(DateUtils.getNowDate());
        cpClubActivity.setCreateBy(SecurityUtils.getUsername());
        return cpClubActivityMapper.insertCpClubActivity(cpClubActivity);
    }

    /**
     * 修改社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    @Override
    public int updateCpClubActivity(CpClubActivity cpClubActivity)
    {
        cpClubActivity.setUpdateTime(DateUtils.getNowDate());
        cpClubActivity.setCreateBy(SecurityUtils.getUsername());
        return cpClubActivityMapper.updateCpClubActivity(cpClubActivity);
    }

    /**
     * 批量删除社团活动
     * 
     * @param ids 需要删除的社团活动主键
     * @return 结果
     */
    @Override
    public int deleteCpClubActivityByIds(Long[] ids)
    {
        return cpClubActivityMapper.deleteCpClubActivityByIds(ids);
    }

    /**
     * 删除社团活动信息
     * 
     * @param id 社团活动主键
     * @return 结果
     */
    @Override
    public int deleteCpClubActivityById(Long id)
    {
        return cpClubActivityMapper.deleteCpClubActivityById(id);
    }

    /**
     * 导出
     * @param cpClubActivity
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpClubActivity> export(CpClubActivity cpClubActivity) {
        List<CpClubActivity> cpClubActivities = cpClubActivityMapper.selectCpClubActivityList(cpClubActivity);
        if(cpClubActivities.isEmpty()){
            throw new GlobalException("没有数据!");
        }
        for (CpClubActivity clubActivity : cpClubActivities) {

            if(Objects.nonNull(clubActivity)){
                String introduction = clubActivity.getIntroduction();
                String cleanedText = introduction.replaceAll("<p>", "").replaceAll("</p>", "");
                clubActivity.setIntroduction(cleanedText);
            }
        }
        return cpClubActivities;
    }
}
