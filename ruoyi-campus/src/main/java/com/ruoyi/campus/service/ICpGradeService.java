package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.exportVo.ExportGrade;

import java.util.List;

/**
 * 设置届别Service接口
 * 
 * @author ruoyi
 * @date 2023-07-28
 */
public interface ICpGradeService 
{
    /**
     * 查询设置届别
     * 
     * @param id 设置届别主键
     * @return 设置届别
     */
    public CpGrade selectCpGradeById(Long id);

    /**
     * 查询设置届别列表
     * 
     * @param cpGrade 设置届别
     * @return 设置届别集合
     */
    public List<CpGrade> selectCpGradeList(CpGrade cpGrade);

    /**
     * 新增设置届别
     * 
     * @param cpGrade 设置届别
     * @return 结果
     */
    public int insertCpGrade(CpGrade cpGrade);

    /**
     * 修改设置届别
     * 
     * @param cpGrade 设置届别
     * @return 结果
     */
    public int updateCpGrade(CpGrade cpGrade);

    /**
     * 批量删除设置届别
     * 
     * @param ids 需要删除的设置届别主键集合
     * @return 结果
     */
    public int deleteCpGradeByIds(Long[] ids);

    /**
     * 删除设置届别信息
     * 
     * @param id 设置届别主键
     * @return 结果
     */
    public int deleteCpGradeById(Long id);

    /**
     *  升级当前年级
     * @param id
     * @return
     */
    int upgrade(CpGrade cpGrade);

    /**
     *  查询列表
     * @return
     */
    List<CpGrade> queryList(CpGrade cpGrade);

    /**
     * 导出
     * @param exportGrade
     * @return
     */
    List<ExportGrade> ExportSelectCpGradeList(ExportGrade exportGrade);
}
