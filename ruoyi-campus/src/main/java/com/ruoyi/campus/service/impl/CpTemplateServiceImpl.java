package com.ruoyi.campus.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ruoyi.campus.domain.CpTemplate;
import com.ruoyi.campus.mapper.CpTemplateMapper;
import com.ruoyi.campus.service.ICpTemplateService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 评价模板Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-16
 */
@Service
public class CpTemplateServiceImpl implements ICpTemplateService {
    @Resource
    private CpTemplateMapper cpTemplateMapper;

    /**
     * 查询评价模板
     *
     * @param id 评价模板主键
     * @return 评价模板
     */
    @Override
    public CpTemplate selectCpTemplateById(Long id) {
        CpTemplate cpTemplate = cpTemplateMapper.selectCpTemplateById(id);
//        String template = cpTemplate.getTemplate();
//        if (template != null) {
//            JSONArray jsonArray = JSONUtil.parseArray(template);
//            //创建返回集合
//            List<String> list = new ArrayList<>();
//            for (int i = 0; i < jsonArray.size(); i++) {
//                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                //拿到template中所有oneIndex的值
//                String oneIndex = jsonObject.getStr("oneIndex");
//                list.add(oneIndex);
//            }
//            cpTemplate.setIndexS(list);
//        }
        return cpTemplate;
    }

    /**
     * 查询评价模板列表
     *
     * @param cpTemplate 评价模板
     * @return 评价模板
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpTemplate> selectCpTemplateList(CpTemplate cpTemplate) {
        return cpTemplateMapper.selectCpTemplateList(cpTemplate);
    }

    /**
     * 新增评价模板
     *
     * @param cpTemplate 评价模板
     * @return 结果
     */
    @Override
    public int insertCpTemplate(CpTemplate cpTemplate) {
        cpTemplate.setCreateTime(DateUtils.getNowDate());
        cpTemplate.setCreateBy(SecurityUtils.getUsername());
        cpTemplate.setEnable(true);
        cpTemplate.setStatus("3");//设置状态为"开始录入"
        return cpTemplateMapper.insertCpTemplate(cpTemplate);
    }

    /**
     * 修改评价模板
     *
     * @param cpTemplate 评价模板
     * @return 结果
     */
    @Override
    @Transactional
    public int updateCpTemplate(CpTemplate cpTemplate) {
        cpTemplate.setUpdateTime(DateUtils.getNowDate());
        cpTemplate.setUpdateBy(SecurityUtils.getUsername());
        //解析template
        JSONArray jsonArray = JSONUtil.parseArray(cpTemplate.getTemplate());
        if (jsonArray.size() != 0) {
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.getStr("oneIndex").trim().equals("") || jsonObject.getStr("onescore").trim().equals("")) {
                    throw new GlobalException("不能保存空数据!");//校验
                }
                //判断oneIndexId字段数据是否为空,为空则设置id
                if (jsonObject.getStr("oneIndexId").equals("")) {
                    jsonObject.set("oneIndexId", this.generateNumericUUID());
                }
            }
            cpTemplate.setTemplate(JSONUtil.toJsonStr(jsonArray));
            cpTemplate.setStatus("1");//设置为录入中
        }
        return cpTemplateMapper.updateCpTemplate(cpTemplate);
    }

    //生成uuid
    public String generateNumericUUID() {
        long timestamp = System.currentTimeMillis();
        String uuid = IdUtil.simpleUUID();
        String randomNum = uuid.substring(uuid.length() - 6);
        String randomWithTimestamp = String.valueOf(timestamp) + randomNum;
        randomWithTimestamp = randomWithTimestamp.substring(randomWithTimestamp.length() - 6);
        return randomWithTimestamp;
    }

    /**
     * 批量删除评价模板
     *
     * @param ids 需要删除的评价模板主键
     * @return 结果
     */
    @Override
    public int deleteCpTemplateByIds(Long[] ids) {
        return cpTemplateMapper.deleteCpTemplateByIds(ids);
    }

    /**
     * 删除评价模板信息
     *
     * @param id 评价模板主键
     * @return 结果
     */
    @Override
    public int deleteCpTemplateById(Long id) {
        return cpTemplateMapper.deleteCpTemplateById(id);
    }

    /**
     * 修改账号是否启用
     *
     * @param id
     * @return
     */
    @Override
    public int updateEnable(Long id) {
        CpTemplate cpTemplate = cpTemplateMapper.selectCpTemplateById(id);
        if (Objects.nonNull(cpTemplate)) {
            Boolean enable = cpTemplate.getEnable();
            if (enable.equals(true)) {
                cpTemplate.setEnable(false);
            } else {
                cpTemplate.setEnable(true);
            }
            cpTemplateMapper.updateCpTemplate(cpTemplate);
        }
        return 1;
    }

    /**
     * 二级指标保存接口
     *
     * @param cpTemplate
     * @return
     */
    @Override
    @Transactional
    public int twoIndex(CpTemplate cpTemplate) {
        //处理传过来的新数据
        JSONObject jsonObject = JSONUtil.parseObj(cpTemplate.getTemplate());
        String oneIndexId = jsonObject.getStr("oneIndexId");
        JSONArray son = jsonObject.getJSONArray("son");
        if (son != null) {
            for (int i = 0; i < son.size(); i++) {
                JSONObject newJsonObject = son.getJSONObject(i);
                if (newJsonObject.getStr("twoIndex").trim().equals("") || newJsonObject.getStr("inspect").trim().equals("")) {
                    throw new GlobalException("不能保存空数据!");//校验
                }
                if (newJsonObject.getStr("twoIndexId").equals("")) {
                    newJsonObject.set("twoIndexId", this.generateNumericUUID());
                }
            }
        }

        //数据库的旧数据
        CpTemplate oldCpTemplate = cpTemplateMapper.selectCpTemplateById(cpTemplate.getId());
        JSONArray oldJsonArray = JSONUtil.parseArray(oldCpTemplate.getTemplate());
        if (oldJsonArray.size() != 0) {
            for (int i = 0; i < oldJsonArray.size(); i++) {
                JSONObject oldJsonObject = oldJsonArray.getJSONObject(i);
                if (oldJsonObject.getStr("oneIndexId").equals(oneIndexId)) {
                    //判断是否存在son的旧数据,存在则删除,再插入新数据
                    if (oldJsonObject.containsKey("son")) {
                        oldJsonObject.remove("son");
                        oldJsonObject.putOnce("son", son);
                    } else {
                        //不存在则直接插入
                        oldJsonObject.putOnce("son", son);
                    }
                }
            }
        }
        oldCpTemplate.setTemplate(JSONUtil.toJsonStr(oldJsonArray));
        oldCpTemplate.setStatus("1");//设置为录入中
        return cpTemplateMapper.updateCpTemplate(oldCpTemplate);
    }

    /**
     * 记录要点保存接口
     *
     * @param cpTemplate
     * @return
     */
    @Override
    @Transactional
    public int record(CpTemplate cpTemplate) {
        String newTwoIndexId = "";
        JSONObject newJiluObj = new JSONObject();
        boolean flag = true;
        //处理传过来的新数据
        JSONObject newJsonObject = JSONUtil.parseObj(cpTemplate.getTemplate());
        String newOneIndexId = newJsonObject.getStr("oneIndexId");
        JSONObject newSonObj = newJsonObject.getJSONObject("son");
        newTwoIndexId = newSonObj.getStr("twoIndexId");
        newJiluObj = newSonObj.getJSONObject("jilu");
        if (newJiluObj != null) {
            if (newJiluObj.getStr("record").trim().equals("") || newJiluObj.getStr("category").trim().equals("")) {
                throw new GlobalException("不能保存空数据!");//校验
            }
            if (newJiluObj.getStr("recordId").equals("")) {
                //recordId首次传过来为空,则设置id
                newJiluObj.set("recordId", this.generateNumericUUID());
                flag = false;
            }
        }

        //处理旧数据
        CpTemplate oldCpTemplate = cpTemplateMapper.selectCpTemplateById(cpTemplate.getId());
        JSONArray oldJsonArray = JSONUtil.parseArray(oldCpTemplate.getTemplate());
        if (oldJsonArray.size() != 0) {
            for (int i = 0; i < oldJsonArray.size(); i++) {
                JSONObject oldJsonObject = oldJsonArray.getJSONObject(i);
                JSONArray oldSonArr = oldJsonObject.getJSONArray("son");
                for (int j = 0; j < oldSonArr.size(); j++) {
                    JSONObject oldSonObj = oldSonArr.getJSONObject(j);
                    if (oldJsonObject.getStr("oneIndexId").equals(newOneIndexId) && oldSonObj.getStr("twoIndexId").equals(newTwoIndexId)) {
                        JSONArray oldJiluArr = oldSonObj.getJSONArray("jilu");
                        if(flag){
                            //flag为true说明是修改调用的,则根据recordId更新数据
                            for (int k = 0; k < oldJiluArr.size(); k++) {
                                JSONObject oldJiluObj = oldJiluArr.getJSONObject(k);
                                if(oldJiluObj.getStr("recordId").equals(Objects.requireNonNull(newJiluObj).getStr("recordId"))){
                                    oldJiluObj.set("recordId", newJiluObj.getStr("recordId"));
                                    oldJiluObj.set("record", newJiluObj.getStr("record"));
                                    oldJiluObj.set("category", newJiluObj.getStr("category"));
                                    oldJiluObj.set("source", newJiluObj.getStr("source"));
                                    oldJiluObj.set("qujian", newJiluObj.getJSONArray("qujian"));
                                    oldJiluObj.set("score", newJiluObj.getStr("score"));
                                    break;
                                }
                            }
                        }else {
                            //flag为false说明是新增调用的
                            //如果存在jilu数组,就直接添加数据
                            if(oldJiluArr != null){
                                oldJiluArr.add(0, newJiluObj);
                            }else {
                                JSONArray newJiluArr = new JSONArray();
                                newJiluArr.add(newJiluObj);
                                //不存在则添加jilu数组
                                oldSonObj.putOnce("jilu", newJiluArr);
                            }
                        }
                    }
                }
            }
        }
        //对值修改后,更新数据库
        oldCpTemplate.setTemplate(JSONUtil.toJsonStr(oldJsonArray));
        oldCpTemplate.setStatus("1");//设置为录入中
        return cpTemplateMapper.updateCpTemplate(oldCpTemplate);
    }

    /**
     * 删除记录要点信息
     *
     * @param cpTemplate
     * @return
     */
    @Override
    @Transactional
    public int deleteRecord(CpTemplate cpTemplate) {
        //处理传过来的数据
        JSONObject newJsonObject = JSONUtil.parseObj(cpTemplate.getTemplate());
        String newOneIndexId = newJsonObject.getStr("oneIndexId");
        String newTwoIndexId = newJsonObject.getStr("twoIndexId");
        String newRecordId = newJsonObject.getStr("recordId");

        //处理数据库的旧数据
        CpTemplate oldCpTemplate = cpTemplateMapper.selectCpTemplateById(cpTemplate.getId());
        JSONArray oldJsonArray = JSONUtil.parseArray(oldCpTemplate.getTemplate());
        for (int i = 0; i < oldJsonArray.size(); i++) {
            JSONObject oldJsonObj = oldJsonArray.getJSONObject(i);
            JSONArray oldSonArr = oldJsonObj.getJSONArray("son");//拿到son数组
            for (int j = 0; j < oldSonArr.size(); j++) {
                JSONObject oldSonObj = oldSonArr.getJSONObject(j);
                JSONArray oldJiluArr = oldSonObj.getJSONArray("jilu");//拿到jilu数组
                for (int k = 0; k < oldJiluArr.size(); k++) {
                    JSONObject oldJiluObj = oldJiluArr.getJSONObject(k);
                    if (oldJsonObj.getStr("oneIndexId").equals(newOneIndexId) && oldSonObj.getStr("twoIndexId").equals(newTwoIndexId) && oldJiluObj.getStr("recordId").equals(newRecordId)) {
                        //删除该条信息后,跳出循环
                        oldJiluArr.remove(k);
                        break;
                    }
                }
            }
        }
        oldCpTemplate.setTemplate(JSONUtil.toJsonStr(oldJsonArray));
        oldCpTemplate.setStatus("1");//设置为录入中
        return cpTemplateMapper.updateCpTemplate(oldCpTemplate);
    }

    /**
     * 录入完成接口
     *
     * @param id
     * @return
     */
    @Override
    public int complete(Long id) {
        CpTemplate cpTemplate = new CpTemplate();
        cpTemplate.setId(id);
        cpTemplate.setStatus("2");//设置状态为录入成功
        return cpTemplateMapper.updateCpTemplate(cpTemplate);
    }
}
