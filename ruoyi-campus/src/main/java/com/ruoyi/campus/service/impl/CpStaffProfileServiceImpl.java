package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.CpStaffProfile;
import com.ruoyi.campus.domain.exportVo.ExportStaffProfile;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpStaffProfileMapper;
import com.ruoyi.campus.service.ICpStaffProfileService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 教工档案Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@Service
public class CpStaffProfileServiceImpl implements ICpStaffProfileService {
    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;
    @Resource
    private CpClassMapper cpClassMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    @Resource
    private ISysUserService userService;
    @Resource
    private SysUserMapper sysUserMapper;

    /**
     * 查询教工档案
     */
    @DataScope(deptAlias = "d")
    @Override
    public  List<CpStaffProfile> selectCpStaffProfileById(CpStaffProfile params) {
        List<CpStaffProfile> cpStaffProfiles = cpStaffProfileMapper.selectCpStaffProfileById(params);
        for (CpStaffProfile cpStaffProfile : cpStaffProfiles) {
            cpStaffProfile.setPassword("******");
        }
        return cpStaffProfiles;
    }

    /**
     * 查询教工档案列表
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpStaffProfile> selectCpStaffProfileList(CpStaffProfile cpStaffProfile) {

        List<CpStaffProfile> cpStaffProfiles = cpStaffProfileMapper.selectCpStaffProfileList(cpStaffProfile);
        return cpStaffProfiles;
    }

    /**
     * 新增教工档案
     *
     * @param cpStaffProfile 教工档案
     * @return 结果
     */
    @Override
    public int insertCpStaffProfile(CpStaffProfile cpStaffProfile) {
        if (StringUtils.isBlank(cpStaffProfile.getUserName()) || StringUtils.isBlank(cpStaffProfile.getNickName())
                || StringUtils.isBlank(cpStaffProfile.getPassword()) || Objects.isNull(cpStaffProfile.getDeptId())) {
            throw new GlobalException("参数错误'");
        }
        if (cpStaffProfile.getName() == null || "".equals(cpStaffProfile.getName())){
            throw new GlobalException("姓名不能为空");
        }
        if (cpStaffProfile.getNumbering() == null || "".equals(cpStaffProfile.getNumbering())){
            throw new GlobalException("编号不能为空");
        }
        if (cpStaffProfile.getIdNumber() == null || "".equals(cpStaffProfile.getIdNumber())){
            throw new GlobalException("身份证号不能为空");
        }
        //
        String  teacherNumbering =  cpStaffProfileMapper.selectTeacherNumbering(cpStaffProfile.getNumbering(), cpStaffProfile.getDeptId());
        if (teacherNumbering!= null){
            throw new GlobalException("编号重复添加");
        }

        String Number=  cpStaffProfileMapper.selectTeacherIdNumber(cpStaffProfile.getIdNumber(), cpStaffProfile.getDeptId());
        if (Number!= null){
            throw new GlobalException("请勿重复添加");
        }
        // 设置创建人 创建时间
        String username = SecurityUtils.getUsername();
        cpStaffProfile.setCreateBy(username);
        cpStaffProfile.setCreateTime(DateUtils.getNowDate());
        SysUser sysUser = new SysUser();
        sysUser.setUserName(cpStaffProfile.getUserName());
        sysUser.setNickName(cpStaffProfile.getNickName());
        sysUser.setPassword(cpStaffProfile.getPassword());
        sysUser.setPhonenumber(cpStaffProfile.getTeacherPhone());
        sysUser.setEmail(cpStaffProfile.getEmail());
        sysUser.setSex(cpStaffProfile.getSex());
        sysUser.setDeptId(cpStaffProfile.getDeptId());
        sysUser.setStatus("0");
        //新建账号
        if (!userService.checkUserNameUnique(sysUser))
        {
            throw new GlobalException("新增用户'" + sysUser.getUserName() + "'失败，登录账号已存在");
        }
        sysUser.setCreateBy(username);
        sysUser.setPassword(SecurityUtils.encryptPassword(sysUser.getPassword()));
        userService.insertUser(sysUser);
        cpStaffProfile.setUserId(sysUser.getUserId());
        return cpStaffProfileMapper.insertCpStaffProfile(cpStaffProfile);
    }

    /**
     * 修改教工档案
     */
    @Override
    @Transactional
    public int updateCpStaffProfile(CpStaffProfile cpStaffProfile) {
        // 查询教师的状态 如果在职就修改离职时间为null
        if (cpStaffProfile.getStatus() == 1) {
            Long id = cpStaffProfile.getId();
            cpStaffProfileMapper.updateLeaveDate(id);
        }
        // 设置修改人 修改时间
        String username = SecurityUtils.getUsername();
        cpStaffProfile.setUpdateBy(username);
        cpStaffProfile.setUpdateTime(DateUtils.getNowDate());

        SysUser sysUser = new SysUser();
        sysUser.setNickName(cpStaffProfile.getNickName());
        sysUser.setPhonenumber(cpStaffProfile.getTeacherPhone());
        sysUser.setEmail(cpStaffProfile.getEmail());
        sysUser.setSex(cpStaffProfile.getSex());
        sysUser.setUserId(cpStaffProfile.getUserId());
        if(cpStaffProfile.getStatus() == 2){//状态:2,表示已离职
            sysUser.setStatus("1");//状态:1表示停用
            cpStaffProfile.setLeaveDate(new Date());
        }
        sysUserMapper.updateUser(sysUser);

        return cpStaffProfileMapper.updateCpStaffProfile(cpStaffProfile);
    }

    /**
     * 批量删除教工档案
     *
     */
    @Override
    public int deleteCpStaffProfileByIds(Long[] ids) {
        // 根据教师id查询 是否有班级 如果有就不删除
        List<CpClass> classs =cpClassMapper.selectTeacherId(ids);
        if (classs.size()>0){
            throw new GlobalException("当前教师代理的有班级,请修改代理的班级班主任后删除");
        }
        return  cpStaffProfileMapper.deleteCpStaffProfileByIds(ids);
    }

    /**
     * 删除教工档案信息
     */
    @Override
    @Transactional
    public int deleteCpStaffProfileById(Long id) {
        CpStaffProfile cpStaffProfile = new CpStaffProfile();
        cpStaffProfile.setId(id);
        List<CpStaffProfile> cpStaffProfiles = cpStaffProfileMapper.selectCpStaffProfileById(cpStaffProfile);
        for (CpStaffProfile staffProfile : cpStaffProfiles) {
            Long userId = staffProfile.getUserId();
            sysUserMapper.deleteUserById(userId);
        }
        return cpStaffProfileMapper.deleteCpStaffProfileById(id);
    }


    /**
     *  导出列表数据
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ExportStaffProfile> ExportSelectCpStaffProfileList(ExportStaffProfile exportStaffProfile) {
        List<ExportStaffProfile> cpStaffProfiles = cpStaffProfileMapper.ExportSelectCpStaffProfileList(exportStaffProfile);

        for (ExportStaffProfile profile : cpStaffProfiles) {
            //性别
            if(profile.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", profile.getSex());
                profile.setSex(label);
            }

            //教职工状态
            if(profile.getStatus() != null){
                String label = dictDataMapper.selectDictLabel("teacher_status", profile.getStatus());
                profile.setStatus(label);
            }
        }
        return cpStaffProfiles;
    }

    /**
     * query接口
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpStaffProfile> selectCpStaffProfileQuery(CpStaffProfile cpStaffProfile) {

        return cpStaffProfileMapper.selectCpStaffProfileQuery(cpStaffProfile);
    }


}
