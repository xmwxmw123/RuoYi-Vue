package com.ruoyi.campus.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.campus.domain.CpSubject;
import com.ruoyi.campus.mapper.CpSubjectMapper;
import com.ruoyi.campus.service.ICpSubjectService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.DateUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 设置学科Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@Service
public class CpSubjectServiceImpl implements ICpSubjectService 
{
    @Resource
    private CpSubjectMapper cpSubjectMapper;

    /**
     * 查询设置学科
     * 
     * @param id 设置学科主键
     * @return 设置学科
     */
    @SneakyThrows
    @Override
    public CpSubject selectCpSubjectById(Long id)
    {
        CpSubject cpSubject = cpSubjectMapper.selectCpSubjectById(id);
        String grades = cpSubject.getGrades();
        // 创建 ObjectMapper 对象
        ObjectMapper objectMapper = new ObjectMapper();
        // 将 JSON 字符串转换为数组
        if (StringUtils.isNotBlank(grades)) {
            String[] grade = objectMapper.readValue(grades, new TypeReference<String[]>() {});
            cpSubject.setGrade(grade);
        }
        return cpSubject;
    }

    /**
     * 查询设置学科列表
     * 
     * @param cpSubject 设置学科
     * @return 设置学科
     */
    @DataScope(deptAlias = "d")
    @SneakyThrows
    @Override
    public List<CpSubject> selectCpSubjectList(CpSubject cpSubject)
    {
        List<CpSubject> subjectListRe = new ArrayList<>();
        List<CpSubject> cpSubjects = cpSubjectMapper.selectCpSubjectList(cpSubject);
        for (CpSubject Subject : cpSubjects) {
            String grades = Subject.getGrades();
            // 创建 ObjectMapper 对象
            ObjectMapper objectMapper = new ObjectMapper();
            // 将 JSON 字符串转换为数组
            if (StringUtils.isNotBlank(grades)) {
                String[] grade = objectMapper.readValue(grades, new TypeReference<String[]>() {});
                Subject.setGrade(grade);
                if (StringUtils.isNotBlank(cpSubject.getGrades())) {
                    if (Arrays.stream(grade).noneMatch(e -> e.replaceAll("\"", "").equals(cpSubject.getGrades()))) {
                        continue;
                    }
                }
                subjectListRe.add(Subject);
            }
        }
        return subjectListRe;
    }

    /**
     * 新增设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    @Override
    @SneakyThrows
    public int insertCpSubject(CpSubject cpSubject)
    {
        //将数组转为json字符串存入数据库
        ObjectMapper objectMapper = new ObjectMapper();
        String grades = objectMapper.writeValueAsString(cpSubject.getGrade());
        cpSubject.setGrades(grades);

        cpSubject.setCreateTime(DateUtils.getNowDate());
        return cpSubjectMapper.insertCpSubject(cpSubject);
    }

    /**
     * 修改设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    @SneakyThrows
    @Override
    public int updateCpSubject(CpSubject cpSubject)
    {
        //将数组转为json字符串存入数据库
        ObjectMapper objectMapper = new ObjectMapper();
        String grades = objectMapper.writeValueAsString(cpSubject.getGrade());
        //修改
        cpSubject.setUpdateTime(DateUtils.getNowDate());
        cpSubject.setGrades(grades);

        return cpSubjectMapper.updateCpSubject(cpSubject);
    }

    /**
     * 批量删除设置学科
     * 
     * @param ids 需要删除的设置学科主键
     * @return 结果
     */
    @Override
    public int deleteCpSubjectByIds(Long[] ids)
    {
        return cpSubjectMapper.deleteCpSubjectByIds(ids);
    }

    /**
     * 删除设置学科信息
     * 
     * @param id 设置学科主键
     * @return 结果
     */
    @Override
    public int deleteCpSubjectById(Long id)
    {
        return cpSubjectMapper.deleteCpSubjectById(id);
    }
}
