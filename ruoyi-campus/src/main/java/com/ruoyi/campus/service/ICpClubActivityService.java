package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpClubActivity;

import java.util.List;

/**
 * 社团活动Service接口
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
public interface ICpClubActivityService 
{
    /**
     * 查询社团活动
     * 
     * @param id 社团活动主键
     * @return 社团活动
     */
    public CpClubActivity selectCpClubActivityById(Long id);

    /**
     * 查询社团活动列表
     * 
     * @param cpClubActivity 社团活动
     * @return 社团活动集合
     */
    public List<CpClubActivity> selectCpClubActivityList(CpClubActivity cpClubActivity);

    /**
     * 查询校园社团活动列表(家长端APP)
     * @return 社团活动集合
     */
    List<CpClubActivity> parentAppClublist(CpClubActivity cpClubActivity);

    /**
     * 新增社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    public int insertCpClubActivity(CpClubActivity cpClubActivity);

    /**
     * 修改社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    public int updateCpClubActivity(CpClubActivity cpClubActivity);

    /**
     * 批量删除社团活动
     * 
     * @param ids 需要删除的社团活动主键集合
     * @return 结果
     */
    public int deleteCpClubActivityByIds(Long[] ids);

    /**
     * 删除社团活动信息
     * 
     * @param id 社团活动主键
     * @return 结果
     */
    public int deleteCpClubActivityById(Long id);

    /**
     * 导出
     * @param cpClubActivity
     * @return
     */
    List<CpClubActivity> export(CpClubActivity cpClubActivity);
}
