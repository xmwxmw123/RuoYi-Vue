package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportSemesters;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.service.ICpSemestersService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * 设置学期Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-27
 */
@Service
public class CpSemestersServiceImpl implements ICpSemestersService 
{
    @Autowired
    private CpSemestersMapper cpSemestersMapper;

    /**
     * 查询设置学期
     * 
     * @param id 设置学期主键
     * @return 设置学期
     */
    @Override
    public CpSemesters selectCpSemestersById(Long id)
    {
        return cpSemestersMapper.selectCpSemestersById(id);
    }

    /**
     * 查询设置学期列表
     * 
     * @param cpSemesters 设置学期
     * @return 设置学期
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpSemesters> selectCpSemestersList(CpSemesters cpSemesters) {
        List<CpSemesters> cpSemestersList = cpSemestersMapper.selectCpSemestersList(cpSemesters);

        return cpSemestersList;
    }

    /**
     * 新增设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    @Override
    public int insertCpSemesters(CpSemesters cpSemesters) {
        // 给 创建人 修改人 学期 创建日期 设置默认值
        String username = SecurityUtils.getUsername();
        cpSemesters.setCreateBy(username);
        cpSemesters.setCreateTime(DateUtils.getNowDate());

        cpSemesters.setCurrentSemester("2");
        cpSemesters.setCreateTime(DateUtils.getNowDate());
        // 学期不能为空 ""
        if (cpSemesters.getSemester() == null || "".equals(cpSemesters.getSemester()) ){
            throw new GlobalException("学期不能为空");

        }
        // 判断学期是否唯一
        if (!isSemesterUnique(cpSemesters.getSemester(), cpSemesters.getDeptId())){
            throw new GlobalException("学期已存在");
        }


        return cpSemestersMapper.insertCpSemesters(cpSemesters);
    }


    /**
     * 检查 semester 是否唯一
     */
    private boolean isSemesterUnique(String semester, Long deptId) {
        // 调用查询方法，根据传入的 semester 查询匹配的记录数量
        
        int count = cpSemestersMapper.countBySemester(semester, deptId);
        // 如果记录数量大于 0，表示该 semester 已存在，返回 false；否则，返回 true。
        return count <= 0;
    }


    /**
     * 修改设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    @Override
    public int updateCpSemesters(CpSemesters cpSemesters)
    {
        // 给字段重新赋值
        String username = SecurityUtils.getUsername();
        cpSemesters.setUpdateBy(username);
        cpSemesters.setUpdateTime(DateUtils.getNowDate());

        return cpSemestersMapper.updateCpSemesters(cpSemesters);
    }

    /**
     * 批量删除设置学期
     * 
     * @param ids 需要删除的设置学期主键
     * @return 结果
     */
    @Override
    public int deleteCpSemestersByIds(Long[] ids)
    {
        return cpSemestersMapper.deleteCpSemestersByIds(ids);
    }

    /**
     * 删除设置学期信息
     * 
     * @param id 设置学期主键
     * @return 结果
     */
    @Override
    public int deleteCpSemestersById(Long id)
    {
        return cpSemestersMapper.deleteCpSemestersById(id);
    }

    @DataScope(deptAlias = "d")
    @Override
    public List<CpSemesters> query(CpSemesters cpSemesters) {
        return cpSemestersMapper.query( cpSemesters);
    }

    //  设置为当前学期
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int setCurrentSemester(Long id, Long deptId) {
        // 判断参数
        if(id == null){
            throw new GlobalException("参数为空");
        }
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester("1", deptId);
        if (Objects.nonNull(cpSemesters)) {
            cpSemesters.setCurrentSemester("2");
            cpSemestersMapper.updateCpSemesters(cpSemesters);
        }
        return cpSemestersMapper.setCurrentSemester(id);

    }

    /** 导出列表数据
     *
     * @param exportSemesters
     * @return
     */
    @Override
    public List<ExportSemesters> ExportSelectCpSemestersList(ExportSemesters exportSemesters) {
        List<ExportSemesters> cpSemestersList = cpSemestersMapper.ExportSelectCpSemestersList(exportSemesters);

        // 导出设置是否为当前学期
        for (ExportSemesters semesters : cpSemestersList) {
            String currentSemester;
            if ("1".equals(semesters.getCurrentSemester())) {
                currentSemester = "是";
            } else {
                currentSemester = "否";
            }
            semesters.setCurrentSemester(currentSemester);
        }

        return cpSemestersList;
    }

}
