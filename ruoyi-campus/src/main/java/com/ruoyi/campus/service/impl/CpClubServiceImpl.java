package com.ruoyi.campus.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ruoyi.campus.domain.CpClub;
import com.ruoyi.campus.domain.CpClubActivity;
import com.ruoyi.campus.domain.CpClubEnroll;
import com.ruoyi.campus.domain.CpClubPlan;
import com.ruoyi.campus.mapper.CpClubActivityMapper;
import com.ruoyi.campus.mapper.CpClubEnrollMapper;
import com.ruoyi.campus.mapper.CpClubMapper;
import com.ruoyi.campus.mapper.CpClubPlanMapper;
import com.ruoyi.campus.service.ICpClubService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 社团管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@Service
public class CpClubServiceImpl implements ICpClubService 
{
    @Resource
    private CpClubMapper cpClubMapper;

    @Resource
    private CpClubActivityMapper cpClubActivityMapper;

    @Resource
    private CpClubPlanMapper cpClubPlanMapper;

    @Resource
    private CpClubEnrollMapper cpClubEnrollMapper;

    /**
     * 查询社团管理
     * 
     * @param id 社团管理主键
     * @return 社团管理
     */
    @Override
    public CpClub selectCpClubById(Long id)
    {
        CpClub club = cpClubMapper.selectCpClubById(id);
        if (Objects.isNull(club)) {
            throw new GlobalException("当前社团不存在");
        }
        List<CpClubPlan> list = cpClubPlanMapper.getCpClubPlan(club.getId());
        if (CollUtil.isNotEmpty(list)) {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            JSONArray jsonArray = new JSONArray(list);
            for (int i = 0; i < jsonArray.size(); i++) {
                jsonArray.getJSONObject(i).putOpt("startTime", sf.format(jsonArray.getJSONObject(i).getDate("startTime")));
                jsonArray.getJSONObject(i).putOpt("endTime", sf.format(jsonArray.getJSONObject(i).getDate("endTime")));
            }
            club.setCpClubPlans(jsonArray.toString());
        }
        return club;
    }

    /**
     * 查询社团管理列表(PC端和教师APP)
     * 
     * @param cpClub 社团管理
     * @return 社团管理
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpClub> selectCpClubList(CpClub cpClub)
    {
        return cpClubMapper.selectCpClubList(cpClub);
    }

    /**
     * 查询社团管理列表(家长端APP)
     */
    @Override
    public List<CpClub> parentAppQuyCpClubList(CpClub cpClub) {
        return cpClubMapper.parentAppQuyCpClubList(cpClub);
    }

    /**
     * 查询当前学生已报名社团(家长端APP)
     */
    @Override
    public List<CpClub> parentAppQuyEnrollClubList(CpClubEnroll clubEnroll) {
        if (Objects.isNull(clubEnroll.getRegisterId()) || Objects.isNull(clubEnroll.getDeptId())) {
            throw new GlobalException("参数错误");
        }
        List<CpClub> result = new ArrayList<>();
        CpClub cpClub = new CpClub();
        cpClub.setClubName(clubEnroll.getClubName());
        cpClub.setDeptId(clubEnroll.getDeptId());
        List<CpClub> cpClubs = this.parentAppQuyCpClubList(cpClub);
        if (CollUtil.isNotEmpty(cpClubs)) {
            for (CpClub dto : cpClubs) {
                //报名记录
                CpClubEnroll cpClubEnroll = new CpClubEnroll();
                cpClubEnroll.setRegisterId(clubEnroll.getRegisterId());
                cpClubEnroll.setClubId(dto.getId());
                cpClubEnroll.setDeptId(clubEnroll.getDeptId());
                List<CpClubEnroll> cpClubEnrolls = cpClubEnrollMapper.accordingRegisterIdQuyCpClubEnroll(cpClubEnroll);
                if (CollUtil.isEmpty(cpClubEnrolls)) {
                    continue;
                }
                //新增报名记录
                dto.setCpClubEnrolls(cpClubEnrolls);
                //活动计划
                List<CpClubPlan> list = cpClubPlanMapper.getCpClubPlan(dto.getId());
                if (CollUtil.isNotEmpty(list)) {
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    JSONArray jsonArray = new JSONArray(list);
                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonArray.getJSONObject(i).putOpt("startTime", sf.format(jsonArray.getJSONObject(i).getDate("startTime")));
                        jsonArray.getJSONObject(i).putOpt("endTime", sf.format(jsonArray.getJSONObject(i).getDate("endTime")));
                    }
                    dto.setCpClubPlans(jsonArray.toString());
                }
                //活动
                CpClubActivity cpClubActivity = new CpClubActivity();
                cpClubActivity.setDeptId(clubEnroll.getDeptId());
                cpClubActivity.setClubId(dto.getId());
                List<CpClubActivity> cpClubActivities = cpClubActivityMapper.parentAppClublist(cpClubActivity);
                if (CollUtil.isNotEmpty(cpClubActivities)) {
                    dto.setCpClubActivities(cpClubActivities);
                }
                result.add(dto);
            }
        }
        return result;
    }

    /**
     * 新增社团管理
     * 
     * @param cpClub 社团管理
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertCpClub(CpClub cpClub)
    {
        if (StringUtils.isBlank(cpClub.getClubName()) || StringUtils.isBlank(cpClub.getGrade())) {
            throw new GlobalException("参数错误");
        }
        int count = cpClubMapper.selectByClubName(cpClub.getClubName(), cpClub.getDeptId());
        if(count > 0){
            throw new GlobalException("社团名称重复!");
        }
        cpClub.setCreateTime(DateUtils.getNowDate());
        cpClub.setCreateBy(SecurityUtils.getUsername());
        cpClubMapper.insertCpClub(cpClub);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JSONArray jsonArray = JSONUtil.parseArray(cpClub.getCpClubPlans());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            CpClubPlan cpClubPlan = new CpClubPlan();
            cpClubPlan.setClubId(cpClub.getId());
            cpClubPlan.setPlanName(jsonObject.getStr("planName"));
            try {
                cpClubPlan.setStartTime(sf.parse(jsonObject.getStr("startTime")));
                cpClubPlan.setEndTime(sf.parse(jsonObject.getStr("endTime")));
            } catch (ParseException e) {
                throw new GlobalException("活动时间格式错误");
            }
            cpClubPlan.setRemake(jsonObject.getStr("remake"));
            cpClubPlanMapper.insertCpClubPlan(cpClubPlan);
            //根据活动计划新增活动
            CpClubActivity cpClubActivity = new CpClubActivity();
            cpClubActivity.setActivityName(cpClubPlan.getPlanName());
            cpClubActivity.setClubId(cpClub.getId());
            cpClubActivity.setClubName(cpClub.getClubName());
            cpClubActivity.setStartTime(cpClubPlan.getStartTime());
            cpClubActivity.setEndTime(cpClubPlan.getEndTime());
            cpClubActivity.setCreateTime(cpClub.getCreateTime());
            cpClubActivity.setCreateBy(cpClub.getCreateBy());
            cpClubActivity.setUserId(cpClub.getUserId());
            cpClubActivity.setDeptId(cpClub.getDeptId());
            cpClubActivity.setPlanId(cpClubPlan.getId());
            cpClubActivityMapper.insertCpClubActivity(cpClubActivity);
        }
        return 1;
    }

    /**
     * 修改社团管理
     * 
     * @param cpClub 社团管理
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateCpClub(CpClub cpClub)
    {
        if (StringUtils.isBlank(cpClub.getCpClubPlans())) {
            throw new GlobalException("活动计划不能为空");
        }
        JSONArray jsonArray = JSONUtil.parseArray(cpClub.getCpClubPlans());//新的活动计划
        CpClub sourceClub = this.selectCpClubById(cpClub.getId());//源社团记录
        JSONArray sourceJson = JSONUtil.parseArray(sourceClub.getCpClubPlans());//老活动计划
        //先遍历老的活动计划在新的里面查找 没有则删除老活动计划
        for (int i = 0; i < sourceJson.size(); i++) {
            JSONObject source = sourceJson.getJSONObject(i);
            //如果新的里面没有老的则删除对应活动计划和活动
            if (jsonArray.stream().map(JSONObject::new).noneMatch(e -> e.getLong("id").equals(source.getLong("id")))) {
                cpClubPlanMapper.delCpClubPlan(source.getLong("id"));
                cpClubActivityMapper.accordingPlanIdDelActivity(source.getLong("id"));
            }
        }
        //遍历新的活动计划在老的里面查找 没有则新增 找到则修改
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < jsonArray.size(); i++) {
            boolean is = false;
            JSONObject newJson = jsonArray.getJSONObject(i);
            for (int j = 0; j < sourceJson.size(); j++) {
                if (sourceJson.getJSONObject(j).getLong("id").equals(newJson.getLong("id"))) {
                    //更新活动计划
                    CpClubPlan cpClubPlan = cpClubPlanMapper.getCpClubPlanInfo(newJson.getLong("id"));
                    cpClubPlan.setPlanName(newJson.getStr("planName"));
                    try {
                        cpClubPlan.setStartTime(sf.parse(newJson.getStr("startTime")));
                        cpClubPlan.setEndTime(sf.parse(newJson.getStr("endTime")));
                    } catch (ParseException e) {
                        throw new GlobalException("活动时间格式错误");
                    }
                    cpClubPlan.setRemake(newJson.getStr("remake"));
                    cpClubPlanMapper.updCpClubPlanInfo(cpClubPlan);
                    is = true;
                }
            }
            //没有则新增
            if (!is) {
                CpClubPlan cpClubPlan = new CpClubPlan();
                cpClubPlan.setClubId(cpClub.getId());
                cpClubPlan.setPlanName(newJson.getStr("planName"));
                try {
                    cpClubPlan.setStartTime(sf.parse(newJson.getStr("startTime")));
                    cpClubPlan.setEndTime(sf.parse(newJson.getStr("endTime")));
                } catch (ParseException e) {
                    throw new GlobalException("活动时间格式错误");
                }
                cpClubPlan.setRemake(newJson.getStr("remake"));
                cpClubPlanMapper.insertCpClubPlan(cpClubPlan);
                //根据活动计划新增活动
                CpClubActivity cpClubActivity = new CpClubActivity();
                cpClubActivity.setActivityName(cpClubPlan.getPlanName());
                cpClubActivity.setClubId(cpClub.getId());
                cpClubActivity.setClubName(cpClub.getClubName());
                cpClubActivity.setStartTime(cpClubPlan.getStartTime());
                cpClubActivity.setEndTime(cpClubPlan.getEndTime());
                cpClubActivity.setCreateTime(DateUtils.getNowDate());
                cpClubActivity.setCreateBy(SecurityUtils.getUsername());
                cpClubActivity.setUserId(sourceClub.getUserId());
                cpClubActivity.setDeptId(sourceClub.getDeptId());
                cpClubActivity.setPlanId(cpClubPlan.getId());
                cpClubActivityMapper.insertCpClubActivity(cpClubActivity);
            }
        }
        cpClub.setUpdateTime(DateUtils.getNowDate());
        cpClub.setUpdateBy(SecurityUtils.getUsername());
        cpClubMapper.updateCpClub(cpClub);
        return 1;
    }

    /**
     * 批量删除社团管理
     * 
     * @param ids 需要删除的社团管理主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCpClubByIds(Long[] ids)
    {
        for (Long id : ids) {
            this.judgeDelete(id);
            //对应删除社团报名记录
            cpClubEnrollMapper.clubIdDelEnrollInfo(id);
        }
        return cpClubMapper.deleteCpClubByIds(ids);
    }

    //根据关联的社团活动的结束时间,判断是否能删除
    public void judgeDelete(Long id){
        List<CpClubActivity> cpClubActivities = cpClubActivityMapper.selectCpClubActivityByClubId(id);
        if(!cpClubActivities.isEmpty()){
            for (CpClubActivity cpClubActivity : cpClubActivities) {
                Date endTime = cpClubActivity.getEndTime();
                Date nowTime = new Date();
                if(endTime.after(nowTime)){
                    throw new GlobalException("当前有社团关联的活动未结束,不能删除!");
                }
                cpClubActivityMapper.deleteCpClubActivityById(cpClubActivity.getId());
            }
            //根据社团id查询活动计划
            List<CpClubPlan> cpClubPlans = cpClubPlanMapper.getCpClubPlan(id);
            if (CollUtil.isNotEmpty(cpClubPlans)) {
                for (CpClubPlan cpClubPlan : cpClubPlans) {
                    //删除活动计划
                    cpClubPlanMapper.delCpClubPlan(cpClubPlan.getId());
                    //删除社团活动
                    cpClubActivityMapper.accordingPlanIdDelActivity(cpClubPlan.getId());
                }
            }
        }
    }
}
