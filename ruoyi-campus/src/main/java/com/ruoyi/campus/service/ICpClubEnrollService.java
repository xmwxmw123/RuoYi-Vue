package com.ruoyi.campus.service;

import java.util.List;
import com.ruoyi.campus.domain.CpClubEnroll;

/**
 * 社团报名Service接口
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
public interface ICpClubEnrollService 
{
    /**
     * 查询社团报名
     * 
     * @param id 社团报名主键
     * @return 社团报名
     */
    public CpClubEnroll selectCpClubEnrollById(Long id);

    /**
     * 查询社团报名列表
     * 
     * @param cpClubEnroll 社团报名
     * @return 社团报名集合
     */
    public List<CpClubEnroll> selectCpClubEnrollList(CpClubEnroll cpClubEnroll);

    /**
     * 新增社团报名
     * @param cpClubEnroll 报名信息
     * @param status 审核状态
     * @param source 来源
     * @return
     */
    public int insertCpClubEnroll(CpClubEnroll cpClubEnroll, Long status, Long source);

    /**
     * 修改社团报名
     * 
     * @param cpClubEnroll 社团报名
     * @return 结果
     */
    public int updateCpClubEnroll(CpClubEnroll cpClubEnroll);

    /**
     * 批量删除社团报名
     * 
     * @param ids 需要删除的社团报名主键集合
     * @return 结果
     */
    public int deleteCpClubEnrollByIds(Long[] ids);

    /**
     * 删除社团报名信息
     * 
     * @param id 社团报名主键
     * @return 结果
     */
    public int deleteCpClubEnrollById(Long id);

    /**
     * 报名审核
     * @param ids 报名审核id  多个以逗号分割
     * @param status 状态(0-待审核 1-通过 2-驳回)
     */
    void enrollExamine(String ids, Long status);
}
