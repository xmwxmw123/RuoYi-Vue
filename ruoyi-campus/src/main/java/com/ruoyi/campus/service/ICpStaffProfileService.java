package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpStaffProfile;
import com.ruoyi.campus.domain.exportVo.ExportStaffProfile;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 教工档案Service接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface ICpStaffProfileService 
{
    /**
     * 查询教工档案
     * 
     * @param id 教工档案主键
     * @return 教工档案
     */
    public  List<CpStaffProfile> selectCpStaffProfileById(CpStaffProfile params);

    /**
     * 查询教工档案列表
     * 
     * @param cpStaffProfile 教工档案
     * @return 教工档案集合
     */
    public List<CpStaffProfile> selectCpStaffProfileList(CpStaffProfile cpStaffProfile);

    /**
     * 新增教工档案
     * 
     * @param cpStaffProfile 教工档案
     * @return 结果
     */
    public int insertCpStaffProfile(CpStaffProfile cpStaffProfile);

    /**
     * 修改教工档案
     * 
     * @param cpStaffProfile 教工档案
     * @return 结果
     */
    public int updateCpStaffProfile(CpStaffProfile cpStaffProfile);

    /**
     * 批量删除教工档案
     * 
     * @param ids 需要删除的教工档案主键集合
     * @return 结果
     */
    public int deleteCpStaffProfileByIds(Long[] ids);

    /**
     * 删除教工档案信息
     * 
     * @param id 教工档案主键
     * @return 结果
     */
    public int deleteCpStaffProfileById(Long id);


    /**
     *  导出列表数据
     * @param exportStaffProfile
     * @return
     */
    List<ExportStaffProfile> ExportSelectCpStaffProfileList(@Param("exportStaffProfile") ExportStaffProfile exportStaffProfile);

    /**
     * query接口
     * @param cpStaffProfile
     * @return
     */
    List<CpStaffProfile> selectCpStaffProfileQuery(CpStaffProfile cpStaffProfile);
}
