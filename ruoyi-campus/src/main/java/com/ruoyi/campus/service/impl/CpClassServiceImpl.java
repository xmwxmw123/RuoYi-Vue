package com.ruoyi.campus.service.impl;

import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.CpStaffProfile;
import com.ruoyi.campus.domain.exportVo.ExportClass;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpGradeMapper;
import com.ruoyi.campus.mapper.CpStaffProfileMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.service.ICpClassService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 设置班级Service业务层处理
 *
 * @author ruoyi
 * @date 2023-07-31
 */
@Service
public class CpClassServiceImpl implements ICpClassService {
    @Resource
    private CpClassMapper cpClassMapper;

    @Resource
    private CpGradeMapper cpGradeMapper;

    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;

    @Resource
    private CpRegisterMapper cpRegisterMapper;

    /**
     * 查询设置班级
     */
    @Override
    public CpClass selectCpClassById(Long id) {
        return cpClassMapper.selectCpClassById(id);
    }

    /**
     * 查询设置班级列表
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpClass> selectCpClassList(CpClass cpClass) {
        return cpClassMapper.selectCpClassList(cpClass);
    }

    /**
     * 新增设置班级
     */
    @Override
    public int insertCpClass(CpClass cpClass) {

        if (cpClass.getClassNumber() == null || "".equals(cpClass.getClassNumber())) {
            throw new GlobalException("班级号不能为空");
        }
        if (cpClass.getClassName() == null || "".equals(cpClass.getClassName())) {
            throw new GlobalException("班级名称不能为空");
        }

        long period = cpClass.getPeriod();
        // 根据届别id 查询届别
        CpGrade cpGrade = cpGradeMapper.selectById(period);
        String rankS = cpGrade.getRankS();

        // 获取届别设置前最大的班级数
        Long classNumber = cpGrade.getClassNumber();
        // 根据届别 查询已知班级数
        int count = cpClassMapper.selectCount(rankS, cpGrade.getDeptId());
        if (rankS != null) {
            if (count >= classNumber) {
                throw new GlobalException("班级数超过最大设置值");
            }
        } else {
            throw new GlobalException("请传入届别");
        }
        int countClassNumber = cpClassMapper.countclassNumber(cpClass.getClassNumber(), cpClass.getPeriod(), cpClass.getDeptId());
        if (countClassNumber > 0) {
            throw new GlobalException("同一级别班级号不能重复");
        }

        // 根据职工id 查询老师
        long teacherId = cpClass.getTeacherId();
        CpStaffProfile cpStaffProfile = cpStaffProfileMapper.selectById(teacherId);
        String teacherName = cpStaffProfile.getName();

        // 查询设置 届别 班主任 创建日期 创建人
        String username = SecurityUtils.getUsername();
        cpClass.setCreateBy(username);
        cpClass.setTeacher(teacherName);
        cpClass.setRankS(rankS);
        cpClass.setCreateTime(DateUtils.getNowDate());
        cpClass.setGraduate("false");
        //班级首位去空,防止重复
        String className = cpClass.getClassName().trim();
        cpClass.setClassName(className);

        if (cpClass.getTeacher() == null || "".equals(cpClass.getTeacher())) {
            throw new GlobalException("班主任不能为空");
        }
        // 判断班级名称是否唯一
        String classUnique = isClassUnique(cpClass, cpClass.getDeptId());
        if (classUnique != null) {
            throw new GlobalException(classUnique);
        }
        // 执行添加操作
        return cpClassMapper.insertCpClass(cpClass);
    }

    /**
     * 检查 className 是否唯一
     */
    private String isClassUnique(CpClass cpClass, Long deptId) {
        long period = cpClass.getPeriod();
        String className = cpClass.getClassName();
        // 调用查询方法，根据传入的 semester 查询匹配的记录数量
        int count = cpClassMapper.countBySemester(className, period, deptId);
        // 如果记录数量大于 0 证明改级别下已有改名称
        if (count > 0) {
            return "同一级别班级名称不能重复";
        }
        return null;
    }


    /**
     * 修改设置班级
     */
    @Override
    public int updateCpClass(CpClass cpClass) {
        // 根据教师id查询教师姓名并从新赋值
        long teacherId = cpClass.getTeacherId();
        if (teacherId == 0) {
            throw new GlobalException("参数有误");
        }
        CpStaffProfile teacherName = cpStaffProfileMapper.selectTeacherName(teacherId);

        // 根据届别id查询 并从新赋值
        long period = cpClass.getPeriod();
        if (period == 0) {
            throw new GlobalException("参数有误");
        }
        CpGrade periodRankS = cpGradeMapper.selectRankById(period);
        System.out.println("periodRankS = " + periodRankS);

        // 给 修改人 届别 老师 修改时间 重新赋值
        String username = SecurityUtils.getUsername();
        cpClass.setUpdateBy(username);
        cpClass.setRankS(periodRankS.getRankS());
        cpClass.setTeacher(teacherName.getName());
        cpClass.setUpdateTime(DateUtils.getNowDate());

        return cpClassMapper.updateCpClass(cpClass);
    }

    /**
     * 批量删除设置班级
     */
    @Override
    @Transactional
    public int deleteCpClassByIds(Long[] ids) {
        for (Long id : ids) {
            int count = cpRegisterMapper.selectStudentByclassId(id);
            if(count > 0){
                throw new GlobalException("班级里面存在学生,不能删除");
            }
        }
        return cpClassMapper.deleteCpClassByIds(ids);
    }

    /**
     * 删除设置班级信息
     */
    @Override
    public int deleteCpClassById(Long id) {
        int count = cpRegisterMapper.selectStudentByclassId(id);
        if(count > 0){
            throw new GlobalException("班级里面存在学生,不能删除");
        }
        return cpClassMapper.deleteCpClassById(id);
    }

    /**
     * 根据届别查询年级
     */
    @Override
    public List<CpClass> queryList(Long id) {
        // 判断参数
        if (id == null) {
            throw new GlobalException("参数为空");
        }
        // 根据届别id查询届别
        CpGrade cpGrade = cpGradeMapper.selectById(id);
        if (cpGrade == null) {
            throw new GlobalException("参数错误");
        }
        String rankS = cpGrade.getRankS();
        // 根据届别查询班级
        List<CpClass> cpClasses = cpClassMapper.queryRankSList(rankS, cpGrade.getDeptId());
        return cpClasses;
    }

    /**
     * 查询班级列表
     */
    @Override
    public List<CpClass> findList(Long id, Long deptId) {

        List<CpClass> classList = cpClassMapper.findList(id, deptId);
        return classList;
    }

    /* 导出查询列表 */
    @Override
    public List<ExportClass> findCpClassList(ExportClass exportClass) throws ParseException {

        List<ExportClass> cpClassList = cpClassMapper.findCpClassList(exportClass);

        return cpClassList;
    }

    @DataScope(deptAlias = "d")
    @Override
    public List<Map<String, String>> findListNotNull(CpClass cpClass) {
        List<Map<String, String>> classList = cpClassMapper.findListNotNull(cpClass);
        return classList;
    }
}
