package com.ruoyi.campus.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ruoyi.campus.domain.CpBdxs;
import com.ruoyi.campus.domain.CpParent;
import com.ruoyi.campus.domain.CpParentStu;
import com.ruoyi.campus.mapper.CpParentMapper;
import com.ruoyi.campus.mapper.CpParentStuMapper;
import com.ruoyi.campus.service.ICpParentService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 家长账号Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-27
 */
@Service
public class CpParentServiceImpl implements ICpParentService 
{
    @Resource
    private CpParentMapper cpParentMapper;
    @Resource
    private CpParentStuMapper cpParentStuMapper;

    /**
     * 查询家长账号
     * 
     * @param id 家长账号主键
     * @return 家长账号
     */
    @Override
    public CpParent selectCpParentById(Long id)
    {
        CpParent cpParent = cpParentMapper.selectCpParentById(id);
        this.selectBdxs(cpParent);
        return cpParent;
    }

    /**
     * 查询家长账号列表
     * 
     * @param cpParent 家长账号
     * @return 家长账号
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpParent> selectCpParentList(CpParent cpParent)
    {
        List<CpParent> cpParents = cpParentMapper.selectCpParentList(cpParent);
        for (CpParent parent : cpParents) {
            this.selectBdxs(parent);
        }
        return cpParents;
    }

    //拿到绑定学生信息
    public void selectBdxs(CpParent cpParent) {
        List<CpBdxs> cpBdxss = cpParentStuMapper.selectBdxs(cpParent.getId());
        if (CollUtil.isNotEmpty(cpBdxss)) {
            String bdxs = JSONUtil.toJsonStr(cpBdxss);
            cpParent.setBdxs(bdxs);
        }
    }

    /**
     * 新增家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertCpParent(CpParent cpParent)
    {
        //校验账号是否存在
        this.checkCpParentExist(cpParent.getParentPhone(), cpParent.getDeptId());
        //新增家长表数据
        cpParent.setCreateTime(DateUtils.getNowDate());
        cpParent.setCreateBy(SecurityUtils.getUsername());
        cpParent.setUsableStatus("0");//设置账号状态为正常
        cpParent.setSource(1L);//来源
        int ret = cpParentMapper.insertCpParent(cpParent);
        //插入绑定学生
        this.insertCpParentStu(cpParent, "1");//审核成功
        return ret;
    }

    /**
     * 修改家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateCpParent(CpParent cpParent)
    {
        cpParent.setUpdateTime(DateUtils.getNowDate());
        cpParent.setUpdateBy(SecurityUtils.getUsername());
        int ret = cpParentMapper.updateCpParent(cpParent);
        //先删除原有绑定的学生数据
        cpParentStuMapper.deleteCpParentStu(cpParent.getId());
        //插入绑定学生
        JSONArray jsonArray = JSONUtil.parseArray(cpParent.getBdxs());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long registerId = (long)jsonObject.getInt("registerId");
            String appellation = jsonObject.getStr("appellation");
            CpParentStu cpParentStu = new CpParentStu();
            cpParentStu.setParentId(cpParent.getId());
            cpParentStu.setRegisterId(registerId);
            cpParentStu.setAppellation(appellation);
            cpParentStu.setAuditStatus("0");
            //如果来源是后太则新增绑定学生全是通过
            if (cpParent.getSource() == 1L) {
                cpParentStu.setAuditStatus("1");//审核状态
            }
            cpParentStuMapper.insertCpParentStu(cpParentStu);
        }
        return ret;
    }

    //新增家长绑定学生数据
    public void insertCpParentStu(CpParent cpParent, String auditStatus){
        JSONArray jsonArray = JSONUtil.parseArray(cpParent.getBdxs());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long registerId = (long)jsonObject.getInt("registerId");
            String appellation = jsonObject.getStr("appellation");
            CpParentStu cpParentStu = new CpParentStu();
            cpParentStu.setParentId(cpParent.getId());
            cpParentStu.setRegisterId(registerId);
            cpParentStu.setAppellation(appellation);
            cpParentStu.setAuditStatus(auditStatus);//审核状态
            cpParentStuMapper.insertCpParentStu(cpParentStu);
        }
    }


    /**
     * 批量删除家长账号
     * 
     * @param ids 需要删除的家长账号主键
     * @return 结果
     */
    @Override
    public int deleteCpParentByIds(Long[] ids)
    {
        for (Long id : ids) {
            //先删除原有绑定的学生数据
            cpParentStuMapper.deleteCpParentStu(id);
        }
        return cpParentMapper.deleteCpParentByIds(ids);
    }

    /**
     * 删除家长账号信息
     * 
     * @param id 家长账号主键
     * @return 结果
     */
    @Override
    public int deleteCpParentById(Long id)
    {
        //先删除原有绑定的学生数据
        cpParentStuMapper.deleteCpParentStu(id);
        return cpParentMapper.deleteCpParentById(id);
    }

    /**
     * 修改家长账号状态
     */
    @Override
    public int updateUsableStatus(Long id) {
        CpParent parent = cpParentMapper.selectCpParentById(id);
        if(ObjectUtils.isNotEmpty(parent)){
            if(parent.getUsableStatus().equals("0")){
                parent.setUsableStatus("1");//设置账号状态为禁用
                cpParentMapper.updateUsableStatus(parent);
            }else{
                parent.setUsableStatus("0");//设置账号状态为正常
                cpParentMapper.updateUsableStatus(parent);
            }
        }
        return 1;
    }

    /**
     * 查询家长账号信息
     * @param username 登录账号
     * @param password 登录密码
     * @param deptId 部门id
     * @return 家长账号信息
     */
    @Override
    public CpParent selectCpParentInfo(String username, String password, Long deptId) {
        return cpParentMapper.selectCpParentInfo(username, password, deptId);
    }

    /**
     * 家长账号注册（家长端）
     * @param name 用户姓名
     * @param username 登录账号
     * @param password 登录密码
     * @param deptId 部门id
     */
    @Override
    public void cpParentRegister(String name, String username, String password, Long deptId) {
        //校验账号是否存在
        this.checkCpParentExist(username, deptId);
        CpParent cpParent = new CpParent();
        cpParent.setParentName(name);
        cpParent.setParentPhone(username);
        cpParent.setPassword(password);
        cpParent.setUsableStatus("0");
        cpParent.setCreateTime(DateUtils.getNowDate());
        cpParent.setDeptId(deptId);
        cpParent.setSource(2L);//来源
        cpParentMapper.insertCpParent(cpParent);
    }

    /**
     * 校验家长账号是否存在
     * @param username 家长手机号码
     * @param deptId 部门id
     */
    private void checkCpParentExist(String username, Long deptId) {
        CpParent parent = new CpParent();
        parent.setParentPhone(username);
        parent.setDeptId(deptId);
        if (null != cpParentMapper.selectCpParentByPhone(parent)) {
            throw new GlobalException("登录账号已存在");
        }
    }

    /**
     * 绑定学生(家长APP端)
     * @param cpParent 参数（只需要传入id，bdxs字段）
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void bindingStudent(CpParent cpParent) {
        CpParent parent = this.selectCpParentById(cpParent.getId());
        if (Objects.isNull(parent)) {
            throw new GlobalException("账号信息不存在");
        }
        //先删除当前家长以前绑定的学生
        cpParentStuMapper.deleteCpParentStu(cpParent.getId());
        JSONArray parentJsonArray = JSONUtil.parseArray(parent.getBdxs());
        JSONArray jsonArray = JSONUtil.parseArray(cpParent.getBdxs());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long registerId = (long)jsonObject.getInt("registerId");
            String appellation = jsonObject.getStr("appellation");
            CpParentStu cpParentStu = new CpParentStu();
            cpParentStu.setParentId(cpParent.getId());
            cpParentStu.setRegisterId(registerId);
            cpParentStu.setAppellation(appellation);
            cpParentStu.setAuditStatus("0");//审核状态 默认0
            if (parentJsonArray.stream().map(JSONObject::new)
                    .anyMatch(e -> e.getLong("parentId").equals(cpParentStu.getParentId()) && e.getLong("registerId").equals(cpParentStu.getRegisterId())
                            && e.getStr("auditStatus").equals("1"))) {
                cpParentStu.setAuditStatus("1");//审核状态
            }
            cpParentStuMapper.insertCpParentStu(cpParentStu);
        }
    }

    /**
     * 审核绑定学生
     * @param id 家长绑定学生表主键
     * @param type  1-通过, 2-未通过
     */
    @Override
    public void examine(Long id, String type) {
        CpParentStu cpParentStu = cpParentStuMapper.getCpParentStu(id);
        if (Objects.isNull(cpParentStu)) {
            throw new GlobalException("记录不存在");
        }
        //如果是未通过则移除
        if (type.equals("2")) {
            cpParentStuMapper.delCpParentStu(id);
        } else {
            cpParentStuMapper.updCpParentStu(type, id);
        }
    }

    /**
     * 我的资料修改（家长APP端）
     * @param cpParent 参数
     */
    @Override
    public void updMyProfile(CpParent cpParent) {
        if (Objects.isNull(cpParent.getId()) || StringUtils.isBlank(cpParent.getParentName()) || StringUtils.isBlank(cpParent.getHead())) {
            throw new GlobalException("参数错误");
        }
        CpParent parent = cpParentMapper.selectCpParentById(cpParent.getId());
        if (Objects.isNull(parent)) {
            throw new GlobalException("账号信息不存在");
        }
        parent.setParentName(cpParent.getParentName());
        parent.setHead(cpParent.getHead());
        cpParentMapper.updateCpParent(parent);
    }
}
