package com.ruoyi.campus.service;

import java.util.List;
import com.ruoyi.campus.domain.CpTimetableSet;
import com.ruoyi.campus.domain.dto.CpTimetableReqDto;
import com.ruoyi.campus.domain.dto.CpTimetableRespDto;

/**
 * 教师任课设置Service接口
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ICpTimetableSetService 
{
    /**
     * 查询教师任课设置
     * 
     * @param id 教师任课设置主键
     * @return 教师任课设置
     */
    public CpTimetableSet selectCpTimetableSetById(Long id);

    /**
     * 查询教师任课设置列表
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 教师任课设置集合
     */
    public List<CpTimetableSet> selectCpTimetableSetList(CpTimetableSet cpTimetableSet);

    /**
     * 新增教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    public int insertCpTimetableSet(CpTimetableSet cpTimetableSet);

    /**
     * 修改教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    public int updateCpTimetableSet(CpTimetableSet cpTimetableSet);

    /**
     * 批量删除教师任课设置
     * 
     * @param ids 需要删除的教师任课设置主键集合
     * @return 结果
     */
    public int deleteCpTimetableSetByIds(Long[] ids);

    /**
     * 删除教师任课设置信息
     * 
     * @param id 教师任课设置主键
     * @return 结果
     */
    public int deleteCpTimetableSetById(Long id);

    /**
     * 排课
     * @param cpTimetableReqDto 参数
     */
    void arrangeCourse(CpTimetableReqDto cpTimetableReqDto);

    /**
     * 查询排课信息
     *
     * @param id 教师任课设置主键
     */
    public CpTimetableRespDto arrangeCourseList(Long id);
}
