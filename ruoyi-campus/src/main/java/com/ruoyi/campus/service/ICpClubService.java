package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpClub;
import com.ruoyi.campus.domain.CpClubEnroll;
import com.ruoyi.campus.domain.dto.CpClubStuDto;

import java.util.List;

/**
 * 社团管理Service接口
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
public interface ICpClubService 
{
    /**
     * 查询社团管理
     * 
     * @param id 社团管理主键
     * @return 社团管理
     */
    public CpClub selectCpClubById(Long id);

    /**
     * 查询社团管理列表(PC端和教师APP)
     * 
     * @param cpClub 社团管理
     * @return 社团管理集合
     */
    public List<CpClub> selectCpClubList(CpClub cpClub);

    /**
     * 查询社团管理列表(家长端APP)
     */
    List<CpClub> parentAppQuyCpClubList(CpClub cpClub);

    /**
     * 查询当前学生已报名社团(家长端APP)
     */
    List<CpClub> parentAppQuyEnrollClubList(CpClubEnroll clubEnroll);

    /**
     * 新增社团管理
     * 
     * @param cpClub 社团管理
     * @return 结果
     */
    public int insertCpClub(CpClub cpClub);

    /**
     * 修改社团管理
     * 
     * @param cpClub 社团管理
     * @return 结果
     */
    public int updateCpClub(CpClub cpClub);

    /**
     * 批量删除社团管理
     * 
     * @param ids 需要删除的社团管理主键集合
     * @return 结果
     */
    public int deleteCpClubByIds(Long[] ids);
}
