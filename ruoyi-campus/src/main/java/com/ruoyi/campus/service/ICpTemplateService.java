package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpTemplate;

import java.util.List;

/**
 * 评价模板Service接口
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
public interface ICpTemplateService 
{
    /**
     * 查询评价模板
     * 
     * @param id 评价模板主键
     * @return 评价模板
     */
    public CpTemplate selectCpTemplateById(Long id);

    /**
     * 查询评价模板列表
     * 
     * @param cpTemplate 评价模板
     * @return 评价模板集合
     */
    public List<CpTemplate> selectCpTemplateList(CpTemplate cpTemplate);

    /**
     * 新增评价模板
     * 
     * @param cpTemplate 评价模板
     * @return 结果
     */
    public int insertCpTemplate(CpTemplate cpTemplate);

    /**
     * 修改评价模板
     * 
     * @param cpTemplate 评价模板
     * @return 结果
     */
    public int updateCpTemplate(CpTemplate cpTemplate);

    /**
     * 批量删除评价模板
     * 
     * @param ids 需要删除的评价模板主键集合
     * @return 结果
     */
    public int deleteCpTemplateByIds(Long[] ids);

    /**
     * 删除评价模板信息
     * 
     * @param id 评价模板主键
     * @return 结果
     */
    public int deleteCpTemplateById(Long id);

    /**
     * 修改账号是否启用
     * @param id
     * @return
     */
    int updateEnable(Long id);

    /**
     * 二级指标保存接口
     * @param cpTemplate
     * @return
     */
    int twoIndex(CpTemplate cpTemplate);

    /**
     * 记录要点保存接口
     * @param cpTemplate
     * @return
     */
    int record(CpTemplate cpTemplate);

    /**
     * 删除记录要点信息
     * @param cpTemplate
     * @return
     */
    int deleteRecord(CpTemplate cpTemplate);

    /**
     * 录入完成接口
     * @param id
     * @return
     */
    int complete(Long id);

}
