package com.ruoyi.campus.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.ruoyi.campus.domain.*;
import com.ruoyi.campus.domain.dto.CpArrangeCourseReqDto;
import com.ruoyi.campus.domain.dto.CpTimetableReqDto;
import com.ruoyi.campus.domain.dto.CpTimetableRespDto;
import com.ruoyi.campus.mapper.*;
import com.ruoyi.campus.service.ICpTimetableSetService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 教师任课设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class CpTimetableSetServiceImpl implements ICpTimetableSetService 
{
    @Resource
    private CpTimetableSetMapper cpTimetableSetMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private CpGradeMapper cpGradeMapper;
    @Resource
    private CpClassMapper cpClassMapper;

    @Resource
    private CpTimetableMapper cpTimetableMapper;

    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;
    /**
     * 查询教师任课设置
     * 
     * @param id 教师任课设置主键
     * @return 教师任课设置
     */
    @Override
    public CpTimetableSet selectCpTimetableSetById(Long id)
    {
        CpTimetableSet cpTimetableSet = cpTimetableSetMapper.selectCpTimetableSetById(id);
        if (Objects.nonNull(cpTimetableSet)) {
            CpSemesters cpSemesters = cpSemestersMapper.selectByid(cpTimetableSet.getSemestersId());
            if (Objects.nonNull(cpSemesters)) {
                cpTimetableSet.setSemesters(cpSemesters.getSemester());
            }
            CpGrade cpGrade = cpGradeMapper.selectById(Long.valueOf(cpTimetableSet.getPeriod()));
            if (Objects.nonNull(cpGrade)) {
                cpTimetableSet.setRankS(cpGrade.getRankS());
            }
            CpClass cpClass = cpClassMapper.selectCpClassById(cpTimetableSet.getClassId());
            if (Objects.nonNull(cpClass)) {
                cpTimetableSet.setClassName(cpClass.getClassName());
            }
        }
        return cpTimetableSet;
    }

    /**
     * 查询教师任课设置列表
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 教师任课设置
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpTimetableSet> selectCpTimetableSetList(CpTimetableSet cpTimetableSet)
    {
        return cpTimetableSetMapper.selectCpTimetableSetList(cpTimetableSet);
    }

    /**
     * 新增教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    @Override
    public int insertCpTimetableSet(CpTimetableSet cpTimetableSet)
    {
        cpTimetableSet.setCreateBy(SecurityUtils.getUsername());
        cpTimetableSet.setCreateTime(DateUtils.getNowDate());
        int count = cpTimetableSetMapper.quyCpTimetableSetRepeat(cpTimetableSet.getSemestersId(), cpTimetableSet.getPeriod(), cpTimetableSet.getGrade(), cpTimetableSet.getClassId(), cpTimetableSet.getDeptId());
        if (count > 0) {
            throw new GlobalException("新增教师任课设置重复");
        }
        return cpTimetableSetMapper.insertCpTimetableSet(cpTimetableSet);
    }

    /**
     * 修改教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    @Override
    public int updateCpTimetableSet(CpTimetableSet cpTimetableSet)
    {
        cpTimetableSet.setUpdateBy(SecurityUtils.getUsername());
        cpTimetableSet.setUpdateTime(DateUtils.getNowDate());

        String staffSetText = cpTimetableSet.getStaffSetText();
        JSONArray jsonArray = new JSONArray(staffSetText);
        for (int i = 0; i < jsonArray.size(); i++) {
            CpTimetable cpTimetable = new CpTimetable();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long staffId = (long)jsonObject.getInt("teacher_id");
            Long subjectId = (long)jsonObject.getInt("subject_id");
            cpTimetable.setStaffId(staffId);
            cpTimetable.setSubjectId(subjectId);
            cpTimetable.setTimetableSet(cpTimetableSet.getId());
            cpTimetable.setDeptId(cpTimetableSet.getDeptId());
            CpStaffProfile cpStaffProfile = cpStaffProfileMapper.selectById(staffId);
            cpTimetable.setUserId(cpStaffProfile.getUserId());
            //修改查询表中的学科和老师
            cpTimetableMapper.updateStaffId(cpTimetable);
        }
        return cpTimetableSetMapper.updateCpTimetableSet(cpTimetableSet);
    }

    /**
     * 批量删除教师任课设置
     * 
     * @param ids 需要删除的教师任课设置主键
     * @return 结果
     */
    @Override
    public int deleteCpTimetableSetByIds(Long[] ids)
    {
        return cpTimetableSetMapper.deleteCpTimetableSetByIds(ids);
    }

    /**
     * 删除教师任课设置信息
     * 
     * @param id 教师任课设置主键
     * @return 结果
     */
    @Override
    public int deleteCpTimetableSetById(Long id)
    {
        return cpTimetableSetMapper.deleteCpTimetableSetById(id);
    }

    /**
     * 排课保存
     * @param cpTimetableReqDto 参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
     public void arrangeCourse(CpTimetableReqDto cpTimetableReqDto) {
         if (Objects.isNull(cpTimetableReqDto.getId()) || CollUtil.isEmpty(cpTimetableReqDto.getArrangeCourse())) {
             throw new GlobalException("参数错误");
         }
        String createBy = SecurityUtils.getUsername();
        Date createDate = DateUtils.getNowDate();
        CpTimetableSet cpTimetableSet = cpTimetableSetMapper.selectCpTimetableSetById(cpTimetableReqDto.getId());
        if (Objects.isNull(cpTimetableSet)) return;

        if (cpTimetableSet.getStatus() == 0) {//未排课
            for (CpArrangeCourseReqDto dto : cpTimetableReqDto.getArrangeCourse()) {
                this.addCpTimetable(cpTimetableSet.getId(), dto, cpTimetableReqDto.getDeptId(), createBy, createDate);
            }
            cpTimetableSet.setStatus(1);
            cpTimetableSetMapper.updateCpTimetableSet(cpTimetableSet);
        } else {
            List<CpTimetable> cpTimetableList = cpTimetableSetMapper.quyCpTimetableList(cpTimetableReqDto.getId(), cpTimetableReqDto.getDeptId());
            for (CpArrangeCourseReqDto dto : cpTimetableReqDto.getArrangeCourse()) {
                Optional<CpTimetable> optional = cpTimetableList.stream().filter(e -> dto.getPitchNumber().equals(e.getPitchNumber()) && dto.getWeek().equals(e.getWeek())).findFirst();
                if (optional.isPresent()) {
                    CpTimetable cpTimetable = optional.get();
                    cpTimetable.setSubjectId(dto.getSubjectId());
                    cpTimetable.setStaffId(dto.getStaffId());
                    cpTimetable.setUpdateBy(createBy);
                    cpTimetable.setUpdateTime(createDate);
                    cpTimetableMapper.updateCpTimetable(cpTimetable);
                } else {
                    this.addCpTimetable(cpTimetableSet.getId(), dto, cpTimetableReqDto.getDeptId(), createBy, createDate);
                }
            }
        }
    }

    private void addCpTimetable(Long id, CpArrangeCourseReqDto dto, Long deptId, String createBy, Date createDate) {
        CpTimetable cpTimetable = new CpTimetable();
        cpTimetable.setTimetableSet(id);
        cpTimetable.setSubjectId(dto.getSubjectId());
        cpTimetable.setStaffId(dto.getStaffId());
        cpTimetable.setPitchNumber(dto.getPitchNumber().toString());
        cpTimetable.setWeek(dto.getWeek().toString());
        cpTimetable.setDeptId(deptId);
        CpStaffProfile cpStaffProfile = cpStaffProfileMapper.selectById(dto.getStaffId());
        cpTimetable.setUserId(cpStaffProfile.getUserId());
        cpTimetable.setCreateBy(createBy);
        cpTimetable.setCreateTime(createDate);
        cpTimetableMapper.insertCpTimetable(cpTimetable);
    }

    /**
     * 查询排课信息
     *
     * @param id 教师任课设置主键
     */
    @Override
    public CpTimetableRespDto arrangeCourseList(Long id) {
        CpTimetableRespDto dto = new CpTimetableRespDto();
        CpTimetableSet cpTimetableSet = cpTimetableSetMapper.selectCpTimetableSetById(id);
        if (Objects.nonNull(cpTimetableSet)) {
            dto.setId(cpTimetableSet.getId());//任课设置id
            dto.setStaffSetText(cpTimetableSet.getStaffSetText());//教师任课设置
            CpSemesters cpSemesters = cpSemestersMapper.selectByid(cpTimetableSet.getSemestersId());
            if (Objects.nonNull(cpSemesters)) {
                dto.setSemesters(cpSemesters.getSemester());
            }
            CpGrade cpGrade = cpGradeMapper.selectById(Long.valueOf(cpTimetableSet.getPeriod()));
            if (Objects.nonNull(cpGrade)) {
                dto.setRankS(cpGrade.getRankS());
            }
            CpClass cpClass = cpClassMapper.selectCpClassById(cpTimetableSet.getClassId());
            if (Objects.nonNull(cpClass)) {
                dto.setClassName(cpClass.getClassName());
            }
            dto.setGrade(cpTimetableSet.getGrade());
            //查询排课记录 组装已排课json
            List<CpTimetable> cpTimetableList = cpTimetableSetMapper.quyCpTimetableList(cpTimetableSet.getId(), cpTimetableSet.getDeptId());
            if (CollUtil.isNotEmpty(cpTimetableList)) {
                List<CpArrangeCourseReqDto> arrangeCourse = new ArrayList<>();
                for (CpTimetable cpTimetable : cpTimetableList) {
                    CpArrangeCourseReqDto cpArrangeCourseReqDto = new CpArrangeCourseReqDto();
                    cpArrangeCourseReqDto.setPitchNumber(cpTimetable.getPitchNumber());
                    cpArrangeCourseReqDto.setStaffId(cpTimetable.getStaffId());
                    cpArrangeCourseReqDto.setSubjectId(cpTimetable.getSubjectId());
                    cpArrangeCourseReqDto.setWeek(cpTimetable.getWeek());
                    arrangeCourse.add(cpArrangeCourseReqDto);
                }
                dto.setArrangeCourse(arrangeCourse);
            }
        }
        return dto;
    }
}
