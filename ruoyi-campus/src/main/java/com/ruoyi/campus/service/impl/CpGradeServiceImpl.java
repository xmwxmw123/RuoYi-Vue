package com.ruoyi.campus.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.CpUpdateStatus;
import com.ruoyi.campus.domain.exportVo.ExportGrade;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpGradeMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.service.ICpGradeService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 设置届别Service业务层处理
 *
 * @author ruoyi
 * @date 2023-07-28
 */
@Service
public class CpGradeServiceImpl implements ICpGradeService {
    @Resource
    private CpGradeMapper cpGradeMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpClassMapper cpClassMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 查询设置届别
     *
     * @param id 设置届别主键
     * @return 设置届别
     */
    @Override
    public CpGrade selectCpGradeById(Long id) {
        return cpGradeMapper.selectCpGradeById(id);
    }

    /**
     * 查询设置届别列表
     *
     * @param cpGrade 设置届别
     * @return 设置届别
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpGrade> selectCpGradeList(CpGrade cpGrade) {
        List<CpGrade> cpGrades = cpGradeMapper.selectCpGradeList(cpGrade);
        List<CpGrade> sortedList = cpGrades.stream()
                .sorted(Comparator.comparing(CpGrade::getRankS).thenComparing(CpGrade::getCreateTime))
                .collect(Collectors.toList());
        return sortedList;

    }

    /**
     * 导出数据
     */
    @Override
    public List<ExportGrade> ExportSelectCpGradeList(ExportGrade exportGrade) {
        List<ExportGrade> cpGrades = cpGradeMapper.ExportSelectCpGradeList(exportGrade);

        for (ExportGrade cpGrade : cpGrades) {
            //年级
            if (cpGrade.getCurrentClass() != null) {
                String label = dictDataMapper.selectDictLabel("grade_list", cpGrade.getCurrentClass());
                cpGrade.setCurrentClass(label);
            }


            if (cpGrade.getGraduate() != null) {
                String label = dictDataMapper.selectDictLabel("boole_type", cpGrade.getGraduate());
                cpGrade.setGraduate(label);
            }
        }
        return cpGrades;
    }


    /**
     * 新增设置届别
     *
     * @param cpGrade 设置届别
     * @return 结果
     */
    @Override
    @Transactional
    public int insertCpGrade(CpGrade cpGrade) {

        // 获取登录用户并赋值 创建人 新增数据
        String username = SecurityUtils.getUsername();
        cpGrade.setCreateBy(username);
        cpGrade.setCreateTime(DateUtils.getNowDate());

        if (cpGrade.getRankS() == null || "".equals(cpGrade.getRankS())) {
            throw new GlobalException("届别不能为空");
        }

        if (cpGrade.getCurrentClass() == null || "".equals(cpGrade.getCurrentClass())) {
            throw new GlobalException("当前年级不能为空");
        }

        if (cpGrade.getGraduate() == null || "".equals(cpGrade.getGraduate())) {
            throw new GlobalException("是否毕业不能为空");
        }

        if (cpGrade.getClassNumber() == null) {
            throw new GlobalException("班级数不能为空");
        }
        //查询界别是否重复,重复返回异常
        String rankS = cpGrade.getRankS();
        int i = cpGradeMapper.selectRankS(rankS, cpGrade.getDeptId());
        if (i > 0) {
            throw new GlobalException("级别名称不可重复");
        }

        //查询currentClass是否存在重复
        String currentClass = cpGrade.getCurrentClass();
        int currentClassCount = cpGradeMapper.selectCurrentClassCount(currentClass, cpGrade.getDeptId());
        if (currentClassCount > 0) {
            throw new GlobalException("存在相同年级, 请先把旧年级升级处理");
        }
        cpGrade.setCreateTime(DateUtils.getNowDate());
        return cpGradeMapper.insertCpGrade(cpGrade);
    }

    /**
     * 修改设置届别
     *
     * @param cpGrade 设置届别
     * @return 结果
     */
    @Override
    @Transactional
    public int updateCpGrade(CpGrade cpGrade) {
        // 获取登录用户并赋值 创建人 修改人
        String username = SecurityUtils.getUsername();
        cpGrade.setUpdateBy(username);
        cpGrade.setUpdateTime(DateUtils.getNowDate());

        CpUpdateStatus cpUpdateStatus = new CpUpdateStatus();
        cpUpdateStatus.setRankS(cpGrade.getRankS());
        //为ture表示修改状态为'毕业',同步学籍表和届别表的状态数据
        if ("true".equals(cpGrade.getGraduate())) {
            cpRegisterMapper.updateGraduate(cpUpdateStatus);
            //修改班级为毕业
            String graduate = "true";
            cpClassMapper.updateCpClassGraduate(graduate, cpGrade.getId());
        } else {
            cpRegisterMapper.updateNotGraduate(cpUpdateStatus);

        }
        //查出年级的班级个数
        int count = cpClassMapper.selectclassCount(cpGrade.getId(), cpGrade.getDeptId());
        if(cpGrade.getClassNumber() < count){
            throw new GlobalException("输入的班级数不能小于当前班级数");
        }

        return cpGradeMapper.updateCpGrade(cpGrade);
    }

    /**
     * 批量删除设置届别
     *
     * @param ids 需要删除的设置届别主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteCpGradeByIds(Long[] ids) {
        // 根据届别id 查询届别下是否有班级 如有班级不可以删除
        for (Long id : ids) {
            int count = cpClassMapper.selectClassByperiod(id);
            if(count > 0){
                throw new GlobalException("当前届别下面存在班级不能删除");
            }
        }
        return cpGradeMapper.deleteCpGradeByIds(ids);
    }

    /**
     * 删除设置届别信息
     *
     * @param id 设置届别主键
     * @return 结果
     */
    @Override
    public int deleteCpGradeById(Long id) {
        int count = cpClassMapper.selectClassByperiod(id);
        if(count > 0){
            throw new GlobalException("当前届别下面存在班级不能删除");
        }
        return cpGradeMapper.deleteCpGradeById(id);
    }


    /**
     * 升级当前年级
     *
     * @param
     * @return
     */
    @Override
    public int upgrade(CpGrade cpGrade) {
        // 获取当前年级 如果当前年级大于9 就抛出错误信息
        String currentClass = cpGrade.getCurrentClass();

        if (StringUtils.isBlank(currentClass)) {
            throw new BaseException("请选择届别");
        }
        int classValue = Integer.parseInt(currentClass);
        if (classValue > 8) {
            throw new BaseException("当前年级已为最大年级");
        }
        //升年级,使学籍表和届别表年级数据同步
        if (classValue < 9) {
            cpRegisterMapper.updateGrade(cpGrade.getCurrentClass(), cpGrade.getRankS(), cpGrade.getDeptId());
        }
        // 执行代码
        return cpGradeMapper.upgrade(cpGrade);
    }


    /**
     * 查询列表
     *
     * @return
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpGrade> queryList(CpGrade cpGrade) {
        List<CpGrade> gradeList = cpGradeMapper.queryList(cpGrade);
        if (CollUtil.isNotEmpty(gradeList)) {
            gradeList = gradeList.stream().sorted(Comparator.comparing(CpGrade::getRankS)) .collect(Collectors.toList());
        }
        return gradeList;
    }


}
