package com.ruoyi.campus.service;

import com.ruoyi.campus.domain.CpTimetable;

import java.util.List;

/**
 * 课表查询Service接口
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ICpTimetableService 
{
    /**
     * 查询课表查询
     * 
     * @param id 课表查询主键
     * @return 课表查询
     */
    public CpTimetable selectCpTimetableById(Long id);

    /**
     * 查询课表查询列表
     * 
     * @param cpTimetable 课表查询
     * @return 课表查询集合
     */
    public List<CpTimetable> selectCpTimetableList(CpTimetable cpTimetable);

    /**
     * 新增课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    public int insertCpTimetable(CpTimetable cpTimetable);

    /**
     * 修改课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    public int updateCpTimetable(CpTimetable cpTimetable);

    /**
     * 批量删除课表查询
     * 
     * @param ids 需要删除的课表查询主键集合
     * @return 结果
     */
    public int deleteCpTimetableByIds(Long[] ids);

    /**
     * 删除课表查询信息
     * 
     * @param id 课表查询主键
     * @return 结果
     */
    public int deleteCpTimetableById(Long id);

    /**
     * 导出
     * @param cpTimetable
     * @return
     */
    List<CpTimetable> export(CpTimetable cpTimetable);
}
