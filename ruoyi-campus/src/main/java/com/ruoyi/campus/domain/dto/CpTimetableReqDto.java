package com.ruoyi.campus.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 排课入参
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpTimetableReqDto
{
    /** 任课设置id */
    private Long id;

    /** 排课对象*/
    private List<CpArrangeCourseReqDto> arrangeCourse;

    /** 部门id */
    private Long deptId;

    /** 用户id */
    private Long userId;


}
