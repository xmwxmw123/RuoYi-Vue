package com.ruoyi.campus.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpClubStuDto {

    //社团id
    private Long clubId;

    //学生ids
    private Long[] registerId;

    //** 当负责人:1是, 0否 */
    private Long head;
}
