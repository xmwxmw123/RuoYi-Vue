package com.ruoyi.campus.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置学期对象 cp_semesters
 *
 * @author ruoyi
 * @date 2023-07-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpSemesters extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 学期 */

    @Excel(name = "学期" )
    private String semester;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long orders;

    @Excel(name = "当前学期")
    private String currentSemester ;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
