package com.ruoyi.campus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 教工档案对象 cp_staff_profile
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CpStaffProfile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private String numbering;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 出生年月 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生年月", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataBirth;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 身高 */
    @Excel(name = "身高")
    private String  height;

    /** 血型 */
    @Excel(name = "血型")
    private String bloodType;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idNumber;

    //邮箱
    @Excel(name = "邮箱")
    private String email;

    /** 籍贯 */
    @Excel(name = "籍贯")
    @Size(max = 20 ,message = "籍贯长度过长")
    private String origin;

    /** 所学专业 */
    @Excel(name = "所学专业")
    private String majorsStudied;

    /** 毕业时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "毕业时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date graduationTime;

    /** 最高学历 */
    @Excel(name = "最高学历")
    private String highestEducation;

    /** 职务 */
    @Excel(name = "职务")
    private String jobTitle;

    /** 评定时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "评定时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date assessmentTime;

    /** 毕业院校 */
    @Excel(name = "毕业院校")
    private String graduateSchool;

    /** 聘任职务 */
    @Excel(name = "聘任职务")
    private String appointmentPosition;

    /** 聘任时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "聘任时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lengthEmployment;

    /** 行政职务 */
    @Excel(name = "行政职务")
    private String administrativePositions;

    /** 工资级别 */
    @Excel(name = "工资级别")
    private String salaryScale;

    /** 基础工资 */
    @Excel(name = "基础工资")
    private BigDecimal baseSalary;

    /*是否在职 1在职 2在职*/
    @Excel(name = "是否在职")
    private Integer status;

    /* 教师电话*/
    @Excel(name = "教师电话")
    private String teacherPhone;

    /*离职日期*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaveDate;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

    /** 用户账号 */
    private String userName;

    /** 用户昵称 */
    private String nickName;

    /** 密码 */
    private String password;

}
