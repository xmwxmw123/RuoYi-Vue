package com.ruoyi.campus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 社团计划
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpClubPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 社团id */
    private Long clubId;

    /** 计划名称 */
    private String planName;

    /** 活动开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /** 活动结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /** 活动描述 */
    private String remake;
}
