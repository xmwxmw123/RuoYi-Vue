package com.ruoyi.campus.domain.exportVo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 *  导出表
 */
@Data
public class ExportSemesters {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 学期 */

    @Excel(name = "学期" )
    private String semester;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long orders;


    @Excel(name = "当前学期")
    private String currentSemester ;



    /** 创建者 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间")
    private String createTime;

    /** 更新者 */
    @Excel(name = "修改人")
    private String updateBy;


    /** 更新时间 */
    @Excel(name = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String updateTime;


}
