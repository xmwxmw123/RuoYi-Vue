package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 课表查询对象 cp_timetable
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpTimetable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    private Long timetableSet;

    /** 学科id */
    private Long subjectId;

    /** 老师id */
    private Long staffId;

    @Excel(name = "学期")
    private String semesters;

    @Excel(name = "级别")
    private String rankS;

    @Excel(name = "年级")
    private String grade;//年级

    @Excel(name = "学科")
    private String subjectName;

    @Excel(name = "班级")
    private String className;

    @Excel(name = "老师")
    private String staffName;

    /** 节数 */
    @Excel(name = "节数")
    private String pitchNumber;

    /** 星期 */
    @Excel(name = "星期")
    private String week;

    /** 部门id */
    private Long deptId;

    /** 用户id */
    private Long userId;

    private String period;//届别id

    private Integer classId;//班级id

    private Long semestersId;//学期id
}
