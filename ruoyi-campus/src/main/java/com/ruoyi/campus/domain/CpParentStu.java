package com.ruoyi.campus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CpParentStu {

    private Long id;

    /** 家长id */
    private Long parentId;

    /** 学籍id */
    private Long registerId;

    /** 家长称谓 */
    private String appellation;

    /** 审核状态:0-待审核, 1-通过, 2-未通过 */
    private String auditStatus;
}
