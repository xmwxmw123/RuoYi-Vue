package com.ruoyi.campus.domain.exportVo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class ExportActivity extends BaseEntity {

    private Long id;

    /** 活动类型 */
    @Excel(name = "活动类型")
    private String activityType;

    /* 主题 */
    @Excel(name = "主题")
    private String topic;

    /** 活动项目 */
    @Excel(name = "活动项目")
    private String activityItems;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
    private Date endTime;

    /** 届别id */
    private String period;

    /** 届别名称 */
    @Excel(name = "届别名称")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /**班级id*/
    private Long classId;

    @Excel(name = "班级名称")
    private String className;

    /** 发放时间 */
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:dd")
    // private String releaseTime;
    private Date releaseTime;

    /** 作品个数 */
    @Excel(name = "作品个数")
    private Long wokesNumber;

    /** 获得 */
    @Excel(name = "获得奖章类型")
    private String obtain;

    /** 获奖个数 */
    @Excel(name = "获奖个数")
    private Long obtainNumber;

    /*状态 1到期 2 未到期*/
    @Excel(name = "状态")
    private String status;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

}
