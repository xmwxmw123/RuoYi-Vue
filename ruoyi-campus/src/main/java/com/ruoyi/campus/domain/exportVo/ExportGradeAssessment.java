
package com.ruoyi.campus.domain.exportVo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExportGradeAssessment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long id;

    /** 学期 */
    @Excel(name = "学期")
    private String semesters;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 总分 */
    @Excel(name = "总分")
    private String score;

    /** 总评 */
    @Excel(name = "总评")
    private String overallReview;

    /** 身高 */
    @Excel(name = "身高")
    private String height;

    /** 体重 */
    @Excel(name = "体重")
    private String weight;

    /** 结果 */
    @Excel(name = "结果")
    private String outcome;

    /** 得分 */
    @Excel(name = "得分")
    private String scoring;

    /** 等级 */
    @Excel(name = "等级")
    private String dj;

    /** 学期id */
    private Long semestersId;

    /** 学籍id */

    private Long registerId;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级名称id */
    private String classId;


//    /** 创建者 */
//    @Excel(name = "创建人")
//    private String createBy;
//
//    /** 创建时间 */
//    @Excel(name = "创建时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private String createTime;
//
//    /** 更新者 */
//    @Excel(name = "修改人")
//    private String updateBy;
//
//    /** 更新时间 */
//    @Excel(name = "修改时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private String updateTime;


}

