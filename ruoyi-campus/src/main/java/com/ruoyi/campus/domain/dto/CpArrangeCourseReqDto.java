package com.ruoyi.campus.domain.dto;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpArrangeCourseReqDto {

    /** 学科id */
    private Long subjectId;

    /** 老师id */
    private Long staffId;

    /** 节数 */
    private String pitchNumber;

    /** 星期 */
    private String week;
}
