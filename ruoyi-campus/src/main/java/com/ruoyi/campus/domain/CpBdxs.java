package com.ruoyi.campus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpBdxs {

    //家长绑定学生表主键
    private Long id;

    //家长账号表主键
    private Long parentId;

    //学籍id
    private Long registerId;

    //学号
    private String studentNum;

    //学生姓名
    private String studentName;

    //年级
    private String grade;

    //班级id
    private String classId;

    //班级名称
    private String className;

    //称谓
    private String appellation;

    //审核状态
    private String auditStatus;
}
