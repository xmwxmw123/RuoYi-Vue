package com.ruoyi.campus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 评价模板对象 cp_template
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 评价模板名称 */
    @Excel(name = "评价模板名称")
    private String templateName;

    /** 模板 */
    private String template;

//    private List<String> indexS;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createBy;

    /** 账号是否启用 */
    @Excel(name = "是否启用", readConverterExp = "true=启用,false=停用")
    private Boolean enable;

    /** 状态:1录入中,2录入成功, 3开始录入 */
    @Excel(name = "状态", readConverterExp = "1=录入中,2=录入成功,3=开始录入")
    private String status;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;

}
