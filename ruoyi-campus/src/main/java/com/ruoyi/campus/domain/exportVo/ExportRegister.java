package com.ruoyi.campus.domain.exportVo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class ExportRegister {
    private static final long serialVersionUID = 1L;

    /** 学籍id */
    private Long id;

    /** 届别id */
    private String period;

    /** 届别 */
    @Excel(name = "级别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 统编号 */
    private String serialNum;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private String birthday;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;

    /** 生源类别 */
    @Excel(name = "生源类别")
    private String source;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 最后修改人 */
    @Excel(name = "修改人")
    private String updateUser;

    /** 创建时间 */
    @Excel(name = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /** 更新时间 */
    @Excel(name = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String updateTime;



    /**部门id*/
    private Long deptId;


}
