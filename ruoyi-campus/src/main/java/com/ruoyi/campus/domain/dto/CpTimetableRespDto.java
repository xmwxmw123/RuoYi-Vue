package com.ruoyi.campus.domain.dto;

import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 排课入参
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpTimetableRespDto
{
    /** 任课设置id */
    private Long id;

    /** 教师任课设置, 数据结构: [{"subject_id":1,"subject_name":"语文","teacher_id":2,"teacher_name":"刘老师"}] */
    @Excel(name = "教师任课设置")
    private String staffSetText;

    //学期名
    private String semesters;

    //届别名
    private String rankS;

    //班级名
    private String className;

    /** 年级 */
    private String grade;

    /** 已排课对象*/
    private List<CpArrangeCourseReqDto> arrangeCourse;
}
