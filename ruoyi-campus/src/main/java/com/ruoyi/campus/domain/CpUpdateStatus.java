package com.ruoyi.campus.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置学生状态对象
 * 
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CpUpdateStatus extends BaseEntity {

    /**
     * 届别名
     */
    private String rankS;

    /**
     * 状态为1则表示在校,用于修改学籍表里的学生在校状态
     */
    private String notGraduate = "1";

    /**
     * 状态为3则表示毕业,用于修改学籍表里的学生毕业状态
     */
    private String graduate = "3";
}

