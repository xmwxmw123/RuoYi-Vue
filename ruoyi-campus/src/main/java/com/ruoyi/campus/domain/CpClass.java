package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置班级对象 cp_class
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CpClass extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 班级号 */
    @Excel(name = "班级号")
    private String classNumber;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 班主任 */
    @Excel(name = "班主任")
    private String teacher;

    /** 届别id*/
    private long period;

    /** 老师id */
    private int teacherId;

    /**是否毕业*/
    private String graduate;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;


}
