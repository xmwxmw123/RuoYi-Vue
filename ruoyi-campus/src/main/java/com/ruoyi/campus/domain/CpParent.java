package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 家长账号对象 cp_parent
 * 
 * @author ruoyi
 * @date 2023-09-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpParent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 家长姓名 */
    @Excel(name = "家长姓名")
    private String parentName;

    /** 电话 */
    @Excel(name = "登录账号")
    private String parentPhone;

    /**
     * 登录密码
     */
    private String password;

    /** 账号状态:0正常, 1禁用 */
    @Excel(name = "账号状态")
    private String usableStatus;

    /** 绑定学生, 数据格式为:"[{register_id: 10,appellation: 爷爷},{register_id: 20,appellation: 奶奶}]"*/
//    private List<HashMap<String, Object>> bdxs;
    private String bdxs;

    private String studentNum;
    private String classId;
    private String grade;
    private String studentName;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;

    /** 来源: 1-后台, 2-家长移动端 */
    private Long source;

    /** 头像 */
    private String head;
}
