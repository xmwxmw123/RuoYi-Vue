package com.ruoyi.campus.domain.exportVo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 *  导出表
 */
@Data
public class ExportGrade {

    private static final long serialVersionUID = 1L;
    /** $column.columnComment */
    private Long id;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 当前班级 */
    @Excel(name = "当前年级")
    private String currentClass;

    /** 是否毕业 */
    @Excel(name = "是否毕业")
    private String graduate;

    /** 班级数 */
    @Excel(name = "班级数")
    private Long classNumber;

    /** 创建者 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间")
    private String createTime;

    /** 更新者 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 更新时间 */
    @Excel(name = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String updateTime;


}
