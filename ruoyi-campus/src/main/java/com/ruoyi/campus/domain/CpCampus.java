package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置校园对象 cp_campus
 * 
 * @author ruoyi
 * @date 2023-09-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpCampus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 校园图片 */
    @Excel(name = "校园图片")
    private String picture;

    /** 校园名称 */
    @Excel(name = "校园名称")
    private String campusName;

    /** 校园简介 */
    @Excel(name = "校园简介")
    private String introduce;

    /** 校园地址 */
    @Excel(name = "校园地址")
    private String site;

    /** 咨询电话 */
    @Excel(name = "咨询电话")
    private String hotline;

    /** 校园事迹 */
    @Excel(name = "校园事迹")
    private String deed;

    /** 校园师资 */
    @Excel(name = "校园师资")
    private String teachers;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;


}
