package com.ruoyi.campus.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 社团管理对象 cp_club
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpClub extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 社团名称 */
    private String clubName;

    /** 年级 */
    private String grade;

    /** 社团学生(学生id数组) */
    private String clubStudent;

    /** 活动数量 */
    private Integer activityNum;

    /** 参加人数 */
    private Integer attendPeopleNum;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;

    /** 社团活动计划 格式：[{id:2,"activityName":"打的","startTime":"2023-10-10T16:00:00.000Z","endTime":"2023-11-14T16:00:00.000Z","desc":"15"}]*/
    private String cpClubPlans;

    /** 当前社团下的社团活动 **/
    private List<CpClubActivity> cpClubActivities;

    /** 当前社团下的报名记录 **/
    private List<CpClubEnroll> cpClubEnrolls;
}
