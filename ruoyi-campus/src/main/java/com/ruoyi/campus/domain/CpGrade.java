package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

/**
 * 设置届别对象 cp_grade
 * 
 * @author ruoyi
 * @date 2023-07-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpGrade extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 届别 */
    @Excel(name = "届别")
    @Size(min = 5, max = 5, message = "级别格式错误,举例正确格式为:2016级")
    private String rankS;

    /** 当前班级 */
    @Excel(name = "当前年级")
    private String currentClass;

    /** 是否毕业 */
    @Excel(name = "是否毕业")
    private String graduate;

    /** 班级数 */
    @Excel(name = "班级数")
    private Long classNumber;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
