package com.ruoyi.campus.domain.exportVo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 *  导出表
 */
@Data
public class ExportClass {

    private static final long serialVersionUID = 1L;

    private Long id;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 班级号 */
    @Excel(name = "班级号")
    private String classNumber;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 班主任 */
    @Excel(name = "班主任")
    private String teacher;

    /* 届别id*/
    private long period;

    /* 老师id */
    private long teacherId;

    /** 创建者 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间")
    private String createTime;

    /** 更新者 */
    @Excel(name = "修改人")
    private String updateBy;


    /** 更新时间 */
    @Excel(name = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String updateTime;


}
