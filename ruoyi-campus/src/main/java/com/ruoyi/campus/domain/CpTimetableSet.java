package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 教师任课设置对象 cp_timetable_set
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpTimetableSet extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 学期id */
    @Excel(name = "学期id")
    private Long semestersId;

    //学期名
    private String semesters;

    /** 届别id */
    @Excel(name = "届别id")
    private String period;

    //届别名
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级id */
    @Excel(name = "班级id")
    private Long classId;

    //班级名
    private String className;

    /** 教师任课设置, 数据结构: [{"subject_id":1,"subject_name":"语文","teacher_id":2,"teacher_name":"刘老师"}] */
    @Excel(name = "教师任课设置")
    private String staffSetText;

    //状态 0-未排课 1-已排课
    private Integer status;

    /** 部门id */
    private Long deptId;

    /** 用户id */
    private Long userId;


}
