package com.ruoyi.campus.domain.exportVo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 *  导出表
 */
@Data
public class ExportStaffProfile extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private String numbering;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 出生年月 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生年月", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataBirth;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 身高 */
    @Excel(name = "身高")
    private String  height;

    /** 血型 */
    @Excel(name = "血型")
    private String bloodType;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idNumber;

    //邮箱
    @Excel(name = "邮箱")
    private String email;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String origin;

    /** 所学专业 */
    @Excel(name = "所学专业")
    private String majorsStudied;

    /** 毕业时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "毕业时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date graduationTime;

    /** 最高学历 */
    @Excel(name = "最高学历")
    private String highestEducation;


    /** 评定时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "评定时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date assessmentTime;

    /** 毕业院校 */
    @Excel(name = "毕业院校")
    private String graduateSchool;

    /** 聘任职务 */
    @Excel(name = "聘任职务")
    private String appointmentPosition;

    /** 聘任时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "聘任时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lengthEmployment;


    /*是否在职 1在职 2在职*/
    @Excel(name = "是否在职")
    private String status;

    /* 教师电话*/
    @Excel(name = "电话号码")
    private String teacherPhone;

    /*离职日期*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaveDate;



}
