package com.ruoyi.campus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 社团活动对象 cp_club_activity
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpClubActivity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 社团活动id */
    private Long id;

    /** 活动名称 */
    @Excel(name = "活动名称")
    private String activityName;

    /** 社团id */
    private Long clubId;

    /** 社团名称 */
    @Excel(name = "社团名称")
    private String clubName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /** 活动简介 */
    @Excel(name = "活动简介")
    private String introduction;

    /** 活动图片 */
    @Excel(name = "活动图片")
    private String activityPicture;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;

    //活动计划id
    private Long planId;
    //活动计划名称
    private String planName;
}
