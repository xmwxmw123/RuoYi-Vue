package com.ruoyi.campus.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置学科对象 cp_subject
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpSubject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学科id */
    private Long id;

    /** 学科名 */
    @Excel(name = "学科名")
    private String subject;

    /** 绑定的年级 */
    private String[] grade;

    private String grades;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long orders;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createUser;

    /** 最后修改人 */
    private String updateUser;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;


}
