package com.ruoyi.campus.StuEvaluation.controller;

import com.github.pagehelper.PageInfo;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreInquiry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryDto;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.campus.StuEvaluation.service.ICpScoreInquiryService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 成绩查询(用于存放各个班级里学生的成绩)Controller
 * 
 * @author ruoyi
 * @date 2023-08-14
 */
@RestController
@RequestMapping("/scoreInquiry/scoreInquiry")
public class CpScoreInquiryController extends BaseController
{
    @Autowired
    private ICpScoreInquiryService cpScoreInquiryService;

    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)列表
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpScoreInquiry cpScoreInquiry)
    {
        startPage();
        List<CpScoreInquiry> list = cpScoreInquiryService.selectCpScoreInquiryList(cpScoreInquiry);
        long total = new PageInfo(list).getTotal();
        List<CpScoreInquiryDto> scoreList = cpScoreInquiryService.handleCpScoreList(cpScoreInquiry, list);
        return getDataTable(scoreList, total);
    }

    /**
     * 导出成绩查询(用于存放各个班级里学生的成绩)列表
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:export')")
    @Log(title = "成绩查询(用于存放各个班级里学生的成绩)", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpScoreInquiryDto cpScoreInquiry)
    {
        List<CpScoreInquiryDto> list = cpScoreInquiryService.export(cpScoreInquiry);
        ExcelUtil<CpScoreInquiryDto> util = new ExcelUtil<CpScoreInquiryDto>(CpScoreInquiryDto.class);
        util.exportExcel(response, list, "成绩查询(用于存放各个班级里学生的成绩)数据");
    }

    /**
     * 获取成绩查询(用于存放各个班级里学生的成绩)详细信息
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpScoreInquiryService.selectCpScoreInquiryById(id));
    }

    /**
     * 新增成绩查询(用于存放各个班级里学生的成绩)
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:add')")
    @Log(title = "成绩查询(用于存放各个班级里学生的成绩)", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpScoreInquiry cpScoreInquiry)
    {

        return toAjax(cpScoreInquiryService.insertCpScoreInquiry(cpScoreInquiry));
    }

    /**
     * 修改成绩查询(用于存放各个班级里学生的成绩)
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:edit')")
    @Log(title = "成绩查询(用于存放各个班级里学生的成绩)", businessType = BusinessType.UPDATE)
    @PutMapping("update")
    public AjaxResult edit(@RequestBody CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto)
    {
        return AjaxResult.success(cpScoreInquiryService.updateCpScoreInquiry(cpScoreInquiryUpdateDto));
    }

    /**
     * 删除成绩查询(用于存放各个班级里学生的成绩)
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:remove')")
    @Log(title = "成绩查询(用于存放各个班级里学生的成绩)", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpScoreInquiryService.deleteCpScoreInquiryByIds(ids));
    }

    /**
     *  开始录入接口
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:add')")
    @GetMapping("/add")
    public AjaxResult addScoreInquiry(CpScoreInquiry cpScoreInquiry)
    {
        startPage();
        cpScoreInquiry.setUserId(getUserId());
        cpScoreInquiry.setDeptId(getDeptId());
        return AjaxResult.success(cpScoreInquiryService.insertScoreInquiry(cpScoreInquiry));
    }

    /**
     * 结束保存接口
     */
    @PreAuthorize("@ss.hasPermi('scoreInquiry:scoreInquiry:query')")
    @PutMapping("updateStatus")
    public AjaxResult updateStatus(@RequestParam("id") Long id)
    {
        return AjaxResult.success(cpScoreInquiryService.updateStatus(id));
    }

}
