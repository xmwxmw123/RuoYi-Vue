package com.ruoyi.campus.StuEvaluation.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 考试管理对象 cp_examination_management
 * 
 * @author ruoyi
 * @date 2023-08-13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpExaminationManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 考试管理表id*/
    private Long id;

    /** 考试名称 */
    @Excel(name = "考试名称")
    private String examinationName;

    //学期id
    private Long semestersId;
    //学期
    private String semesters;

    /** 开始时间*/
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间 ", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 成绩操作 */
    private String operation;

    /** 创建人 */
    private String createUser;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
