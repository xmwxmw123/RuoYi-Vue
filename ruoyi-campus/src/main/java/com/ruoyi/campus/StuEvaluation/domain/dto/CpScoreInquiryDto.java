package com.ruoyi.campus.StuEvaluation.domain.dto;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpScoreInquiryDto extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long scoreEntryId;
    private Long registerId;
    //考试名称
    @Excel(name = "考试名称")
    private String examinationName;
    //考试id
    private int examinationNameId;
    //学期id
    private long semestersId;
    //学期
    @Excel(name = "学期")
    private String semesters;
    //学号
    @Excel(name = "学号")
    private String studentNum;
    //学生
    @Excel(name = "学生")
    private String StudentName;
    //级别
    @Excel(name = "级别")
    private String rankS;
    //年级
    @Excel(name = "年级")
    private String grade;
    //班级名称
    @Excel(name = "班级名称")
    private String className;
    private Long subjectId;
    //学科
    @Excel(name = "学科")
    private String subject;
    //学科分数
    @Excel(name = "学科分数")
    private Double subjectScore;
    //班级排名
    @Excel(name = "班级排名")
    private int classRanking;
    //年级排名
    @Excel(name = "年级排名")
    private int gradeRanking;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
