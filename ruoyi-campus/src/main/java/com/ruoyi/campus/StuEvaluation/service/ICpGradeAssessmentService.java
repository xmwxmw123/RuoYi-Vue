package com.ruoyi.campus.StuEvaluation.service;


import com.ruoyi.campus.StuEvaluation.domain.CpGradeAssessment;
import com.ruoyi.campus.domain.exportVo.ExportGradeAssessment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 体质测评Service接口
 * 
 * @author ruoyi
 * @date 2023-08-07
 */
public interface ICpGradeAssessmentService 
{
    /**
     * 查询体质测评
     * 
     * @param id 体质测评主键
     * @return 体质测评
     */
    public CpGradeAssessment selectCpGradeAssessmentById(Long id);

    /**
     * 查询体质测评列表
     * 
     * @param cpGradeAssessment 体质测评
     * @return 体质测评集合
     */
    public List<CpGradeAssessment> selectCpGradeAssessmentList(CpGradeAssessment cpGradeAssessment);

    /**
     * 新增体质测评
     * 
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    public int  insertCpGradeAssessment(CpGradeAssessment cpGradeAssessment);

    /**
     * 修改体质测评
     * 
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    public int updateCpGradeAssessment(CpGradeAssessment cpGradeAssessment);

    /**
     * 批量删除体质测评
     * 
     * @param ids 需要删除的体质测评主键集合
     * @return 结果
     */
    public int deleteCpGradeAssessmentByIds(Long[] ids);

    /**
     * 删除体质测评信息
     * 
     * @param id 体质测评主键
     * @return 结果
     */
    public int deleteCpGradeAssessmentById(Long id);


    /**
     *  批量新增
     * @param cpGradeAssessment
     * @return
     */
    String batchAdd( CpGradeAssessment cpGradeAssessment);

    /* 导出列表*/
    List<ExportGradeAssessment> exportSelectCpGradeAssessmentList(ExportGradeAssessment exportGradeAssessment);

}
