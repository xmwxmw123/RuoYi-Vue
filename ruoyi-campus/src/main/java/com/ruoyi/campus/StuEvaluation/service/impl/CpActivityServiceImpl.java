package com.ruoyi.campus.StuEvaluation.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityPartakeMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityService;
import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.exportVo.ExportActivity;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpGradeMapper;
import com.ruoyi.campus.mapper.CpStaffProfileMapper;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 活动列表Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-09
 */
@Service
public class CpActivityServiceImpl implements ICpActivityService {
    @Resource
    private CpActivityMapper cpActivityMapper;
    @Resource
    private CpGradeMapper cpGradeMapper;
    @Resource
    private CpClassMapper cpClassMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    @Resource
    private CpActivityPartakeMapper cpActivityPartakeMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;

    /**
     * 查询活动列表
     *
     * @param id 活动列表主键
     * @return 活动列表
     */
    @Override
    public CpActivity selectCpActivityById(Long id) {
        CpActivity cpActivity = cpActivityMapper.selectCpActivityById(id);
        if (Objects.nonNull(cpActivity)) {
            CpGrade cpGrade = cpGradeMapper.selectById(Long.valueOf(cpActivity.getPeriod()));
            if (Objects.nonNull(cpGrade)) {
                cpActivity.setRankS(cpGrade.getRankS());
            }
            CpClass cpClass = cpClassMapper.selectCpClassById(cpActivity.getClassId());
            if (Objects.nonNull(cpClass)) {
                cpActivity.setClassName(cpClass.getClassName());
            }
        }
        return cpActivity;
    }

    /**
     * 查询活动列表教师APP
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpActivity> appList(CpActivity cpActivity) {
        //用userid获取教职工表的id
        Long tercherId = cpStaffProfileMapper.selectCpStaffProfileByUserId(cpActivity.getUserId());
        cpActivity.setTercherId(tercherId);
        return cpActivityMapper.appList(cpActivity);
    }

    /**
     * 查询活动列表家长端APP
     */
    @Override
    public List<CpActivity> parentAppPracticeList(CpActivity cpActivity) {
        //创建返回集合
        return cpActivityMapper.parentAppPracticeList(cpActivity);
    }

    /**
     * 查询活动列表列表PC
     *
     * @param cpActivity 活动列表
     * @return 活动列表
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpActivity> selectCpActivityList(CpActivity cpActivity) {
        //创建返回集合
        return cpActivityMapper.selectCpActivityList(cpActivity);
    }

    public List<CpActivity> selectCpActivityList(List<CpActivity> cpActivityList) {
        if (null != cpActivityList && !cpActivityList.isEmpty()) {
            for (CpActivity activity : cpActivityList) {
                CpGrade cpGrade = cpGradeMapper.selectById(Long.valueOf(activity.getPeriod()));
                if (Objects.nonNull(cpGrade)) {
                    activity.setRankS(cpGrade.getRankS());
                }
                CpClass cpClass = cpClassMapper.selectCpClassById(activity.getClassId());
                if (Objects.nonNull(cpClass)) {
                    activity.setClassName(cpClass.getClassName());
                }
                //获取用户头像(app需要)
                String avatar = sysUserMapper.selectUserAvatar(activity.getCreateBy(), activity.getDeptId());
                activity.setAvatar(avatar);
            }
            return cpActivityList.stream().sorted(Comparator.comparing(CpActivity::getStartTime)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    /**
     * 新增接口
     *
     * @param cpActivity 活动列表
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertCpActivity(CpActivity cpActivity) {
        // 判断参数合法性
        String message = checkIfEmpty(cpActivity);
        if (message != null) {
            throw new GlobalException(message);
        }
        if (!isTopicUnique(cpActivity.getTopic(), cpActivity.getDeptId())) {
            throw new GlobalException("主题不能重复");
        }
        JSONArray jsonArray = new JSONArray(cpActivity.getSyfw());
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONArray classArrJarr = jsonObject.getJSONArray("classId");
            for (int j = 0; j < classArrJarr.size(); j++) {
                CpActivity insDto = new CpActivity();
                BeanUtils.copyProperties(cpActivity, insDto);
                insDto.setPeriod(jsonObject.getStr("period"));
                insDto.setGrade(jsonObject.getStr("currentClass"));
                insDto.setClassId(classArrJarr.getLong(j));
                String username = SecurityUtils.getUsername();
                insDto.setCreateBy(username);
                insDto.setCreateTime(DateUtils.getNowDate());
                cpActivityMapper.insertCpActivity(insDto);

                //把班级下的学生同步到活动参与情况表
                Long classId = classArrJarr.getLong(j);
                List<CpRegister> cpRegisters = cpRegisterMapper.selectClassName(classId.toString(), cpActivity.getDeptId());
                if (cpRegisters.isEmpty()) {
                    throw new GlobalException("当前有选中的班级没有学生");
                }
                for (CpRegister cpRegister : cpRegisters) {
                    CpActivityPartake cpActivityPartake = new CpActivityPartake();
                    cpActivityPartake.setActivityId(insDto.getId());
                    cpActivityPartake.setRegisterId(cpRegister.getId());
                    cpActivityPartake.setPeriod(cpRegister.getPeriod());
                    cpActivityPartake.setRankS(cpRegister.getRankS());
                    cpActivityPartake.setGrade(cpRegister.getGrade());
                    cpActivityPartake.setClassId(cpRegister.getClassId());
                    cpActivityPartake.setClassName(cpRegister.getClassName());
                    cpActivityPartake.setCreateTime(new Date());
                    cpActivityPartake.setCreateBy(SecurityUtils.getUsername());
                    cpActivityPartake.setStatus("1");//设置状态为待审
                    cpActivityPartake.setUserId(cpActivity.getUserId());
                    cpActivityPartake.setDeptId(cpActivity.getDeptId());

                    cpActivityPartakeMapper.insertCpActivityPartake(cpActivityPartake);
                }
            }
        }
        return 1;
    }

    // 判断主题是否唯一
    public boolean isTopicUnique(String topic, Long deptId) {
        // 调用数据访问层方法，查询数据库中是否存在相同主题的活动
        int count = cpActivityMapper.countCpActivityByTopic(topic, deptId);
        return count == 0; // 如果计数为0，则表示主题唯一
    }

    // 判断参数合法性
    public String checkIfEmpty(CpActivity cpActivity) {
        if (cpActivity.getActivityType() == null || cpActivity.getActivityType().isEmpty()) {
            return "活动类型不能为空";
        }
        if (cpActivity.getTopic() == null || cpActivity.getTopic().isEmpty()) {
            return "主题不能为空";
        }
        if (cpActivity.getActivityItems() == null || cpActivity.getActivityItems().isEmpty()) {
            return "活动项目不能为空";
        }
        if (cpActivity.getWokesNumber() == null) {
            return "上传作品不能为空";
        }
        if (cpActivity.getObtain() == null) {
            return "获奖类型不能为空";
        }
        if (cpActivity.getObtainNumber() == null) {
            return "获奖个数不能为空";
        }
        // 获取活动开始时间、结束时间和当前时间
        LocalDateTime startTime = cpActivity.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime endTime = cpActivity.getEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime currentDate = LocalDateTime.now();

        // 判断活动是否已经开始
        if (startTime.isBefore(currentDate)) {
            return "活动开始时间小于当前时间";
        }
        // 判断活动结束时间是否早于开始时间
        if (endTime.isBefore(startTime)) {
            return "活动结束时间小于开始时间";
        }
        return null;
    }

    /**
     * 修改活动列表
     *
     * @param cpActivity 活动列表
     * @return 结果
     */
    @Override
    public int updateCpActivity(CpActivity cpActivity) {
        String username = SecurityUtils.getUsername();
        cpActivity.setUpdateBy(username);
        cpActivity.setUpdateTime(DateUtils.getNowDate());
        return cpActivityMapper.updateCpActivity(cpActivity);
    }

    /**
     * 批量删除活动列表
     *
     * @param ids 需要删除的活动列表主键
     * @return 结果
     */
    @Override
    public int deleteCpActivityByIds(Long[] ids) {
        for (Long id : ids) {
            this.deleteCpActivityById(id);
        }
        return 1;
    }

    /**
     * 删除活动列表信息
     *
     * @param id 活动列表主键
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteCpActivityById(Long id) {
        CpActivity cpActivity = cpActivityMapper.selectCpActivityById(id);
        if (Objects.isNull(cpActivity)) {
            throw new GlobalException("当前活动不存在");
        }
        List<CpActivityPartake> cpActivityPartakes = cpActivityPartakeMapper.getCpActivityPartakeByActivityId(cpActivity.getId(), cpActivity.getDeptId());
        if (CollUtil.isNotEmpty(cpActivityPartakes)) {
            for (CpActivityPartake cpActivityPartake : cpActivityPartakes) {
                cpActivityPartakeMapper.deleteCpActivityPartakeById(cpActivityPartake.getId());
            }
        }
        cpActivityMapper.deleteCpActivityById(id);
        return 1;
    }

    /**
     * 根据届别id查询班级
     *
     * @param period 届别id
     * @return 结果集
     */
    @Override
    public String selectRankById(String period) {
        JSONObject resuJsonObject = new JSONObject();
        CpGrade cpGrade = cpGradeMapper.selectRankById(Long.parseLong(period));
        resuJsonObject.putOpt("currentClass", Objects.nonNull(cpGrade) ? cpGrade.getCurrentClass() : "");//年级
        JSONArray resultJarr = new JSONArray();
        //查询当前界别下所有班级列表
        List<CpClass> cpClasses = cpClassMapper.selectCpClassByPeriod(Long.parseLong(period), cpGrade.getDeptId());
        if (null == cpClasses || cpClasses.isEmpty()) {
            resuJsonObject.putOpt("classArrs", resultJarr);
            return resuJsonObject.toString();
        }
        //遍历活动列表 解析syfw适用范围 遍历适用范围
        for (CpClass cpClass : cpClasses) {
            JSONObject classArrJsom = new JSONObject();
            classArrJsom.putOpt("classId", cpClass.getId());
            classArrJsom.putOpt("className", cpClass.getClassName());
            resultJarr.add(classArrJsom);
        }
        resuJsonObject.putOpt("classArrs", resultJarr);
        return resuJsonObject.toString();
    }

    /**
     * 导出活动列表列表
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<ExportActivity> exportCpActivityList(ExportActivity exportActivity) {
        List<ExportActivity> activityList = cpActivityMapper.exportSelectCpActivityList(exportActivity);
        if (activityList.isEmpty()) {
            throw new GlobalException("没有数据");
        }
        for (ExportActivity activity : activityList) {

            /* 1 到期  其他未到期*/
            activityList.forEach(
                    act -> activity.setStatus
                            ("1".equals(act.getStatus()) ? "到期" : "未到期"));

            CpGrade cpGrade = cpGradeMapper.selectById(Long.valueOf(activity.getPeriod()));//届别
            if (Objects.nonNull(cpGrade)) {
                activity.setRankS(cpGrade.getRankS());
            }
            CpClass cpClass = cpClassMapper.selectCpClassById(activity.getClassId());//班级
            if (Objects.nonNull(cpClass)) {
                activity.setClassName(cpClass.getClassName());
            }
            //年级
            if (activity.getGrade() != null) {
                String label = dictDataMapper.selectDictLabel("grade_list", activity.getGrade());
                activity.setGrade(label);
            }
            activity.setReleaseTime(activity.getCreateTime());//发布时间
            //活动类型
            if (activity.getActivityType() != null) {
                String label = dictDataMapper.selectDictLabel("activity_type", activity.getActivityType());
                activity.setActivityType(label);
            }

            //活动项目    使用replaceAll()函数去掉<p>标签
            String activityItems = activity.getActivityItems();
            String cleanedText = activityItems.replaceAll("<p>", "").replaceAll("</p>", "");
            activity.setActivityItems(cleanedText);

            //表扬章奖章类型
            if (activity.getObtain() != null) {
                String label = dictDataMapper.selectDictLabel("praise_medal_type", activity.getObtain());
                activity.setObtain(label);
            }
        }

        return activityList;
    }
}
