package com.ruoyi.campus.StuEvaluation.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 奖章记录对象 cp_medal_record
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpMedalRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 奖章记录表id */
    private Long id;

    /** 学期id */
    private Long semestersId;

    /** 学期 */
    @Excel(name = "学期")
    private String semesters;

    /** 届别id */
    private String period;

    /** 界别 */
    @Excel(name = "界别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级id */
    private String classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String name;

    /** A奖章数量 */
    private int medalNumA;

    /** B奖章数量 */
    private int medalNumB;

    /** C奖章数量 */
    private int medalNumC;

    /** D奖章数量 */
    private int medalNumD;

    /** E奖章数量 */
    private int medalNumE;

    /** 奖章总数量 */
    private int medalTotal;

    /** 奖章id */
    private int medalId;

    /** 奖章名称 */
    @Excel(name = "奖章名称")
    private String medalName;

    /** 奖章分类 */
    @Excel(name = "奖章分类")
    private String medalClassification;

    /** 奖章来源 */
    @Excel(name = "奖章来源")
    private String medalSource;

    /** 发放人 */
    @Excel(name = "发放人")
    private String createUser;

    /**撤销字段,0表示未撤销,1表示已撤销 */
    private int revocation;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

    //标识:1=A, 2=B, 3=C, 4=D, 5=E
    private Long identification;

    //奖章枚举
    public enum MedalEnum{

        A("品德发展水平"),

        B("学业发展水平"),

        C("身心发展水平"),

        D("审美素养提升"),

        E("劳动实践活动");

        private String value;
        private MedalEnum(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
}
