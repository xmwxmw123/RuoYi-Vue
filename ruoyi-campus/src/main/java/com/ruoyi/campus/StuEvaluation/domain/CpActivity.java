package com.ruoyi.campus.StuEvaluation.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 活动列表对象 cp_activity
 * 
 * @author ruoyi
 * @date 2023-08-09
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpActivity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 活动类型 */
    @Excel(name = "活动类型")
    private String activityType;

    /** 活动主题 */
    @Excel(name = "活动主题")
    private String topic;

    /** 活动项目 */
    @Excel(name = "活动项目")
    private String activityItems;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 届别id */
    private String period;

    /** 届别名称 */
    @Excel(name = "届别名称")
    private String rankS;

    /** 作品个数 */
    @Excel(name = "作品个数")
    private Long wokesNumber;

    @Excel(name = "获得奖章类型")
    private String obtain;

    /** 获奖个数 */
    @Excel(name = "获奖个数")
    private Long obtainNumber;

    /**年级ID*/
    @Excel(name = "年级")
    private String grade;

    /**班级id*/
    private Long classId;

    @Excel(name = "班级名称")
    private String className;

    /** 适用范围 */
    private String syfw;

    /** 用户头像 */
    private String avatar;

    /** 班主任id */
    private Long tercherId;

    /** 班主任 */
    private String tercherName;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
