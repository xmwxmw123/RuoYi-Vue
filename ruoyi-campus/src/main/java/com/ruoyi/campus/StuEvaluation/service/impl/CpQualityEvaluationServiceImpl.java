package com.ruoyi.campus.StuEvaluation.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.campus.StuEvaluation.domain.CpQualityEvaluation;
import com.ruoyi.campus.StuEvaluation.mapper.CpQualityEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpQualityEvaluationService;
import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportQualityEvaluation;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 综合素质评价Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-02
 */
@Service
public class CpQualityEvaluationServiceImpl implements ICpQualityEvaluationService {
    @Resource
    private CpQualityEvaluationMapper cpQualityEvaluationMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpClassMapper cpClassMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 查询综合素质评价
     *
     * @param id 综合素质评价主键
     * @return 综合素质评价
     */
    @Override
    public CpQualityEvaluation selectCpQualityEvaluationById(Long id) {
        return cpQualityEvaluationMapper.selectCpQualityEvaluationById(id);
    }

    /**
     * 查询综合素质评价列表(PC端和教师APP端)
     *
     * @param cpQualityEvaluation 综合素质评价
     * @return 综合素质评价集合
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpQualityEvaluation> selectCpQualityEvaluationList(CpQualityEvaluation cpQualityEvaluation) {
        return cpQualityEvaluationMapper.selectCpQualityEvaluationList(cpQualityEvaluation);
    }

    /**
     * 查询综合素质评价列表(家长端)
     */
    @Override
    public List<CpQualityEvaluation> parentAppQuyCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation) {
        if (StringUtils.isBlank(cpQualityEvaluation.getStudentNum()) || Objects.isNull(cpQualityEvaluation.getDeptId())) {
            throw new GlobalException("参数错误");
        }
        return cpQualityEvaluationMapper.parentAppQuyCpQualityEvaluation(cpQualityEvaluation);
    }

    /**
     * 新增综合素质评价
     *
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    @Override
    @Transactional
    public int insertCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation) {
        //根据学号查询学生
        CpRegister cpRegister = cpRegisterMapper.selectStudenByNum(cpQualityEvaluation.getStudentNum(), cpQualityEvaluation.getDeptId());
        if (cpRegister == null) {
            throw new BaseException("没有该学号的学生");
        }
        if(CommonEnum.registerStatus.GRADUATE.getValue().equals(cpRegister.getStatus())){
            throw new BaseException("该学生已毕业");
        }else if(CommonEnum.registerStatus.QUIT_SCHOOL.getValue().equals(cpRegister.getStatus())){
            throw new BaseException("该学生已休学");
        }
        //  根据学期id查询学期
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(CommonEnum.currentSemester.CURRENT.getValue(), cpQualityEvaluation.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        int count2 = cpQualityEvaluationMapper.selectStuCount(cpRegister.getStudentNum(), cpRegister.getGrade(), cpRegister.getClassId(), cpRegister.getPeriod(), cpSemesters.getId(), cpQualityEvaluation.getDeptId());
        if (count2 > 0) {
            throw new BaseException("请勿重复添加学生!");
        }else {
            //查询老师信息
            CpClass cpClass = cpClassMapper.selectTeacher(cpRegister.getClassId());
            //查询出来后赋值插入综合素质评价表
            cpQualityEvaluation.setPeriod(cpRegister.getPeriod());
            cpQualityEvaluation.setRankS(cpRegister.getRankS());
            cpQualityEvaluation.setGrade(cpRegister.getGrade());
            cpQualityEvaluation.setClassId(cpRegister.getClassId());
            cpQualityEvaluation.setClassName(cpRegister.getClassName());
            cpQualityEvaluation.setStudentNum(cpRegister.getStudentNum());
            cpQualityEvaluation.setName(cpRegister.getName());
            cpQualityEvaluation.setStatus(CommonEnum.QualityEvaluationStatus.AWAIT.getValue());
            cpQualityEvaluation.setTeacher(cpClass.getTeacher());
            cpQualityEvaluation.setTeacherId(cpClass.getTeacherId());
            cpQualityEvaluation.setSemesters(cpSemesters.getSemester());
            cpQualityEvaluation.setSemestersId(cpQualityEvaluation.getSemestersId());
            //创建人 创建时间
            cpQualityEvaluation.setCreateUser(SecurityUtils.getUsername());
            cpQualityEvaluation.setCreateTime(DateUtils.getNowDate());
            return cpQualityEvaluationMapper.insertCpQualityEvaluation(cpQualityEvaluation);
        }
    }

    /**
     * 修改综合素质评价
     *
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    @Override
    public int updateCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation) {
        cpQualityEvaluation.setUpdateTime(DateUtils.getNowDate());
        cpQualityEvaluation.setUpdateUser(SecurityUtils.getUsername());
        return cpQualityEvaluationMapper.updateCpQualityEvaluation(cpQualityEvaluation);
    }

    /**
     * 更新家长寄语
     */
    @Override
    public void updParentMessage(CpQualityEvaluation cpQualityEvaluation) {
        if (Objects.isNull(cpQualityEvaluation.getId()) || StringUtils.isBlank(cpQualityEvaluation.getParentMessage())) {
            throw new BaseException("参数错误");
        }
        CpQualityEvaluation dto = cpQualityEvaluationMapper.selectCpQualityEvaluationById(cpQualityEvaluation.getId());
        if (Objects.isNull(dto)) {
            throw new BaseException("当前记录不存在");
        }
        dto.setParentMessage(cpQualityEvaluation.getParentMessage());
        cpQualityEvaluationMapper.updateCpQualityEvaluation(dto);
    }

    /**
     * 批量删除综合素质评价
     *
     * @param ids 需要删除的综合素质评价主键
     * @return 结果
     */
    @Override
    public int deleteCpQualityEvaluationByIds(Long[] ids) {
        return cpQualityEvaluationMapper.deleteCpQualityEvaluationByIds(ids);
    }

    /**
     * 删除综合素质评价信息
     *
     * @param id 综合素质评价主键
     * @return 结果
     */
    @Override
    public int deleteCpQualityEvaluationById(Long id) {
        return cpQualityEvaluationMapper.deleteCpQualityEvaluationById(id);
    }

    /**
     * 批量新增
     */
    @Override
    @Transactional
    public String insertCpQualityEvaluationBatchAdd(CpQualityEvaluation cpQualityEvaluation) {
        //  根据学期id查询学期
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(CommonEnum.currentSemester.CURRENT.getValue(), cpQualityEvaluation.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        //根据条件查询出班级里的所有学生
        List<CpRegister> cpRegisters = cpRegisterMapper.selectClassName(cpQualityEvaluation.getClassId(), cpQualityEvaluation.getDeptId());
        if (cpRegisters.isEmpty()) {
            throw new RuntimeException("该班级下没有学生");
        }
        List<CpRegister> addrRegisterList = new ArrayList<>();
        //查询出老师的名称和id
        CpClass cpClass = cpClassMapper.selectTeacher(cpQualityEvaluation.getClassId());
        for (CpRegister cpRegister : cpRegisters) {
            int count2 = cpQualityEvaluationMapper.selectStuCount(cpRegister.getStudentNum(), cpRegister.getGrade(), cpRegister.getClassId(), cpRegister.getPeriod(), cpSemesters.getId(), cpQualityEvaluation.getDeptId());
            if (count2 > 0) {
                continue;
            }
            //状态赋值为1,表示状态为:待点评
            cpRegister.setStatus(CommonEnum.QualityEvaluationStatus.AWAIT.getValue());
            cpRegister.setTeacher(cpClass.getTeacher());
            cpRegister.setTeacherId(cpClass.getTeacherId());
            cpRegister.setCreateUser(SecurityUtils.getUsername());
            cpRegister.setCreateTime(new Date());
            cpRegister.setSemesters(cpSemesters.getSemester());
            cpRegister.setSemestersId(cpQualityEvaluation.getSemestersId());
            cpRegister.setCreateUser(SecurityUtils.getUsername());
            cpRegister.setCreateTime(new Date());
            cpRegister.setUserId(cpQualityEvaluation.getUserId());
            cpRegister.setDeptId(cpQualityEvaluation.getDeptId());
            addrRegisterList.add(cpRegister);
        }
        if (CollUtil.isNotEmpty(addrRegisterList)) {
            cpQualityEvaluationMapper.insertCpQualityEvaluationBatchAdd(addrRegisterList);
        }
        return "操作成功";
    }

    /**
     * 导出
     * @param exportQualityEvaluation
     * @return
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ExportQualityEvaluation> exportCpQualityEvaluationList(ExportQualityEvaluation exportQualityEvaluation) {
        List<ExportQualityEvaluation> cpQualityEvaluations = cpQualityEvaluationMapper.exportCpQualityEvaluationList(exportQualityEvaluation);
        if (cpQualityEvaluations == null) {
            throw new GlobalException("获取质量评价列表失败");
        }
        for (ExportQualityEvaluation cpQualityEvaluation : cpQualityEvaluations) {
            //年级
            if (cpQualityEvaluation.getGrade() != null) {
                String label = dictDataMapper.selectDictLabel("grade_list", cpQualityEvaluation.getGrade());
                cpQualityEvaluation.setGrade(label);
            }
            //德育等级
            if (cpQualityEvaluation.getMoralismGrade() != null) {
                String label = dictDataMapper.selectDictLabel("moral_grade", cpQualityEvaluation.getMoralismGrade());
                cpQualityEvaluation.setMoralismGrade(label);
            }
            //素质评价状态
            if (cpQualityEvaluation.getStatus() != null) {
                String label = dictDataMapper.selectDictLabel("student_evaluation", cpQualityEvaluation.getStatus());
                cpQualityEvaluation.setStatus(label);
            }
        }
        return cpQualityEvaluations;
    }
}
