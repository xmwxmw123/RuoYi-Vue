package com.ruoyi.campus.StuEvaluation.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpScoreAndInquiryEntry {
    //届别id
    private String period;
    //年级
    private String grade;
    //班级id
    private int classId;
    //学科id
    private int subjectId;
    /** 成绩查询表id */
    private Long id;
    //成绩录入表id
    private Long scoreEntryId;
    /** 学籍表id */
    private Long registerId;
    /** 学科成绩 */
    private Double subjectScore;
    //创建时间
    private Date createTime;
    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
