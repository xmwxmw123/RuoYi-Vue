package com.ruoyi.campus.StuEvaluation.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RankSAndClassName {

    //届别id
    private String period;

    //届别
    private String rankS;

    //班级
    private String className;

    //班级id
    private int classId;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
