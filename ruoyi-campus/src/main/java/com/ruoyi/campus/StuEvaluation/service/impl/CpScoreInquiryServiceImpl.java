package com.ruoyi.campus.StuEvaluation.service.impl;

import com.ruoyi.campus.StuEvaluation.domain.CpScoreAndInquiryEntry;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreInquiry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryDto;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.campus.StuEvaluation.domain.dto.ScoreInquiryDto;
import com.ruoyi.campus.StuEvaluation.mapper.CpExaminationManagementMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpScoreInquiryMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpScoreInquiryService;
import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpGradeMapper;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 成绩查询(用于存放各个班级里学生的成绩)Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-14
 */
@Service
public class CpScoreInquiryServiceImpl implements ICpScoreInquiryService {
    @Resource
    private CpScoreInquiryMapper cpScoreInquiryMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpExaminationManagementMapper cpExaminationManagementMapper;
    @Resource
    private CpGradeMapper cpGradeMapper;

    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)
     *
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 成绩查询(用于存放各个班级里学生的成绩)
     */
    @Override
    public CpScoreInquiry selectCpScoreInquiryById(Long id) {
        return cpScoreInquiryMapper.selectCpScoreInquiryById(id);
    }

    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)列表
     *
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 成绩查询(用于存放各个班级里学生的成绩)
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpScoreInquiry> selectCpScoreInquiryList(CpScoreInquiry cpScoreInquiry) {
        return cpScoreInquiryMapper.selectCpScoreInquiryList(cpScoreInquiry);
    }

    /**
     * 分页逻辑
     * @param cpScoreInquiry 参数
     * @param cpScoreInquiryList 集合
     * @return
     */
    @Override
    public List<CpScoreInquiryDto> handleCpScoreList(CpScoreInquiry cpScoreInquiry, List<CpScoreInquiry> cpScoreInquiryList) {
        //创建返回集合
        List<CpScoreInquiryDto> cpScoreInquiryDtos = new ArrayList<>();
        if (null == cpScoreInquiryList || cpScoreInquiryList.isEmpty()) return cpScoreInquiryDtos;

        for (CpScoreInquiry dto : cpScoreInquiryList) {
            CpScoreInquiryDto resultDto = this.achievementDto(dto, dto.getDeptId());
            if (Objects.nonNull(resultDto)) {
                cpScoreInquiryDtos.add(resultDto);
            }
        }
        cpScoreInquiryDtos = cpScoreInquiryDtos.stream().sorted(Comparator.comparing(CpScoreInquiryDto::getCreateTime).reversed()).collect(Collectors.toList());
        return cpScoreInquiryDtos;
    }

    private CpScoreInquiryDto achievementDto(Object obj, Long deptId) {
        //创建返回对象
        CpScoreInquiryDto cpScoreInquiryDto = new CpScoreInquiryDto();
        Long scoreEntryId = null;
        Long registerId = null;
        Double subjectScore = null;
        if (obj instanceof CpScoreInquiry) {
            scoreEntryId = ((CpScoreInquiry) obj).getScoreEntryId();
            registerId = ((CpScoreInquiry) obj).getRegisterId();
            subjectScore = ((CpScoreInquiry) obj).getSubjectScore();
        } else {
            scoreEntryId = ((CpScoreInquiryDto) obj).getScoreEntryId();
            registerId = ((CpScoreInquiryDto) obj).getRegisterId();
            subjectScore = ((CpScoreInquiryDto) obj).getSubjectScore();
        }
        //查询score_entry表获取数据并赋值
        CpScoreEntry cpScoreEntry = cpExaminationManagementMapper.selectpScoreEntry(scoreEntryId, deptId);
        if (cpScoreEntry != null) {
            //查询register表获取数据并赋值
            CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(registerId);
            if (Objects.isNull(cpRegister)) return null;

            cpScoreInquiryDto.setRankS(cpScoreEntry.getRankS());
            cpScoreInquiryDto.setGrade(cpScoreEntry.getGrade());
            cpScoreInquiryDto.setClassName(cpScoreEntry.getClassName());
            cpScoreInquiryDto.setSubject(cpScoreEntry.getSubject());
            cpScoreInquiryDto.setSemesters(cpScoreEntry.getSemesters());
            cpScoreInquiryDto.setSemestersId(cpScoreEntry.getSemestersId());
            cpScoreInquiryDto.setExaminationName(cpScoreEntry.getExaminationName());
            cpScoreInquiryDto.setExaminationNameId(cpScoreEntry.getExaminationManagementId());
            cpScoreInquiryDto.setStudentNum(cpRegister.getStudentNum());
            cpScoreInquiryDto.setStudentName(cpRegister.getName());
            cpScoreInquiryDto.setSubjectScore(subjectScore);
            cpScoreInquiryDto.setCreateTime(cpScoreEntry.getCreateTime());
            //根据届别和年级查询分数记录
            CpScoreEntry params = new CpScoreEntry();
            params.setExaminationManagementId(cpScoreEntry.getExaminationManagementId());
            params.setPeriod(cpScoreEntry.getPeriod());
            params.setGrade(cpScoreEntry.getGrade());
            params.setSubjectId(cpScoreEntry.getSubjectId());
            params.setDeptId(deptId);
            List<CpScoreAndInquiryEntry> cpScoreInquiriesList = cpScoreInquiryMapper.selectClassOrGradeRanking(params);
            if (null != cpScoreInquiriesList && !cpScoreInquiriesList.isEmpty()) {
                //根据分数进行分组处理同分数的情况
                Map<Double, List<CpScoreAndInquiryEntry>> map = new TreeMap<>(cpScoreInquiriesList.stream()
                        .collect(Collectors.groupingBy(CpScoreAndInquiryEntry::getSubjectScore)));
                //处理年级排名
                int ncount = map.size();
                for (Double key : map.keySet()) {
                    for (CpScoreAndInquiryEntry dto : map.get(key)) {
                        if (registerId.equals(dto.getRegisterId())) {
                            cpScoreInquiryDto.setGradeRanking(ncount);//年级排名
                        }
                    }
                    ncount = ncount - 1;
                }
                //处理班级排名
                map = new TreeMap<>(cpScoreInquiriesList.stream().filter(e -> e.getClassId() == cpScoreEntry.getClassId())
                        .collect(Collectors.groupingBy(CpScoreAndInquiryEntry::getSubjectScore)));
                int ccount = map.size();
                for (Double key : map.keySet()) {
                    for (CpScoreAndInquiryEntry dto : map.get(key)) {
                        if (registerId.equals(dto.getRegisterId())) {
                            cpScoreInquiryDto.setClassRanking(ccount);//班级排名
                        }
                    }
                    ccount = ccount - 1;
                }
            }
            //赋值完塞入集合
            return cpScoreInquiryDto;
        }
        return null;
    }

    /**
     * 新增成绩查询(用于存放各个班级里学生的成绩)
     *
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    @Override
    public int insertCpScoreInquiry(CpScoreInquiry cpScoreInquiry) {
        return cpScoreInquiryMapper.insertCpScoreInquiry(cpScoreInquiry);
    }

    /**
     * 修改成绩查询(用于存放各个班级里学生的成绩)
     *
     * @param cpScoreInquiryUpdateDto 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult updateCpScoreInquiry(CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto) {
        CpScoreEntry cpScoreEntry = new CpScoreEntry();
        cpScoreEntry.setId(cpScoreInquiryUpdateDto.getId());
        cpScoreEntry.setStatus(CommonEnum.ScoreEntryStatus.UNDER_WAY.getValue());
        cpExaminationManagementMapper.updateCpScoreEntry(cpScoreEntry);
        //修改学生学科的分数
        List<CpScoreInquiry> cpScoreInquirys = cpScoreInquiryUpdateDto.getArr();
        for (CpScoreInquiry cpScoreInquiry : cpScoreInquirys) {
            cpScoreInquiry.setUpdateTime(new Date());
            cpScoreInquiry.setUpdateBy(SecurityUtils.getUsername());
            cpScoreInquiryMapper.updateCpScoreInquiry(cpScoreInquiry);
        }
        return AjaxResult.success();
    }

    /**
     * 批量删除成绩查询(用于存放各个班级里学生的成绩)
     *
     * @param ids 需要删除的成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 结果
     */
    @Override
    public int deleteCpScoreInquiryByIds(Long[] ids) {
        return cpScoreInquiryMapper.deleteCpScoreInquiryByIds(ids);
    }

    /**
     * 删除成绩查询(用于存放各个班级里学生的成绩)信息
     *
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 结果
     */
    @Override
    public int deleteCpScoreInquiryById(Long id) {
        return cpScoreInquiryMapper.deleteCpScoreInquiryById(id);
    }

    /**
     * 开始录入接口
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @DataScope(deptAlias = "d")
    public AjaxResult insertScoreInquiry(CpScoreInquiry cpScoreInquiry) {
        //查询ScoreEntry表的数据
        CpScoreEntry cpScoreEntry = cpExaminationManagementMapper.selectpScoreEntry(cpScoreInquiry.getScoreEntryId(), cpScoreInquiry.getDeptId());
        //查出班级所有的学生
        List<CpRegister> cpRegisters = cpRegisterMapper.selectStudentByclass(cpScoreInquiry.getGrade(), String.valueOf(cpScoreInquiry.getClassId()), cpScoreInquiry.getDeptId());
        ArrayList<ScoreInquiryDto> result = new ArrayList<>();
        for (CpRegister cpRegister : cpRegisters) {
            if (cpRegister.getGrade().equals(cpScoreEntry.getGrade()) && cpRegister.getClassId().equals(Integer.toString(cpScoreEntry.getClassId()))) {
                cpScoreInquiryMapper.insertCpScoreInquiry(this.assembleScoreInquiry(cpScoreEntry, cpRegister, cpScoreInquiry));
            }
        }
        List<ScoreInquiryDto> scoreInquiryDtos = cpScoreInquiryMapper.selectClassStudent(cpScoreInquiry);
        if(scoreInquiryDtos.isEmpty()){
            throw new GlobalException("学生所在的班级已升级,不可再录入成绩");
        }
        CpGrade cpGrade = cpGradeMapper.selectCurrentClass(cpScoreInquiry.getPeriod(), cpScoreInquiry.getDeptId());
        if(!cpGrade.getCurrentClass().equals(cpScoreInquiry.getGrade())){
            throw new GlobalException("学生所在的班级已升级,不可再录入成绩");
        }
        for (ScoreInquiryDto scoreInquiryDto : scoreInquiryDtos) {
            int count = cpRegisterMapper.selectStuCount(scoreInquiryDto.getGrade(), scoreInquiryDto.getRegisterId(), scoreInquiryDto.getClassId(), scoreInquiryDto.getPeriod(), scoreInquiryDto.getDeptId());
            if(count > 0){
                result.add(scoreInquiryDto);
            }
        }

        return AjaxResult.success(result);
    }

    private CpScoreInquiry assembleScoreInquiry(CpScoreEntry cpScoreEntry, CpRegister cpRegister, CpScoreInquiry cpScoreInquiry) {
        //将学籍id插入到录入表
        CpScoreInquiry cpScoreInquiry1 = new CpScoreInquiry();
        cpScoreInquiry1.setClassId(cpScoreEntry.getClassId());
        cpScoreInquiry1.setRegisterId(cpRegister.getId());
        cpScoreInquiry1.setScoreEntryId(cpScoreInquiry.getScoreEntryId());
        cpScoreInquiry1.setCreateBy(SecurityUtils.getUsername());
        cpScoreInquiry1.setCreateTime(new Date());
        cpScoreInquiry1.setUserId(cpScoreInquiry.getUserId());
        cpScoreInquiry1.setDeptId(cpScoreInquiry.getDeptId());
        return cpScoreInquiry1;
    }

    /**
     * 修改录入状态
     *
     * @param scoreEntryId
     * @return
     */
    @Override
    public AjaxResult updateStatus(Long scoreEntryId) {
        CpScoreEntry cpScoreEntry = new CpScoreEntry();
        cpScoreEntry.setId(scoreEntryId.intValue());
        cpScoreEntry.setStatus(CommonEnum.ScoreEntryStatus.FINISH.getValue());
        cpExaminationManagementMapper.updateCpScoreEntry(cpScoreEntry);
        return AjaxResult.success();
    }

    /**
     * 导出
     *
     * @param cpScoreInquiry
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpScoreInquiryDto> export(CpScoreInquiryDto cpScoreInquiry) {
        List<CpScoreInquiryDto> exportList = cpScoreInquiryMapper.export(cpScoreInquiry);
        if (exportList.isEmpty()) {
            throw new GlobalException("没有数据");
        }
        List<CpScoreInquiryDto> cpScoreInquiryDtos = new ArrayList<>();
        for (CpScoreInquiryDto dto : exportList) {
            CpScoreInquiryDto resultDto = this.achievementDto(dto, dto.getDeptId());
            if (Objects.nonNull(resultDto)) {
                cpScoreInquiryDtos.add(resultDto);
            }
        }
        return cpScoreInquiryDtos;
    }
}
