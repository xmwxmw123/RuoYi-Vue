package com.ruoyi.campus.StuEvaluation.service.impl;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.ruoyi.campus.StuEvaluation.domain.CpExaminationManagement;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import com.ruoyi.campus.StuEvaluation.domain.RankSAndClassName;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.campus.StuEvaluation.mapper.CpExaminationManagementMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpExaminationManagementService;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.CpSubject;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.mapper.CpSubjectMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysDictTypeService;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 考试管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-13
 */
@Service
public class CpExaminationManagementServiceImpl implements ICpExaminationManagementService {
    @Resource
    private CpExaminationManagementMapper cpExaminationManagementMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpSubjectMapper cpSubjectMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;

    @Resource
    private ISysDictTypeService iSysDictTypeService;

    /**
     * 查询考试管理
     *
     * @param id 考试管理主键
     * @return 考试管理
     */
    @Override
    public CpExaminationManagement selectCpExaminationManagementById(Long id) {
        return cpExaminationManagementMapper.selectCpExaminationManagementById(id);
    }

    /**
     * 查询考试管理列表
     *
     * @param cpExaminationManagement 考试管理
     * @return 考试管理
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpExaminationManagement> selectCpExaminationManagementList(CpExaminationManagement cpExaminationManagement) {
        return cpExaminationManagementMapper.selectCpExaminationManagementList(cpExaminationManagement);
    }

    /**
     * 新增考试管理
     *
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    @SneakyThrows
    @Override
    public int insertCpExaminationManagement(CpExaminationManagement cpExaminationManagement) {
        cpExaminationManagement.setCreateTime(DateUtils.getNowDate());
        cpExaminationManagement.setCreateUser(SecurityUtils.getUsername());
        //查询当前学期,current_semester = 1,表示当前学期
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(CommonEnum.currentSemester.CURRENT.getValue(), cpExaminationManagement.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        cpExaminationManagement.setSemestersId(cpSemesters.getId());
        cpExaminationManagement.setSemesters(cpSemesters.getSemester());
        return cpExaminationManagementMapper.insertCpExaminationManagement(cpExaminationManagement);
    }

    /**
     * 修改考试管理
     *
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    @Override
    public int updateCpExaminationManagement(CpExaminationManagement cpExaminationManagement) {
        cpExaminationManagement.setUpdateTime(new Date());
        cpExaminationManagement.setUpdateBy(SecurityUtils.getUsername());
        return cpExaminationManagementMapper.updateCpExaminationManagement(cpExaminationManagement);
    }

    /**
     * 批量删除考试管理
     */
    @Override
    public int deleteCpExaminationManagementByIds(Long[] ids) {
        for (Long id : ids) {
            this.judge(id);
        }
        return cpExaminationManagementMapper.deleteCpExaminationManagementByIds(ids);
    }

    /**
     * 删除考试管理信息
     */
    @Override
    public int deleteCpExaminationManagementById(Long id) {
        this.judge(id);
        return cpExaminationManagementMapper.deleteCpExaminationManagementById(id);
    }

    //删除考试校验
    public void judge(Long id) {
        int count = cpExaminationManagementMapper.selectScoreEntry(id);
        if (count > 0) {
            throw new GlobalException("已经录入过成绩,不能删除考试");
        }
    }

    /**
     * 查询则插入记录通过数据库去重
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insCpScoreEntry(CpScoreEntry cpScoreEntry) {
        // 所有的年级
        List<String> grades = new ArrayList<>();
        List<SysDictData> sysDictDataList = iSysDictTypeService.selectDictDataByType("grade_list");
        if (null == sysDictDataList || sysDictDataList.isEmpty()) {
            throw new GlobalException("未查询到年级数据");
        }
        sysDictDataList.forEach(e -> grades.add(e.getDictValue()));
        CpExaminationManagement examinationManagement = cpExaminationManagementMapper.selectCpExaminationManagementById((long) cpScoreEntry.getExaminationManagementId());
        List<CpScoreEntry> cpScoreEntries = new ArrayList<>();
        for (String grade : grades) {
            // 根据年级查询出所有对应的届别和班级
            List<RankSAndClassName> rankSAndClassNames = cpRegisterMapper.selectCpRegisterByGrade(grade, cpScoreEntry.getDeptId());
            for (RankSAndClassName rankSAndClassName : rankSAndClassNames) {
                cpScoreEntries.addAll(createCpScoreEntries(cpScoreEntry.getExaminationManagementId(), cpScoreEntry.getDeptId(), cpScoreEntry.getUserId(), grade, rankSAndClassName, examinationManagement));
            }
        }
        // 批量插入数据
        if (!cpScoreEntries.isEmpty()) {
            cpExaminationManagementMapper.batchInsertScoreEntry(cpScoreEntries);
        }
    }

    /**
     * 查询成绩录入列表
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpScoreEntry> selectCpScoreEntryList(CpScoreEntry cpScoreEntry) {
        return cpExaminationManagementMapper.selectpScoreEntryList(cpScoreEntry);
    }

    private List<CpScoreEntry> createCpScoreEntries(int id, Long deptId, Long userId, String grade, RankSAndClassName rankSAndClassName, CpExaminationManagement examinationManagement) {
        List<CpScoreEntry> cpScoreEntries = new ArrayList<>();
        // 查询出所有的学科
        List<CpSubject> cpSubjects = cpSubjectMapper.selectCpSubject(deptId);
        for (CpSubject cpSubject : cpSubjects) {
            JSONArray jsonArray = JSONUtil.parseArray(cpSubject.getGrades());
            for (int i = 0; i < jsonArray.size(); i++) {
                String arr = jsonArray.getStr(i);
                if (arr.equals(grade)) {
                    // 赋值插入
                    CpScoreEntry cpScoreEntry = new CpScoreEntry();
                    cpScoreEntry.setExaminationManagementId(id);
                    cpScoreEntry.setExaminationName(examinationManagement.getExaminationName());
                    cpScoreEntry.setGrade(grade);
                    cpScoreEntry.setSubject(cpSubject.getSubject());
                    cpScoreEntry.setSubjectId(cpSubject.getId());
                    cpScoreEntry.setRankS(rankSAndClassName.getRankS());
                    cpScoreEntry.setPeriod(rankSAndClassName.getPeriod());
                    cpScoreEntry.setClassName(rankSAndClassName.getClassName());
                    cpScoreEntry.setClassId(rankSAndClassName.getClassId());
                    cpScoreEntry.setSemestersId(examinationManagement.getSemestersId());
                    cpScoreEntry.setSemesters(examinationManagement.getSemesters());
                    cpScoreEntry.setCreateBy(SecurityUtils.getUsername());
                    cpScoreEntry.setCreateTime(new Date());
                    cpScoreEntry.setUserId(userId);
                    cpScoreEntry.setDeptId(deptId);
                    // 设置录入状态
                    cpScoreEntry.setStatus("1");
                    cpScoreEntries.add(cpScoreEntry);
                }
            }
        }
        return cpScoreEntries;
    }


    /**
     * 导出考试管理
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpExaminationManagement> export(CpExaminationManagement cpExaminationManagement) {
        List<CpExaminationManagement> exportList = cpExaminationManagementMapper.export(cpExaminationManagement);
        if (exportList.isEmpty()) {
            throw new GlobalException("没有数据");
        }
        return exportList;
    }

    /**
     * 录入满分接口
     * @param cpScoreInquiryUpdateDto
     * @return
     */
    @Override
    public AjaxResult updateFullMark(CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto) {
        CpScoreEntry cpScoreEntry = new CpScoreEntry();
        cpScoreEntry.setId(cpScoreInquiryUpdateDto.getId());
        cpScoreEntry.setFullMark(StringUtils.isNotEmpty(cpScoreInquiryUpdateDto.getFullMark()) ? cpScoreInquiryUpdateDto.getFullMark() : null);
        cpExaminationManagementMapper.updateCpScoreEntry(cpScoreEntry);

        return AjaxResult.success();
    }
}
