package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpQualityEvaluation;
import com.ruoyi.campus.StuEvaluation.service.ICpQualityEvaluationService;
import com.ruoyi.campus.domain.exportVo.ExportQualityEvaluation;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 综合素质评价Controller
 * 
 * @author ruoyi
 * @date 2023-08-02
 */
@RestController
@RequestMapping("/qualityEvaluation/qualityEvaluation")
@Validated
public class CpQualityEvaluationController extends BaseController
{
    @Autowired
    private ICpQualityEvaluationService cpQualityEvaluationService;

    /**
     * 查询综合素质评价列表
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpQualityEvaluation cpQualityEvaluation)
    {
        startPage();
        List<CpQualityEvaluation> list = cpQualityEvaluationService.selectCpQualityEvaluationList(cpQualityEvaluation);
        return getDataTable(list);
    }

    /**
     * 导出综合素质评价列表
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:export')")
    @Log(title = "综合素质评价", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportQualityEvaluation exportQualityEvaluation)
    {
        List<ExportQualityEvaluation> list = cpQualityEvaluationService.exportCpQualityEvaluationList(exportQualityEvaluation);
        ExcelUtil<ExportQualityEvaluation> util = new ExcelUtil<ExportQualityEvaluation>(ExportQualityEvaluation.class);
        util.exportExcel(response, list, "综合素质评价数据");
    }

    /**
     * 获取综合素质评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpQualityEvaluationService.selectCpQualityEvaluationById(id));
    }

    /**
     * 新增综合素质评价
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:add')")
    @Log(title = "综合素质评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpQualityEvaluation cpQualityEvaluation)
    {
        cpQualityEvaluation.setUserId(getUserId());
        cpQualityEvaluation.setDeptId(getDeptId());
        return toAjax(cpQualityEvaluationService.insertCpQualityEvaluation(cpQualityEvaluation));
    }

    /**
     * 综合素质 评价功能
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:edit')")
    @Log(title = "综合素质评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @Valid CpQualityEvaluation cpQualityEvaluation)
    {
        return toAjax(cpQualityEvaluationService.updateCpQualityEvaluation(cpQualityEvaluation));
    }

    /**
     * 删除综合素质评价
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:remove')")
    @Log(title = "综合素质评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpQualityEvaluationService.deleteCpQualityEvaluationByIds(ids));
    }

    /**
     * 批量新增综合素质评价
     */
    @PreAuthorize("@ss.hasPermi('qualityEvaluation:qualityEvaluation:add')")
    @Log(title = "综合素质评价", businessType = BusinessType.INSERT)
    @PostMapping("/batchAdd")
    public AjaxResult batchAdd(@RequestBody CpQualityEvaluation cpQualityEvaluation)
    {
        cpQualityEvaluation.setDeptId(getDeptId());
        cpQualityEvaluation.setUserId(getUserId());
        return AjaxResult.success(cpQualityEvaluationService.insertCpQualityEvaluationBatchAdd(cpQualityEvaluation));
    }
}
