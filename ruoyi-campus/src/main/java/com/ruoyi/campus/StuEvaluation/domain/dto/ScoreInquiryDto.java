package com.ruoyi.campus.StuEvaluation.domain.dto;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScoreInquiryDto extends BaseEntity {
    //成绩表id
    private Long scoreInquiryId;
    //学号
    private String studentNum;
    //学生名称
    private String studentName;
    //班级名称
    private String className;

    //学科成绩
    private Double subjectScore;
    //学生人数
    private int count;

    private String classId;
    private String grade;
    private Long registerId;
    private String period;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

}
