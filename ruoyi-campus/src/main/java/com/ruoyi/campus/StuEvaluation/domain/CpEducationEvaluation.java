package com.ruoyi.campus.StuEvaluation.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 五育评价对象 cp_education_evaluation
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CpEducationEvaluation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 奖章记录表id */
    private Long id;

    /** 学期id */
    private Long semestersId;

    /** 学期 */
    @Excel(name = "学期")
    private String semesters;

    /** 届别id */
    private String period;

    /** 界别 */
    @Excel(name = "界别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级id */
    private String classId;

    //学籍id
    private Long registerId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String name;

    /** A奖章数量 */
    private int medalNumA;

    /** B奖章数量 */
    private int medalNumB;

    /** C奖章数量 */
    private int medalNumC;

    /** D奖章数量 */
    private int medalNumD;

    /** E奖章数量 */
    private int medalNumE;

    /** 奖章总数量 */
    private int medalTotal;

    /** 奖章id */
    private Long medalId;

    /** 奖章名称 */
    @Excel(name = "奖章名称")
    private String medalName;

    /** 奖章分类 */
    @Excel(name = "奖章分类")
    private String medalClassification;

    /** 奖章来源 */
    @Excel(name = "奖章来源")
    private String medalSource;

    /** 发放人 */
    @Excel(name = "发放人")
    private String createUser;

    /** 撤销字段,0表示未撤销,1表示已撤销 */
    private int revocation;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
