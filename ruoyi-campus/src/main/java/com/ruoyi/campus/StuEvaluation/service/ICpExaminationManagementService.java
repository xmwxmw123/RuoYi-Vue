package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpExaminationManagement;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 考试管理Service接口
 * 
 * @author ruoyi
 * @date 2023-08-13
 */
public interface ICpExaminationManagementService 
{
    /**
     * 查询考试管理
     * 
     * @param id 考试管理主键
     * @return 考试管理
     */
    public CpExaminationManagement selectCpExaminationManagementById(Long id);

    /**
     * 查询考试管理列表
     * 
     * @param cpExaminationManagement 考试管理
     * @return 考试管理集合
     */
    public List<CpExaminationManagement> selectCpExaminationManagementList(CpExaminationManagement cpExaminationManagement);

    /**
     * 新增考试管理
     * 
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    public int insertCpExaminationManagement(CpExaminationManagement cpExaminationManagement);

    /**
     * 修改考试管理
     * 
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    public int updateCpExaminationManagement(CpExaminationManagement cpExaminationManagement);

    /**
     * 批量删除考试管理
     * 
     * @param ids 需要删除的考试管理主键集合
     * @return 结果
     */
    public int deleteCpExaminationManagementByIds(Long[] ids);

    /**
     * 删除考试管理信息
     * 
     * @param id 考试管理主键
     * @return 结果
     */
    public int deleteCpExaminationManagementById(Long id);

    /**
     * 查询则插入记录通过数据库去重
     */
    void insCpScoreEntry(CpScoreEntry cpScoreEntry);

    /**
     * 查询成绩录入列表
     * @param cpScoreEntry
     * @return
     */
    List<CpScoreEntry> selectCpScoreEntryList(CpScoreEntry cpScoreEntry);

    /**
     * 导出考试管理
     * @param cpExaminationManagement
     * @return
     */
    List<CpExaminationManagement> export(CpExaminationManagement cpExaminationManagement);

    /**
     * 录入满分接口
     * @param cpScoreInquiryUpdateDto
     * @return
     */
    AjaxResult updateFullMark(CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto);
}
