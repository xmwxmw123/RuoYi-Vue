package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 活动参与情况Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-07
 */
public interface CpActivityPartakeMapper 
{
    /**
     * 查询活动参与情况
     * 
     * @param id 活动参与情况主键
     * @return 活动参与情况
     */
    public CpActivityPartake selectCpActivityPartakeById(Long id);

    /**
     * 查询活动参与情况列表(PC和教师APP)
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况集合
     */
    public List<CpActivityPartake> selectCpActivityPartakeList(CpActivityPartake cpActivityPartake);

    /**
     * 查询活动参与情况列表（家长APP）
     *
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况集合
     */
    List<CpActivityPartake> parentAppActivityPartakeList(CpActivityPartake cpActivityPartake);

    /**
     * 新增活动参与情况
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    public int insertCpActivityPartake(CpActivityPartake cpActivityPartake);

    /**
     * 修改活动参与情况
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    public int updateCpActivityPartake(CpActivityPartake cpActivityPartake);

    /**
     * 删除活动参与情况
     * 
     * @param id 活动参与情况主键
     * @return 结果
     */
    public int deleteCpActivityPartakeById(Long id);

    /**
     * 批量删除活动参与情况
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpActivityPartakeByIds(Long[] ids);

    /**
     * 导出
     * @param cpActivityPartake
     * @return
     */
    List<CpActivityPartake> export(CpActivityPartake cpActivityPartake);

    @Select("select * from cp_activity_partake where activity_id = #{activityId} and dept_id = #{deptId}")
    List<CpActivityPartake> getCpActivityPartakeByActivityId(@Param("activityId") Long activityId, @Param("deptId") Long deptId);
}
