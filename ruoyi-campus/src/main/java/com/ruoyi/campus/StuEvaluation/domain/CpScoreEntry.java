package com.ruoyi.campus.StuEvaluation.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpScoreEntry  extends BaseEntity {

    private static final long serialVersionUID = 1L;

    //考试管理表id
    private  int id;
    //考试管理id
    private int examinationManagementId;
    //考试管理名
    private String examinationName;
    //届别id
    private String period;
    //届别
    private String rankS;
    //年级
    private String grade;
    //班级
    private String className;
    //班级id
    private int classId;
    //学科名id
    private long subjectId;
    //学科名
    private String subject;
    //满分
    private String fullMark;
    //学期id
    private long semestersId;
    //学期
    private String semesters;
    //录入状态
    private String status;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

}
