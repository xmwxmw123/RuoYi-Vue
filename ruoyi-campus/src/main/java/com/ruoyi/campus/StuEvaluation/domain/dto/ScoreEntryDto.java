package com.ruoyi.campus.StuEvaluation.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScoreEntryDto {
    //届别
    private String rankS;

    //年级
    private String grade;

    //班级
    private String className;

    //学科
    private String subject;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
