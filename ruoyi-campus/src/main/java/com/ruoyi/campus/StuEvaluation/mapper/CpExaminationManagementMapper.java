package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpExaminationManagement;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 考试管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-13
 */
public interface CpExaminationManagementMapper 
{
    /**
     * 查询考试管理
     * 
     * @param id 考试管理主键
     * @return 考试管理
     */
    public CpExaminationManagement selectCpExaminationManagementById(Long id);

    /**
     * 查询考试管理列表
     * 
     * @param cpExaminationManagement 考试管理
     * @return 考试管理集合
     */
    public List<CpExaminationManagement> selectCpExaminationManagementList(CpExaminationManagement cpExaminationManagement);

    /**
     * 新增考试管理
     * 
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    public int insertCpExaminationManagement(CpExaminationManagement cpExaminationManagement);

    /**
     * 修改考试管理
     * 
     * @param cpExaminationManagement 考试管理
     * @return 结果
     */
    public int updateCpExaminationManagement(CpExaminationManagement cpExaminationManagement);

    /**
     * 删除考试管理
     * 
     * @param id 考试管理主键
     * @return 结果
     */
    public int deleteCpExaminationManagementById(Long id);

    /**
     * 批量删除考试管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpExaminationManagementByIds(Long[] ids);

    /**
     * 查询成绩录入列表
     * @param cpScoreEntry
     */
    List<CpScoreEntry> selectpScoreEntryList(CpScoreEntry cpScoreEntry);

    /**
     * 查询ScoreEntry表的数据
     * @param scoreEntryId
     */
    @Select("select * from cp_score_entry where id=#{scoreEntryId} and dept_id = #{deptId}")
    CpScoreEntry selectpScoreEntry(@Param("scoreEntryId") Long scoreEntryId, @Param("deptId") Long deptId);

    void updateCpScoreEntry(CpScoreEntry cpScoreEntry);

    /**
     * 导出考试管理
     * @param cpExaminationManagement
     */
    List<CpExaminationManagement> export(CpExaminationManagement cpExaminationManagement);


    int selectScoreEntry(Long id);

    /**
     * 批量插入数据到成绩录入表
     * @param cpScoreEntries
     */
    void batchInsertScoreEntry(List<CpScoreEntry> cpScoreEntries);
}
