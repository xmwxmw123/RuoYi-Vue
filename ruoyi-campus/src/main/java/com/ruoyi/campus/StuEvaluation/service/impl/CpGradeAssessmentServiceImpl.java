package com.ruoyi.campus.StuEvaluation.service.impl;

import com.ruoyi.campus.StuEvaluation.domain.CpGradeAssessment;
import com.ruoyi.campus.StuEvaluation.mapper.CpGradeAssessmentMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpGradeAssessmentService;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportGradeAssessment;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * 体质测评Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-07
 */
@Service
public class CpGradeAssessmentServiceImpl implements ICpGradeAssessmentService {
    @Resource
    private CpGradeAssessmentMapper cpGradeAssessmentMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;


    /**
     * 查询体质测评
     *
     * @param id 体质测评主键
     * @return 体质测评
     */
    @Override
    public CpGradeAssessment selectCpGradeAssessmentById(Long id) {
        return cpGradeAssessmentMapper.selectCpGradeAssessmentById(id);
    }

    /**
     * 查询体质测评列表
     *
     * @param cpGradeAssessment 体质测评
     * @return 体质测评
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpGradeAssessment> selectCpGradeAssessmentList(CpGradeAssessment cpGradeAssessment) {
        List<CpGradeAssessment> cpGradeAssessments = cpGradeAssessmentMapper.selectCpGradeAssessmentList(cpGradeAssessment);
        if (cpGradeAssessment == null) {
            throw new GlobalException("参数有误");
        }
        return cpGradeAssessments;
    }

    /**
     * 新增体质测评
     *
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    @Override
    public int insertCpGradeAssessment(CpGradeAssessment cpGradeAssessment) {
        // 判断是否有该学号
        CpRegister student = cpRegisterMapper.selectStudenByNum(cpGradeAssessment.getStudentNum(), cpGradeAssessment.getDeptId());
        if (student == null) {
            throw new GlobalException("没有该学号的学生");
        }
        if(CommonEnum.registerStatus.GRADUATE.getValue().equals(student.getStatus())){
            throw new BaseException("该学生已毕业");
        }else if(CommonEnum.registerStatus.QUIT_SCHOOL.getValue().equals(student.getStatus())){
            throw new BaseException("该学生已休学");
        }
        //  根据学期id查询学期
        String currentSemester = CommonEnum.currentSemester.CURRENT.getValue();
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(currentSemester, cpGradeAssessment.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        int count = cpGradeAssessmentMapper.selectStudentNum(student.getStudentNum(), student.getGrade(), student.getClassId(), student.getPeriod(), cpSemesters.getId(), student.getDeptId());
        if (count > 0) {
            throw new GlobalException("请勿重复添加学生");
        }else {
            cpGradeAssessment.setSemesters(cpSemesters.getSemester());
            cpGradeAssessment.setRankS(student.getRankS());
            cpGradeAssessment.setPeriod(student.getPeriod());
            cpGradeAssessment.setClassId(student.getClassId());
            cpGradeAssessment.setRegisterId(student.getId());
            cpGradeAssessment.setGrade(student.getGrade());
            cpGradeAssessment.setName(student.getName());
            cpGradeAssessment.setStudentNum(student.getStudentNum());
            cpGradeAssessment.setSex(student.getSex());
            cpGradeAssessment.setClassName(student.getClassName());
            String username = SecurityUtils.getUsername();
            cpGradeAssessment.setCreateBy(username);
            cpGradeAssessment.setCreateTime(DateUtils.getNowDate());
            return cpGradeAssessmentMapper.insertCpGradeAssessment(cpGradeAssessment);
        }
    }

    /**
     * 修改体质测评
     *
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    @Override
    public int updateCpGradeAssessment(CpGradeAssessment cpGradeAssessment) {
        String username = SecurityUtils.getUsername();
        cpGradeAssessment.setUpdateBy(username);
        cpGradeAssessment.setUpdateTime(DateUtils.getNowDate());
        return cpGradeAssessmentMapper.updateCpGradeAssessment(cpGradeAssessment);
    }

    /**
     * 批量删除体质测评
     *
     * @param ids 需要删除的体质测评主键
     * @return 结果
     */
    @Override
    public int deleteCpGradeAssessmentByIds(Long[] ids) {
        return cpGradeAssessmentMapper.deleteCpGradeAssessmentByIds(ids);
    }

    /**
     * 删除体质测评信息
     *
     * @param id 体质测评主键
     * @return 结果
     */
    @Override
    public int deleteCpGradeAssessmentById(Long id) {
        return cpGradeAssessmentMapper.deleteCpGradeAssessmentById(id);
    }

    /**
     * 批量新增
     */
    @Override
    public String batchAdd(CpGradeAssessment cpGradeAssessment) {
        //  根据学期id查询学期
        String currentSemester = CommonEnum.currentSemester.CURRENT.getValue();
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(currentSemester, cpGradeAssessment.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        // 根据班级名字查询班级
        List<CpRegister> listClassName = cpRegisterMapper.selectClassName(cpGradeAssessment.getClassId(), cpGradeAssessment.getDeptId());
        String username = SecurityUtils.getUsername();
        // 查询给字段赋值
        for (CpRegister cpRegister : listClassName) {
            int count = cpGradeAssessmentMapper.selectStudentNum(cpRegister.getStudentNum(), cpRegister.getGrade(), cpRegister.getClassId(), cpRegister.getPeriod(), cpSemesters.getId(), cpRegister.getDeptId());
            if (count > 0) {
                continue;
            }
            CpGradeAssessment cpGradeAssessment1 = new CpGradeAssessment();
            cpGradeAssessment1.setClassId(cpGradeAssessment.getClassId());
            cpGradeAssessment1.setSemestersId(cpGradeAssessment.getSemestersId());
            cpGradeAssessment1.setCreateBy(username);
            cpGradeAssessment1.setCreateTime(DateUtils.getNowDate());
            cpGradeAssessment1.setSemesters(cpSemesters.getSemester());
            cpGradeAssessment1.setStudentNum(cpRegister.getStudentNum());
            cpGradeAssessment1.setName(cpRegister.getName());
            cpGradeAssessment1.setSex(cpRegister.getSex());
            cpGradeAssessment1.setRankS(cpRegister.getRankS());
            cpGradeAssessment1.setPeriod(cpRegister.getPeriod());
            cpGradeAssessment1.setRegisterId(cpRegister.getId());
            cpGradeAssessment1.setGrade(cpRegister.getGrade());
            cpGradeAssessment1.setClassName(cpRegister.getClassName());
            cpGradeAssessment1.setDeptId(cpGradeAssessment.getDeptId());
            cpGradeAssessment1.setUserId(cpGradeAssessment.getUserId());
            cpGradeAssessmentMapper.batchAdd(cpGradeAssessment1);
        }
        return "操作成功";
    }

    /**
     * 导出
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<ExportGradeAssessment> exportSelectCpGradeAssessmentList(ExportGradeAssessment exportGradeAssessment) {
        List<ExportGradeAssessment> exportGradeAssessments =
                cpGradeAssessmentMapper.exportSelectCpGradeAssessmentList(exportGradeAssessment);


        for (ExportGradeAssessment gradeAssessment : exportGradeAssessments) {
            //性别
            if(gradeAssessment.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", gradeAssessment.getSex());
                gradeAssessment.setSex(label);
            }

            //年级
            if(gradeAssessment.getGrade() != null){
                String label = dictDataMapper.selectDictLabel("grade_list", gradeAssessment.getGrade());
                gradeAssessment.setGrade(label);
            }

            //等级
            if (gradeAssessment.getDj() != null) {
                String label = dictDataMapper.selectDictLabel("moral_grade", gradeAssessment.getDj());
                gradeAssessment.setDj(label);
            }
        }
        return exportGradeAssessments;
    }
}
