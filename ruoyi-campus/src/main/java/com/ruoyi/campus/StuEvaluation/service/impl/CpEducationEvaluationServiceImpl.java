package com.ruoyi.campus.StuEvaluation.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpBatchDto;
import com.ruoyi.campus.StuEvaluation.mapper.CpEducationEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpEducationEvaluationService;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 五育评价Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-08
 */
@Service
public class CpEducationEvaluationServiceImpl implements ICpEducationEvaluationService {
    @Resource
    private CpEducationEvaluationMapper cpEducationEvaluationMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;

    /**
     * 查询五育评价
     *
     * @param id 五育评价主键
     * @return 五育评价
     */
    @Override
    public CpEducationEvaluation selectCpEducationEvaluationById(Long id) {
        return cpEducationEvaluationMapper.selectCpEducationEvaluationById(id);
    }

    /**
     * 查询五育评价列表
     *
     * @param cpEducationEvaluation 五育评价
     * @return 五育评价
     */
    @DataScope(deptAlias = "d")
    @Override
    @Transactional
    public List<CpEducationEvaluation> selectCpEducationEvaluationList(CpEducationEvaluation cpEducationEvaluation) {
        //查询当前学期,current_semester = 1,表示当前学期
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(CommonEnum.currentSemester.CURRENT.getValue(), cpEducationEvaluation.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        //把学籍表的学生同步到奖章表
        List<CpRegister> cpRegisters = cpRegisterMapper.selectStudentList(cpEducationEvaluation.getDeptId());
        if (cpRegisters == null) {
            throw new GlobalException("该班级没有学生");
        }
        List<CpEducationEvaluation> resultList = new ArrayList<>();
        cpEducationEvaluation.setSemestersId(cpSemesters.getId());
        List<CpEducationEvaluation> list = cpEducationEvaluationMapper.selectCpEducationEvaluationList(cpEducationEvaluation);
        if (CollUtil.isEmpty(list)) {
            for (CpRegister cpRegister : cpRegisters) {
                if (cpRegister.getGrade().equals(cpEducationEvaluation.getGrade()) && cpRegister.getClassId().equals(cpEducationEvaluation.getClassId())) {
                    resultList.add(this.assembleEducationEval(cpSemesters, cpRegister, cpEducationEvaluation.getDeptId(), cpEducationEvaluation.getUserId()));
                }
            }
        } else {
            for (CpRegister cpRegister : cpRegisters) {
                if (list.stream().anyMatch(e -> e.getStudentNum().equals(cpRegister.getStudentNum()))) continue;
                if (cpRegister.getGrade().equals(cpEducationEvaluation.getGrade()) && cpRegister.getClassId().equals(cpEducationEvaluation.getClassId())) {
                    resultList.add(this.assembleEducationEval(cpSemesters, cpRegister, cpEducationEvaluation.getDeptId(), cpEducationEvaluation.getUserId()));
                }
            }
        }
        if (CollUtil.isNotEmpty(resultList)) {
            resultList.forEach(e -> cpEducationEvaluationMapper.insertCpEducationEvaluation(e));
        }

        if(CollUtil.isNotEmpty(list)){
            for (CpEducationEvaluation educationEvaluation : list) {
                int count = cpRegisterMapper.selectStuCount(educationEvaluation.getGrade(), educationEvaluation.getRegisterId(), educationEvaluation.getClassId(), educationEvaluation.getPeriod(), educationEvaluation.getDeptId());
                if (count > 0){
                    resultList.add(educationEvaluation);
                }
            }
        }

        return resultList;
    }

    private CpEducationEvaluation assembleEducationEval(CpSemesters cpSemesters, CpRegister cpRegister, Long deptId, Long userId) {
        CpEducationEvaluation build = CpEducationEvaluation.builder()
                .semestersId(cpSemesters.getId())
                .semesters(cpSemesters.getSemester())
                .period(cpRegister.getPeriod())
                .rankS(cpRegister.getRankS())
                .grade(cpRegister.getGrade())
                .classId(cpRegister.getClassId())
                .className(cpRegister.getClassName())
                .registerId(cpRegister.getId())
                .studentNum(cpRegister.getStudentNum())
                .name(cpRegister.getName())
                .revocation(CommonEnum.revocation.ZERO.getValue())
                .userId(userId)
                .deptId(deptId)
                //奖章初始化0
                .medalNumA(0)
                .medalNumB(0)
                .medalNumC(0)
                .medalNumD(0)
                .medalNumE(0)
                .medalTotal(0)
                .build();
        return build;
    }

    /**
     * 新增五育评价
     *
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    @Override
    public int insertCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation) {
        cpEducationEvaluation.setCreateTime(DateUtils.getNowDate());
        // 判断是否有该学号
        CpRegister student = cpRegisterMapper.selectStudenByNum(cpEducationEvaluation.getStudentNum(), cpEducationEvaluation.getDeptId());
        if (student == null) {
            throw new GlobalException("没有该学号的学生");
        }
        if (CommonEnum.registerStatus.GRADUATE.getValue().equals(student.getStatus())) {
            throw new BaseException("该学生已毕业");
        } else if (CommonEnum.registerStatus.QUIT_SCHOOL.getValue().equals(student.getStatus())) {
            throw new BaseException("该学生已休学");
        }
        int count = cpEducationEvaluationMapper.selectStu(cpEducationEvaluation.getStudentNum(), cpEducationEvaluation.getDeptId());
        if (count > 0) {
            throw new GlobalException("当前学生请已被添加，请勿重复添加");
        }
        return cpEducationEvaluationMapper.insertCpEducationEvaluation(cpEducationEvaluation);
    }

    /**
     * 修改五育评价
     *
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    @Override
    public int updateCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation) {
        //设置发放时间和发放人
        cpEducationEvaluation.setCreateUser(SecurityUtils.getUsername());
        cpEducationEvaluation.setCreateTime(new Date());
        cpEducationEvaluation.setMedalClassification("表扬章");
        cpEducationEvaluation.setMedalSource("五育评价");
        //A,B,C,D,E数量相加得到总奖章数
        int medalTotal = cpEducationEvaluation.getMedalNumA() + cpEducationEvaluation.getMedalNumB() + cpEducationEvaluation.getMedalNumC()
                + cpEducationEvaluation.getMedalNumD() + cpEducationEvaluation.getMedalNumE();
        cpEducationEvaluation.setMedalTotal(medalTotal);
        return cpEducationEvaluationMapper.insertCpEducationEvaluation(cpEducationEvaluation);
    }

    /**
     * 批量新增五育评价（发奖章功能）
     *
     * @param cpBatchDto
     * @return
     */
    @Override
    @Transactional
    public AjaxResult updateCpEducationEvaluationBatch(CpBatchDto cpBatchDto) {
        String[] studentNum = cpBatchDto.getStudentNum();
        int medalNumA = cpBatchDto.getMedalNumA();
        int medalNumB = cpBatchDto.getMedalNumB();
        int medalNumC = cpBatchDto.getMedalNumC();
        int medalNumD = cpBatchDto.getMedalNumD();
        int medalNumE = cpBatchDto.getMedalNumE();
        //不为零的话存入map集合
        Map<String, Integer> map = new HashMap<>();
        if (medalNumA > 0) {
            map.put("A", medalNumA);
        }
        if (medalNumB > 0) {
            map.put("B", medalNumB);
        }
        if (medalNumC > 0) {
            map.put("C", medalNumC);
        }
        if (medalNumD > 0) {
            map.put("D", medalNumD);
        }
        if (medalNumE > 0) {
            map.put("E", medalNumE);
        }
        //查询当前学期,current_semester = 1,表示当前学期
        String currentSemester = CommonEnum.currentSemester.CURRENT.getValue();
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(currentSemester, cpBatchDto.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        for (String stu : studentNum) {
            CpRegister cpRegister = cpRegisterMapper.selectStudent(stu, cpBatchDto.getDeptId());
            if (cpRegister == null || CommonEnum.registerStatus.GRADUATE.getValue().equals(cpRegister.getStatus())
                    || CommonEnum.registerStatus.QUIT_SCHOOL.getValue().equals(cpRegister.getStatus())) {
                throw new GlobalException(cpRegister.getName() + "已休学");

            }
            for (String key : map.keySet()) {
                CpMedalRecord cpMedalRecord = new CpMedalRecord();
                if ("A".equals(key)) {
                    cpMedalRecord.setMedalNumA(map.get(key));
                } else if ("B".equals(key)) {
                    cpMedalRecord.setMedalNumB(map.get(key));
                } else if ("C".equals(key)) {
                    cpMedalRecord.setMedalNumC(map.get(key));
                } else if ("D".equals(key)) {
                    cpMedalRecord.setMedalNumD(map.get(key));
                } else if ("E".equals(key)) {
                    cpMedalRecord.setMedalNumE(map.get(key));
                }
                BeanUtils.copyProperties(cpRegister, cpMedalRecord);
                //塞值
                cpMedalRecord.setSemesters(cpSemesters.getSemester());
                cpMedalRecord.setSemestersId(cpSemesters.getId());
                cpMedalRecord.setStudentNum(stu);
                //奖章类型:"1"表示表扬章,"2"表示提示章
                cpMedalRecord.setMedalClassification("1");
                cpMedalRecord.setMedalSource("五育评价");
                cpMedalRecord.setCreateUser(SecurityUtils.getUsername());
                cpMedalRecord.setCreateTime(new Date());
                cpMedalRecord.setUserId(cpBatchDto.getUserId());
                cpMedalRecord.setDeptId(cpBatchDto.getDeptId());
                cpMedalRecord.setRevocation(CommonEnum.revocation.ZERO.getValue());
                //赋完值插入数据
                cpMedalRecordMapper.insertCpMedalRecord(cpMedalRecord);
            }
            //查出原本MedalTotal的值
            CpEducationEvaluation cpEducationEvaluation1 = cpEducationEvaluationMapper.selectMedalTotal(stu, cpRegister.getGrade(), cpSemesters.getId(), cpBatchDto.getDeptId(),cpRegister.getClassId(), cpRegister.getPeriod());
            //向奖章表插入数据的同时也要在五育表里面同步奖章数
            CpEducationEvaluation cpEducationEvaluation = new CpEducationEvaluation();
            BeanUtils.copyProperties(cpBatchDto, cpEducationEvaluation);
            cpEducationEvaluation.setMedalTotal(cpEducationEvaluation1.getMedalTotal() + medalNumA + medalNumB + medalNumC + medalNumD + medalNumE);
            cpEducationEvaluation.setMedalNumA(cpEducationEvaluation1.getMedalNumA() + medalNumA);
            cpEducationEvaluation.setMedalNumB(cpEducationEvaluation1.getMedalNumB() + medalNumB);
            cpEducationEvaluation.setMedalNumC(cpEducationEvaluation1.getMedalNumC() + medalNumC);
            cpEducationEvaluation.setMedalNumD(cpEducationEvaluation1.getMedalNumD() + medalNumD);
            cpEducationEvaluation.setMedalNumE(cpEducationEvaluation1.getMedalNumE() + medalNumE);
            cpEducationEvaluation.setStudentNum(stu);
            cpEducationEvaluation.setClassId(cpRegister.getClassId());
            cpEducationEvaluation.setPeriod(cpRegister.getPeriod());
            cpEducationEvaluation.setSemestersId(cpSemesters.getId());
            cpEducationEvaluationMapper.updateCpEducationEvaluationByStudentNum(cpEducationEvaluation);
        }
        return AjaxResult.success();
    }

    /**
     * 批量删除五育评价
     *
     * @param ids 需要删除的五育评价主键
     * @return 结果
     */
    @Override
    public int deleteCpEducationEvaluationByIds(Long[] ids) {
        return cpEducationEvaluationMapper.deleteCpEducationEvaluationByIds(ids);
    }

    /**
     * 删除五育评价信息
     *
     * @param id 五育评价主键
     * @return 结果
     */
    @Override
    public int deleteCpEducationEvaluationById(Long id) {
        return cpEducationEvaluationMapper.deleteCpEducationEvaluationById(id);
    }
}
