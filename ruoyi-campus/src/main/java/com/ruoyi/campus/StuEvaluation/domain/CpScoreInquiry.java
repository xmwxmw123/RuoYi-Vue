package com.ruoyi.campus.StuEvaluation.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 成绩查询(用于存放各个班级里学生的成绩)对象 cp_score_inquiry
 * 
 * @author ruoyi
 * @date 2023-08-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpScoreInquiry extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    //考试名称
    private String examinationName;
    //学生姓名
    private String studentName;
    //学科名
    private Long subjectId;
    //学期id
    private Long semestersId;
    //年级
    private String grade;
    //级别id
    private String period;

    /** 成绩查询表id */
    private Long id;

    /** 成绩录入表id */
    @Excel(name = "成绩录入表id")
    private Long scoreEntryId;

    /** 班级id*/
    private int classId;

    /** 班级*/
    private String className;

    /** 学籍表id */
    @Excel(name = "学籍表id")
    private Long registerId;

    /** 学科成绩 */
    @Excel(name = "学科成绩")
    private Double subjectScore;

    /** 班级排名 */
    private int classRanking;

    /** 年级排名 */
    private int gradeRanking;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

}

