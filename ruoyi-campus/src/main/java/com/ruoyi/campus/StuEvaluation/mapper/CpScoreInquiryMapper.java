package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpScoreAndInquiryEntry;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreInquiry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryDto;
import com.ruoyi.campus.StuEvaluation.domain.dto.ScoreInquiryDto;
import com.ruoyi.campus.homepage.domain.CpHomePage;
import com.ruoyi.campus.homepage.domain.CpHomePageAssessmentVo;

import java.util.List;

/**
 * 成绩查询(用于存放各个班级里学生的成绩)Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-14
 */
public interface CpScoreInquiryMapper 
{
    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 成绩查询(用于存放各个班级里学生的成绩)
     */
    public CpScoreInquiry selectCpScoreInquiryById(Long id);

    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)列表
     * 
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 成绩查询(用于存放各个班级里学生的成绩)集合
     */
    public List<CpScoreInquiry> selectCpScoreInquiryList(CpScoreInquiry cpScoreInquiry);

    /**
     * 新增成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    public int insertCpScoreInquiry(CpScoreInquiry cpScoreInquiry);

    /**
     * 修改成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    public int updateCpScoreInquiry(CpScoreInquiry cpScoreInquiry);

    /**
     * 删除成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 结果
     */
    public int deleteCpScoreInquiryById(Long id);

    /**
     * 批量删除成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpScoreInquiryByIds(Long[] ids);

    /**
     * 查询班级排名和年级排名
     * @param cpScoreEntry 参数
     * @return 结果
     */
    List<CpScoreAndInquiryEntry> selectClassOrGradeRanking(CpScoreEntry cpScoreEntry);

    /**
     * 导出
     * @param cpScoreInquiry
     */
    List<CpScoreInquiryDto> export(CpScoreInquiryDto cpScoreInquiry);

    /**
     * 显示班级学生
     */
    List<ScoreInquiryDto> selectClassStudent(CpScoreInquiry cpScoreInquiry);

    /**
     * 根据条件查询分数进行分类
     * @param cpHomePage
     */
    CpHomePageAssessmentVo selectStuCount(CpHomePage cpHomePage);


}
