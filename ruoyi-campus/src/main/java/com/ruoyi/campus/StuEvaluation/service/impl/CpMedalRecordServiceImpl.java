package com.ruoyi.campus.StuEvaluation.service.impl;

import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.StuEvaluation.mapper.CpEducationEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpMedalRecordService;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 奖章记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-08
 */
@Service
public class CpMedalRecordServiceImpl implements ICpMedalRecordService {
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private CpEducationEvaluationMapper cpEducationEvaluationMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 查询奖章记录
     *
     * @param id 奖章记录主键
     * @return 奖章记录
     */
    @Override
    public CpMedalRecord selectCpMedalRecordById(Long id) {
        return cpMedalRecordMapper.selectCpMedalRecordById(id);
    }

    /**
     * 查询奖章记录列表(PC端和教师APP)
     *
     * @param cpMedalRecord 奖章记录
     * @return 奖章记录
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpMedalRecord> selectCpMedalRecordList(CpMedalRecord cpMedalRecord) {
        return cpMedalRecordMapper.selectCpMedalRecordList(cpMedalRecord);
    }

    /**
     * 查询当前学生奖章记录(家长端app用)
     */
    @Override
    public List<CpMedalRecord> parentAppQuyCpMedalRecord(CpMedalRecord cpMedalRecord) {
        if (StringUtils.isBlank(cpMedalRecord.getStudentNum()) || Objects.isNull(cpMedalRecord.getDeptId())) {
            throw new RuntimeException("参数错误");
        }
        return cpMedalRecordMapper.parentAppQuyCpMedalRecord(cpMedalRecord);
    }

    /**
     * 新增奖章记录
     *
     * @param cpMedalRecord 奖章记录
     * @return 结果
     */
    @Override
    public int insertCpMedalRecord(CpMedalRecord cpMedalRecord) {
        cpMedalRecord.setCreateTime(DateUtils.getNowDate());
        return cpMedalRecordMapper.insertCpMedalRecord(cpMedalRecord);
    }

    /**
     * 撤销奖章记录
     *
     * @param id 奖章记录
     * @return 结果
     */
    @Override
    public AjaxResult updateCpMedalRecord(Long id) {
        //根据id查询出当前撤销奖章的信息
        CpMedalRecord cpMedalRecord = cpMedalRecordMapper.selectCpMedalRecordById(id);
        //判断数据是否已经撤销过,1表示已撤销
        if (cpMedalRecord.getRevocation() == 1) {
            throw new RuntimeException("该条数据已经撤销过!");
        }
        //调用撤销方法
        RemovalOfMedal(cpMedalRecord);
        //修改字段revocation,表示已撤销
        int revocation = CommonEnum.revocation.ONE.getValue();
        cpMedalRecordMapper.updateRevocation(id, revocation);
        return AjaxResult.success();
    }

    /**
     * 批量撤销奖章
     *
     * @param ids
     * @return
     */
    @Override
    public AjaxResult batchRevoke(Long[] ids) {
        for (Long id : ids) {
            //根据id查询出当前撤销奖章的信息
            CpMedalRecord cpMedalRecord = cpMedalRecordMapper.selectCpMedalRecordById(id);
            //判断数据是否已经撤销过,1表示已撤销
            if (cpMedalRecord.getRevocation() == 1) {
                throw new RuntimeException("该条数据已经撤销过!");
            }
            //调用撤销方法
            RemovalOfMedal(cpMedalRecord);
            //修改字段revocation,表示已撤销
            int revocation = CommonEnum.revocation.ONE.getValue();
            cpMedalRecordMapper.updateRevocation(id, revocation);
        }
        return AjaxResult.success();
    }


    /**
     * 批量删除奖章记录
     *
     * @param ids 需要删除的奖章记录主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult deleteCpMedalRecordByIds(Long[] ids) {
        for (Long id : ids) {
            //根据id查询出当前删除奖章的信息
            CpMedalRecord cpMedalRecord = cpMedalRecordMapper.selectCpMedalRecordById(id);
            if (cpMedalRecord.getRevocation() == 1) {
                //revocation=1表示已撤销状态,只用删除记录即可
                cpMedalRecordMapper.deleteCpMedalRecordById(id);
            } else {
                //调用删除方法
                RemovalOfMedal(cpMedalRecord);
            }
            //删除奖章记录
            cpMedalRecordMapper.deleteCpMedalRecordById(id);
        }
        return AjaxResult.success();
    }

    /**
     * 删除奖章记录信息
     *
     * @param id 奖章记录主键
     * @return 结果
     */
    @Override
    public AjaxResult deleteCpMedalRecordById(Long id) {
        //根据id查询出当前删除奖章的信息
        CpMedalRecord cpMedalRecord = cpMedalRecordMapper.selectCpMedalRecordById(id);
        if (cpMedalRecord.getRevocation() == 1) {
            //revocation=1表示已撤销状态,只用删除记录即可
            cpMedalRecordMapper.deleteCpMedalRecordById(id);
        } else {
            //调用删除方法
            RemovalOfMedal(cpMedalRecord);
        }
        //删除奖章记录
        cpMedalRecordMapper.deleteCpMedalRecordById(id);

        return AjaxResult.success();
    }

    //删除和撤销奖章方法
    public void RemovalOfMedal(CpMedalRecord cpMedalRecord) {
        int newMedalNumA = cpMedalRecord.getMedalNumA();
        int newMedalNumB = cpMedalRecord.getMedalNumB();
        int newMedalNumC = cpMedalRecord.getMedalNumC();
        int newMedalNumD = cpMedalRecord.getMedalNumD();
        int newMedalNumE = cpMedalRecord.getMedalNumE();

        CpEducationEvaluation cpEducationEvaluation = cpEducationEvaluationMapper.selectMedalNum(cpMedalRecord);
        if (newMedalNumA > 0) {
            //减去A奖章
            int medalNum = cpEducationEvaluation.getMedalNumA() - newMedalNumA;
            //奖章总数减去删除的奖章
            int medalTotal = cpEducationEvaluation.getMedalTotal() - newMedalNumA;
            cpEducationEvaluation.setMedalNumA(medalNum);
            cpEducationEvaluation.setMedalTotal(medalTotal);

        }else if (newMedalNumB > 0) {
            //减去B奖章
            int medalNum = cpEducationEvaluation.getMedalNumB() - newMedalNumB;
            //奖章总数减去删除的奖章
            int medalTotal = cpEducationEvaluation.getMedalTotal() - newMedalNumB;
            cpEducationEvaluation.setMedalNumB(medalNum);
            cpEducationEvaluation.setMedalTotal(medalTotal);

        }else if (newMedalNumC > 0) {
            //减去C奖章
            int medalNum = cpEducationEvaluation.getMedalNumC() - newMedalNumC;
            //奖章总数减去删除的奖章
            int medalTotal = cpEducationEvaluation.getMedalTotal() - newMedalNumC;
            cpEducationEvaluation.setMedalNumC(medalNum);
            cpEducationEvaluation.setMedalTotal(medalTotal);

        }else if (newMedalNumD > 0) {
            //减去D奖章
            int medalNum = cpEducationEvaluation.getMedalNumD() - newMedalNumD;
            //奖章总数减去删除的奖章
            int medalTotal = cpEducationEvaluation.getMedalTotal() - newMedalNumD;
            cpEducationEvaluation.setMedalNumD(medalNum);
            cpEducationEvaluation.setMedalTotal(medalTotal);

        }else if (newMedalNumE > 0) {
            //减去E奖章
            int medalNum = cpEducationEvaluation.getMedalNumE() - newMedalNumE;
            //奖章总数减去删除的奖章
            int medalTotal = cpEducationEvaluation.getMedalTotal() - newMedalNumE;
            cpEducationEvaluation.setMedalNumE(medalNum);
            cpEducationEvaluation.setMedalTotal(medalTotal);

        }
        //更新奖章
        cpEducationEvaluationMapper.updateMedalNum(cpEducationEvaluation);
    }

    /**
     * 导出奖章记录
     *
     * @param cpMedalRecord
     * @return
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpMedalRecord> export(CpMedalRecord cpMedalRecord) {

        List<CpMedalRecord> exportList = cpMedalRecordMapper.export(cpMedalRecord);
        if (exportList.isEmpty()) {
            throw new GlobalException("没有数据");
        }
        for (CpMedalRecord medalRecord : exportList) {
            //年级
            if (!medalRecord.getGrade().isEmpty()) {
                String label = dictDataMapper.selectDictLabel("grade_list", medalRecord.getGrade());
                medalRecord.setGrade(label);
            }

            //奖章
            if (medalRecord.getMedalNumA() > 0) {
                medalRecord.setMedalName(CpMedalRecord.MedalEnum.A.getValue() + "*" + medalRecord.getMedalNumA());

            }else if (medalRecord.getMedalNumB() > 0) {
                medalRecord.setMedalName(CpMedalRecord.MedalEnum.B.getValue() + "*" + medalRecord.getMedalNumB());

            }else if (medalRecord.getMedalNumC() > 0) {
                medalRecord.setMedalName(CpMedalRecord.MedalEnum.C.getValue() + "*" + medalRecord.getMedalNumC());

            }else if (medalRecord.getMedalNumD() > 0) {
                medalRecord.setMedalName(CpMedalRecord.MedalEnum.D.getValue() + "*" + medalRecord.getMedalNumD());

            }else if (medalRecord.getMedalNumE() > 0) {
                medalRecord.setMedalName(CpMedalRecord.MedalEnum.E.getValue() + "*" + medalRecord.getMedalNumE());
            }

            //奖章类型
            if (!medalRecord.getMedalClassification().isEmpty()) {
                String medalClassification = dictDataMapper.selectDictLabel("medal_type", medalRecord.getMedalClassification());

                medalRecord.setMedalClassification(medalClassification);
            }

        }
        return exportList;
    }


}
