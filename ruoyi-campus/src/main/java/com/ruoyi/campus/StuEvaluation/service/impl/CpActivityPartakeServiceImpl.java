package com.ruoyi.campus.StuEvaluation.service.impl;

import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;
import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityPartakeMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpEducationEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityPartakeService;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 活动参与情况Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-07
 */
@Service
public class CpActivityPartakeServiceImpl implements ICpActivityPartakeService {
    @Resource
    private CpActivityPartakeMapper cpActivityPartakeMapper;
    @Resource
    private CpActivityMapper cpActivityMapper;
    @Resource
    private SysDictDataMapper sysDictDataMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private CpEducationEvaluationMapper cpEducationEvaluationMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;

    /**
     * 查询活动参与情况
     *
     * @param id 活动参与情况主键
     * @return 活动参与情况
     */
    @Override
    public CpActivityPartake selectCpActivityPartakeById(Long id) {
        return cpActivityPartakeMapper.selectCpActivityPartakeById(id);
    }

    /**
     * 查询活动参与情况列表
     *
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpActivityPartake> selectCpActivityPartakeList(CpActivityPartake cpActivityPartake) {
        return cpActivityPartakeMapper.selectCpActivityPartakeList(cpActivityPartake);
    }

    /**
     * 查询活动参与情况列表（家长端APP）
     *
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况集合
     */
    @Override
    public List<CpActivityPartake> parentAppActivityPartakeList(CpActivityPartake cpActivityPartake) {
        return cpActivityPartakeMapper.parentAppActivityPartakeList(cpActivityPartake);
    }

    /**
     * 新增活动参与情况
     *
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    @Override
    public int insertCpActivityPartake(CpActivityPartake cpActivityPartake) {
        cpActivityPartake.setCreateTime(DateUtils.getNowDate());
        return cpActivityPartakeMapper.insertCpActivityPartake(cpActivityPartake);
    }

    /**
     * 修改活动参与情况
     *
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    @Override
    public int updateCpActivityPartake(CpActivityPartake cpActivityPartake) {
        return cpActivityPartakeMapper.updateCpActivityPartake(cpActivityPartake);
    }

    /**
     * 批量删除活动参与情况
     *
     * @param ids 需要删除的活动参与情况主键
     * @return 结果
     */
    @Override
    public int deleteCpActivityPartakeByIds(Long[] ids) {
        return cpActivityPartakeMapper.deleteCpActivityPartakeByIds(ids);
    }

    /**
     * 删除活动参与情况信息
     *
     * @param id 活动参与情况主键
     * @return 结果
     */
    @Override
    public int deleteCpActivityPartakeById(Long id) {
        return cpActivityPartakeMapper.deleteCpActivityPartakeById(id);
    }

    /**
     * 上传作品图片
     *
     * @param cpActivityPartake
     * @return
     */
    @Override
    public int uploadWorks(CpActivityPartake cpActivityPartake) {

        String works = cpActivityPartake.getWorks();
        String[] imageArray = works.split(",");
        Long count = (long) imageArray.length;
        cpActivityPartake.setSchedule(count);
        cpActivityPartake.setUploadTime(new Date());
        return cpActivityPartakeMapper.updateCpActivityPartake(cpActivityPartake);
    }

    /**
     * 发放奖章
     * @param cpActivityPartake
     * @return
     */
    @Override
    @Transactional
    public int distributeMedal(CpActivityPartake cpActivityPartake) {
        //查询当前学期
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(CommonEnum.currentSemester.CURRENT.getValue(), cpActivityPartake.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }
        CpEducationEvaluation cpEducationEvaluation = cpEducationEvaluationMapper.selectMedalTotal(cpActivityPartake.getStudentNum(), cpActivityPartake.getGrade(),
                cpSemesters.getId(), cpActivityPartake.getDeptId(),
                cpActivityPartake.getClassId(), cpActivityPartake.getPeriod());

        //如果为空表示没有初始化数据,初始化即可
        if (Objects.isNull(cpEducationEvaluation)) {
            cpEducationEvaluation = CpEducationEvaluation.builder()
                    .semestersId(cpSemesters.getId())
                    .semesters(cpSemesters.getSemester())
                    .period(cpActivityPartake.getPeriod())
                    .rankS(cpActivityPartake.getRankS())
                    .grade(cpActivityPartake.getGrade())
                    .classId(cpActivityPartake.getClassId())
                    .className(cpActivityPartake.getClassName())
                    .registerId(cpActivityPartake.getRegisterId())
                    .studentNum(cpActivityPartake.getStudentNum())
                    .name(cpActivityPartake.getStudentName())
                    .revocation(CommonEnum.revocation.ZERO.getValue())
                    .userId(cpActivityPartake.getUserId())
                    .deptId(cpActivityPartake.getDeptId())
                    //奖章初始化0
                    .medalNumA(0)
                    .medalNumB(0)
                    .medalNumC(0)
                    .medalNumD(0)
                    .medalNumE(0)
                    .medalTotal(0)
                    .build();
            cpEducationEvaluationMapper.insertCpEducationEvaluation(cpEducationEvaluation);
        }

        CpActivity cpActivity = cpActivityMapper.selectCpActivityById(cpActivityPartake.getActivityId());
        String obtain = cpActivity.getObtain();
        Long obtainNumber = cpActivity.getObtainNumber();
        //查询字典获取dict_label字段的数据
        String label = sysDictDataMapper.selectDictLabel("praise_medal_type", obtain);
        CpMedalRecord cpMedalRecord = new CpMedalRecord();

        if ("品德发展水平".equals(label)) {
            cpEducationEvaluation.setMedalNumA(obtainNumber.intValue() + cpEducationEvaluation.getMedalNumA());
            cpMedalRecord.setMedalNumA(obtainNumber.intValue());

        } else if ("学业发展水平".equals(label)) {
            cpEducationEvaluation.setMedalNumB(obtainNumber.intValue() + cpEducationEvaluation.getMedalNumB());
            cpMedalRecord.setMedalNumB(obtainNumber.intValue());

        } else if ("身心发展水平".equals(label)) {
            cpEducationEvaluation.setMedalNumC(obtainNumber.intValue() + cpEducationEvaluation.getMedalNumC());
            cpMedalRecord.setMedalNumC(obtainNumber.intValue());

        } else if ("审美素养提升".equals(label)) {
            cpEducationEvaluation.setMedalNumD(obtainNumber.intValue() + cpEducationEvaluation.getMedalNumD());
            cpMedalRecord.setMedalNumD(obtainNumber.intValue());

        } else if ("劳动实践活动".equals(label)) {
            cpEducationEvaluation.setMedalNumE(obtainNumber.intValue() + cpEducationEvaluation.getMedalNumE());
            cpMedalRecord.setMedalNumE(obtainNumber.intValue());

        }
        //赋值奖章总数
        cpEducationEvaluation.setMedalTotal(cpEducationEvaluation.getMedalTotal() + obtainNumber.intValue());
        //更新五育评价数据
        cpEducationEvaluationMapper.updateCpEducationEvaluationByStudentNum(cpEducationEvaluation);

        //更新奖章记录数据
        BeanUtils.copyProperties(cpActivityPartake, cpMedalRecord);
        cpMedalRecord.setSemesters(cpSemesters.getSemester());
        cpMedalRecord.setSemestersId(cpSemesters.getId());
        cpMedalRecord.setName(cpActivityPartake.getStudentName());
        //奖章类型:"1"表示表扬章,"2"表示提示章
        cpMedalRecord.setMedalClassification("1");
        cpMedalRecord.setMedalSource("实践活动");
        cpMedalRecord.setCreateUser(SecurityUtils.getUsername());
        cpMedalRecord.setCreateTime(new Date());
        cpMedalRecord.setRevocation(CommonEnum.revocation.ZERO.getValue());
        //赋完值插入数据
        cpMedalRecordMapper.insertCpMedalRecord(cpMedalRecord);

        //更新活动参与情况
        cpActivityPartake.setStatus("2");//状态改为已阅
        cpActivityPartakeMapper.updateCpActivityPartake(cpActivityPartake);
        return 1;
    }

    /**
     * 导出
     * @param cpActivityPartake
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpActivityPartake> export(CpActivityPartake cpActivityPartake) {
        List<CpActivityPartake> cpActivityPartakes = cpActivityPartakeMapper.export(cpActivityPartake);
        if(!cpActivityPartakes.isEmpty()){
            for (CpActivityPartake activityPartake : cpActivityPartakes) {
                //活动类型
                if(Objects.nonNull(activityPartake.getActivityType())){
                    String label = sysDictDataMapper.selectDictLabel("activity_type", activityPartake.getActivityType());
                    activityPartake.setActivityType(label);
                }
                //活动项目    使用replaceAll()函数去掉<p>标签
                String activityItems = activityPartake.getActivityItems();
                if(Objects.nonNull(activityItems)){
                    String cleanedText = activityItems.replaceAll("<p>", "").replaceAll("</p>", "");
                    activityPartake.setActivityItems(cleanedText);
                }
                //年级
                if(Objects.nonNull(activityPartake.getGrade())){
                    String label = sysDictDataMapper.selectDictLabel("grade_list", activityPartake.getGrade());
                    activityPartake.setGrade(label);
                }
                //状态
                if(Objects.nonNull(activityPartake.getStatus())){
                    String label = sysDictDataMapper.selectDictLabel("participation_status", activityPartake.getStatus());
                    activityPartake.setStatus(label);
                }
            }
        }
        return cpActivityPartakes;
    }


}
