package com.ruoyi.campus.StuEvaluation.mapper;


import com.ruoyi.campus.StuEvaluation.domain.CpGradeAssessment;
import com.ruoyi.campus.domain.exportVo.ExportGradeAssessment;
import com.ruoyi.campus.homepage.domain.CpHomePage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 体质测评Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-07
 */
public interface CpGradeAssessmentMapper 
{
    /**
     * 查询体质测评
     * 
     * @param id 体质测评主键
     * @return 体质测评
     */
    public CpGradeAssessment selectCpGradeAssessmentById(Long id);

    /**
     * 查询体质测评列表
     * 
     * @param cpGradeAssessment 体质测评
     * @return 体质测评集合
     */
    public List<CpGradeAssessment> selectCpGradeAssessmentList(CpGradeAssessment cpGradeAssessment);

    /**
     * 新增体质测评
     * 
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    public int insertCpGradeAssessment( CpGradeAssessment cpGradeAssessment);

    /**
     * 修改体质测评
     * 
     * @param cpGradeAssessment 体质测评
     * @return 结果
     */
    public int updateCpGradeAssessment(CpGradeAssessment cpGradeAssessment);

    /**
     * 删除体质测评
     * 
     * @param id 体质测评主键
     * @return 结果
     */
    public int deleteCpGradeAssessmentById(Long id);

    /**
     * 批量删除体质测评
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpGradeAssessmentByIds(Long[] ids);


    List<ExportGradeAssessment> exportSelectCpGradeAssessmentList(ExportGradeAssessment exportGradeAssessment);

    /**
     * 首页体质测评
     * @param cpHomePage
     */
    List<CpGradeAssessment> selectCpGradeAssessmentStuCount(CpHomePage cpHomePage);


    /**
     * 批量新增
     * @param cpGradeAssessment1
     * @return
     */
    int batchAdd(CpGradeAssessment cpGradeAssessment1);



    // 根据学号查询是否有重复添加的学生
    int selectStudentNum(@Param("studentNum") String studentNum,@Param("grade") String grade,
                         @Param("classId") String classId,@Param("period") String period,
                         @Param("semestersId") Long semestersId, @Param("deptId") Long deptId);

}
