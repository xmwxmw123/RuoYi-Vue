package com.ruoyi.campus.StuEvaluation.service;


import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.domain.exportVo.ExportActivity;

import java.util.List;

/**
 * 活动列表Service接口
 * 
 * @author ruoyi
 * @date 2023-08-09
 */
public interface ICpActivityService 
{
    /**
     * 查询活动列表
     * 
     * @param id 活动列表主键
     * @return 活动列表
     */
    public CpActivity selectCpActivityById(Long id);

    /**
     * 查询活动列表列表
     * 
     * @param cpActivity 活动列表
     * @return 活动列表集合
     */
    public List<CpActivity> selectCpActivityList(CpActivity cpActivity);

    public List<CpActivity> selectCpActivityList(List<CpActivity> cpActivityList);

    /**
     * 新增活动列表
     * 
     * @param cpActivity 活动列表
     * @return 结果
     */
    public int insertCpActivity(CpActivity cpActivity);


    /**
     * 修改活动列表
     * 
     * @param cpActivity 活动列表
     * @return 结果
     */
    public int updateCpActivity(CpActivity cpActivity);

    /**
     * 批量删除活动列表
     * 
     * @param ids 需要删除的活动列表主键集合
     * @return 结果
     */
    public int deleteCpActivityByIds(Long[] ids);

    /**
     * 删除活动列表信息
     * 
     * @param id 活动列表主键
     * @return 结果
     */
    public int deleteCpActivityById(Long id);

    /**
     * 根据届别id查询班级
     * @param
     * @return
     */
    String selectRankById(String period);


    /* 导出列表*/
    List<ExportActivity> exportCpActivityList(ExportActivity exportActivity);

    /**
     * 查询活动列表教师APP
     */
    List<CpActivity> appList(CpActivity cpActivity);

    /**
     * 查询活动列表家长端APP
     */
    List<CpActivity> parentAppPracticeList(CpActivity cpActivity);
}
