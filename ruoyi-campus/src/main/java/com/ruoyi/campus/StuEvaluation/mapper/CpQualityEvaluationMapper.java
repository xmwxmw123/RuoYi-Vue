package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpQualityEvaluation;
import com.ruoyi.campus.domain.exportVo.ExportQualityEvaluation;
import com.ruoyi.campus.register.domain.CpRegister;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 综合素质评价Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-02
 */
public interface CpQualityEvaluationMapper 
{
    /**
     * 查询综合素质评价
     * 
     * @param id 综合素质评价主键
     * @return 综合素质评价
     */
    public CpQualityEvaluation selectCpQualityEvaluationById(Long id);

    /**
     * 查询综合素质评价列表(PC端和教师APP端)
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 综合素质评价集合
     */
    public List<CpQualityEvaluation> selectCpQualityEvaluationList(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 查询综合素质评价列表(家长端)
     */
    List<CpQualityEvaluation> parentAppQuyCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 新增综合素质评价
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    public int insertCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 修改综合素质评价
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    public int updateCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 删除综合素质评价
     * 
     * @param id 综合素质评价主键
     * @return 结果
     */
    public int deleteCpQualityEvaluationById(Long id);

    /**
     * 批量删除综合素质评价
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpQualityEvaluationByIds(Long[] ids);

    /**
     * 批量新增
     * @param cpRegisters
     * @return
     */
    int insertCpQualityEvaluationBatchAdd(List<CpRegister> cpRegisters);

    /**
     * 使用count记数判断学生是否已经添加过
     * @param studentNum
     * @return
     */

    int selectStuCount(@Param("studentNum") String studentNum,@Param("grade") String grade,
                       @Param("classId") String classId,@Param("period") String period,
                       @Param("semestersId") Long semestersId, @Param("deptId") Long deptId);

    /* 导出 */
    List<ExportQualityEvaluation> exportCpQualityEvaluationList(ExportQualityEvaluation exportQualityEvaluation);


}
