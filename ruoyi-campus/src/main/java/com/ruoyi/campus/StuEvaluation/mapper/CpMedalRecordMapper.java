package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.homepage.domain.CpHomePageMedalVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 奖章记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface CpMedalRecordMapper 
{
    /**
     * 查询奖章记录
     * 
     * @param id 奖章记录主键
     * @return 奖章记录
     */
    public CpMedalRecord selectCpMedalRecordById(Long id);

    /**
     * 查询奖章记录列表(PC端和教师APP)
     * 
     * @param cpMedalRecord 奖章记录
     * @return 奖章记录集合
     */
    public List<CpMedalRecord> selectCpMedalRecordList(CpMedalRecord cpMedalRecord);

    /**
     * 查询奖章记录列表(家长端)
     */
    List<CpMedalRecord> parentAppQuyCpMedalRecord(CpMedalRecord cpMedalRecord);

    /**
     * 新增奖章记录
     * 
     * @param cpMedalRecord 奖章记录
     * @return 结果
     */
    public int insertCpMedalRecord(CpMedalRecord cpMedalRecord);

    /**
     * 修改奖章记录
     * 
     * @param cpMedalRecord 奖章记录
     * @return 结果
     */
    public int updateCpMedalRecord(CpMedalRecord cpMedalRecord);

    /**
     * 删除奖章记录
     * 
     * @param id 奖章记录主键
     * @return 结果
     */
    public int deleteCpMedalRecordById(Long id);

    /**
     * 批量删除奖章记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpMedalRecordByIds(Long[] ids);

    /**
     * 修改revocation字段,1表示已撤销
     * @param id
     */
    @Update("update cp_medal_record set revocation=#{revocation} where id=#{id}")
    void updateRevocation(@Param("id") Long id, @Param("revocation") int revocation);

    /**
     * 导出奖章记录
     * @param cpMedalRecord
     */
    List<CpMedalRecord> export(CpMedalRecord cpMedalRecord);

    /**
     * 首页查出本学期奖章数
     */
    CpHomePageMedalVo selectStuMedal1(@Param("semestersId") Long semestersId,@Param("classId") Long classId, @Param("grade") String grade, @Param("deptId") Long deptId);

    /**
     * 首页查出本周奖章数
     */
    CpHomePageMedalVo selectStuMedal2(@Param("classId") Long classId, @Param("grade") String grade, @Param("deptId") Long deptId);

    /**
     * 首页查出本月奖章数
     */
    CpHomePageMedalVo selectStuMedal3(@Param("classId") Long classId, @Param("grade") String grade, @Param("deptId") Long deptId);

    /**
     * 转班后修改revocation为已转班
     */
    void updateCpRevocation(@Param("grade") String grade, @Param("classId") String classId, @Param("studentNum")String studentNum, @Param("period")String period,
                            @Param("deptId")Long deptId, @Param("revocation")int revocation);

}
