package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpScoreInquiry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryDto;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 成绩查询(用于存放各个班级里学生的成绩)Service接口
 * 
 * @author ruoyi
 * @date 2023-08-14
 */
public interface ICpScoreInquiryService 
{
    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 成绩查询(用于存放各个班级里学生的成绩)
     */
    public CpScoreInquiry selectCpScoreInquiryById(Long id);

    /**
     * 查询成绩查询(用于存放各个班级里学生的成绩)列表
     * 
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 成绩查询(用于存放各个班级里学生的成绩)集合
     */
    public List<CpScoreInquiry> selectCpScoreInquiryList(CpScoreInquiry cpScoreInquiry);

    /**
     * 查询分页逻辑
     * @param cpScoreInquiry 参数
     * @param cpScoreInquiryList 集合
     * @return 集合
     */
    public List<CpScoreInquiryDto> handleCpScoreList(CpScoreInquiry cpScoreInquiry, List<CpScoreInquiry> cpScoreInquiryList);

    /**
     * 新增成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param cpScoreInquiry 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    public int insertCpScoreInquiry(CpScoreInquiry cpScoreInquiry);

    /**
     * 修改成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param   cpScoreInquiryUpdateDto 成绩查询(用于存放各个班级里学生的成绩)
     * @return 结果
     */
    public AjaxResult updateCpScoreInquiry(CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto);

    /**
     * 批量删除成绩查询(用于存放各个班级里学生的成绩)
     * 
     * @param ids 需要删除的成绩查询(用于存放各个班级里学生的成绩)主键集合
     * @return 结果
     */
    public int deleteCpScoreInquiryByIds(Long[] ids);

    /**
     * 删除成绩查询(用于存放各个班级里学生的成绩)信息
     * 
     * @param id 成绩查询(用于存放各个班级里学生的成绩)主键
     * @return 结果
     */
    public int deleteCpScoreInquiryById(Long id);

    /**
     * 开始录入接口
     * @param cpScoreInquiry
     * @return
     */
    AjaxResult insertScoreInquiry(CpScoreInquiry cpScoreInquiry);

    /**
     * 修改录入状态
     * @param scoreEntryId
     * @return
     */
    AjaxResult updateStatus(Long scoreEntryId);

    /**
     * 导出
     * @param cpScoreInquiry
     * @return
     */
    List<CpScoreInquiryDto> export(CpScoreInquiryDto cpScoreInquiry);
}
