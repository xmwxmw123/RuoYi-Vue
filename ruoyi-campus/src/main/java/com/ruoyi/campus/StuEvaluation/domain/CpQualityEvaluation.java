package com.ruoyi.campus.StuEvaluation.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

/**
 * 综合素质评价对象 cp_quality_evaluation
 * 
 * @author ruoyi
 * @date 2023-08-02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpQualityEvaluation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 综合素质评价表id */
    private Long id;

    /** 学期id */
    private Integer semestersId;

    /** 学期 */
    @Excel(name = "学期")
    private String semesters;

    /** 届别id */
    private String period;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级名称id */
    private String classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String name;

    /** 德育量化 */
    @NotBlank(message = "德育量化不能为空")
    @DecimalMin(value = "1.0", inclusive = true, message = "德育量化必须大于等于1")
    @DecimalMax(value = "10.0", inclusive = true, message = "德育量化必须小于等于10")
    @Excel(name = "德育量化")
    private String moralism;

    /** 德育等级 */
    @NotBlank(message = "德育等级不能为空")
    @Excel(name = "德育等级")
    private String moralismGrade;

    /** 班主任id */
    @Excel(name = "班主任id")
    private int teacherId;

    /** 班主任 */
    @Excel(name = "班主任")
    private String teacher;

    /** 班主任评语 */
    @NotBlank(message = "班主任评语不能为空")
    @Excel(name = "班主任评语")
    private String teacherMessage;

    /** 家长寄语 */
    @Excel(name = "家长寄语")
    private String parentMessage;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 创建人 */
    private String createUser;

    /** 最后修改人 */
    private String updateUser;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
