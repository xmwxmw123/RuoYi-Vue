package com.ruoyi.campus.StuEvaluation.mapper;

import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 五育评价Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface CpEducationEvaluationMapper 
{
    /**
     * 查询五育评价
     * 
     * @param id 五育评价主键
     * @return 五育评价
     */
    public CpEducationEvaluation selectCpEducationEvaluationById(Long id);

    /**
     * 查询五育评价列表
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 五育评价集合
     */
    public List<CpEducationEvaluation> selectCpEducationEvaluationList(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 新增五育评价
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    public int insertCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 修改五育评价
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    public int updateCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 根据学号修改奖章
     * @param cpEducationEvaluation
     */
    int updateCpEducationEvaluationByStudentNum(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 删除五育评价
     * 
     * @param id 五育评价主键
     * @return 结果
     */
    public int deleteCpEducationEvaluationById(Long id);

    /**
     * 批量删除五育评价
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpEducationEvaluationByIds(Long[] ids);

    /**
     * 查询奖章表学生个数
     * @return
     */
    @Select("select count(1) from cp_medal_record where student_num=#{studentNum} and dept_id = #{deptId}")
    int selectStu(@Param("studentNum") String studentNum, @Param("deptId") Long deptId);

    /**
     * 根据条件查奖章数量
     * @param studentNum
     * @param grade
     * @param semestersId
     */
    CpEducationEvaluation selectMedalTotal(@Param("studentNum") String studentNum, @Param("grade") String grade, @Param("semestersId") Long semestersId,
                                           @Param("deptId") Long deptId, @Param("classId") String classId, @Param("period") String period);

    /**
     * 查出奖章数量
     */

    CpEducationEvaluation selectMedalNum(CpMedalRecord cpMedalRecord);

    /**
     * 撤销删除奖章,通用
     */
    void updateMedalNum(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 删除五育表原本的数据
     */
    void deleteStudent(@Param("classId") String classId, @Param("registerId")Long registerId,
                       @Param("period")String period, @Param("semestersId")Long semestersId, @Param("deptId")Long deptId);

    List<CpEducationEvaluation> selectCpEducationEvaluationStudent( @Param("grade") String grade, @Param("classId") String classId,
                                                                    @Param("studentNum") String studentNum, @Param("period") String period, @Param("deptId") Long deptId);
}
