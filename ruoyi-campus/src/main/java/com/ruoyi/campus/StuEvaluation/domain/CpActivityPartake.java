package com.ruoyi.campus.StuEvaluation.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 活动参与情况对象 cp_activity_partake
 * 
 * @author ruoyi
 * @date 2023-10-07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpActivityPartake extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 实践活动id */
    private Long activityId;

    //活动主题
    @Excel(name = "活动主题")
    private String topic;

    //活动类型
    @Excel(name = "活动类型")
    private String activityType;

    //活动项目
    @Excel(name = "活动项目")
    private String activityItems;

    /** 学籍id */
    private Long registerId;

    //级别id
    private String period;

    /** 级别 */
    @Excel(name = "级别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    //班级id
    private String classId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    //学号
    @Excel(name = "学号")
    private String studentNum;

    //学生姓名
    @Excel(name = "学生姓名")
    private String studentName;

    //获奖条件作品个数
    private Long wokesNumber;

    //作品个数进度
    private Long schedule;

    /** 活动照片 */
    private String works;

    /** 状态:1待审,2已阅 */
    @Excel(name = "状态")
    private String status;

    //上传时间
    @Excel(name = "上传时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date uploadTime;

    /** 用户id */
    private Long userId;

    /** 部门id */
    private Long deptId;

}
