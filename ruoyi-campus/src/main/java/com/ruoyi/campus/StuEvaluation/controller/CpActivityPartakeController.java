package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityPartakeService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 活动参与情况Controller
 * 
 * @author ruoyi
 * @date 2023-10-07
 */
@RestController
@RequestMapping("/activityPartake/activityPartake")
public class CpActivityPartakeController extends BaseController
{
    @Autowired
    private ICpActivityPartakeService cpActivityPartakeService;

    /**
     * 查询活动参与情况列表
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpActivityPartake cpActivityPartake)
    {
        startPage();
        List<CpActivityPartake> list = cpActivityPartakeService.selectCpActivityPartakeList(cpActivityPartake);
        return getDataTable(list);
    }

    /**
     * 导出活动参与情况列表
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:export')")
    @Log(title = "活动参与情况", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpActivityPartake cpActivityPartake)
    {
        List<CpActivityPartake> list = cpActivityPartakeService.export(cpActivityPartake);
        ExcelUtil<CpActivityPartake> util = new ExcelUtil<CpActivityPartake>(CpActivityPartake.class);
        util.exportExcel(response, list, "活动参与情况数据");
    }

    /**
     * 获取活动参与情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpActivityPartakeService.selectCpActivityPartakeById(id));
    }

    /**
     * 新增活动参与情况
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:add')")
    @Log(title = "活动参与情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpActivityPartake cpActivityPartake)
    {
        return toAjax(cpActivityPartakeService.insertCpActivityPartake(cpActivityPartake));
    }

    /**
     * 修改活动参与情况
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:edit')")
    @Log(title = "活动参与情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpActivityPartake cpActivityPartake)
    {
        return toAjax(cpActivityPartakeService.updateCpActivityPartake(cpActivityPartake));
    }

    /**
     * 删除活动参与情况
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:remove')")
    @Log(title = "活动参与情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpActivityPartakeService.deleteCpActivityPartakeByIds(ids));
    }

    /**
     * 上传作品图片
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:edit')")
    @Log(title = "上传作品图片", businessType = BusinessType.UPDATE)
    @PutMapping("/uploadWorks")
    public AjaxResult uploadWorks(@RequestBody CpActivityPartake cpActivityPartake)
    {
        return toAjax(cpActivityPartakeService.uploadWorks(cpActivityPartake));
    }

    /**
     * 发放奖章
     */
    @PreAuthorize("@ss.hasPermi('activityPartake:activityPartake:edit')")
    @Log(title = "活动参与情况", businessType = BusinessType.UPDATE)
    @PutMapping("/distributeMedal")
    public AjaxResult distributeMedal(@RequestBody CpActivityPartake cpActivityPartake)
    {
        cpActivityPartake.setDeptId(getDeptId());
        cpActivityPartake.setUserId(getUserId());
        return toAjax(cpActivityPartakeService.distributeMedal(cpActivityPartake));
    }

}
