package com.ruoyi.campus.StuEvaluation.domain.dto;

import com.ruoyi.campus.StuEvaluation.domain.CpScoreInquiry;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpScoreInquiryUpdateDto {

    //满分
    private String fullMark;

    //成绩表id
    private int id;

    //成绩表对象
    private List<CpScoreInquiry> arr;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;

}
