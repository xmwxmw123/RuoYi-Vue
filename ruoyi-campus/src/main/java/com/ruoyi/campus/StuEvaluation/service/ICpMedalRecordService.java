package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 奖章记录Service接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface ICpMedalRecordService 
{
    /**
     * 查询奖章记录
     * 
     * @param id 奖章记录主键
     * @return 奖章记录
     */
    public CpMedalRecord selectCpMedalRecordById(Long id);

    /**
     * 查询奖章记录列表(PC端和教师APP)
     * 
     * @param cpMedalRecord 奖章记录
     * @return 奖章记录集合
     */
    public List<CpMedalRecord> selectCpMedalRecordList(CpMedalRecord cpMedalRecord);

    /**
     * 查询当前学生奖章记录(家长端app用)
     */
    List<CpMedalRecord> parentAppQuyCpMedalRecord(CpMedalRecord cpMedalRecord);

    /**
     * 新增奖章记录
     * 
     * @param cpMedalRecord 奖章记录
     * @return 结果
     */
    public int insertCpMedalRecord(CpMedalRecord cpMedalRecord);

    /**
     * 撤销奖章记录
     * 
     * @param id 奖章记录
     * @return 结果
     */
    public AjaxResult updateCpMedalRecord(Long id);

    /**
     * 批量删除奖章记录
     * 
     * @param ids 需要删除的奖章记录主键集合
     * @return 结果
     */
    public AjaxResult deleteCpMedalRecordByIds(Long[] ids);

    /**
     * 删除奖章记录信息
     * 
     * @param id 奖章记录主键
     * @return 结果
     */
    public AjaxResult deleteCpMedalRecordById(Long id);

    /**
     * 导出奖章记录
     * @param cpMedalRecord
     * @return
     */
    List<CpMedalRecord> export(CpMedalRecord cpMedalRecord);

    /**
     * 批量撤销奖章
     * @param ids
     * @return
     */
    AjaxResult batchRevoke(Long[] ids);
}
