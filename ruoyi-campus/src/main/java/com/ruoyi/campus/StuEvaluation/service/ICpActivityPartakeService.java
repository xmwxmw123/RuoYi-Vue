package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;

import java.util.List;

/**
 * 活动参与情况Service接口
 * 
 * @author ruoyi
 * @date 2023-10-07
 */
public interface ICpActivityPartakeService 
{
    /**
     * 查询活动参与情况
     * 
     * @param id 活动参与情况主键
     * @return 活动参与情况
     */
    public CpActivityPartake selectCpActivityPartakeById(Long id);

    /**
     * 查询活动参与情况列表（PC和教师APP）
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况集合
     */
    public List<CpActivityPartake> selectCpActivityPartakeList(CpActivityPartake cpActivityPartake);

    /**
     * 查询活动参与情况列表（家长端APP）
     *
     * @param cpActivityPartake 活动参与情况
     * @return 活动参与情况集合
     */
    List<CpActivityPartake> parentAppActivityPartakeList(CpActivityPartake cpActivityPartake);

    /**
     * 新增活动参与情况
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    public int insertCpActivityPartake(CpActivityPartake cpActivityPartake);

    /**
     * 修改活动参与情况
     * 
     * @param cpActivityPartake 活动参与情况
     * @return 结果
     */
    public int updateCpActivityPartake(CpActivityPartake cpActivityPartake);

    /**
     * 批量删除活动参与情况
     * 
     * @param ids 需要删除的活动参与情况主键集合
     * @return 结果
     */
    public int deleteCpActivityPartakeByIds(Long[] ids);

    /**
     * 删除活动参与情况信息
     * 
     * @param id 活动参与情况主键
     * @return 结果
     */
    public int deleteCpActivityPartakeById(Long id);

    /**
     * 上传作品图片
     * @param cpActivityPartake
     * @return
     */
    int uploadWorks(CpActivityPartake cpActivityPartake);

    /**
     * 发放奖章
     * @param cpActivityPartake
     * @return
     */
    int distributeMedal(CpActivityPartake cpActivityPartake);

    /**
     * 导出
     * @param cpActivityPartake
     * @return
     */
    List<CpActivityPartake> export(CpActivityPartake cpActivityPartake);


}
