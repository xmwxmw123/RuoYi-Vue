package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpBatchDto;
import com.ruoyi.campus.StuEvaluation.service.ICpEducationEvaluationService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 五育评价Controller
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@RestController
@RequestMapping("/educationEvaluation/educationEvaluation")
public class CpEducationEvaluationController extends BaseController
{
    @Autowired
    private ICpEducationEvaluationService cpEducationEvaluationService;

    /**
     * 查询五育评价列表
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpEducationEvaluation cpEducationEvaluation)
    {
        startPage();
        cpEducationEvaluation.setDeptId(getDeptId());
        cpEducationEvaluation.setUserId(getUserId());
        List<CpEducationEvaluation> list = cpEducationEvaluationService.selectCpEducationEvaluationList(cpEducationEvaluation);
        return getDataTable(list);
    }

    /**
     * 导出五育评价列表
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:export')")
    @Log(title = "五育评价", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpEducationEvaluation cpEducationEvaluation)
    {
        cpEducationEvaluation.setUserId(getUserId());
        cpEducationEvaluation.setDeptId(getDeptId());
        List<CpEducationEvaluation> list = cpEducationEvaluationService.selectCpEducationEvaluationList(cpEducationEvaluation);
        ExcelUtil<CpEducationEvaluation> util = new ExcelUtil<CpEducationEvaluation>(CpEducationEvaluation.class);
        util.exportExcel(response, list, "五育评价数据");
    }

    /**
     * 获取五育评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpEducationEvaluationService.selectCpEducationEvaluationById(id));
    }

    /**
     * 新增五育评价
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:add')")
    @Log(title = "五育评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpEducationEvaluation cpEducationEvaluation)
    {
        cpEducationEvaluation.setUserId(getUserId());
        cpEducationEvaluation.setDeptId(getDeptId());
        return toAjax(cpEducationEvaluationService.insertCpEducationEvaluation(cpEducationEvaluation));
    }

    /**
     * 修改五育评价
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:edit')")
    @Log(title = "五育评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpEducationEvaluation cpEducationEvaluation)
    {
        cpEducationEvaluation.setDeptId(getDeptId());
        return toAjax(cpEducationEvaluationService.updateCpEducationEvaluation(cpEducationEvaluation));
    }

    /**
     * 批量修改五育评价
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:edit')")
    @Log(title = "五育评价", businessType = BusinessType.UPDATE)
    @PutMapping("/batch")
    public AjaxResult editBatch(@RequestBody CpBatchDto cpBatchDto)
    {
        cpBatchDto.setUserId(getUserId());
        cpBatchDto.setDeptId(getDeptId());
        return AjaxResult.success(cpEducationEvaluationService.updateCpEducationEvaluationBatch(cpBatchDto));
    }

    /**
     * 删除五育评价
     */
    @PreAuthorize("@ss.hasPermi('educationEvaluation:educationEvaluation:remove')")
    @Log(title = "五育评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpEducationEvaluationService.deleteCpEducationEvaluationByIds(ids));
    }
}
