package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpGradeAssessment;
import com.ruoyi.campus.StuEvaluation.service.ICpGradeAssessmentService;
import com.ruoyi.campus.domain.exportVo.ExportGradeAssessment;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 体质测评Controller
 *
 * @author ruoyi
 * @date 2023-08-07
 */
@RestController
@RequestMapping("/gradeAssessment/gradeAssessment")
public class CpGradeAssessmentController extends BaseController {
    @Resource
    private ICpGradeAssessmentService cpGradeAssessmentService;

    /**
     * 查询体质测评列表
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:list')")
    @GetMapping("/list")

    public TableDataInfo list(CpGradeAssessment cpGradeAssessment) {
        startPage();
        List<CpGradeAssessment> list = cpGradeAssessmentService.selectCpGradeAssessmentList(cpGradeAssessment);
        return getDataTable(list);
    }

    /**
     * 导出体质测评列表
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:export')")
    @Log(title = "体质测评", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportGradeAssessment exportGradeAssessment) {
        List<ExportGradeAssessment> list = cpGradeAssessmentService.exportSelectCpGradeAssessmentList(exportGradeAssessment);
        ExcelUtil<ExportGradeAssessment> util = new ExcelUtil<ExportGradeAssessment>(ExportGradeAssessment.class);
        util.exportExcel(response, list, "体质测评数据");
    }

    /**
     * 获取体质测评详细信息
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(cpGradeAssessmentService.selectCpGradeAssessmentById(id));
    }

    /**
     * 新增体质测评
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:add')")
    @Log(title = "体质测评", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpGradeAssessment cpGradeAssessment) {
        cpGradeAssessment.setUserId(getUserId());
        cpGradeAssessment.setDeptId(getDeptId());
        return toAjax(cpGradeAssessmentService.insertCpGradeAssessment(cpGradeAssessment));
    }

    /**
     * 修改体质测评
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:edit')")
    @Log(title = "体质测评", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpGradeAssessment cpGradeAssessment) {
        return toAjax(cpGradeAssessmentService.updateCpGradeAssessment(cpGradeAssessment));
    }

    /**
     * 删除体质测评
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:remove')")
    @Log(title = "体质测评", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(cpGradeAssessmentService.deleteCpGradeAssessmentByIds(ids));
    }

    /**
     * 批量新增
     */
    @PreAuthorize("@ss.hasPermi('gradeAssessment:gradeAssessment:add')")
    @Log(title = "体质测评", businessType = BusinessType.INSERT)
    @PostMapping("/batchAdd")
    public AjaxResult batchAdd(@RequestBody CpGradeAssessment cpGradeAssessment) {
        cpGradeAssessment.setUserId(getUserId());
        cpGradeAssessment.setDeptId(getDeptId());
        return AjaxResult.success(cpGradeAssessmentService.batchAdd(cpGradeAssessment));
    }

}
