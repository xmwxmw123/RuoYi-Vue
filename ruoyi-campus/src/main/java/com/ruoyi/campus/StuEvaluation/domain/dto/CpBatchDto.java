package com.ruoyi.campus.StuEvaluation.domain.dto;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 五育评价对象 cp_education_evaluation
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CpBatchDto extends BaseEntity
{
    /** 学号 */
    @Excel(name = "学号")
    private String[] studentNum;

    /** A奖章数量 */
    private int medalNumA;

    /** B奖章数量 */
    private int medalNumB;

    /** C奖章数量 */
    private int medalNumC;

    /** D奖章数量 */
    private int medalNumD;

    /** E奖章数量 */
    private int medalNumE;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;
}
