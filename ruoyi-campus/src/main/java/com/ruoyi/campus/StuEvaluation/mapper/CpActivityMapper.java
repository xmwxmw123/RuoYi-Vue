package com.ruoyi.campus.StuEvaluation.mapper;


import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.domain.exportVo.ExportActivity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 活动列表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-09
 */
public interface CpActivityMapper 
{
    /**
     * 查询活动列表
     * 
     * @param id 活动列表主键
     * @return 活动列表
     */
    public CpActivity selectCpActivityById(Long id);

    /**
     * 查询活动列表教师APP
     */
    List<CpActivity> appList(CpActivity cpActivity);

    /**
     * 查询活动列表家长端APP
     */
    List<CpActivity> parentAppPracticeList(CpActivity cpActivity);

    /**
     * 查询活动列表列表
     * 
     * @param cpActivity 活动列表
     * @return 活动列表集合
     */
    public List<CpActivity> selectCpActivityList(CpActivity cpActivity);

    /**
     * 新增活动列表
     * 
     * @param cpActivity 活动列表
     * @return 结果
     */
    public int insertCpActivity(CpActivity cpActivity);

    /**
     * 修改活动列表
     * 
     * @param cpActivity 活动列表
     * @return 结果
     */
    public int updateCpActivity(CpActivity cpActivity);

    /**
     * 删除活动列表
     * 
     * @param id 活动列表主键
     * @return 结果
     */
    public int deleteCpActivityById(Long id);

    /**
     * 批量删除活动列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpActivityByIds(Long[] ids);


    /* 判断主题是否唯一 */
    @Select("SELECT count(*) FROM cp_activity WHERE topic = #{topic} and dept_id = #{deptId}")
    Integer countCpActivityByTopic(@Param("topic") String topic, @Param("deptId") Long deptId);

    /*根据届别id查所有活动列表*/
    @Select("select * from cp_activity where period=#{period} and dept_id = #{deptId}")
    List<CpActivity> selectCpActivityByPeriod(@Param("period") String period, @Param("deptId") Long deptId);

    /* 导出活动列表*/
    List<ExportActivity> exportSelectCpActivityList(ExportActivity exportActivity);

    /**
     * 查询出正在进行的活动
     */
    @Select("select count(*) from cp_activity where end_time > CURRENT_DATE and dept_id = #{deptId}")
    int selectActivityCount1(@Param("deptId") Long deptId);

    /**
     * 查询本周创建的活动
     */
    int selectActivityCount2(@Param("deptId") Long deptId);

    /**
     * 查询本月创建的活动
     */
    int selectActivityCount3(@Param("deptId") Long deptId);


}
