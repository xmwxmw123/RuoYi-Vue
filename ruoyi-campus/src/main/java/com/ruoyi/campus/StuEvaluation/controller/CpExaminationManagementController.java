package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpExaminationManagement;
import com.ruoyi.campus.StuEvaluation.domain.CpScoreEntry;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpScoreInquiryUpdateDto;
import com.ruoyi.campus.StuEvaluation.service.ICpExaminationManagementService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 考试管理Controller
 * 
 * @author ruoyi
 * @date 2023-08-13
 */
@RestController
@RequestMapping("/examinationManagement/examinationManagement")
public class CpExaminationManagementController extends BaseController
{
    @Autowired
    private ICpExaminationManagementService cpExaminationManagementService;

    /**
     * 查询考试管理列表
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpExaminationManagement cpExaminationManagement)
    {
        startPage();
        List<CpExaminationManagement> list = cpExaminationManagementService.selectCpExaminationManagementList(cpExaminationManagement);
        return getDataTable(list);
    }

    /**
     * 导出考试管理列表
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:export')")
    @Log(title = "考试管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpExaminationManagement cpExaminationManagement)
    {
        List<CpExaminationManagement> list = cpExaminationManagementService.export(cpExaminationManagement);
        ExcelUtil<CpExaminationManagement> util = new ExcelUtil<CpExaminationManagement>(CpExaminationManagement.class);
        util.exportExcel(response, list, "考试管理数据");
    }

    /**
     * 获取考试管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpExaminationManagementService.selectCpExaminationManagementById(id));
    }

    /**
     * 新增考试管理
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:add')")
    @Log(title = "考试管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpExaminationManagement cpExaminationManagement)
    {
        cpExaminationManagement.setUserId(getUserId());
        cpExaminationManagement.setDeptId(getDeptId());
        return toAjax(cpExaminationManagementService.insertCpExaminationManagement(cpExaminationManagement));
    }

    /**
     * 修改考试管理
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:edit')")
    @Log(title = "考试管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpExaminationManagement cpExaminationManagement)
    {
        return toAjax(cpExaminationManagementService.updateCpExaminationManagement(cpExaminationManagement));
    }

    /**
     * 删除考试管理
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:remove')")
    @Log(title = "考试管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpExaminationManagementService.deleteCpExaminationManagementByIds(ids));
    }

    /**
     * 查询成绩录入列表
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:list')")
    @GetMapping("/scoreEntry/list")
    public TableDataInfo scoreEntryList(CpScoreEntry cpScoreEntry)
    {
        cpScoreEntry.setDeptId(getDeptId());
        cpScoreEntry.setUserId(getUserId());
        cpExaminationManagementService.insCpScoreEntry(cpScoreEntry);
        startPage();
        List<CpScoreEntry> list = cpExaminationManagementService.selectCpScoreEntryList(cpScoreEntry);
        return getDataTable(list);
    }

    /**
     * 录入满分接口
     */
    @PreAuthorize("@ss.hasPermi('examinationManagement:examinationManagement:add')")
    @PutMapping("/fullMark")
    public AjaxResult updateFullMark(@RequestBody CpScoreInquiryUpdateDto cpScoreInquiryUpdateDto)
    {
        return cpExaminationManagementService.updateFullMark(cpScoreInquiryUpdateDto);
    }

}
