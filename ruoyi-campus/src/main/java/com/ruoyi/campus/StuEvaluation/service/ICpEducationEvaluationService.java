package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.domain.dto.CpBatchDto;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 五育评价Service接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface ICpEducationEvaluationService 
{
    /**
     * 查询五育评价
     * 
     * @param id 五育评价主键
     * @return 五育评价
     */
    public CpEducationEvaluation selectCpEducationEvaluationById(Long id);

    /**
     * 查询五育评价列表
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 五育评价集合
     */
    public List<CpEducationEvaluation> selectCpEducationEvaluationList(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 新增五育评价
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    public int insertCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 修改五育评价
     * 
     * @param cpEducationEvaluation 五育评价
     * @return 结果
     */
    public int updateCpEducationEvaluation(CpEducationEvaluation cpEducationEvaluation);

    /**
     * 批量修改五育评价
     * @param cpBatchDto
     * @return
     */
    AjaxResult updateCpEducationEvaluationBatch(CpBatchDto cpBatchDto);

    /**
     * 批量删除五育评价
     * 
     * @param ids 需要删除的五育评价主键集合
     * @return 结果
     */
    public int deleteCpEducationEvaluationByIds(Long[] ids);

    /**
     * 删除五育评价信息
     * 
     * @param id 五育评价主键
     * @return 结果
     */
    public int deleteCpEducationEvaluationById(Long id);


}
