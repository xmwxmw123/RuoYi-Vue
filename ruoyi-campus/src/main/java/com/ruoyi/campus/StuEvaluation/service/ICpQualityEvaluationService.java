package com.ruoyi.campus.StuEvaluation.service;

import com.ruoyi.campus.StuEvaluation.domain.CpQualityEvaluation;
import com.ruoyi.campus.domain.exportVo.ExportQualityEvaluation;

import java.util.List;

/**
 * 综合素质评价Service接口
 * 
 * @author ruoyi
 * @date 2023-08-02
 */
public interface ICpQualityEvaluationService 
{
    /**
     * 查询综合素质评价
     * 
     * @param id 综合素质评价主键
     * @return 综合素质评价
     */
    public CpQualityEvaluation selectCpQualityEvaluationById(Long id);

    /**
     * 查询综合素质评价列表(PC端和教师APP端)
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 综合素质评价集合
     */
    public List<CpQualityEvaluation> selectCpQualityEvaluationList(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 查询综合素质评价列表(家长端)
     */
    List<CpQualityEvaluation> parentAppQuyCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 新增综合素质评价
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    public int insertCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 修改综合素质评价
     * 
     * @param cpQualityEvaluation 综合素质评价
     * @return 结果
     */
    public int updateCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 更新家长寄语
     */
    void updParentMessage(CpQualityEvaluation cpQualityEvaluation);

    /**
     * 批量删除综合素质评价
     * 
     * @param ids 需要删除的综合素质评价主键集合
     * @return 结果
     */
    public int deleteCpQualityEvaluationByIds(Long[] ids);

    /**
     * 删除综合素质评价信息
     * 
     * @param id 综合素质评价主键
     * @return 结果
     */
    public int deleteCpQualityEvaluationById(Long id);

    /**
     * 批量新增
     * @param cpQualityEvaluation
     * @return
     */
    String insertCpQualityEvaluationBatchAdd(CpQualityEvaluation cpQualityEvaluation);

    /* 导出 */
    List<ExportQualityEvaluation> exportCpQualityEvaluationList(ExportQualityEvaluation exportQualityEvaluation);
}
