package com.ruoyi.campus.StuEvaluation.controller;

import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.StuEvaluation.service.ICpMedalRecordService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 奖章记录Controller
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@RestController
@RequestMapping("/medalRecord/medalRecord")
public class CpMedalRecordController extends BaseController
{
    @Autowired
    private ICpMedalRecordService cpMedalRecordService;

    /**
     * 查询奖章记录列表
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpMedalRecord cpMedalRecord)
    {
        startPage();
        List<CpMedalRecord> list = cpMedalRecordService.selectCpMedalRecordList(cpMedalRecord);
        return getDataTable(list);
    }

    /**
     * 导出奖章记录列表
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:export')")
    @Log(title = "奖章记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpMedalRecord cpMedalRecord)
    {
        List<CpMedalRecord> list = cpMedalRecordService.export(cpMedalRecord);
        ExcelUtil<CpMedalRecord> util = new ExcelUtil<CpMedalRecord>(CpMedalRecord.class);
        util.exportExcel(response, list, "奖章记录数据");
    }

    /**
     * 获取奖章记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpMedalRecordService.selectCpMedalRecordById(id));
    }

    /**
     * 新增奖章记录
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:add')")
    @Log(title = "奖章记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpMedalRecord cpMedalRecord)
    {
        cpMedalRecord.setUserId(getUserId());
        cpMedalRecord.setDeptId(getDeptId());
        return toAjax(cpMedalRecordService.insertCpMedalRecord(cpMedalRecord));
    }

    /**
     * 撤销奖章记录
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:query')")
    @Log(title = "奖章记录", businessType = BusinessType.UPDATE)
    @PutMapping("/revocation")
    public AjaxResult edit(@RequestParam("id") Long id)
    {
        return AjaxResult.success(cpMedalRecordService.updateCpMedalRecord(id));
    }

    /**
     * 批量撤销奖章(app端)
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:query')")
    @Log(title = "奖章记录", businessType = BusinessType.UPDATE)
    @PutMapping("/batchRevoke/{ids}")
    public AjaxResult batchRevoke(@PathVariable Long[] ids)
    {
        return AjaxResult.success(cpMedalRecordService.batchRevoke(ids));
    }

    /**
     * 删除奖章记录
     */
    @PreAuthorize("@ss.hasPermi('medalRecord:medalRecord:remove')")
    @Log(title = "奖章记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return AjaxResult.success(cpMedalRecordService.deleteCpMedalRecordByIds(ids));
    }
}
