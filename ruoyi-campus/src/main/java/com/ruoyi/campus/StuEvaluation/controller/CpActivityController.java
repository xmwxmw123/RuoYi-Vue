package com.ruoyi.campus.StuEvaluation.controller;

import com.github.pagehelper.PageInfo;
import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityService;
import com.ruoyi.campus.domain.exportVo.ExportActivity;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 活动列表Controller
 * 
 * @author ruoyi
 * @date 2023-08-09
 */
@RestController
@RequestMapping("/activity/activity")
public class CpActivityController extends BaseController
{
    @Resource
    private ICpActivityService cpActivityService;

    /**
     * 查询活动列表列表
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpActivity cpActivity)
    {
        startPage();
        List<CpActivity> list = cpActivityService.selectCpActivityList(cpActivity);
        long total = new PageInfo(list).getTotal();
        list = cpActivityService.selectCpActivityList(list);
        return getDataTable(list, total);
    }
    /**
     * 查询活动列表列表(教师端app用)
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:list')")
    @GetMapping("/appList")
    public TableDataInfo appList(CpActivity cpActivity)
    {
        startPage();
        cpActivity.setUserId(getUserId());
        cpActivity.setDeptId(getDeptId());
        List<CpActivity> list = cpActivityService.appList(cpActivity);
        long total = new PageInfo(list).getTotal();
        list = cpActivityService.selectCpActivityList(list);
        return getDataTable(list, total);
    }


    /**
     * 导出活动列表列表
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:export')")
    @Log(title = "活动列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportActivity exportActivity)
    {
        List<ExportActivity> list = cpActivityService.exportCpActivityList(exportActivity);
        ExcelUtil<ExportActivity> util = new ExcelUtil<ExportActivity>(ExportActivity.class);
        util.exportExcel(response, list, "活动列表数据");
    }

    /**
     * 获取活动列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpActivityService.selectCpActivityById(id));
    }

    /**
     * 新增活动列表
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:add')")
    @Log(title = "活动列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpActivity cpActivity)
    {
        cpActivity.setUserId(getUserId());
        cpActivity.setDeptId(getDeptId());
     return toAjax(cpActivityService.insertCpActivity(cpActivity));

    }

    /**
     * 修改活动列表
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:edit')")
    @Log(title = "活动列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpActivity cpActivity)
    {

        return toAjax(cpActivityService.updateCpActivity(cpActivity));
    }

    /**
     * 删除活动列表
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:remove')")
    @Log(title = "活动列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpActivityService.deleteCpActivityByIds(ids));
    }


    /**
     *  根据届别id查询班级
     */
    @PreAuthorize("@ss.hasPermi('activity:activity:query')")
    @GetMapping("selectRankById")
    public AjaxResult selectRankById(@RequestParam("period") String period ) {

        return AjaxResult.success(cpActivityService.selectRankById(period));
    }



}
