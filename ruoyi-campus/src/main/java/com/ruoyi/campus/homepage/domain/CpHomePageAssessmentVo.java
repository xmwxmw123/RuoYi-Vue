package com.ruoyi.campus.homepage.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpHomePageAssessmentVo {

    //规则:分数>=90为A;     90<分数<=80为B;    80<分数<=60为C;     60<分数为D


    //A的数量 和 A的百分占比
    private int numA;
    private String percentageA;

    private int numB;
    private String percentageB;

    private int numC;
    private String percentageC;

    private int numD;
    private String percentageD;

    //总数
    private int numTotal;
}
