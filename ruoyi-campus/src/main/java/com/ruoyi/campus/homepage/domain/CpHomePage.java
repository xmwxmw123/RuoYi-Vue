package com.ruoyi.campus.homepage.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpHomePage {


    //标识：1选择本学期，2选择本周，3选择本月
    private int identification;

    //级别id
    private String period;
    //年级
    private String grade;
    //班级id
    private Long classId;

    //考试id
    private Long examinationManagementId;
    //学科id
    private Long subjectId;

    //学期id
    private Long semestersId;

    private Long userId;

    private Long deptId;

}
