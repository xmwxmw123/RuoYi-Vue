package com.ruoyi.campus.homepage.service;


import com.ruoyi.campus.homepage.domain.CpHomePage;
import com.ruoyi.common.core.domain.AjaxResult;

/**
 * 首页数据
 */
public interface CpHomePageService
{
    /**
     * 首页数据总览
     * @param cpHomePage
     * @return
     */
    AjaxResult homePageData(CpHomePage cpHomePage);

    /**
     * 首页学生综合素质发展水平
     * @param cpHomePage
     * @return
     */
    AjaxResult homePageMedal(CpHomePage cpHomePage);

    /**
     * 首页学业测评
     * @param cpHomePage
     * @return
     */
    AjaxResult homePageAssessment(CpHomePage cpHomePage);

    /**
     * 首页体质测评
     * @param cpHomePage
     * @return
     */
    AjaxResult homePageFitness(CpHomePage cpHomePage);

    /**
     * 首页查询级别,年级,班级
     * @return
     */
    AjaxResult homePage(Long deptId);
}
