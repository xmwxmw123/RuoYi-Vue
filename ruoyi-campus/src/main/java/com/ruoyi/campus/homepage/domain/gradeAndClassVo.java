package com.ruoyi.campus.homepage.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class gradeAndClassVo {

    //级别
    private Long period;
    private String rankS;

    //年级
    private String grade;
    private String gradeName;

    //班级
    private List<Map<String, Object>> clazz ;

//    private Long classId;
//    private String className;

}
