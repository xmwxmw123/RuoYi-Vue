package com.ruoyi.campus.homepage.controller;

import com.ruoyi.campus.homepage.domain.CpHomePage;
import com.ruoyi.campus.homepage.service.CpHomePageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 首页数据
 */
@RestController
@RequestMapping("/homepage")
public class CpHomePageController extends BaseController
{
    @Resource
    private CpHomePageService cpHomePageService;

    /**
     * 首页数据总览
     */
    @PostMapping("/HomePageData")
    public AjaxResult HomePageData(@RequestBody CpHomePage cpHomePage)
    {
        cpHomePage.setUserId(getUserId());
        cpHomePage.setDeptId(getDeptId());
        return cpHomePageService.homePageData(cpHomePage);
    }

    /**
     * 首页学生综合素质发展水平
     */
    @PostMapping("/HomePageMedal")
    public AjaxResult HomePageMedal(@RequestBody CpHomePage cpHomePage)
    {
        cpHomePage.setUserId(getUserId());
        cpHomePage.setDeptId(getDeptId());
        return cpHomePageService.homePageMedal(cpHomePage);
    }

    /**
     * 首页学业测评
     */
    @PostMapping("/HomePageAssessment")
    public AjaxResult homePageAssessment(@RequestBody CpHomePage cpHomePage)
    {
        cpHomePage.setUserId(getUserId());
        cpHomePage.setDeptId(getDeptId());
        return cpHomePageService.homePageAssessment(cpHomePage);
    }

    /**
     * 首页体质测评
     */
    @PostMapping("/HomePageFitness")
    public AjaxResult homePageFitness(@RequestBody CpHomePage cpHomePage)
    {
        cpHomePage.setUserId(getUserId());
        cpHomePage.setDeptId(getDeptId());
        return cpHomePageService.homePageFitness(cpHomePage);
    }

    /**
     * 首页查询级别,年级,班级
     */
    @GetMapping()
    public AjaxResult homePage()
    {
        return cpHomePageService.homePage(getDeptId());
    }
}
