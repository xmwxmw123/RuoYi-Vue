package com.ruoyi.campus.homepage.service.impl;

import com.ruoyi.campus.StuEvaluation.domain.CpGradeAssessment;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpGradeAssessmentMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpScoreInquiryMapper;
import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.homepage.domain.*;
import com.ruoyi.campus.homepage.mapper.CpHomePageMapper;
import com.ruoyi.campus.homepage.service.CpHomePageService;
import com.ruoyi.campus.mapper.CpClassMapper;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.mapper.CpStaffProfileMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.system.mapper.SysDictDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 首页数据
 */
@Service
@Slf4j
public class CpHomePageServiceImpl implements CpHomePageService
{
    @Resource
    private CpHomePageMapper cpHomePageMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpStaffProfileMapper cpStaffProfileMapper;
    @Resource
    private CpClassMapper cpClassMapper;
    @Resource
    private CpActivityMapper cpActivityMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private CpScoreInquiryMapper cpScoreInquiryMapper;
    @Resource
    private CpGradeAssessmentMapper cpGradeAssessmentMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 首页学生数
     */
    @Override
    public AjaxResult homePageData(CpHomePage cpHomePage) {
        int identification = cpHomePage.getIdentification();
        String status = CommonEnum.registerStatus.AT_SCHOOL.getValue();
        //首页学生数
        int studentCount = cpRegisterMapper.selectStudentCount(cpHomePage.getDeptId());
        //首页老师数
        int teacherCount = cpStaffProfileMapper.selectTeacherCount(status, cpHomePage.getDeptId());
        //首页班级数
        String graduate = "false";//是否毕业状态,false未毕业
        int classCount = cpClassMapper.selectlassCount(graduate, cpHomePage.getDeptId());
        //首页实践活动数
        int activityCount = 0;
        if(identification == 0){
             activityCount = cpActivityMapper.selectActivityCount1(cpHomePage.getDeptId());

        }else if(identification == 1){
            activityCount = cpActivityMapper.selectActivityCount2(cpHomePage.getDeptId());

        }else if(identification == 2){
            activityCount = cpActivityMapper.selectActivityCount3(cpHomePage.getDeptId());
        }
        //返回对象
        CpHomePageDataVo build = CpHomePageDataVo.builder()
                .studentCount(studentCount)
                .teacherCount(teacherCount)
                .classCount(classCount)
                .activityCount(activityCount)
                .build();

        return AjaxResult.success(build);
    }

    /**
     * 首页学生综合素质发展水平
     */
    @Override
    public AjaxResult homePageMedal(CpHomePage cpHomePage) {
        int identification = cpHomePage.getIdentification();

        CpHomePageMedalVo cpHomePageMedalVo = new CpHomePageMedalVo();
        //判断前端传过来的标识,0标识本学期,1标识本周,2表示本月
        if (identification == 0) {
            String currentSemester = CommonEnum.currentSemester.CURRENT.getValue();
            CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(currentSemester, cpHomePage.getDeptId());
            if (cpSemesters == null) {
                throw new GlobalException("请先在系统设置里面设置当前学期");
            }
            //本学期
            cpHomePageMedalVo = cpMedalRecordMapper.selectStuMedal1(cpSemesters.getId(), cpHomePage.getClassId(),  cpHomePage.getGrade(), cpHomePage.getDeptId());
        } else if (identification == 1) {
            //本周
            cpHomePageMedalVo = cpMedalRecordMapper.selectStuMedal2(cpHomePage.getClassId(),  cpHomePage.getGrade(), cpHomePage.getDeptId());
        } else if (identification == 2){
            //本月
            cpHomePageMedalVo = cpMedalRecordMapper.selectStuMedal3(cpHomePage.getClassId(),  cpHomePage.getGrade(), cpHomePage.getDeptId());
        }

        if (cpHomePageMedalVo != null) {
            cpHomePageMedalVo.setMedalTotal(
                    cpHomePageMedalVo.getMedalNumA() + cpHomePageMedalVo.getMedalNumB() + cpHomePageMedalVo.getMedalNumC() + cpHomePageMedalVo.getMedalNumD() + cpHomePageMedalVo.getMedalNumE()
            );
        } else {
            //没有数据返回空
            return AjaxResult.success(new CpHomePageMedalVo());
        }

        return AjaxResult.success(cpHomePageMedalVo);
    }

    /**
     * 首页学业测评
     */
    @Override
    public AjaxResult homePageAssessment(CpHomePage cpHomePage) {

        CpHomePageAssessmentVo cpHomePageAssessmentVo = cpScoreInquiryMapper.selectStuCount(cpHomePage);

        if(cpHomePageAssessmentVo != null){
            int numA = cpHomePageAssessmentVo.getNumA();
            int numB = cpHomePageAssessmentVo.getNumB();
            int numC = cpHomePageAssessmentVo.getNumC();
            int numD = cpHomePageAssessmentVo.getNumD();
            cpHomePageAssessmentVo.setNumTotal(numA+numB+numC+numD);
            int numTotal = cpHomePageAssessmentVo.getNumTotal();
            //将numABCD转为百分比
            cpHomePageAssessmentVo.setPercentageA(convertToPercentage(numTotal, numA));
            cpHomePageAssessmentVo.setPercentageB(convertToPercentage(numTotal, numB));
            cpHomePageAssessmentVo.setPercentageC(convertToPercentage(numTotal, numC));
            cpHomePageAssessmentVo.setPercentageD(convertToPercentage(numTotal, numD));
        }else {
            //cpHomePageAssessmentVo为空,返回空值
            cpHomePageAssessmentVo = new CpHomePageAssessmentVo();
            return AjaxResult.success(cpHomePageAssessmentVo);
        }


        return AjaxResult.success(cpHomePageAssessmentVo);
    }

        //百分比转换方法
        public static String convertToPercentage(int total, int count) {
        if (total == 0) {
            return "0%";
        }
        double percentage = ((double) count / total) * 100;
        // 保留两位小数
        return String.format("%.2f%%", percentage);
    }


    /**
     * 首页体质测评
     */
    @Override
    public AjaxResult homePageFitness(CpHomePage cpHomePage) {
        //查询出每条数据,逐一对比
        List<CpGradeAssessment> cpGradeAssessments = cpGradeAssessmentMapper.selectCpGradeAssessmentStuCount(cpHomePage);
        //初始化值
        int numA=0;
        int numB=0;
        int numC=0;
        int numD=0;
        for (CpGradeAssessment cpGradeAssessment : cpGradeAssessments) {

            if("1".equals(cpGradeAssessment.getDj())){
                numA++;
            }else if("2".equals(cpGradeAssessment.getDj())){
                numB++;
            }else if("3".equals(cpGradeAssessment.getDj())){
                numC++;
            }else if("4".equals(cpGradeAssessment.getDj())){
                numD++;
            }
        }

        CpHomePageAssessmentVo cpHomePageAssessmentVo = new CpHomePageAssessmentVo();
        cpHomePageAssessmentVo.setNumA(numA);
        cpHomePageAssessmentVo.setNumB(numB);
        cpHomePageAssessmentVo.setNumC(numC);
        cpHomePageAssessmentVo.setNumD(numD);
        cpHomePageAssessmentVo.setNumTotal(numA + numB + numC + numD);
        return AjaxResult.success(cpHomePageAssessmentVo);
    }

    /**
     * 首页查询级别,年级,班级
     */
    @Override
    public AjaxResult homePage(Long deptId) {

        List<gradeAndClassVo> gradeAndClassVos = new ArrayList<>();

        //查出未毕业的级别
        CpGrade grade = new CpGrade();
        grade.setGraduate("false");
        grade.setDeptId(deptId);
        List<CpGrade> cpGrades = cpHomePageMapper.selectRankS(grade);
        //校验
        if(cpGrades == null) return null;
        for (CpGrade cpGrade : cpGrades) {
            Long period = cpGrade.getId();
            //查出所有班级
            List<CpClass> cpClasses = cpClassMapper.selectCpClassByPeriod(period, deptId);
            if(cpClasses == null) return null;
            gradeAndClassVo gradeAndClassVo = new gradeAndClassVo();
            List<Map<String, Object>> clazz = new ArrayList<>();
            gradeAndClassVo.setPeriod(period);
            gradeAndClassVo.setRankS(cpGrade.getRankS());
            gradeAndClassVo.setGrade(cpGrade.getCurrentClass());
            String label = dictDataMapper.selectDictLabel("grade_list", cpGrade.getCurrentClass());
            gradeAndClassVo.setGradeName(label);
            //遍历班级赋值
            for (CpClass cpClass : cpClasses) {
                //赋值
                Map<String, Object> map = new HashMap<>();
                map.put("className", cpClass.getClassName());
                map.put("classId", cpClass.getId());
                clazz.add(map);
                gradeAndClassVo.setClazz(clazz);
                gradeAndClassVos.add(gradeAndClassVo);
            }

        }
        List<gradeAndClassVo> deduplicatedList = gradeAndClassVos.stream()
                .distinct()
                .collect(Collectors.toList()); // 去重操作

        return AjaxResult.success(deduplicatedList);

    }


}
