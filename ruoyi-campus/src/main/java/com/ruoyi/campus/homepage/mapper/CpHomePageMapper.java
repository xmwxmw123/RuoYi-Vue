package com.ruoyi.campus.homepage.mapper;


import com.ruoyi.campus.domain.CpGrade;

import java.util.List;

/**
 * 首页数据
 */
public interface CpHomePageMapper
{

    /**
     * 首页查询级别,年级,班级
     */
    List<CpGrade> selectRankS(CpGrade grade);
}
