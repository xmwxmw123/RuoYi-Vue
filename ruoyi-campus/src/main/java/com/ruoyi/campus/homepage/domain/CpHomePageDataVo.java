package com.ruoyi.campus.homepage.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CpHomePageDataVo {

    //首页学生数
    private int studentCount;
    //首页老师数
    private int teacherCount;
    //首页班级数
    private int classCount;
    //首页实践活动数
    private int activityCount;


}
