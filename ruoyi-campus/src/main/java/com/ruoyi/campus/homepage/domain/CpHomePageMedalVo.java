package com.ruoyi.campus.homepage.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CpHomePageMedalVo {

    //品德发展水平
    private int medalNumA;
    //学业发展水平
    private int medalNumB;
    //身心发展水平
    private int medalNumC;
    //审美素养提升
    private int medalNumD;
    //劳动实践活动
    private int medalNumE;
    //总数量
    private int medalTotal;

}
