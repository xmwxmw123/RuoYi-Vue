package com.ruoyi.campus.register.controller;

import com.ruoyi.campus.register.domain.CpGraduationFile;
import com.ruoyi.campus.register.service.ICpGraduationFileService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 毕业学生档案Controller
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/graduationFile/graduationFile")
public class CpGraduationFileController extends BaseController
{
    @Autowired
    private ICpGraduationFileService cpGraduationFileService;

    /**
     * 查询毕业学生档案列表
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpGraduationFile cpGraduationFile)
    {
        startPage();
        List<CpGraduationFile> list = cpGraduationFileService.selectCpGraduationFileList(cpGraduationFile);
        return getDataTable(list);
    }

    /**
     * 导出毕业学生档案列表
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:export')")
    @Log(title = "毕业学生档案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpGraduationFile cpGraduationFile)
    {
        List<CpGraduationFile> list = cpGraduationFileService.export(cpGraduationFile);
        ExcelUtil<CpGraduationFile> util = new ExcelUtil<CpGraduationFile>(CpGraduationFile.class);
        util.exportExcel(response, list, "毕业学生档案数据");
    }

    /**
     * 获取毕业学生档案详细信息
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpGraduationFileService.selectCpGraduationFileById(id));
    }

    /**
     * 新增毕业学生档案
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:add')")
    @Log(title = "毕业学生档案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpGraduationFile cpGraduationFile)
    {
        cpGraduationFile.setUserId(getUserId());
        cpGraduationFile.setDeptId(getDeptId());
        return toAjax(cpGraduationFileService.insertCpGraduationFile(cpGraduationFile));
    }

    /**
     * 修改毕业学生档案
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:edit')")
    @Log(title = "毕业学生档案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpGraduationFile cpGraduationFile)
    {
        return toAjax(cpGraduationFileService.updateCpGraduationFile(cpGraduationFile));
    }

    /**
     * 删除毕业学生档案
     */
    @PreAuthorize("@ss.hasPermi('graduationFile:graduationFile:remove')")
    @Log(title = "毕业学生档案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpGraduationFileService.deleteCpGraduationFileByIds(ids));
    }
}
