package com.ruoyi.campus.register.service;

import com.ruoyi.campus.register.domain.CpVariation;

import java.util.List;

/**
 * 变动学生Service接口
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
public interface ICpVariationService 
{
    /**
     * 查询变动学生
     * 
     * @param id 变动学生主键
     * @return 变动学生
     */
    public CpVariation selectCpVariationById(Long id);

    /**
     * 查询变动学生列表
     * 
     * @param cpVariation 变动学生
     * @return 变动学生集合
     */
    public List<CpVariation> selectCpVariationList(CpVariation cpVariation);

    /**
     * 新增变动学生
     * 
     * @param cpVariation 变动学生
     * @return 结果
     */
    public int insertCpVariation(CpVariation cpVariation);

    /**
     * 修改变动学生
     * 
     * @param cpVariation 变动学生
     * @return 结果
     */
    public int updateCpVariation(CpVariation cpVariation);

    /**
     * 批量删除变动学生
     * 
     * @param ids 需要删除的变动学生主键集合
     * @return 结果
     */
    public int deleteCpVariationByIds(Long[] ids);

    /**
     * 删除变动学生信息
     * 
     * @param id 变动学生主键
     * @return 结果
     */
    public int deleteCpVariationById(Long id);

    /**
     * 导出
     * @param cpVariation
     * @return
     */
    List<CpVariation> export(CpVariation cpVariation);
}
