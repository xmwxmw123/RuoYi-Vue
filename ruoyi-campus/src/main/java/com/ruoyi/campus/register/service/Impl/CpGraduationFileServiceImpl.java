package com.ruoyi.campus.register.service.Impl;

import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpQualityEvaluationMapper;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.register.domain.CpGraduationFile;
import com.ruoyi.campus.register.mapper.CpGraduationFileMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.register.service.ICpGraduationFileService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 毕业学生档案Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@Service
public class CpGraduationFileServiceImpl implements ICpGraduationFileService 
{
    @Resource
    private CpGraduationFileMapper cpGraduationFileMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpQualityEvaluationMapper cpQualityEvaluationMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    /**
     * 查询毕业学生档案
     * 
     * @param id 毕业学生档案主键
     * @return 毕业学生档案
     */
    @Override
    public CpGraduationFile selectCpGraduationFileById(Long id)
    {
        return cpGraduationFileMapper.selectCpGraduationFileById(id);
    }

    /**
     * 查询毕业学生档案列表
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 毕业学生档案
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpGraduationFile> selectCpGraduationFileList(CpGraduationFile cpGraduationFile)
    {
        //过滤出状态为:毕业,的学生
        String status = CommonEnum.registerStatus.GRADUATE.getValue();
        cpGraduationFile.setStatus(status);
        List<CpGraduationFile> cpGraduationFiles = cpGraduationFileMapper.selectCpGraduationFileList(cpGraduationFile);

        return cpGraduationFiles;
    }

    /**
     * 新增毕业学生档案
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 结果
     */
    @Override
    public int insertCpGraduationFile(CpGraduationFile cpGraduationFile)
    {
        cpGraduationFile.setCreateTime(DateUtils.getNowDate());
        return cpGraduationFileMapper.insertCpGraduationFile(cpGraduationFile);
    }

    /**
     * 修改毕业学生档案
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 结果
     */
    @Override
    public int updateCpGraduationFile(CpGraduationFile cpGraduationFile)
    {
        cpGraduationFile.setUpdateTime(DateUtils.getNowDate());
        return cpGraduationFileMapper.updateCpGraduationFile(cpGraduationFile);
    }

    /**
     * 批量删除毕业学生档案
     * 
     * @param ids 需要删除的毕业学生档案主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCpGraduationFileByIds(Long[] ids)
    {
//        for (Long id : ids) {
//            CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//            String studentNum = cpRegister.getStudentNum();
//            int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count1 > 0){
//                throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//            }
//            int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count2 > 0){
//                throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//            }
//        }
        return cpGraduationFileMapper.deleteCpGraduationFileByIds(ids);
    }

    /**
     * 删除毕业学生档案信息
     * 
     * @param id 毕业学生档案主键
     * @return 结果
     */
    @Override
    public int deleteCpGraduationFileById(Long id)
    {
//        CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//        String studentNum = cpRegister.getStudentNum();
//        int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count1 > 0){
//            throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//        }
//        int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count2 > 0){
//            throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//        }
        return cpGraduationFileMapper.deleteCpGraduationFileById(id);
    }

    /**
     * 导出
     * @param cpGraduationFile
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpGraduationFile> export(CpGraduationFile cpGraduationFile) {
        List<CpGraduationFile> exportList = cpGraduationFileMapper.export(cpGraduationFile);
        if(exportList.isEmpty()){
            throw new GlobalException("没有数据");
        }

        for (CpGraduationFile graduationFile : exportList) {

            //年级
            if (graduationFile.getGrade()!=null){
                String label = dictDataMapper.selectDictLabel("grade_list", graduationFile.getGrade());
                graduationFile.setGrade(label);
            }

            //学籍状态
            if (graduationFile.getStatus()  !=null){
                String label = dictDataMapper.selectDictLabel("student_status", graduationFile.getStatus());
                graduationFile.setStatus(label);
            }

            //生源
            if (graduationFile.getSource() !=null) {
                String label = dictDataMapper.selectDictLabel("source_category", graduationFile.getSource());
                graduationFile.setSource(label);
            }

            //性别
            if(graduationFile.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", graduationFile.getSex());
                graduationFile.setSex(label);
            }

        }
        return exportList;
    }
}
