package com.ruoyi.campus.register.controller;

import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.register.controller.vo.AdoptGradeStudentIdarrQueryRequestVO;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.service.ICpRegisterService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 学籍Controller
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@RestController
@RequestMapping("/register/register")
public class CpRegisterController extends BaseController
{
    @Autowired
    private ICpRegisterService cpRegisterService;

    /**
     * 查询学籍列表
     */
    @PreAuthorize("@ss.hasPermi('register:register:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpRegister cpRegister)
    {
        startPage();
        List<CpRegister> list = cpRegisterService.selectCpRegisterList(cpRegister);
        return getDataTable(list);
    }

    /**
     * 导出学籍列表
     */
    @PreAuthorize("@ss.hasPermi('register:register:export')")
    @Log(title = "学籍", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportExistStu exportExistStu)
    {
        List<ExportExistStu> list = cpRegisterService.export(exportExistStu);
        ExcelUtil<ExportExistStu> util = new ExcelUtil<ExportExistStu>(ExportExistStu.class);
        util.exportExcel(response, list, "学籍数据");
    }

    /**
     * 获取学籍详细信息
     */
    @PreAuthorize("@ss.hasPermi('register:register:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpRegisterService.selectCpRegisterById(id));
    }

    /**
     * 新增学籍
     */
    @PreAuthorize("@ss.hasPermi('register:register:add')")
    @Log(title = "学籍", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Valid @RequestBody CpRegister cpRegister)
    {
        cpRegister.setUserId(getUserId());
        cpRegister.setDeptId(getDeptId());
        return toAjax(cpRegisterService.insertCpRegister(cpRegister));
    }

    /**
     * 修改学籍
     */
    @PreAuthorize("@ss.hasPermi('register:register:edit')")
    @Log(title = "学籍", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody CpRegister cpRegister)
    {
        return AjaxResult.success(cpRegisterService.updateCpRegister(cpRegister));
    }

    /**
     * 删除学籍
     */
    @PreAuthorize("@ss.hasPermi('register:register:remove')")
    @Log(title = "学籍", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpRegisterService.deleteCpRegisterByIds(ids));
    }

    /**
     * 转班
     * @return
     */
    @PreAuthorize("@ss.hasPermi('register:register:edit')")
    @PutMapping("/changeClass")
    public AjaxResult changeClass(@RequestBody CpRegister cpRegister){
        cpRegister.setUserId(getUserId());
        cpRegister.setDeptId(getDeptId());
        return AjaxResult.success(cpRegisterService.updateChangeClass(cpRegister));
    }
    /**
     * 休学
     * @return
     */
    @PreAuthorize("@ss.hasPermi('register:register:edit')")
    @PutMapping("/furlough")
    public AjaxResult furlough(@RequestBody CpRegister cpRegister){
        return AjaxResult.success(cpRegisterService.furlough(cpRegister));
    }

    /**
     * 复学
     * @return
     */
    @PreAuthorize("@ss.hasPermi('register:register:edit')")
    @PutMapping("/reentry")
    public AjaxResult reentry(@RequestBody CpRegister cpRegister){
        return AjaxResult.success(cpRegisterService.reentry(cpRegister));
    }

    /**
     * 根据条案件查询学生
     */
    @PreAuthorize("@ss.hasPermi('register:register:query')")
    @GetMapping("/query")
    public TableDataInfo query(CpRegister cpRegister)
    {
        cpRegister.setDeptId(getDeptId());
        cpRegister.setUserId(getUserId());
        return getDataTable(cpRegisterService.selectCpRegisterQuery(cpRegister));
    }

    /**
     * 根据条案件查询学生(app用)
     */
    @PreAuthorize("@ss.hasPermi('register:register:query')")
    @GetMapping("/appQuery")
    public TableDataInfo appQuery(CpRegister cpRegister)
    {
        startPage();
        cpRegister.setDeptId(getDeptId());
        cpRegister.setUserId(getUserId());
        return getDataTable(cpRegisterService.selectCpRegisterQuery(cpRegister));
    }

    /**
     * 根据年级和学生id数组查询学生
     */
    @PreAuthorize("@ss.hasPermi('register:register:query')")
    @GetMapping("/adoptGradeStudentIdarrQueryList")
    public AjaxResult adoptGradeStudentIdarrQueryList(AdoptGradeStudentIdarrQueryRequestVO idarrQueryRequestVO)
    {
        return success(cpRegisterService.adoptGradeStudentIdarrQueryList(idarrQueryRequestVO));
    }
}
