package com.ruoyi.campus.register.service;

import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.register.controller.vo.AdoptGradeStudentIdarrQueryRequestVO;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.common.core.domain.AjaxResult;

import java.util.List;

/**
 * 学籍Service接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface ICpRegisterService 
{
    /**
     * 查询学籍
     * 
     * @param id 学籍主键
     * @return 学籍
     */
    public CpRegister selectCpRegisterById(Long id);

    /**
     * 查询学籍列表
     *
     * @param cpRegister 学籍
     * @return 学籍集合
     */
    public List<CpRegister> selectCpRegisterList(CpRegister cpRegister);

    /**
     * 新增学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    public int insertCpRegister(CpRegister cpRegister);

    /**
     * 修改学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    public int updateCpRegister(CpRegister cpRegister);

    /**
     * 批量删除学籍
     * 
     * @param ids 需要删除的学籍主键集合
     * @return 结果
     */
    public int deleteCpRegisterByIds(Long[] ids);

    /**
     * 删除学籍信息
     * 
     * @param id 学籍主键
     * @return 结果
     */
    public int deleteCpRegisterById(Long id);

    /**
     * 转班
     * @param cpRegister
     * @return
     */
    AjaxResult updateChangeClass(CpRegister cpRegister);

    /**
     * 导出学籍
     * @param exportExistStu
     * @return
     */
    List<ExportExistStu> export(ExportExistStu exportExistStu);

    /**
     * 休学
     * @param cpRegister
     * @return
     */
    int furlough(CpRegister cpRegister);

    /**
     * 复学
     * @param cpRegister
     * @return
     */
    int reentry(CpRegister cpRegister);

    /**
     * 根据条案件查询学生
     * @param cpRegister
     */
    List<CpRegister> selectCpRegisterQuery(CpRegister cpRegister);

    /**
     * 根据年级和学生id数组查询学生
     * @param idarrQueryRequestVO 参数
     * @return 学生信息列表
     */
    List<CpRegister> adoptGradeStudentIdarrQueryList(AdoptGradeStudentIdarrQueryRequestVO idarrQueryRequestVO);
}
