package com.ruoyi.campus.register.controller;

import com.ruoyi.campus.register.domain.CpVariation;
import com.ruoyi.campus.register.service.ICpVariationService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 变动学生Controller
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/variation/variation")
public class CpVariationController extends BaseController
{
    @Autowired
    private ICpVariationService cpVariationService;

    /**
     * 查询变动学生列表
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpVariation cpVariation)
    {
        startPage();
        List<CpVariation> list = cpVariationService.selectCpVariationList(cpVariation);
        return getDataTable(list);
    }

    /**
     * 导出变动学生列表
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:export')")
    @Log(title = "变动学生", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpVariation cpVariation)
    {
        List<CpVariation> list = cpVariationService.export(cpVariation);
        ExcelUtil<CpVariation> util = new ExcelUtil<CpVariation>(CpVariation.class);
        util.exportExcel(response, list, "变动学生数据");
    }

    /**
     * 获取变动学生详细信息
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpVariationService.selectCpVariationById(id));
    }

    /**
     * 新增变动学生
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:add')")
    @Log(title = "变动学生", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpVariation cpVariation)
    {
        cpVariation.setUserId(getUserId());
        cpVariation.setDeptId(getDeptId());
        return toAjax(cpVariationService.insertCpVariation(cpVariation));
    }

    /**
     * 修改变动学生
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:edit')")
    @Log(title = "变动学生", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpVariation cpVariation)
    {
        return toAjax(cpVariationService.updateCpVariation(cpVariation));
    }

    /**
     * 删除变动学生
     */
    @PreAuthorize("@ss.hasPermi('variation:variation:remove')")
    @Log(title = "变动学生", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpVariationService.deleteCpVariationByIds(ids));
    }
}
