package com.ruoyi.campus.register.controller.vo;

import lombok.Data;

/**
 * 根据年级和学生id数组查询学生参数对象
 */
@Data
public class AdoptGradeStudentIdarrQueryRequestVO {

    /**
     * 年级（逗号分割）
     */
    private String[] grade;

    /**
     * 学生id列表 （逗号分割）
     */
    private String[] studentIds;
}
