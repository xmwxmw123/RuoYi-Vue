package com.ruoyi.campus.register.service.Impl;

import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpQualityEvaluationMapper;
import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.register.domain.CpExistStu;
import com.ruoyi.campus.register.mapper.CpExistStuMapper;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.register.service.ICpExistStuService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 在校学生Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@Service
public class CpExistStuServiceImpl implements ICpExistStuService 
{
    @Resource
    private CpExistStuMapper cpExistStuMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpQualityEvaluationMapper cpQualityEvaluationMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    /**
     * 查询在校学生
     * 
     * @param id 在校学生主键
     * @return 在校学生
     */
    @Override
    public CpExistStu selectCpExistStuById(Long id)
    {
        return cpExistStuMapper.selectCpExistStuById(id);
    }

    /**
     * 查询在校学生列表
     * 
     * @param cpExistStu 在校学生
     * @return 在校学生
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpExistStu> selectCpExistStuList(CpExistStu cpExistStu)
    {
        //过滤出状态为:在校,的学生
//        String status = CommonEnum.registerStatus.AT_SCHOOL.getValue();
//        cpExistStu.setStatus(status);
        List<CpExistStu> cpExistStus = cpExistStuMapper.selectCpExistStuList(cpExistStu);

        return cpExistStus;
    }

    /**
     * 批量删除在校学生
     * 
     * @param ids 需要删除的在校学生主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCpExistStuByIds(Long[] ids)
    {
//        for (Long id : ids) {
//            CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//            String studentNum = cpRegister.getStudentNum();
//            int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count1 > 0){
//                throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//            }
//            int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count2 > 0){
//                throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//            }
//        }
        return cpExistStuMapper.deleteCpExistStuByIds(ids);
    }

    /**
     * 删除在校学生信息
     * 
     * @param id 在校学生主键
     * @return 结果
     */
    @Override
    public int deleteCpExistStuById(Long id)
    {
//        CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//        String studentNum = cpRegister.getStudentNum();
//        int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count1 > 0){
//            throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//        }
//        int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count2 > 0){
//            throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//        }
        return cpExistStuMapper.deleteCpExistStuById(id);
    }

    /**
     * 导出
     * @param excelExistStu
     * @return
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ExportExistStu> export(ExportExistStu excelExistStu) {
        List<ExportExistStu> cpExistStus = cpExistStuMapper.export(excelExistStu);
        if (cpExistStus == null) {
            throw new GlobalException("列表为空");
        }
        for (ExportExistStu existStus : cpExistStus) {
            //年级
            if (existStus.getGrade()!=null){
                String label = dictDataMapper.selectDictLabel("grade_list", existStus.getGrade());
                existStus.setGrade(label);

            }

            //学籍状态
            if (existStus.getStatus()  !=null){
                String label = dictDataMapper.selectDictLabel("student_status", existStus.getStatus());
                existStus.setStatus(label);
            }

            //生源
            if (existStus.getSource() !=null) {
                String label = dictDataMapper.selectDictLabel("source_category", existStus.getSource());
                existStus.setSource(label);
            }

            //性别
            if(existStus.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", existStus.getSex());
                existStus.setSex(label);
            }
        }

        return cpExistStus;
    }
}
