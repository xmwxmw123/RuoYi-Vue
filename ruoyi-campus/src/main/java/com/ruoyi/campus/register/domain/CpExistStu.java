package com.ruoyi.campus.register.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 在校学生对象 cp_existStu
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CpExistStu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学籍id */
    private Long id;

    /** 届别id */
    @Excel(name = "届别id")
    private String period;

    /** 届别 */
    @Excel(name = "届别")
    private String rankS;

    /** 年级 */
    @Excel(name = "年级")
    private String grade;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String className;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNum;

    /** 统编号 */
    @Excel(name = "统编号")
    private String serialNum;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;

    /** 生源类别 */
    @Excel(name = "生源类别")
    private String source;

    //家长姓名
    private String patriarchName;

    //家长电话
    private String patriarchCall;

    /** 创建人 */
    private String createUser;

    /** 最后修改人 */
    private String updateUser;

    /**部门id*/
    private Long deptId;

    /**用户id*/
    private Long userId;


}
