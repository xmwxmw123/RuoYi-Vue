package com.ruoyi.campus.register.mapper;

import com.ruoyi.campus.StuEvaluation.domain.RankSAndClassName;
import com.ruoyi.campus.domain.CpUpdateStatus;
import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.register.controller.vo.AdoptGradeStudentIdarrQueryRequestVO;
import com.ruoyi.campus.register.domain.CpRegister;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 学籍Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface CpRegisterMapper 
{
    /**
     * 查询学籍
     * 
     * @param id 学籍主键
     * @return 学籍
     */
    public CpRegister selectCpRegisterById(Long id);

    /**
     * 查询学籍列表
     * 
     * @param cpRegister 学籍
     * @return 学籍集合
     */
    public List<CpRegister> selectCpRegisterList(CpRegister cpRegister);

    /**
     * 新增学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    public int insertCpRegister(CpRegister cpRegister);

    /**
     * 修改学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    public int updateCpRegister(CpRegister cpRegister);

    /**
     * 删除学籍
     * 
     * @param id 学籍主键
     * @return 结果
     */
    public int deleteCpRegisterById(Long id);

    /**
     * 批量删除学籍
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpRegisterByIds(Long[] ids);

    /**
     * 查询自增数据,用于拼接学号
     */
    @Select("select max(auto_increment+0) from cp_register where period =#{period} and dept_id = #{deptId}")
    String selectAutoIncrement(@Param("period") String period, @Param("deptId") Long deptId);

    /**
     * 修改学籍表学生状态为'毕业'
     * @param cpUpdateStatus
     */
    void updateGraduate(CpUpdateStatus cpUpdateStatus);

    /**
     * 修改学籍表学生状态为'在校'
     * @param cpUpdateStatus
     */
    void updateNotGraduate(CpUpdateStatus cpUpdateStatus);

    /**
     * 升级年级,使得学籍表和届别表数据同步
     * @param currentClass
     * @param rankS
     */
    @Update("update cp_register set grade = #{currentClass} + 1 where rank_s=#{rankS} and dept_id = #{deptId}")
    void updateGrade(@Param("currentClass") String currentClass,
                     @Param("rankS") String rankS, @Param("deptId") Long deptId);

    /**
     * 根据学号查询学生信息
     * @param studentNum
     */
    @Select("select * from cp_register where student_num=#{studentNum} and dept_id = #{deptId}")
    CpRegister selectStudent(@Param("studentNum") String studentNum, @Param("deptId") Long deptId);

    /***
     * 查询学号是否存在
     * @param studentNum
     * @return
     */
    @Select("select * from cp_register where student_num=#{studentNum} and dept_id = #{deptId}")
    CpRegister selectStudenByNum(@Param("studentNum") String studentNum, @Param("deptId") Long deptId);


    /**
     *  根据班级名称查询
     * @return
     */
    @Select("select * from cp_register where class_id=#{classId} and dept_id = #{deptId} and status in ('1','2')")
    List<CpRegister> selectClassName(@Param("classId") String classId, @Param("deptId") Long deptId);

    /**
     * 查询出所有学生
     * @return
     */
    @Select("select * from cp_register where dept_id = #{deptId} and status in ('1','2')")
    List<CpRegister> selectStudentList(@Param("deptId") Long deptId);

    /**
     * 根据学号查当前年级
     */
    CpRegister selectGradeByStudentNum(@Param("studentNum") String studentNum, @Param("deptId") Long deptId);

    /**
     * 根据年级查出班级信息
     * @param grade
     */
    @Select("select distinct rank_s, period, class_name, class_id from cp_register where grade=#{grade} and dept_id = #{deptId} and status in ('1','2')")
    List<RankSAndClassName> selectCpRegisterByGrade(@Param("grade") String grade, @Param("deptId") Long deptId);

    /**
     * 导出学籍
     * @param exportExistStu
     * @return
     */
    List<ExportExistStu> export(ExportExistStu exportExistStu);

    /**
     * 查询在校的人数 status状态为在校和复学的学生
     * @param
     */
    @Select("select count(*) from cp_register where status in ('1','2') and dept_id = #{deptId}")
    int selectStudentCount(@Param("deptId") Long deptId);

    /**
     *
     * @return
     */
    int selectStuCount(@Param("grade") String grade, @Param("registerId") Long registerId, @Param("classId") String classId,@Param("period") String period, @Param("deptId") Long deptId);

    @Select("select * from cp_register where grade=#{grade} and class_id=#{classId} and dept_id = #{deptId} and status in ('1','2')")
    List<CpRegister> selectStudentByclass(@Param("grade") String grade, @Param("classId") String classId, @Param("deptId") Long deptId);

    /**
     * 班级id查询学生
     * @param id
     */
    int selectStudentByclassId(Long id);

    /**
     * 根据条案件查询学生
     * @param cpRegister
     */
    List<CpRegister> selectQuery(CpRegister cpRegister);

    /**
     * 根据年级和学生id数组查询学生
     * @param idarrQueryRequestVO 参数
     * @return 学生信息列表
     */
    List<CpRegister> adoptGradeStudentIdarrQueryList(AdoptGradeStudentIdarrQueryRequestVO idarrQueryRequestVO);
}
