package com.ruoyi.campus.register.service.Impl;

import com.ruoyi.campus.StuEvaluation.domain.CpEducationEvaluation;
import com.ruoyi.campus.StuEvaluation.mapper.CpEducationEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpQualityEvaluationMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpScoreInquiryMapper;
import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.enums.CommonEnum;
import com.ruoyi.campus.mapper.CpSemestersMapper;
import com.ruoyi.campus.register.controller.vo.AdoptGradeStudentIdarrQueryRequestVO;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.register.service.ICpRegisterService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 学籍Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@Service
public class CpRegisterServiceImpl implements ICpRegisterService
{
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private CpQualityEvaluationMapper cpQualityEvaluationMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private CpEducationEvaluationMapper cpEducationEvaluationMapper;
    @Resource
    private CpScoreInquiryMapper cpScoreInquiryMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    @Resource
    private CpSemestersMapper cpSemestersMapper;
    /**
     * 查询学籍
     * 
     * @param id 学籍主键
     * @return 学籍
     */
    @Override
    public CpRegister selectCpRegisterById(Long id)
    {
        return cpRegisterMapper.selectCpRegisterById(id);
    }

    /**
     * 查询学籍列表
     * 
     * @param cpRegister 学籍
     * @return 学籍
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpRegister> selectCpRegisterList(CpRegister cpRegister)
    {
        //查询所有列表
        List<CpRegister> cpRegisters = cpRegisterMapper.selectCpRegisterList(cpRegister);
        return cpRegisters;
    }

    /**
     * 新增学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    @Override
    public int insertCpRegister(CpRegister cpRegister)
    {
        if(cpRegister.getRankS() == null ||  "".equals(cpRegister.getRankS())){
            throw new BaseException("届别不能为空");

        }else if(cpRegister.getName() == null ||  "".equals(cpRegister.getName())){
            throw new BaseException("姓名不能为空");

        }else if(cpRegister.getSex() == null || "".equals(cpRegister.getSex())){
            throw new BaseException("性别不能为空");

        }else if (cpRegister.getClassName() == null ||  "".equals(cpRegister.getClassName())){
            throw new BaseException("班级不能为空");
        }
        // 设置创建时间
        cpRegister.setCreateTime(DateUtils.getNowDate());
        // 获取当前的用户名称并赋值
        cpRegister.setCreateUser(SecurityUtils.getUsername());
        String status = CommonEnum.registerStatus.AT_SCHOOL.getValue();
        cpRegister.setStatus(status);
        //某某学校简称，用于拼接学号....（待开发....）
        //获取届别,用于拼接学号
        String rankS = cpRegister.getRankS();
        String substring = rankS.substring(2, 4);
        //获取当前月份,用于拼接学号
        String formattedMonth = LocalDate.now().format(DateTimeFormatter.ofPattern("YYYYMM"));
        String sex = cpRegister.getSex();
        //生成自增数字,用于拼接学号(首先去查自增数列的最大值,为空则设置0001,有值则自增,每个届别都要从0001开始自增)
        String maxNumber = cpRegisterMapper.selectAutoIncrement(cpRegister.getPeriod(), cpRegister.getDeptId());
        String newSequenceNumber = maxNumber != null ? generateStudentNumber((Integer.parseInt(maxNumber) + 1)) : "0001";
        //拼接学号并赋值
        String studentNum= substring+formattedMonth.substring(2, 6)+String.valueOf(sex)+newSequenceNumber;
        cpRegister.setStudentNum(studentNum);
        //每条学生数据都对应一个自增数,所以也要存储起来
        cpRegister.setAutoIncrement(Integer.parseInt(newSequenceNumber));

        return cpRegisterMapper.insertCpRegister(cpRegister);

    }

    // 格式化为4位数字，不足位数前面补0,例如:0001
    String generateStudentNumber(int sequenceNumber) {
        String formattedNumber = String.format("%04d", sequenceNumber);
        return formattedNumber;
    }

    /**
     * 修改学籍
     * 
     * @param cpRegister 学籍
     * @return 结果
     */
    @Override
    public int updateCpRegister(CpRegister cpRegister)
    {
        if(cpRegister.getName() == null || "".equals(cpRegister.getName())){
            throw new BaseException("姓名不能为空");
        }else if(cpRegister.getId() == null){
            throw new BaseException("id不能为空");
        }
        //修改时间
        cpRegister.setUpdateTime(DateUtils.getNowDate());
        //修改人
        String username = SecurityUtils.getUsername();
        cpRegister.setUpdateUser(username);
        return cpRegisterMapper.updateCpRegister(cpRegister);
    }

    /**
     * 批量删除学籍
     * 
     * @param ids 需要删除的学籍主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCpRegisterByIds(Long[] ids)
    {
//        for (Long id : ids) {
//            CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//            String studentNum = cpRegister.getStudentNum();
//            int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count1 > 0){
//                throw new BaseException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//            }
//            int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count2 > 0){
//                throw new BaseException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//            }
//        }

        return cpRegisterMapper.deleteCpRegisterByIds(ids);
    }

    /**
     * 删除学籍信息
     * 
     * @param id 学籍主键
     * @return 结果
     */
    @Override
    public int deleteCpRegisterById(Long id)
    {
//        CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//        String studentNum = cpRegister.getStudentNum();
//        int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count1 > 0){
//            throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//        }
//        int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count2 > 0){
//            throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//        }

        return cpRegisterMapper.deleteCpRegisterById(id);
    }

    /**
     * 转班
     * @param cpRegister
     * @return
     */
    @Override
    @Transactional
    public AjaxResult updateChangeClass(CpRegister cpRegister) {
        if(cpRegister.getRankS() == null || "".equals(cpRegister.getRankS())){
            throw new BaseException("届别不能为空");
        }else if(cpRegister.getId() == null){
            throw new BaseException("id不能为空");
        }

        //获取原本的班级数据
        CpRegister cpRegister1 = cpRegisterMapper.selectCpRegisterById(cpRegister.getId());
        //获取学期
        String currentSemester = CommonEnum.currentSemester.CURRENT.getValue();
        CpSemesters cpSemesters = cpSemestersMapper.selectCpCurrentSemester(currentSemester, cpRegister1.getDeptId());
        if (cpSemesters == null) {
            throw new GlobalException("请先在系统设置里面设置当前学期");
        }

        List<CpEducationEvaluation> cpEducationEvaluations = cpEducationEvaluationMapper.selectCpEducationEvaluationStudent(cpRegister.getGrade(), cpRegister.getClassId(), cpRegister.getStudentNum(), cpRegister.getPeriod(), cpRegister.getDeptId());

        if(cpEducationEvaluations.size() > 0){
            int revocation = 0;
            cpMedalRecordMapper.updateCpRevocation(cpRegister.getGrade(), cpRegister.getClassId(), cpRegister.getStudentNum(), cpRegister.getPeriod(), cpRegister.getDeptId(), revocation);
            cpMedalRecordMapper.updateCpRevocation(cpRegister1.getGrade(), cpRegister1.getClassId(), cpRegister1.getStudentNum(), cpRegister1.getPeriod(), cpRegister1.getDeptId(), (revocation+2));
        }else {
            int revocation = 2;
            cpMedalRecordMapper.updateCpRevocation(cpRegister1.getGrade(),cpRegister1.getClassId(), cpRegister1.getStudentNum(), cpRegister1.getPeriod(), cpRegister1.getDeptId(), revocation);
        }

        cpRegister.setStatus(CommonEnum.registerStatus.CHANGE_CLASSES.getValue());
        return AjaxResult.success(cpRegisterMapper.updateCpRegister(cpRegister));
    }

    /**
     * 导出学籍
     * @param exportExistStu
     * @return
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<ExportExistStu> export(ExportExistStu exportExistStu) {

        List<ExportExistStu> cpRegisters = cpRegisterMapper.export(exportExistStu);
        if (cpRegisters == null) {
            throw new GlobalException("列表为空");
        }
        for (ExportExistStu existStus : cpRegisters) {
            //年级
            if (existStus.getGrade()!=null){
                String label = dictDataMapper.selectDictLabel("grade_list", existStus.getGrade());
                existStus.setGrade(label);
            }

            //学籍状态
            if (existStus.getStatus()  !=null){
                String label = dictDataMapper.selectDictLabel("student_status", existStus.getStatus());
                existStus.setStatus(label);
            }

            //生源
            if (existStus.getSource() !=null) {
                String label = dictDataMapper.selectDictLabel("source_category", existStus.getSource());
                existStus.setSource(label);
            }

            //性别
            if(existStus.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", existStus.getSex());
                existStus.setSex(label);
            }
        }
        return cpRegisters;
    }

    /**
     * 休学
     * @param cpRegister
     * @return
     */
    @Override
    public int furlough(CpRegister cpRegister) {

        String status = CommonEnum.registerStatus.QUIT_SCHOOL.getValue();
        //设置状态为休学
        cpRegister.setStatus(status);
        return cpRegisterMapper.updateCpRegister(cpRegister);
    }

    /**
     * 复学
     * @param cpRegister
     * @return
     */
    @Override
    public int reentry(CpRegister cpRegister) {

        String status = CommonEnum.registerStatus.AT_SCHOOL.getValue();
        //设置状态为在校
        cpRegister.setStatus(status);
        return cpRegisterMapper.updateCpRegister(cpRegister);
    }

    /**
     * 根据条案件查询学生
     * @param cpRegister
     */
    @Override
    public List<CpRegister> selectCpRegisterQuery(CpRegister cpRegister) {
        List<CpRegister> cpRegisters = cpRegisterMapper.selectQuery(cpRegister);
        if(cpRegisters==null){
            return new ArrayList<CpRegister>();
        }
        return cpRegisters;
    }

    /**
     * 根据年级和学生id数组查询学生
     * @param idarrQueryRequestVO 参数
     * @return 学生信息列表
     */
    @Override
    public List<CpRegister> adoptGradeStudentIdarrQueryList(AdoptGradeStudentIdarrQueryRequestVO idarrQueryRequestVO) {
        return cpRegisterMapper.adoptGradeStudentIdarrQueryList(idarrQueryRequestVO);
    }
}
