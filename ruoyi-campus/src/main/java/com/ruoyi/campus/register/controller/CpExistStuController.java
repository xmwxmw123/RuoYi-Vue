package com.ruoyi.campus.register.controller;

import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.register.domain.CpExistStu;
import com.ruoyi.campus.register.service.ICpExistStuService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 在校学生Controller
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/existStu/existStu")
public class CpExistStuController extends BaseController
{
    @Autowired
    private ICpExistStuService cpExistStuService;

    /**
     * 查询在校学生列表
     */
    @PreAuthorize("@ss.hasPermi('existStu:existStu:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpExistStu cpExistStu)
    {
        startPage();
        List<CpExistStu> list = cpExistStuService.selectCpExistStuList(cpExistStu);
        return getDataTable(list);
    }

    /**
     * 导出在校学生列表
     */
    @PreAuthorize("@ss.hasPermi('existStu:existStu:export')")
    @Log(title = "在校学生", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportExistStu excelExistStu)
    {
        List<ExportExistStu> list = cpExistStuService.export(excelExistStu);
        ExcelUtil<ExportExistStu> util = new ExcelUtil<ExportExistStu>(ExportExistStu.class);
        util.exportExcel(response, list, "在校学生数据");
    }

    /**
     * 获取在校学生详细信息
     */
    @PreAuthorize("@ss.hasPermi('existStu:existStu:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpExistStuService.selectCpExistStuById(id));
    }

    /**
     * 删除在校学生
     */
    @PreAuthorize("@ss.hasPermi('existStu:existStu:remove')")
    @Log(title = "在校学生", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpExistStuService.deleteCpExistStuByIds(ids));
    }
}
