package com.ruoyi.campus.register.service;

import com.ruoyi.campus.domain.exportVo.ExportExistStu;
import com.ruoyi.campus.register.domain.CpExistStu;

import java.util.List;

/**
 * 在校学生Service接口
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
public interface ICpExistStuService 
{
    /**
     * 查询在校学生
     * 
     * @param id 在校学生主键
     * @return 在校学生
     */
    public CpExistStu selectCpExistStuById(Long id);

    /**
     * 查询在校学生列表
     * 
     * @param cpExistStu 在校学生
     * @return 在校学生集合
     */
    public List<CpExistStu> selectCpExistStuList(CpExistStu cpExistStu);

    /**
     * 批量删除在校学生
     * 
     * @param ids 需要删除的在校学生主键集合
     * @return 结果
     */
    public int deleteCpExistStuByIds(Long[] ids);

    /**
     * 删除在校学生信息
     * 
     * @param id 在校学生主键
     * @return 结果
     */
    public int deleteCpExistStuById(Long id);


    /**
     * 导出
     * @param excelExistStu
     * @return
     */
    List<ExportExistStu> export(ExportExistStu excelExistStu);

}
