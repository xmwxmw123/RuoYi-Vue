package com.ruoyi.campus.register.service.Impl;

import com.ruoyi.campus.StuEvaluation.mapper.CpMedalRecordMapper;
import com.ruoyi.campus.StuEvaluation.mapper.CpQualityEvaluationMapper;
import com.ruoyi.campus.register.domain.CpVariation;
import com.ruoyi.campus.register.mapper.CpRegisterMapper;
import com.ruoyi.campus.register.mapper.CpVariationMapper;
import com.ruoyi.campus.register.service.ICpVariationService;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 变动学生Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
@Service
public class CpVariationServiceImpl implements ICpVariationService 
{
    @Resource
    private CpVariationMapper cpVariationMapper;
    @Resource
    private CpQualityEvaluationMapper cpQualityEvaluationMapper;
    @Resource
    private CpMedalRecordMapper cpMedalRecordMapper;
    @Resource
    private CpRegisterMapper cpRegisterMapper;
    @Resource
    private SysDictDataMapper dictDataMapper;
    /**
     * 查询变动学生
     * 
     * @param id 变动学生主键
     * @return 变动学生
     */
    @Override
    public CpVariation selectCpVariationById(Long id)
    {
        return cpVariationMapper.selectCpVariationById(id);
    }

    /**
     * 查询变动学生列表
     * 
     * @param cpVariation 变动学生
     * @return 变动学生
     */
    @DataScope(deptAlias = "d")
    @Override
    public List<CpVariation> selectCpVariationList(CpVariation cpVariation)
    {

        List<CpVariation> cpVariations = cpVariationMapper.selectCpVariationList(cpVariation);

        return cpVariations;
    }

    /**
     * 新增变动学生
     * 
     * @param cpVariation 变动学生
     * @return 结果
     */
    @Override
    public int insertCpVariation(CpVariation cpVariation)
    {
        cpVariation.setCreateTime(DateUtils.getNowDate());
        return cpVariationMapper.insertCpVariation(cpVariation);
    }

    /**
     * 修改变动学生
     * 
     * @param cpVariation 变动学生
     * @return 结果
     */
    @Override
    public int updateCpVariation(CpVariation cpVariation)
    {
        cpVariation.setUpdateTime(DateUtils.getNowDate());
        return cpVariationMapper.updateCpVariation(cpVariation);
    }

    /**
     * 批量删除变动学生
     * 
     * @param ids 需要删除的变动学生主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteCpVariationByIds(Long[] ids)
    {
//        for (Long id : ids) {
//            CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//            String studentNum = cpRegister.getStudentNum();
//            int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count1 > 0){
//                throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//            }
//            int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//            if(count2 > 0){
//                throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//            }
//        }
        return cpVariationMapper.deleteCpVariationByIds(ids);
    }

    /**
     * 删除变动学生信息
     * 
     * @param id 变动学生主键
     * @return 结果
     */
    @Override
    public int deleteCpVariationById(Long id)
    {
//        CpRegister cpRegister = cpRegisterMapper.selectCpRegisterById(id);
//        String studentNum = cpRegister.getStudentNum();
//        int count1 = cpQualityEvaluationMapper.selectCpQualityEvaluationByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count1 > 0){
//            throw new RuntimeException("该学生存在于综合素质评价,不能删除,请先删除综合素质评价的数据");
//        }
//        int count2 = cpMedalRecordMapper.selectCpMedalRecordByStudentNum(studentNum, cpRegister.getDeptId());
//        if(count2 > 0){
//            throw new RuntimeException("该学生存在于奖章记录,不能删除,请先删除奖章记录的数据");
//        }
        return cpVariationMapper.deleteCpVariationById(id);
    }

    /**
     * 导出
     * @param cpVariation
     * @return
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<CpVariation> export(CpVariation cpVariation) {

        List<CpVariation> exportList = cpVariationMapper.export(cpVariation);

        for (CpVariation variation : exportList) {
            //年级
            if (variation.getGrade()!=null){
                String label = dictDataMapper.selectDictLabel("grade_list", variation.getGrade());
                variation.setGrade(label);
            }

            //学籍状态
            if (variation.getStatus()  !=null){
                String label = dictDataMapper.selectDictLabel("student_status", variation.getStatus());
                variation.setStatus(label);
            }

            //生源
            if (variation.getSource() !=null) {
                String label = dictDataMapper.selectDictLabel("source_category", variation.getSource());
                variation.setSource(label);
            }

            //性别
            if(variation.getSex() != null){
                String label = dictDataMapper.selectDictLabel("sys_user_sex", variation.getSex());
                variation.setSex(label);
            }
        }
        return exportList;
    }
}
