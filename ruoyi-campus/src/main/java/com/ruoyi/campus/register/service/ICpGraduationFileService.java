package com.ruoyi.campus.register.service;

import com.ruoyi.campus.register.domain.CpGraduationFile;

import java.util.List;

/**
 * 毕业学生档案Service接口
 * 
 * @author ruoyi
 * @date 2023-08-01
 */
public interface ICpGraduationFileService 
{
    /**
     * 查询毕业学生档案
     * 
     * @param id 毕业学生档案主键
     * @return 毕业学生档案
     */
    public CpGraduationFile selectCpGraduationFileById(Long id);

    /**
     * 查询毕业学生档案列表
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 毕业学生档案集合
     */
    public List<CpGraduationFile> selectCpGraduationFileList(CpGraduationFile cpGraduationFile);

    /**
     * 新增毕业学生档案
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 结果
     */
    public int insertCpGraduationFile(CpGraduationFile cpGraduationFile);

    /**
     * 修改毕业学生档案
     * 
     * @param cpGraduationFile 毕业学生档案
     * @return 结果
     */
    public int updateCpGraduationFile(CpGraduationFile cpGraduationFile);

    /**
     * 批量删除毕业学生档案
     * 
     * @param ids 需要删除的毕业学生档案主键集合
     * @return 结果
     */
    public int deleteCpGraduationFileByIds(Long[] ids);

    /**
     * 删除毕业学生档案信息
     * 
     * @param id 毕业学生档案主键
     * @return 结果
     */
    public int deleteCpGraduationFileById(Long id);

    /**
     * 导出
     * @param cpGraduationFile
     * @return
     */
    List<CpGraduationFile> export(CpGraduationFile cpGraduationFile);
}
