package com.ruoyi.campus.enums;

/**
 * 通用枚举
 */
public class CommonEnum {
    /**
     * 学籍状态
     */
    public enum registerStatus{
        //在校
        AT_SCHOOL("1"),
        //转班
        CHANGE_CLASSES("2"),
        //毕业
        GRADUATE("3"),
        //休学
        QUIT_SCHOOL("4"),
        //复学
        RESUMING_SCHOOL("5");

        private String value;
        private registerStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    /**
     * 撤销字段,0表示未撤销,1表示已撤销,2表示已转班
     */
    public enum revocation{
        ZERO(0),
        ONE(1),
        TWO(2);
        private int value;
        private revocation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    /**
     * 1表示当前学期
     */
    public enum currentSemester{
        CURRENT("1");

        private String value;
        private currentSemester(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    /**
     * 素质评价状态,1表示待评,2表示已评
     */
    public enum QualityEvaluationStatus{
        AWAIT("1"),
        ALREADY("2");

        private String value;
        private QualityEvaluationStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    /**
     * 成绩录入状态
     */
    public enum ScoreEntryStatus{
        //待录入
        WAIT("1"),
        //进行中
        UNDER_WAY("2"),
        //已结束
        FINISH("3");
        private String value;
        private ScoreEntryStatus(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
}
