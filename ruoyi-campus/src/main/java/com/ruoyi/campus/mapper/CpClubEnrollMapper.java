package com.ruoyi.campus.mapper;

import java.util.List;
import com.ruoyi.campus.domain.CpClubEnroll;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

/**
 * 社团报名Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
public interface CpClubEnrollMapper 
{
    /**
     * 查询社团报名
     * 
     * @param id 社团报名主键
     * @return 社团报名
     */
    public CpClubEnroll selectCpClubEnrollById(Long id);

    /**
     * 查询社团报名列表
     * 
     * @param cpClubEnroll 社团报名
     * @return 社团报名集合
     */
    public List<CpClubEnroll> selectCpClubEnrollList(CpClubEnroll cpClubEnroll);

    /**
     * 根据学生id查询报名记录
     */
    List<CpClubEnroll> accordingRegisterIdQuyCpClubEnroll(CpClubEnroll cpClubEnroll);


    /**
     * 新增社团报名
     * 
     * @param cpClubEnroll 社团报名
     * @return 结果
     */
    public int insertCpClubEnroll(CpClubEnroll cpClubEnroll);

    /**
     * 修改社团报名
     * 
     * @param cpClubEnroll 社团报名
     * @return 结果
     */
    public int updateCpClubEnroll(CpClubEnroll cpClubEnroll);

    /**
     * 删除社团报名
     * 
     * @param id 社团报名主键
     * @return 结果
     */
    public int deleteCpClubEnrollById(Long id);

    /**
     * 批量删除社团报名
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpClubEnrollByIds(Long[] ids);

    /**
     * 根据社团id删除报名记录
     * @param clubId 社团id
     */
    @Delete("delete from cp_club_enroll where club_id = #{clubId}")
    void clubIdDelEnrollInfo(@Param("clubId") Long clubId);
}
