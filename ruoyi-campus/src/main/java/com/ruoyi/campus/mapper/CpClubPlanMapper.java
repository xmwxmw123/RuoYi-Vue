package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpClubPlan;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 社团计划Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
public interface CpClubPlanMapper
{
    /**
     * 新增社团计划
     * @param cpClubPlan 社团计划
     */
    void insertCpClubPlan(CpClubPlan cpClubPlan);

    /**
     * 根据id查询活动计划
     */
    @Select("select * from cp_club_plan where id = #{id}")
    CpClubPlan getCpClubPlanInfo(@Param("id") Long id);

    /**
     * 更新活动计划
     */
    void updCpClubPlanInfo(CpClubPlan cpClubPlan);

    /**
     * 通过社团id查询社团信息
     * @param id 社团id
     * @return 社团信息
     */
    @Select("select * from cp_club_plan where club_id = #{id}")
    List<CpClubPlan> getCpClubPlan(@Param("id") Long id);

    /**
     * 根据id删除计划
     */
    @Delete("delete from cp_club_plan where id = #{id}")
    void delCpClubPlan(@Param("id") Long id);
}
