package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpParent;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 家长账号Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-27
 */
public interface CpParentMapper
{
    /**
     * 根据家长手机号码查询家长账号
     *
     * @param parent 参数
     * @return 家长账号
     */
    public CpParent selectCpParentByPhone(CpParent parent);

    /**
     * 查询家长账号
     * 
     * @param id 家长账号主键
     * @return 家长账号
     */
    public CpParent selectCpParentById(Long id);

    /**
     * 查询家长账号列表
     * 
     * @param cpParent 家长账号
     * @return 家长账号集合
     */
    public List<CpParent> selectCpParentList(CpParent cpParent);

    /**
     * 新增家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    public int insertCpParent(CpParent cpParent);

    /**
     * 修改家长账号
     * 
     * @param cpParent 家长账号
     * @return 结果
     */
    public int updateCpParent(CpParent cpParent);

    /**
     * 删除家长账号
     * 
     * @param id 家长账号主键
     * @return 结果
     */
    public int deleteCpParentById(Long id);

    /**
     * 批量删除家长账号
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpParentByIds(Long[] ids);

    /**
     * 修改家长账号状态
     * @param cpParent
     * @return
     */
    int updateUsableStatus(CpParent cpParent);

    /**
     * 查询家长账号信息
     * @param username 登录账号
     * @param password 登录密码
     * @param deptId 部门id
     * @return 家长信息
     */
    @Select("select * from cp_parent where parent_phone = #{username} and password = #{password} and dept_id = #{deptId}")
    CpParent selectCpParentInfo(@Param("username") String username, @Param("password") String password, @Param("deptId") Long deptId);
}
