package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpClubActivity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 社团活动Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
public interface CpClubActivityMapper 
{
    /**
     * 查询社团活动
     * 
     * @param id 社团活动主键
     * @return 社团活动
     */
    public CpClubActivity selectCpClubActivityById(Long id);

    /**
     * 查询社团活动列表
     * 
     * @param cpClubActivity 社团活动
     * @return 社团活动集合
     */
    public List<CpClubActivity> selectCpClubActivityList(CpClubActivity cpClubActivity);

    /**
     * 查询校园社团活动列表(家长端APP)
     */
    List<CpClubActivity> parentAppClublist(CpClubActivity cpClubActivity);

    /**
     * 新增社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    public int insertCpClubActivity(CpClubActivity cpClubActivity);

    /**
     * 修改社团活动
     * 
     * @param cpClubActivity 社团活动
     * @return 结果
     */
    public int updateCpClubActivity(CpClubActivity cpClubActivity);

    /**
     * 删除社团活动
     * 
     * @param id 社团活动主键
     * @return 结果
     */
    public int deleteCpClubActivityById(Long id);

    /**
     * 批量删除社团活动
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpClubActivityByIds(Long[] ids);

    /**
     * 查询社团活动数量
     * @param clubId
     */
    Long selectActivityNum(Long clubId);

    /**
     * 根据社团id查询社团活动
     * @param clubId
     */
    List<CpClubActivity> selectCpClubActivityByClubId(Long clubId);

    /**
     * 根据活动计划i更新活动
     */
    @Select("select * from cp_club_activity where plan_id = #{planId}")
    CpClubActivity accordingPlanIdQuyActivity(@Param("planId") Long planId);

    /**
     * 根据活动计划id删除活动
     */
    @Delete("delete from cp_club_activity where plan_id = #{planId}")
    void accordingPlanIdDelActivity(@Param("planId") Long planId);
}
