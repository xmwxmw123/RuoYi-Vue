package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportSemesters;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 设置学期Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-27
 */
@Mapper
public interface CpSemestersMapper 
{
    /**
     * 查询设置学期
     * 
     * @param id 设置学期主键
     * @return 设置学期
     */
    public CpSemesters selectCpSemestersById(Long id);

    /**
     * 查询设置学期列表
     * 
     * @param cpSemesters 设置学期
     * @return 设置学期集合
     */
    public List<CpSemesters> selectCpSemestersList(CpSemesters cpSemesters);

    /**
     * 新增设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    public int insertCpSemesters(CpSemesters cpSemesters);

    /**
     * 修改设置学期
     * 
     * @param cpSemesters 设置学期
     * @return 结果
     */
    public int updateCpSemesters(CpSemesters cpSemesters);

    /**
     * 删除设置学期
     * 
     * @param id 设置学期主键
     * @return 结果
     */
    public int deleteCpSemestersById(Long id);

    /**
     * 批量删除设置学期
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpSemestersByIds(Long[] ids);


    /* 查询学期列表*/;
    //@Select("SELECT * FROM cp_semesters  ")
    List<CpSemesters> query(CpSemesters cpSemesters);

    /*设置为当前学期*/
    @Update(" UPDATE cp_semesters set current_semester = 1 WHERE id= #{id}")
    int setCurrentSemester(Long id);


    /**
     *  导出列表数据
     * @param exportSemesters
     * @return
     */
    List<ExportSemesters> ExportSelectCpSemestersList(ExportSemesters exportSemesters);


    /**
     *  查询根据学期查询是否唯一
     * @param semester
     * @return
     */
    @Select("SELECT count(1) from cp_semesters where semester = #{semester} and dept_id = #{deptId}")
    int countBySemester(@Param("semester") String semester, @Param("deptId") Long deptId);

    /**
     * 查询当前学期,current_semester = 1,表示当前学期
     * @param currentSemester
     * @return
     */
    @Select("select * from cp_semesters where current_semester = #{currentSemester} and dept_id = #{deptId}")
    CpSemesters selectCpCurrentSemester(@Param("currentSemester") String currentSemester, @Param("deptId") Long deptId);


    @Select("select semester from cp_semesters where id=#{semestersId} and dept_id = #{deptId}")
    CpSemesters selectSemesters(@Param("rank") String rank, @Param("deptId") Long deptId);

    CpSemesters selectByid(Long semestersId);
}
