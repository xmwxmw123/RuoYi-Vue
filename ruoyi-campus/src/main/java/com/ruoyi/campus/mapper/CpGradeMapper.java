package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.exportVo.ExportGrade;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设置届别Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-28
 */
@Mapper
public interface CpGradeMapper 
{
    /**
     * 查询设置届别
     * 
     * @param id 设置届别主键
     * @return 设置届别
     */
    public CpGrade selectCpGradeById(Long id);

    /**
     * 查询设置届别列表
     * 
     * @param cpGrade 设置届别
     * @return 设置届别集合
     */
    public List<CpGrade> selectCpGradeList(CpGrade cpGrade);

    /**
     * 新增设置届别
     * 
     * @param cpGrade 设置届别
     * @return 结果
     */
    public int insertCpGrade(CpGrade cpGrade);

    /**
     * 修改设置届别
     * 
     * @param cpGrade 设置届别
     * @return 结果
     */
    public int updateCpGrade(CpGrade cpGrade);

    /**
     * 删除设置届别
     * 
     * @param id 设置届别主键
     * @return 结果
     */
    public int deleteCpGradeById(Long id);

    /**
     * 批量删除设置届别
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpGradeByIds(Long[] ids);


    /**
     *  升级当前年级
     * @param
     * @return
     */
    int upgrade(CpGrade cpGrade);


    /**
     *  查询届别列表
     * @param cpGrade
     * @return
     */
    List<CpGrade>  queryList(CpGrade cpGrade );


    /**
     *  根据id查询班级
     * @param id
     * @return
     */
    CpGrade selectById(Long id);

    /**
     * 查询界别重复的个数
     * @return
     */
    @Select("select count(*) from cp_grade where rank_s=#{rankS} and dept_id=#{deptId}")
    int selectRankS(@Param("rankS") String rankS, @Param("deptId") Long deptId);

    // 根据届别id查询 并从新赋值
    CpGrade selectRankById(long period);

    /**
     *  导出查询列表
     * @param exportGrade
     * @return
     */
    List<ExportGrade> ExportSelectCpGradeList (ExportGrade exportGrade);

    /**
     * 根据届别id查询班级
     * @param period
     * @return
     */
    List<Map<String, String>> findRankById(@Param("period") Long[] period);

    /**
     *  根据 period 查询届别
     * @param period
     * @return
     */
    @Select("select * from cp_grade where id = #{period} and dept_id=#{deptId}")
    CpGrade selectRank(@Param("period") long period, @Param("deptId") Long deptId);

    /**
     *   根据届别id查询所有的届别
     * @param gradeIdList
     * @return
     */
    List<CpGrade> selectIdRan(List<Object> gradeId);


    List<Map<String,String>>  selectGradeByIdClass(ArrayList<Integer> gradeId);

    /**
     * 查出currentClass存不存在相同
     * @param currentClass
     * @return
     */
    @Select("select count(*) from cp_grade where current_class=#{currentClass} and dept_id=#{deptId}")
    int selectCurrentClassCount(@Param("currentClass") String currentClass, @Param("deptId") Long deptId);

    CpGrade selectCurrentClass(@Param("period") String period, @Param("deptId") Long deptId);
}
