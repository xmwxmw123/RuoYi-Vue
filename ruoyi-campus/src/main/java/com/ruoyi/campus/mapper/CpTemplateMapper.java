package com.ruoyi.campus.mapper;

import java.util.List;
import com.ruoyi.campus.domain.CpTemplate;

/**
 * 评价模板Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
public interface CpTemplateMapper 
{
    /**
     * 查询评价模板
     * 
     * @param id 评价模板主键
     * @return 评价模板
     */
    public CpTemplate selectCpTemplateById(Long id);

    /**
     * 查询评价模板列表
     * 
     * @param cpTemplate 评价模板
     * @return 评价模板集合
     */
    public List<CpTemplate> selectCpTemplateList(CpTemplate cpTemplate);

    /**
     * 新增评价模板
     * 
     * @param cpTemplate 评价模板
     * @return 结果
     */
    public int insertCpTemplate(CpTemplate cpTemplate);

    /**
     * 修改评价模板
     * 
     * @param cpTemplate 评价模板
     * @return 结果
     */
    public int updateCpTemplate(CpTemplate cpTemplate);

    /**
     * 删除评价模板
     * 
     * @param id 评价模板主键
     * @return 结果
     */
    public int deleteCpTemplateById(Long id);

    /**
     * 批量删除评价模板
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpTemplateByIds(Long[] ids);
}
