package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpBdxs;
import com.ruoyi.campus.domain.CpParentStu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 家长绑定学生mapper
 */
public interface CpParentStuMapper {
    /**
     * 新增绑定学生
     * @param cpParentStu
     * @return
     */
    int insertCpParentStu(CpParentStu cpParentStu);

    /**
     *
     * @param parentId
     */
    List<CpBdxs> selectBdxs(Long parentId);

    /**
     * 根据parentId删除学生数据
     * @param parentId
     */
    void deleteCpParentStu(Long parentId);

    /**
     * 根据id查询家长绑定学生关系
     * @param id 家长绑定学生主键
     * @return 家长绑定学生信息
     */
    @Select("select * from cp_parent_stu where id = #{id}")
    CpParentStu getCpParentStu(@Param("id") Long id);

    /**
     * 更新审核状态
     * @param type 审核状态
     * @param id 主键
     */
    @Update("update cp_parent_stu set audit_status=#{type} where id=#{id}")
    void updCpParentStu(@Param("type") String type, @Param("id") Long id);

    @Delete("delete from cp_parent_stu where id=#{id}")
    void delCpParentStu(@Param("id") Long id);
}
