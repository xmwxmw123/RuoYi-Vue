package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpStaffProfile;
import com.ruoyi.campus.domain.exportVo.ExportStaffProfile;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 教工档案Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface CpStaffProfileMapper 
{
    /**
     * 查询教工档案
     * 
     * @param id 教工档案主键
     * @return 教工档案
     */
    public  List<CpStaffProfile> selectCpStaffProfileById(CpStaffProfile params);

    /**
     * 查询教工档案列表
     * 
     * @param cpStaffProfile 教工档案
     * @return 教工档案集合
     */
    public List<CpStaffProfile> selectCpStaffProfileList(CpStaffProfile cpStaffProfile);

    /**
     * 新增教工档案
     * 
     * @param cpStaffProfile 教工档案
     * @return 结果
     */
    public int insertCpStaffProfile(CpStaffProfile cpStaffProfile);

    /**
     * 修改教工档案
     * 
     * @param cpStaffProfile 教工档案
     * @return 结果
     */
    public int updateCpStaffProfile(CpStaffProfile cpStaffProfile);

    /**
     * 删除教工档案
     * 
     * @param id 教工档案主键
     * @return 结果
     */
    public int deleteCpStaffProfileById(Long id);

    /**
     * 批量删除教工档案
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpStaffProfileByIds(Long[] ids);

    /**
     *  根据职工id查询教师
     * @param id
     * @return
     */
    CpStaffProfile selectById(Long id);

    /**
     *  删除离职时间
     * @param id
     */
    void updateLeaveDate(Long id);

    /*根据教师id 查询教师想姓名*/
    CpStaffProfile selectTeacherName(long teacherId);


    /**
     *  导出列表数据
     * @param exportStaffProfile
     * @return
     */
    List<ExportStaffProfile> ExportSelectCpStaffProfileList(ExportStaffProfile exportStaffProfile);

    /**
     * 查询出在职老师数量
     * @param status
     */
    @Select("select count(*) from cp_staff_profile where status=#{status} and dept_id = #{deptId}")
    int selectTeacherCount(@Param("status") String status, @Param("deptId") Long deptId);


    //   查询身份证号是否唯一
    @Select("SELECT * FROM  cp_staff_profile WHERE id_number = #{idNumber} and dept_id = #{deptId}")
    String selectTeacherIdNumber(@Param("idNumber") String idNumber, @Param("deptId") Long deptId);


    // 查询编号是否唯一
    @Select("SELECT * FROM  cp_staff_profile WHERE numbering = #{numbering} and dept_id = #{deptId}")
    String selectTeacherNumbering(@Param("numbering") String numbering, @Param("deptId") Long deptId);

    /**
     * query接口
     * @param cpStaffProfile
     * @return
     */
    List<CpStaffProfile> selectCpStaffProfileQuery(CpStaffProfile cpStaffProfile);

    /**
     * 根据userid获取到教职工表的id
     * @param userId
     */
    Long selectCpStaffProfileByUserId(Long userId);
}
