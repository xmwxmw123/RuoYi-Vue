package com.ruoyi.campus.mapper;

import java.util.List;
import com.ruoyi.campus.domain.CpCampus;

/**
 * 设置校园Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-08
 */
public interface CpCampusMapper 
{
    /**
     * 查询设置校园
     * 
     * @param id 设置校园主键
     * @return 设置校园
     */
    public CpCampus selectCpCampusById(Long id);

    /**
     * 查询设置校园列表(PC端和教师APP)
     * 
     * @param cpCampus 设置校园
     * @return 设置校园集合
     */
    public List<CpCampus> selectCpCampusList(CpCampus cpCampus);

    /**
     * 查询设置校园列表(家长端APP)
     */
    List<CpCampus> parentAppQuyCpCampusList(Long deptId);

    /**
     * 新增设置校园
     * 
     * @param cpCampus 设置校园
     * @return 结果
     */
    public int insertCpCampus(CpCampus cpCampus);

    /**
     * 修改设置校园
     * 
     * @param cpCampus 设置校园
     * @return 结果
     */
    public int updateCpCampus(CpCampus cpCampus);

    /**
     * 删除设置校园
     * 
     * @param id 设置校园主键
     * @return 结果
     */
    public int deleteCpCampusById(Long id);

    /**
     * 批量删除设置校园
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpCampusByIds(Long[] ids);
}
