package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpSubject;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 设置学科Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
public interface CpSubjectMapper 
{
    /**
     * 查询设置学科
     * 
     * @param id 设置学科主键
     * @return 设置学科
     */
    public CpSubject selectCpSubjectById(Long id);

    /**
     * 查询设置学科列表
     * 
     * @param cpSubject 设置学科
     * @return 设置学科集合
     */
    public List<CpSubject> selectCpSubjectList(CpSubject cpSubject);

    /**
     * 新增设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    public int insertCpSubject(CpSubject cpSubject);

    /**
     * 修改设置学科
     * 
     * @param cpSubject 设置学科
     * @return 结果
     */
    public int updateCpSubject(CpSubject cpSubject);

    /**
     * 删除设置学科
     * 
     * @param id 设置学科主键
     * @return 结果
     */
    public int deleteCpSubjectById(Long id);

    /**
     * 批量删除设置学科
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpSubjectByIds(Long[] ids);

    /**
     * 根据年级查询数据
     */

    void selectCpSubjectByGrade();

    /**
     *
     */
    @Select("select * from cp_subject where dept_id = #{deptId}")
    List<CpSubject> selectCpSubject(@Param("deptId") Long deptId);
}
