package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.domain.exportVo.ExportClass;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 设置班级Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface CpClassMapper 
{

    /**
     *
     * @param ids
     * @return
     */
    List<CpClass> selectClass(Long[] ids);
    /**
     * 查询设置班级
     * 
     * @param id 设置班级主键
     * @return 设置班级
     */
    public CpClass selectCpClassById(Long id);

    /**
     * 查询设置班级列表
     * 
     * @param cpClass 设置班级
     * @return 设置班级集合
     */
    public List<CpClass> selectCpClassList(CpClass cpClass);

    /**
     * 新增设置班级
     * 
     * @param cpClass 设置班级
     * @return 结果
     */
    public int insertCpClass(CpClass cpClass);

    /**
     * 修改设置班级
     * 
     * @param cpClass 设置班级
     * @return 结果
     */
    public int updateCpClass(CpClass cpClass);

    /**
     * 删除设置班级
     * 
     * @param id 设置班级主键
     * @return 结果
     */
    public int deleteCpClassById(Long id);

    /**
     * 批量删除设置班级
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpClassByIds(Long[] ids);


    /**
     * 根据届别查询年级
     * @return
     */
    List<CpClass> queryRankSList(@Param("rankS") String rankS, @Param("deptId") Long deptId);


    /**
     *   查询班级总数
     */

    int selectCount(@Param("rankS") String rankS, @Param("deptId") Long deptId);

    /**
     *   查询班级列表
     *   查询班级列表
     * @return
     */

    List<CpClass> findList(@Param("id") Long id, @Param("deptId") Long deptId);

    //TODO
    List<ExportClass> findCpClassList(ExportClass exportClass);

    List<CpClass> selectTeacherId(Long[] ids);

    List<CpClass> selectCpClassByIdRanks(Long[] ids);

    /**
     * 查询根据班级查询老师
     * @param classId
     * @return
     */
    @Select("select teacher,teacher_id from cp_class where id=#{classId}")
    CpClass selectTeacher(String classId);

    /*判断班级名称是否重复*/
    @Select("SELECT count(1)  from cp_class where period = #{period}  and  class_name = #{className} and dept_id = #{deptId}")
    int countBySemester(@Param("className") String className ,@Param("period") Long period, @Param("deptId") Long deptId);


    /* 根据班级id查询*/
    List<CpClass>  QueryMethodClassId(List<Long> classIds);

    /**
     * 根据届别id查询班级
     * @param period
     */
    @Select("select c.id, c.class_name from cp_class c where period=#{period} and dept_id = #{deptId}")
    List<CpClass> selectCpClassByPeriod(@Param("period") Long period, @Param("deptId") Long deptId);


    /**
     * 修改班级状态为毕业
     */
    @Update("update cp_class set graduate =#{graduate} where period =#{id} ")
    void updateCpClassGraduate(@Param("graduate") String graduate, @Param("id") Long id);

    /**
     * 查询未毕业的班级
     * @param graduate
     */
    @Select("select count(*) from cp_class where graduate=#{graduate} and dept_id = #{deptId}")
    int selectlassCount(@Param("graduate") String graduate, @Param("deptId") Long deptId);


    List<Map<String, String>> findListNotNull(CpClass cpClass);


    @Select("SELECT count(1) from cp_class where class_number = #{classNumber} and period = #{period} and dept_id = #{deptId}")
    int countclassNumber( @Param("classNumber") String classNumber,  @Param("period") long period, @Param("deptId") Long deptId);


    int selectclassCount(@Param("id") Long id, @Param("deptId") Long deptId);

    /**
     * 根据级别id查询班级
     * @param period
     * @return
     */
    int selectClassByperiod(Long period);
}
