package com.ruoyi.campus.mapper;

import com.ruoyi.campus.domain.CpTimetable;

import java.util.List;

/**
 * 课表查询Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
public interface CpTimetableMapper 
{
    /**
     * 查询课表查询
     * 
     * @param id 课表查询主键
     * @return 课表查询
     */
    public CpTimetable selectCpTimetableById(Long id);

    /**
     * 查询课表查询列表
     * 
     * @param cpTimetable 课表查询
     * @return 课表查询集合
     */
    public List<CpTimetable> selectCpTimetableList(CpTimetable cpTimetable);

    /**
     * 新增课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    public int insertCpTimetable(CpTimetable cpTimetable);

    /**
     * 修改课表查询
     * 
     * @param cpTimetable 课表查询
     * @return 结果
     */
    public int updateCpTimetable(CpTimetable cpTimetable);

    /**
     * 删除课表查询
     * 
     * @param id 课表查询主键
     * @return 结果
     */
    public int deleteCpTimetableById(Long id);

    /**
     * 批量删除课表查询
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpTimetableByIds(Long[] ids);

    void updateStaffId(CpTimetable cpTimetable);
}
