package com.ruoyi.campus.mapper;

import java.util.List;

import com.ruoyi.campus.domain.CpTimetable;
import com.ruoyi.campus.domain.CpTimetableSet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 教师任课设置Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
public interface CpTimetableSetMapper 
{
    /**
     * 查询教师任课设置
     * 
     * @param id 教师任课设置主键
     * @return 教师任课设置
     */
    public CpTimetableSet selectCpTimetableSetById(Long id);

    /**
     * 查询教师任课设置列表
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 教师任课设置集合
     */
    public List<CpTimetableSet> selectCpTimetableSetList(CpTimetableSet cpTimetableSet);

    /**
     * 新增教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    public int insertCpTimetableSet(CpTimetableSet cpTimetableSet);

    /**
     * 修改教师任课设置
     * 
     * @param cpTimetableSet 教师任课设置
     * @return 结果
     */
    public int updateCpTimetableSet(CpTimetableSet cpTimetableSet);

    /**
     * 删除教师任课设置
     * 
     * @param id 教师任课设置主键
     * @return 结果
     */
    public int deleteCpTimetableSetById(Long id);

    /**
     * 批量删除教师任课设置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCpTimetableSetByIds(Long[] ids);

    @Select("SELECT * from cp_timetable where timetable_set = #{timetableSt} and dept_id = #{deptId}")
    List<CpTimetable> quyCpTimetableList(@Param("timetableSt") Long timetableSt, @Param("deptId") Long deptId);

    @Select("SELECT count(1) from cp_timetable_set where semesters_id = #{semestersId} and period = #{period} and grade = #{grade} and class_id = #{classId} and dept_id = #{deptId}")
    Integer quyCpTimetableSetRepeat(@Param("semestersId") Long semestersId, @Param("period") String period,
                                    @Param("grade") String grade, @Param("classId") Long classId, @Param("deptId") Long deptId);
}
