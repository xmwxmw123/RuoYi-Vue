package com.ruoyi.campus.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.campus.domain.dto.CpTimetableReqDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.campus.domain.CpTimetableSet;
import com.ruoyi.campus.service.ICpTimetableSetService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 教师任课设置Controller
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/timetableSet/timetableSet")
public class CpTimetableSetController extends BaseController
{
    @Autowired
    private ICpTimetableSetService cpTimetableSetService;

    /**
     * 查询教师任课设置列表
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpTimetableSet cpTimetableSet)
    {
        startPage();
        List<CpTimetableSet> list = cpTimetableSetService.selectCpTimetableSetList(cpTimetableSet);
        return getDataTable(list);
    }

    /**
     * 导出教师任课设置列表
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:export')")
    @Log(title = "教师任课设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpTimetableSet cpTimetableSet)
    {
        List<CpTimetableSet> list = cpTimetableSetService.selectCpTimetableSetList(cpTimetableSet);
        ExcelUtil<CpTimetableSet> util = new ExcelUtil<CpTimetableSet>(CpTimetableSet.class);
        util.exportExcel(response, list, "教师任课设置数据");
    }

    /**
     * 获取教师任课设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpTimetableSetService.selectCpTimetableSetById(id));
    }

    /**
     * 新增教师任课设置
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:add')")
    @Log(title = "教师任课设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpTimetableSet cpTimetableSet)
    {
        cpTimetableSet.setDeptId(getDeptId());
        cpTimetableSet.setUserId(getUserId());
        return toAjax(cpTimetableSetService.insertCpTimetableSet(cpTimetableSet));
    }

    /**
     * 修改教师任课设置
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:edit')")
    @Log(title = "教师任课设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpTimetableSet cpTimetableSet)
    {
        return toAjax(cpTimetableSetService.updateCpTimetableSet(cpTimetableSet));
    }

    /**
     * 删除教师任课设置
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:remove')")
    @Log(title = "教师任课设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpTimetableSetService.deleteCpTimetableSetByIds(ids));
    }

    /**
     * 排课保存
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:add')")
    @PostMapping("/arrangeCourse")
    public AjaxResult arrangeCourse(@RequestBody CpTimetableReqDto cpTimetableReqDto)
    {
        cpTimetableReqDto.setDeptId(getDeptId());
        cpTimetableReqDto.setUserId(getUserId());
        cpTimetableSetService.arrangeCourse(cpTimetableReqDto);
        return success();
    }

    /**
     * 排课查询
     */
    @PreAuthorize("@ss.hasPermi('timetableSet:timetableSet:query')")
    @GetMapping(value = "/arrangeCourseList")
    public AjaxResult arrangeCourseList(@RequestParam("id") Long id)
    {
        return success(cpTimetableSetService.arrangeCourseList(id));
    }
}
