package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpClubActivity;
import com.ruoyi.campus.service.ICpClubActivityService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 社团活动Controller
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@RestController
@RequestMapping("/clubActivity/clubActivity")
public class CpClubActivityController extends BaseController
{
    @Autowired
    private ICpClubActivityService cpClubActivityService;

    /**
     * 查询社团活动列表
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpClubActivity cpClubActivity)
    {
        startPage();
        List<CpClubActivity> list = cpClubActivityService.selectCpClubActivityList(cpClubActivity);
        return getDataTable(list);
    }

    /**
     * 查询社团活动列表(app用)
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:list')")
    @GetMapping("/appList")
    public TableDataInfo appList(CpClubActivity cpClubActivity)
    {
        startPage();
        cpClubActivity.setDeptId(getDeptId());
        cpClubActivity.setUserId(getUserId());
        List<CpClubActivity> list = cpClubActivityService.selectCpClubActivityList(cpClubActivity);
        return getDataTable(list);
    }

    /**
     * 导出社团活动列表
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:export')")
    @Log(title = "社团活动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpClubActivity cpClubActivity)
    {
        List<CpClubActivity> list = cpClubActivityService.export(cpClubActivity);
        ExcelUtil<CpClubActivity> util = new ExcelUtil<CpClubActivity>(CpClubActivity.class);
        util.exportExcel(response, list, "社团活动数据");
    }

    /**
     * 获取社团活动详细信息
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpClubActivityService.selectCpClubActivityById(id));
    }

    /**
     * 新增社团活动
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:add')")
    @Log(title = "社团活动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpClubActivity cpClubActivity)
    {
        cpClubActivity.setUserId(getUserId());
        cpClubActivity.setDeptId(getDeptId());
        return toAjax(cpClubActivityService.insertCpClubActivity(cpClubActivity));
    }

    /**
     * 修改社团活动
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:edit')")
    @Log(title = "社团活动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpClubActivity cpClubActivity)
    {
        return toAjax(cpClubActivityService.updateCpClubActivity(cpClubActivity));
    }

    /**
     * 删除社团活动
     */
    @PreAuthorize("@ss.hasPermi('clubActivity:clubActivity:remove')")
    @Log(title = "社团活动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpClubActivityService.deleteCpClubActivityByIds(ids));
    }
}
