package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpCampus;
import com.ruoyi.campus.service.ICpCampusService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 设置校园Controller
 * 
 * @author ruoyi
 * @date 2023-09-08
 */
@RestController
@RequestMapping("/campus/campus")
public class CpCampusController extends BaseController
{
    @Autowired
    private ICpCampusService cpCampusService;

    /**
     * 查询设置校园列表
     */
    @PreAuthorize("@ss.hasPermi('campus:campus:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpCampus cpCampus)
    {
        startPage();
        List<CpCampus> list = cpCampusService.selectCpCampusList(cpCampus);
        return getDataTable(list);
    }

    /**
     * 获取设置校园详细信息
     */
    @PreAuthorize("@ss.hasPermi('campus:campus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpCampusService.selectCpCampusById(id));
    }

    /**
     * 新增设置校园
     */
    @PreAuthorize("@ss.hasPermi('campus:campus:add')")
    @Log(title = "设置校园", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpCampus cpCampus)
    {
        cpCampus.setDeptId(getDeptId());
        cpCampus.setUserId(getUserId());
        return toAjax(cpCampusService.insertCpCampus(cpCampus));
    }

    /**
     * 修改设置校园
     */
    @PreAuthorize("@ss.hasPermi('campus:campus:edit')")
    @Log(title = "设置校园", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpCampus cpCampus)
    {
        return toAjax(cpCampusService.updateCpCampus(cpCampus));
    }

    /**
     * 删除设置校园
     */
    @PreAuthorize("@ss.hasPermi('campus:campus:remove')")
    @Log(title = "设置校园", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpCampusService.deleteCpCampusByIds(ids));
    }

    /**
     * 头像上传
     */
    @Log(title = "设置校园图片", businessType = BusinessType.UPDATE)
    @PostMapping("/picture")
    public AjaxResult picture(@RequestParam("picturefile") MultipartFile file) throws Exception
    {
        if (!file.isEmpty())
        {
            String picture = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("imgUrl", picture);
            return ajax;
        }
        return error("上传图片异常，请联系管理员");
    }
}