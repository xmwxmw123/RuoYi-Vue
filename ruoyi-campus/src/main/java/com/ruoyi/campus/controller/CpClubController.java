package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpClub;
import com.ruoyi.campus.domain.dto.CpClubStuDto;
import com.ruoyi.campus.service.ICpClubService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 社团管理Controller
 * 
 * @author ruoyi
 * @date 2023-09-22
 */
@RestController
@RequestMapping("/club/club")
public class CpClubController extends BaseController
{
    @Autowired
    private ICpClubService cpClubService;

    /**
     * 查询社团管理列表
     */
    @PreAuthorize("@ss.hasPermi('club:club:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpClub cpClub)
    {
        startPage();
        List<CpClub> list = cpClubService.selectCpClubList(cpClub);
        return getDataTable(list);
    }

    /**
     * 查询社团列表(不带分页的List接口)
     */
    @PreAuthorize("@ss.hasPermi('club:club:list')")
    @GetMapping("/queryList")
    public TableDataInfo queryList(CpClub cpClub)
    {
        List<CpClub> list = cpClubService.selectCpClubList(cpClub);
        return getDataTable(list);
    }

    /**
     * 导出社团管理列表
     */
//    @PreAuthorize("@ss.hasPermi('club:club:export')")
//    @Log(title = "社团管理", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CpClub cpClub)
//    {
//        List<CpClub> list = cpClubService.selectCpClubList(cpClub);
//        ExcelUtil<CpClub> util = new ExcelUtil<CpClub>(CpClub.class);
//        util.exportExcel(response, list, "社团管理数据");
//    }

    /**
     * 获取社团管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('club:club:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpClubService.selectCpClubById(id));
    }

    /**
     * 新增社团管理
     */
    @PreAuthorize("@ss.hasPermi('club:club:add')")
    @Log(title = "社团管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpClub cpClub)
    {
        cpClub.setUserId(getUserId());
        cpClub.setDeptId(getDeptId());
        return toAjax(cpClubService.insertCpClub(cpClub));
    }

    /**
     * 修改社团管理
     */
    @PreAuthorize("@ss.hasPermi('club:club:edit')")
    @Log(title = "社团管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpClub cpClub)
    {
        return toAjax(cpClubService.updateCpClub(cpClub));
    }

    /**
     * 删除社团管理
     */
    @PreAuthorize("@ss.hasPermi('club:club:remove')")
    @Log(title = "社团管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpClubService.deleteCpClubByIds(ids));
    }
}
