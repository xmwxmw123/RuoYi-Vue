package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpGrade;
import com.ruoyi.campus.domain.exportVo.ExportGrade;
import com.ruoyi.campus.service.ICpGradeService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 设置届别Controller
 * 
 * @author ruoyi
 * @date 2023-07-28
 */
@RestController
@RequestMapping("/grade/grade")
public class    CpGradeController extends BaseController
{
    @Autowired
    private ICpGradeService cpGradeService;

    /**
     * 查询设置届别列表
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpGrade cpGrade)
    {
        startPage();
        List<CpGrade> list = cpGradeService.selectCpGradeList(cpGrade);
        return getDataTable(list);
    }

    /**
     * 导出设置届别列表
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:export')")
    @Log(title = "设置届别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportGrade exportGrade) {
        List<ExportGrade> list = cpGradeService.ExportSelectCpGradeList(exportGrade);
        ExcelUtil<ExportGrade> util = new ExcelUtil<ExportGrade>(ExportGrade.class);
        util.exportExcel(response, list, "设置届别数据");
    }

    /**
     * 获取设置届别详细信息
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(cpGradeService.selectCpGradeById(id));
    }

    /**
     * 新增设置届别
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:add')")
    @Log(title = "设置届别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Validated CpGrade cpGrade)
    {

        cpGrade.setUserId(getUserId());
        cpGrade.setDeptId(getDeptId());
        return toAjax(cpGradeService.insertCpGrade(cpGrade));
    }

    /**
     * 修改设置届别
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:edit')")
    @Log(title = "设置届别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpGrade cpGrade)
    {
        return toAjax(cpGradeService.updateCpGrade(cpGrade));
    }

    /**
     * 删除设置届别
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:remove')")
    @Log(title = "设置届别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpGradeService.deleteCpGradeByIds(ids));
    }

    /**
     *  查询届别列表
     * @return
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:query')")
    @GetMapping("query")
    public AjaxResult queryList(CpGrade cpGrade){
        List<CpGrade> query = cpGradeService.queryList(cpGrade);
        return AjaxResult.success(query);
    }


    /**
     *  升级当前年级
     */
    @PreAuthorize("@ss.hasPermi('grade:grade:edit')")
    @Log(title = "升级当前年级", businessType = BusinessType.UPDATE)
    @PostMapping("upgrade")
    public AjaxResult upgrade(@RequestBody CpGrade cpGrade){
        cpGrade.setUserId(getUserId());
        cpGrade.setDeptId(getDeptId());
        return  toAjax(cpGradeService.upgrade( cpGrade));
    }



}
