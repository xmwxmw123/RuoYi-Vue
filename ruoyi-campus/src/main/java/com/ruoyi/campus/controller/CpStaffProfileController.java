package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpStaffProfile;
import com.ruoyi.campus.domain.exportVo.ExportStaffProfile;
import com.ruoyi.campus.service.ICpStaffProfileService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 教工档案Controller
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@RestController
@RequestMapping("/staffProfile/staffProfile")
public class CpStaffProfileController extends BaseController
{
    @Resource
    private ICpStaffProfileService cpStaffProfileService;

    /**
     * 查询教工档案列表
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpStaffProfile cpStaffProfile)
    {
        startPage();
        List<CpStaffProfile> list = cpStaffProfileService.selectCpStaffProfileList(cpStaffProfile);
        return getDataTable(list);
    }

    /**
     * 导出教工档案列表
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:export')")
    @Log(title = "教工档案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportStaffProfile exportStaffProfile)
    {
        List<ExportStaffProfile> list = cpStaffProfileService.ExportSelectCpStaffProfileList(exportStaffProfile);
        ExcelUtil<ExportStaffProfile> util = new ExcelUtil<ExportStaffProfile>(ExportStaffProfile.class);
        util.exportExcel(response, list, "教工档案数据");
    }

    /**
     * 获取教工档案详细信息
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:query')")
    @GetMapping("selectById")
    public AjaxResult getInfo(CpStaffProfile params)
    {
        List<CpStaffProfile>  cpStaffProfile = cpStaffProfileService.selectCpStaffProfileById(params);
        return AjaxResult.success(cpStaffProfile);
    }

    /**
     * 新增教工档案
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:add')")
    @Log(title = "教工档案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add( @RequestBody CpStaffProfile cpStaffProfile)
    {
        return toAjax(cpStaffProfileService.insertCpStaffProfile(cpStaffProfile));
    }

    /**
     * 修改教工档案
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:edit')")
    @Log(title = "教工档案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid @RequestBody CpStaffProfile cpStaffProfile)
    {
        return toAjax(cpStaffProfileService.updateCpStaffProfile(cpStaffProfile));
    }

    /**
     * 删除教工档案
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:remove')")
    @Log(title = "教工档案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpStaffProfileService.deleteCpStaffProfileByIds(ids));
    }

    /**
     *
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:query')")
    @Log(title = "教工档案", businessType = BusinessType.DELETE)
    @GetMapping("/{id}")
    public AjaxResult findById(@PathVariable Long id) {
        CpStaffProfile cpStaffProfile = new CpStaffProfile();
        cpStaffProfile.setId(id);
        return success(cpStaffProfileService.selectCpStaffProfileById(cpStaffProfile));
    }

    /**
     * list接口 , 查询出所有老师
     * @return
     */
    @PreAuthorize("@ss.hasPermi('staffProfile:staffProfile:query')")
    @GetMapping("/query")
    public List<CpStaffProfile> query(CpStaffProfile cpStaffProfile) {
        return cpStaffProfileService.selectCpStaffProfileQuery(cpStaffProfile);
    }
}
