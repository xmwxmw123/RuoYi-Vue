package com.ruoyi.campus.controller;

import com.github.pagehelper.PageInfo;
import com.ruoyi.campus.StuEvaluation.domain.CpActivity;
import com.ruoyi.campus.StuEvaluation.domain.CpActivityPartake;
import com.ruoyi.campus.StuEvaluation.domain.CpMedalRecord;
import com.ruoyi.campus.StuEvaluation.domain.CpQualityEvaluation;
import com.ruoyi.campus.StuEvaluation.mapper.CpActivityPartakeMapper;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityPartakeService;
import com.ruoyi.campus.StuEvaluation.service.ICpActivityService;
import com.ruoyi.campus.StuEvaluation.service.ICpMedalRecordService;
import com.ruoyi.campus.StuEvaluation.service.ICpQualityEvaluationService;
import com.ruoyi.campus.domain.*;
import com.ruoyi.campus.register.domain.CpRegister;
import com.ruoyi.campus.register.service.ICpRegisterService;
import com.ruoyi.campus.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

/**
 * 家长端Controller
 * 
 * @author ruoyi
 * @date 2023-09-27
 */
@RestController
@RequestMapping("/parent/parent")
public class CpParentController extends BaseController
{
    @Autowired
    private ICpParentService cpParentService;

    @Autowired
    private ICpRegisterService cpRegisterService;

    @Resource
    private ICpActivityService cpActivityService;

    @Autowired
    private ICpClubActivityService cpClubActivityService;

    @Resource
    private ICpActivityPartakeService cpActivityPartakeService;

    @Resource
    private ICpClubService cpClubService;

    @Resource
    private ICpClubEnrollService cpClubEnrollService;

    @Resource
    private ICpQualityEvaluationService cpQualityEvaluationService;

    @Resource
    private ICpMedalRecordService cpMedalRecordService;

    @Resource
    private ICpCampusService cpCampusService;

    /**
     * 查询家长账号列表
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpParent cpParent)
    {
        startPage();
        List<CpParent> list = cpParentService.selectCpParentList(cpParent);
        return getDataTable(list);
    }

    /**
     * 导出家长账号列表
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:export')")
    @Log(title = "家长账号", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpParent cpParent)
    {
        List<CpParent> list = cpParentService.selectCpParentList(cpParent);
        ExcelUtil<CpParent> util = new ExcelUtil<CpParent>(CpParent.class);
        util.exportExcel(response, list, "家长账号数据");
    }

    /**
     * 获取家长账号详细信息
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpParentService.selectCpParentById(id));
    }

    /**
     * 新增家长账号
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:add')")
    @Log(title = "家长账号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpParent cpParent)
    {
        cpParent.setUserId(getUserId());
        cpParent.setDeptId(getDeptId());
        return toAjax(cpParentService.insertCpParent(cpParent));
    }

    /**
     * 修改家长账号
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:edit')")
    @Log(title = "家长账号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpParent cpParent)
    {
        return toAjax(cpParentService.updateCpParent(cpParent));
    }

    /**
     * 删除家长账号
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:remove')")
    @Log(title = "家长账号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpParentService.deleteCpParentByIds(ids));
    }

    /**
     * 修改家长账号状态
     */
    @PreAuthorize("@ss.hasPermi('parent:parent:edit')")
    @Log(title = "家长账号", businessType = BusinessType.UPDATE)
    @PutMapping("/usableStatus")
    public AjaxResult updateUsableStatus(Long id)
    {
        return toAjax(cpParentService.updateUsableStatus(id));
    }

    /**
     * 绑定学生(家长APP端)
     * @param cpParent 参数（只需要传入id，bdxs字段）
     */
    @PostMapping("/bindingStudent")
    public AjaxResult bindingStudent(@RequestBody CpParent cpParent)
    {
        cpParentService.bindingStudent(cpParent);
        return toAjax(1);
    }

    /**
     * 查询当前家长下面绑定的学生列表 (家长APP端)
     * @param id 家长账号信息主键
     */
    @GetMapping( "/getBindingStudentList")
    public AjaxResult getBindingStudentList(@RequestParam("id") Long id)
    {
        return success(cpParentService.selectCpParentById(id));
    }

    /**
     * 家长端绑定学生 - 点击添加查询学生列表 (家长APP端)
     */
    @GetMapping("/queryParentRegister")
    public TableDataInfo queryParentRegister(CpRegister cpRegister)
    {
        if (Objects.isNull(cpRegister.getDeptId())) {
            throw new GlobalException("参数错误");
        }
        cpRegister.setDeptId(cpRegister.getDeptId());
        return getDataTable(cpRegisterService.selectCpRegisterQuery(cpRegister));
    }

    /**
     * 审核绑定学生 (PC端)
     * @param id 家长绑定学生表主键
     * @param type  1-通过, 2-未通过
     */
    @GetMapping("/examine")
    public AjaxResult examine(@RequestParam("id") Long id, @RequestParam("type") String type) {
        cpParentService.examine(id, type);
        return toAjax(1);
    }

    /**
     * 我的资料修改（家长APP端）
     * @param cpParent 参数
     */
    @PostMapping("/updMyProfile")
    public AjaxResult updMyProfile(@RequestBody CpParent cpParent) {
        cpParentService.updMyProfile(cpParent);
        return toAjax(1);
    }

    /**
     * 实践活动 - 根据班级id查询实践活动列表(家长端app用)
     */
    @GetMapping("/parentAppPracticeList")
    public AjaxResult appList(CpActivity cpActivity)
    {
        List<CpActivity> list = cpActivityService.parentAppPracticeList(cpActivity);
        list = cpActivityService.selectCpActivityList(list);
        return success(list);
    }

    /**
     * 实践活动 - 查询活动参与情况(家长端APP)
     */
    @GetMapping("/parentAppActivityPartakeList")
    public AjaxResult parentAppActivityPartakeList(CpActivityPartake cpActivityPartake)
    {
        List<CpActivityPartake> list = cpActivityPartakeService.parentAppActivityPartakeList(cpActivityPartake);
        return success(list);
    }

    /**
     * 实践活动 - 上传作品图片(家长端APP)
     */
    @PutMapping("/parentAppActivityPartakeUploadWorks")
    public AjaxResult uploadWorks(@RequestBody CpActivityPartake cpActivityPartake)
    {
        return toAjax(cpActivityPartakeService.uploadWorks(cpActivityPartake));
    }

    /**
     * 社团管理 - 根据部门id查询社团活动列表(家长端app用)
     */
    @GetMapping("/parentAppClublist")
    public AjaxResult parentAppClublist(CpClubActivity cpClubActivity)
    {
        List<CpClubActivity> list = cpClubActivityService.parentAppClublist(cpClubActivity);
        return success(list);
    }

    /**
     * 社团管理 - 查询社团管理列表(家长端app用)
     */
    @GetMapping("/parentAppQuyCpClubList")
    public AjaxResult parentAppQuyCpClubList(CpClub cpClub)
    {
        List<CpClub> list = cpClubService.parentAppQuyCpClubList(cpClub);
        return success(list);
    }

    /**
     * 社团管理 - 查询社团详情(家长端app用)
     */
    @GetMapping("/parentAppGetCpClubInfo")
    public AjaxResult parentAppGetCpClubInfo(@RequestParam("id") Long id)
    {
        return success(cpClubService.selectCpClubById(id));
    }

    /**
     * 社团管理 - 社团报名(家长端app用)
     */
    @PostMapping("parentAppClubEnroll")
    public AjaxResult parentAppClubEnroll(@RequestBody CpClubEnroll cpClubEnroll)
    {
        return toAjax(cpClubEnrollService.insertCpClubEnroll(cpClubEnroll, 0L, 2L));
    }

    /**
     * 社团管理 - 查询当前学生已报名社团(家长端app用)
     */
    @GetMapping("parentAppQuyEnrollClubList")
    public AjaxResult parentAppQuyEnrollClubList(CpClubEnroll clubEnroll)
    {
        List<CpClub> list = cpClubService.parentAppQuyEnrollClubList(clubEnroll);
        return success(list);
    }

    /**
     * 家长练习簿 - 查询当前学生综合素质评价(家长端app用)
     */
    @GetMapping("/parentAppQuyCpQualityEvaluation")
    public AjaxResult parentAppQuyCpQualityEvaluation(CpQualityEvaluation cpQualityEvaluation)
    {
        List<CpQualityEvaluation> list = cpQualityEvaluationService.parentAppQuyCpQualityEvaluation(cpQualityEvaluation);
        return success(list);
    }

    /**
     * 家长练习簿 - 更新家长寄语(家长端app用)
     */
    @PostMapping(value = "/updParentMessage")
    public AjaxResult updParentMessage(@RequestBody CpQualityEvaluation cpQualityEvaluation)
    {
        cpQualityEvaluationService.updParentMessage(cpQualityEvaluation);
        return success();
    }

    /**
     * 家长练习簿 - 查询当前学生奖章记录(家长端app用)
     */
    @GetMapping("/parentAppQuyCpMedalRecord")
    public AjaxResult parentAppQuyCpMedalRecord(CpMedalRecord cpMedalRecord)
    {
        List<CpMedalRecord> list = cpMedalRecordService.parentAppQuyCpMedalRecord(cpMedalRecord);
        return success(list);
    }

    /**
     * 查询校园信息（家长APP端）
     * @param deptId 部门id
     */
    @GetMapping("/parentAppQuyCpCampus")
    public AjaxResult parentAppQuyCpCampus(@RequestParam("deptId") Long deptId)
    {
        List<CpCampus> list = cpCampusService.parentAppQuyCpCampusList(deptId);
        return success(list);
    }
}
