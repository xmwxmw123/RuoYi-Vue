package com.ruoyi.campus.controller;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.campus.domain.CpTimetable;
import com.ruoyi.campus.service.ICpTimetableService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 课表查询Controller
 * 
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/timetable/timetable")
public class CpTimetableController extends BaseController
{
    @Autowired
    private ICpTimetableService cpTimetableService;

    /**
     * 查询课表查询列表
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpTimetable cpTimetable)
    {
        startPage();
        List<CpTimetable> list = cpTimetableService.selectCpTimetableList(cpTimetable);
        return getDataTable(list);
    }

    /**
     * App查询课表查询列表
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:list')")
    @GetMapping("/getCpTimetableList")
    public AjaxResult getCpTimetableList(CpTimetable cpTimetable)
    {
        Map<String,List<CpTimetable>> listMap = new HashMap<>();
        List<CpTimetable> list = cpTimetableService.selectCpTimetableList(cpTimetable);
        if (CollUtil.isNotEmpty(list)) {
            Map<String,List<CpTimetable>> stringListMap = list.stream().collect(Collectors.groupingBy(CpTimetable::getWeek));
            for (String key : stringListMap.keySet()) {
                List<CpTimetable> cpTimetableList = stringListMap.get(key).stream().sorted(Comparator.comparing(CpTimetable::getPitchNumber)).collect(Collectors.toList());
                listMap.put(key, cpTimetableList);
            }
        }
        return AjaxResult.success(listMap);
    }

    /**
     * 导出课表查询列表
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:export')")
    @Log(title = "课表查询", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpTimetable cpTimetable)
    {
        List<CpTimetable> list = cpTimetableService.export(cpTimetable);
        ExcelUtil<CpTimetable> util = new ExcelUtil<CpTimetable>(CpTimetable.class);
        util.exportExcel(response, list, "课表查询数据");
    }

    /**
     * 获取课表查询详细信息
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpTimetableService.selectCpTimetableById(id));
    }

    /**
     * 新增课表查询
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:add')")
    @Log(title = "课表查询", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpTimetable cpTimetable)
    {
        cpTimetable.setUserId(getUserId());
        cpTimetable.setDeptId(getDeptId());
        return toAjax(cpTimetableService.insertCpTimetable(cpTimetable));
    }

    /**
     * 修改课表查询
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:edit')")
    @Log(title = "课表查询", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpTimetable cpTimetable)
    {
        return toAjax(cpTimetableService.updateCpTimetable(cpTimetable));
    }

    /**
     * 删除课表查询
     */
    @PreAuthorize("@ss.hasPermi('timetable:timetable:remove')")
    @Log(title = "课表查询", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpTimetableService.deleteCpTimetableByIds(ids));
    }
}
