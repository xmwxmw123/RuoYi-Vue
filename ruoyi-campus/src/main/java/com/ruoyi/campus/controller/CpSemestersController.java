package com.ruoyi.campus.controller;



import com.ruoyi.campus.domain.CpSemesters;
import com.ruoyi.campus.domain.exportVo.ExportSemesters;
import com.ruoyi.campus.service.ICpSemestersService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 设置学期Controller
 * 
 * @author ruoyi
 * @date 2023-07-27
 */
@RestController
@RequestMapping("/semesters/semesters")
public class CpSemestersController extends BaseController
{
    @Autowired
    private ICpSemestersService cpSemestersService;

    /**
     * 查询设置学期列表
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpSemesters cpSemesters)
    {
        startPage();
        List<CpSemesters> list = cpSemestersService.selectCpSemestersList(cpSemesters);
        return getDataTable(list);
    }

    /**
     * 导出设置学期列表
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:export')")
    @Log(title = "设置学期", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportSemesters exportSemesters)
    {
        List<ExportSemesters> list = cpSemestersService.ExportSelectCpSemestersList(exportSemesters);
        ExcelUtil<ExportSemesters> util = new ExcelUtil<ExportSemesters>(ExportSemesters.class);
        util.exportExcel(response, list, "设置学期数据");
    }

    /**
     * 获取设置学期详细信息
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpSemestersService.selectCpSemestersById(id));
    }

    /**
     * 新增设置学期
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:add')")
    @Log(title = "设置学期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpSemesters cpSemesters)
    {
        cpSemesters.setUserId(getUserId());
        cpSemesters.setDeptId(getDeptId());
        return toAjax(cpSemestersService.insertCpSemesters(cpSemesters));
    }

    /**
     * 修改设置学期
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:edit')")
    @Log(title = "设置学期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpSemesters cpSemesters)
    {
        return toAjax(cpSemestersService.updateCpSemesters(cpSemesters));
    }

    /**
     * 删除设置学期
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:remove')")
    @Log(title = "设置学期", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpSemestersService.deleteCpSemestersByIds(ids));
    }

    /**
     *  查询列表
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:query')")
    @GetMapping("query")
    public AjaxResult queryList(CpSemesters cpSemesters){
        List<CpSemesters> queryList = cpSemestersService.query( cpSemesters);
        return AjaxResult.success(queryList);
    }

    /**
     * 设置当前学期
     */
    @PreAuthorize("@ss.hasPermi('semesters:semesters:query')")
    @Log(title = "设置班级", businessType = BusinessType.UPDATE)
    @PostMapping("updatesemester")
    public  AjaxResult  setCurrentSemester(Long id){
        return toAjax( cpSemestersService.setCurrentSemester(id, getDeptId()));
    }


}
