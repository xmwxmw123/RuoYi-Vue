package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpSubject;
import com.ruoyi.campus.service.ICpSubjectService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 设置学科Controller
 * 
 * @author ruoyi
 * @date 2023-08-08
 */
@RestController
@RequestMapping("/subject/subject")
public class CpSubjectController extends BaseController
{
    @Autowired
    private ICpSubjectService cpSubjectService;

    /**
     * 查询设置学科列表
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpSubject cpSubject)
    {
        startPage();
        List<CpSubject> list = cpSubjectService.selectCpSubjectList(cpSubject);
        return getDataTable(list);
    }

    /**
     * list接口, 查询设置学科列表
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:list')")
    @GetMapping("/query")
    public TableDataInfo query(CpSubject cpSubject)
    {
        List<CpSubject> list = cpSubjectService.selectCpSubjectList(cpSubject);
        return getDataTable(list);
    }

    /**
     * 导出设置学科列表
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:export')")
    @Log(title = "设置学科", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpSubject cpSubject)
    {
        List<CpSubject> list = cpSubjectService.selectCpSubjectList(cpSubject);
        ExcelUtil<CpSubject> util = new ExcelUtil<CpSubject>(CpSubject.class);
        util.exportExcel(response, list, "设置学科数据");
    }

    /**
     * 获取设置学科详细信息
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpSubjectService.selectCpSubjectById(id));
    }

    /**
     * 新增设置学科
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:add')")
    @Log(title = "设置学科", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpSubject cpSubject)
    {
        cpSubject.setUserId(getUserId());
        cpSubject.setDeptId(getDeptId());
        return toAjax(cpSubjectService.insertCpSubject(cpSubject));
    }

    /**
     * 修改设置学科
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:edit')")
    @Log(title = "设置学科", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpSubject cpSubject)
    {
        return toAjax(cpSubjectService.updateCpSubject(cpSubject));
    }

    /**
     * 删除设置学科
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:remove')")
    @Log(title = "设置学科", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpSubjectService.deleteCpSubjectByIds(ids));
    }
}
