package com.ruoyi.campus.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.campus.domain.CpClubEnroll;
import com.ruoyi.campus.service.ICpClubEnrollService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 社团报名Controller
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
@RestController
@RequestMapping("/clubEnroll/clubEnroll")
public class CpClubEnrollController extends BaseController
{
    @Autowired
    private ICpClubEnrollService cpClubEnrollService;

    /**
     * 查询社团报名列表
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpClubEnroll cpClubEnroll)
    {
        startPage();
        List<CpClubEnroll> list = cpClubEnrollService.selectCpClubEnrollList(cpClubEnroll);
        return getDataTable(list);
    }

    /**
     * 导出社团报名列表
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:export')")
    @Log(title = "社团报名", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpClubEnroll cpClubEnroll)
    {
        List<CpClubEnroll> list = cpClubEnrollService.selectCpClubEnrollList(cpClubEnroll);
        ExcelUtil<CpClubEnroll> util = new ExcelUtil<CpClubEnroll>(CpClubEnroll.class);
        util.exportExcel(response, list, "社团报名数据");
    }

    /**
     * 获取社团报名详细信息
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpClubEnrollService.selectCpClubEnrollById(id));
    }

    /**
     * 新增社团报名
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:add')")
    @Log(title = "社团报名", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpClubEnroll cpClubEnroll)
    {
        return toAjax(cpClubEnrollService.insertCpClubEnroll(cpClubEnroll, 1L, 1L));
    }

    /**
     * 修改社团报名
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:edit')")
    @Log(title = "社团报名", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpClubEnroll cpClubEnroll)
    {
        return toAjax(cpClubEnrollService.updateCpClubEnroll(cpClubEnroll));
    }

    /**
     * 删除社团报名
     */
    @PreAuthorize("@ss.hasPermi('clubEnroll:clubEnroll:remove')")
    @Log(title = "社团报名", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpClubEnrollService.deleteCpClubEnrollByIds(ids));
    }

    /**
     * 报名审核
     * @param ids 报名审核id  多个以逗号分割
     * @param status 状态(0-待审核 1-通过 2-驳回)
     */
    @GetMapping("/enrollExamine")
    public AjaxResult enrollExamine(@RequestParam("ids") String ids, @RequestParam("status") Long status)
    {
        cpClubEnrollService.enrollExamine(ids, status);
        return success();
    }
}
