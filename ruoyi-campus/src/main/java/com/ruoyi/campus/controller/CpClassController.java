package com.ruoyi.campus.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.campus.domain.exportVo.ExportClass;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.campus.domain.CpClass;
import com.ruoyi.campus.service.ICpClassService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 设置班级Controller
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@RestController
@RequestMapping("/class/class")
public class CpClassController extends BaseController
{
    @Autowired
    private ICpClassService cpClassService;

    /**
     * 查询设置班级列表
     */
    @PreAuthorize("@ss.hasPermi('class:class:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpClass cpClass)
    {
        startPage();
        List<CpClass> list = cpClassService.selectCpClassList(cpClass);
        return getDataTable(list);
    }

    /**
     * 导出设置班级列表
     */
    // todo 待完善
    @PreAuthorize("@ss.hasPermi('class:class:export')")
    @Log(title = "设置班级", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExportClass exportClass) throws ParseException {
        List<ExportClass> list = cpClassService.findCpClassList(exportClass);
        ExcelUtil<ExportClass> util = new ExcelUtil<ExportClass>(ExportClass.class);

        util.exportExcel(response, list, "设置班级数据");
    }

    /**
     * 获取设置班级详细信息
     */
    @PreAuthorize("@ss.hasPermi('class:class:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpClassService.selectCpClassById(id));
    }

    /**
     * 新增设置班级
     */
    @PreAuthorize("@ss.hasPermi('class:class:add')")
    @Log(title = "设置班级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpClass cpClass)
    {
        cpClass.setUserId(getUserId());
        cpClass.setDeptId(getDeptId());
        return toAjax(cpClassService.insertCpClass(cpClass));
    }

    /**
     * 修改设置班级
     */
    @PreAuthorize("@ss.hasPermi('class:class:edit')")
    @Log(title = "设置班级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpClass cpClass)
    {
        return toAjax(cpClassService.updateCpClass(cpClass));
    }

    /**
     * 删除设置班级
     */
    @PreAuthorize("@ss.hasPermi('class:class:remove')")
    @Log(title = "设置班级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpClassService.deleteCpClassByIds(ids));
    }


    /**
     *  根据届别id查询年级
     */
    @GetMapping("/queryRankSList")
    @PreAuthorize("@ss.hasPermi('class:class:query')")
    public AjaxResult queryList(Long id){

        List<CpClass> cpClasses = cpClassService.queryList(id);
        return AjaxResult.success(cpClasses);
    }


    /**
     *  查询班级列表
     *
     */
    @GetMapping("/findList")
    @PreAuthorize("@ss.hasPermi('class:class:list')")
    public AjaxResult findList( Long id){

        List<CpClass> cpClasses = cpClassService.findList(id, getDeptId());

        return AjaxResult.success(cpClasses);
    }

    /**
     *  查询班级下有学生的班级
     */
    @GetMapping("findListNotNull")
    @PreAuthorize("@ss.hasPermi('class:class:list')")
    public AjaxResult findListNotNull(CpClass cpClass){
        List<Map<String,String>> cpClasses = cpClassService.findListNotNull(cpClass);
        return AjaxResult.success(cpClasses) ;
    }





}
