package com.ruoyi.campus.controller;

import com.ruoyi.campus.domain.CpTemplate;
import com.ruoyi.campus.service.ICpTemplateService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 评价模板Controller
 * 
 * @author ruoyi
 * @date 2023-10-16
 */
@RestController
@RequestMapping("/template/template")
public class CpTemplateController extends BaseController
{
    @Autowired
    private ICpTemplateService cpTemplateService;

    /**
     * 查询评价模板列表
     */
    @PreAuthorize("@ss.hasPermi('template:template:list')")
    @GetMapping("/list")
    public TableDataInfo list(CpTemplate cpTemplate)
    {
        startPage();
        List<CpTemplate> list = cpTemplateService.selectCpTemplateList(cpTemplate);
        return getDataTable(list);
    }

    /**
     * 导出评价模板列表
     */
    @PreAuthorize("@ss.hasPermi('template:template:export')")
    @Log(title = "评价模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CpTemplate cpTemplate)
    {
        List<CpTemplate> list = cpTemplateService.selectCpTemplateList(cpTemplate);
        ExcelUtil<CpTemplate> util = new ExcelUtil<CpTemplate>(CpTemplate.class);
        util.exportExcel(response, list, "评价模板数据");
    }

    /**
     * 获取评价模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('template:template:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cpTemplateService.selectCpTemplateById(id));
    }

    /**
     * 新增评价模板
     */
    @PreAuthorize("@ss.hasPermi('template:template:add')")
    @Log(title = "评价模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CpTemplate cpTemplate)
    {
        cpTemplate.setUserId(getUserId());
        cpTemplate.setDeptId(getDeptId());
        return toAjax(cpTemplateService.insertCpTemplate(cpTemplate));
    }

    /**
     * 修改评价模板
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CpTemplate cpTemplate)
    {
        return toAjax(cpTemplateService.updateCpTemplate(cpTemplate));
    }

    /**
     * 删除评价模板
     */
    @PreAuthorize("@ss.hasPermi('template:template:remove')")
    @Log(title = "评价模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cpTemplateService.deleteCpTemplateByIds(ids));
    }

    /**
     * 修改账号是否启用
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @PutMapping("/updateEnable")
    public AjaxResult updateEnable(@RequestParam Long id)
    {
        return toAjax(cpTemplateService.updateEnable(id));
    }

    /**
     * 二级指标保存接口
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @PutMapping("/twoIndex")
    public AjaxResult twoIndex(@RequestBody CpTemplate cpTemplate)
    {
        return toAjax(cpTemplateService.twoIndex(cpTemplate));
    }

    /**
     * 记录要点保存接口
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @PutMapping("/record")
    public AjaxResult record(@RequestBody CpTemplate cpTemplate)
    {
        return toAjax(cpTemplateService.record(cpTemplate));
    }

    /**
     * 删除记录要点信息
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @DeleteMapping("/deleteRecord")
    public AjaxResult deleteRecord(@RequestBody CpTemplate cpTemplate)
    {
        return toAjax(cpTemplateService.deleteRecord(cpTemplate));
    }

    /**
     * 录入完成接口
     */
    @PreAuthorize("@ss.hasPermi('template:template:edit')")
    @Log(title = "评价模板", businessType = BusinessType.UPDATE)
    @PutMapping("/complete")
    public AjaxResult complete(@RequestParam Long id)
    {
        return toAjax(cpTemplateService.complete(id));
    }

}
