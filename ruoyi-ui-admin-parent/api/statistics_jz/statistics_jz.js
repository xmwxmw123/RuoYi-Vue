import request from '@/utils/request'

// 获取奖章统计学生信息
export function getMedalRecord(query) {
  return request({
    url: '/medalRecord/medalRecord/list',
	method: 'get',
	params: query
  })
}

// 获取学期
export function getSemesters() {
  return request({
    url: '/semesters/semesters/query',
	method: 'get',
  })
}
// 保存会员信息
export function  saveVipData(data) {
  return request({
    url: '/cg/user-info/create',
    'method': 'post',
	 'data': data
  })
}

// 卡种类型
export function getIdCardType(query) {
  return request({
    url: '/cg/card-type/page',
	method: 'get',
	params: query
  })
}


// 保存会员卡信息
export function  saveVipCard(data) {
  return request({
    url: '/cg/user-card/create',
    'method': 'post',
	 'data': data
  })
}
// 适用范围
export function getDeviceInfoList(query) {
  return request({
    url: '/cg/device-info/list',
	method: 'get',
	params: query
  })
}







