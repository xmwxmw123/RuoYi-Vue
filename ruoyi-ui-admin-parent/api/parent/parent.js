import request from '@/utils/request'


// 获取成绩信息
export function  getCampusClubs(query) {
  return request({
    url: '/club/club/list',
	method: 'get',
	params: query
  })
}

//查询社团详细信息   

export function  searchForClubs(query) {
  return request({
    url: '/parent/parent/parentAppGetCpClubInfo',
	method: 'get',
	params: query
  })
}
//报名
export function  signUp(data) {
  return request({
    url: '/parent/parent/parentAppClubEnroll',
	method: 'post',
	data: data
  })
}

//已报名社团
export function  registeredClub(query) {
  return request({
    url: '/parent/parent/parentAppQuyEnrollClubList',
	method: 'get',
	params: query
  })
}

//查询综合素质评价
export function  queryComprehquery(query) {
  return request({
    url: '/parent/parent/parentAppQuyCpQualityEvaluation',
	method: 'get',
	params: query
  })
}

//查询综合素质评价详情
export function  queryComprehqueryDe(query) {
  return request({
    url: '/parent/parent/parentAppGetCpQualityEvaluation',
	method: 'get',
	params: query
  })
}
//修改家长寄语

export function  setParentalMessages (data) {
  return request({
    url: '/parent/parent/updParentMessage',
	method: 'post',
	data: data
  })
}
//查询奖章记录
export function  searchForMedals(query) {
  return request({
    url: '/parent/parent/parentAppQuyCpMedalRecord',
	method: 'get',
	params: query
  })
}

//查询校园信息
export function  queryCampus(query) {
  return request({
    url: '/parent/parent/parentAppQuyCpCampus',
	method: 'get',
	params: query
  })
}






 






