import request from '@/utils/request'

// 全部数据
export function  getAllStudent(data) {
  return request({
    url: '/parent/parent/parentAppPracticeList',
	method:'get',
    'data':data
  })
}

// 批量删除
export function  deleteList(data) {
  return request({
    url: '/activity/activity/'+data,
    method: 'DELETE',
	
  })
}
//获取级别
export function  getGrade() {
  return request({
    url: '/grade/grade/query?graduate='+false,
	method:'get',
  })
}
//获取对应年级的班级
export function  getClass(id) {
  return request({
    url: '/activity/activity/selectRankById?period='+id,
	method:'get',
  })
}
//新增
export function  activity(data) {
  return request({
    url: '/activity/activity',
	'method': 'post',
	 'data': data
  })
}

//修改
export function editActivity(data) {
  return request({
    url: '/activity/activity',
	'method': 'PUT',
	 'data': data
  })
}
//获取对应年级的班级
export function  setActivity(data) {
  return request({
    url: '/activity/activity/'+data,
	'method': 'GET',
	
  })
}
//根据id获取数据
export function  getIdClass(query) {
  return request({
    url: '/parent/parent/parentAppActivityPartakeList',
	method:'get',
	  params: query
  })
}

//根据校园消息
export function  getSchoolMessage(query) {
  return request({
    url: '/clubActivity/clubActivity/appList',
	method:'get',
	  params: query
  })
}

















