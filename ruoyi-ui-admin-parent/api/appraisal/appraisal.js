import request from '@/utils/request'

// 全部数据
export function  gradeAssessment(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment/list',
	method:'get',
    'data':data
  })
}

// 批量删除
export function  deleteList(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment/'+data,
    method: 'DELETE',
	
  })
}

//获取批量新增时的班级
export function  getStudentClass() {
  return request({
    url: '/class/class/findListNotNull',
    'method': 'get',
  })
}

//批量新增
export function  getStudentList(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment/batchAdd',
    method: 'post',
	 'data':data
  })
}
//单个新增
export function  getStudent(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment',
    method:'post',
	'data':data
  })
}

//修改
export function  setStudent(data) {
  return request({
    url: '/gradeAssessment/gradeAssessment',
    method: 'PUT',
	 'data':data
  })
}


//获取当前学期
export function  getNowSemesters(query) {
  return request({
    url: '/semesters/semesters/query',
   method: 'get',
   params: query
  })
}







