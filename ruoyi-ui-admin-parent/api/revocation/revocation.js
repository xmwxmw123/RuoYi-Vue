import request from '@/utils/request'

// 批量删除
export function  batchDelete(data) {
  return request({
    url: '/medalRecord/medalRecord/'+data,
	method: 'DELETE',

  })
}

// 批量撤销
export function batchBackout(data) {
  return request({
    url: '/medalRecord/medalRecord/batchRevoke/'+data,
    method: 'PUT',
	
  })
}

//获取场地详细信息
export function  getSiteData(data) {
  return request({
    url: '/court/calendar/courtCalendarList',
    'method': 'post',
	 'data': data
  })
}





