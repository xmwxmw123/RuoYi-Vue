import request from '@/utils/request'
import {getRefreshToken} from "@/utils/auth";
import service from "@/utils/request";
// 登录方法
export function login(username, password, code, uuid,deptId) {
  const data = {
    username,
    password,
    code,
    uuid,
	deptId
  }
  return request({
    url: '/parentLogin',
    headers: {
      isToken: false
    },
    'method': 'post',
    'data': data
  })
}

// 获取用户组Id
export function getInfo(name) {
  return request({
    url: '/system/tenant/get-id-by-name?name='+name,
	headers: {
	  isToken: false
	},
    'method': 'get',
	
  })
}

// 获取用户权限等详细信息
export function getUserJurisdiction(data) {
  return request({
    url: '/system/auth/get-permission-info',
    'method': 'get',
  })
}

// 获取用户详细信息
export function getUser(data) {
  return request({
    url: '/system/tenant/getTenantById?id='+data,

    'method': 'get',
  })
}

// 获取用户详细信息
export function getDept(data) {
  return request({
    url: '/system/dept/parentDeptList',
    'method': 'get',
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    'method': 'post'
  })
}
// 修改密码
export function editPassword(data) {
  return request({
    url: '/system/user/profile/updatePwd?newPassword='+data.newPassword+'&oldPassword='+data.oldPassword,
    'method': 'PUT',
	
  })
}


// 刷新访问令牌
export function refreshToken() {
  return request({
    url: '/system/auth/refresh-token?refreshToken=' + getRefreshToken(),
    method: 'post'
  })
}

// 注册
export function register(data) {
  return request({
    url: '/parentRegister',
	headers: {
	  isToken: false
	},
    method: 'post',
	 'data': data
  })
}


// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}


