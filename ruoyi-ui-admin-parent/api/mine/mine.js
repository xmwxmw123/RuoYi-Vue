import request from '@/utils/request'

// 获取个人信息
export function getParticular() {
  return request({
    url: '/system/user/profile',
	method: 'get',
  })
}
// 获取班级动态
export function getClassActivity(query) {
  return request({
    url: '/activityPartake/activityPartake/list',
	method: 'get',
	params: query
  })
}



// 保存个人信息
export function saveUser(data) {
  return request({
    url: '/system/user/profile',
	method: 'PUT',
	'data':data
  })
}


// 获取学校信息
export function getSchool() {
  return request({
    url: '/campus/campus/list',
	method: 'get',
  })
}

//保存用户信息
export function saveUserDe(data) {
  return request({
    url: '/parent/parent/updMyProfile',
    method: 'post',
	 'data': data
  })
}














