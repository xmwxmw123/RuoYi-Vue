import request from '@/utils/request'

// 获取学生列表studentList
export function getstudentList(query) {
  return request({
    url: '/parent/parent/queryParentRegister',
	method: 'get',
	params: query
  })
}
// 获取已绑定学生列表studentList
export function getBdstudentList(query) {
  return request({
    url: '/parent/parent/getBindingStudentList',
	method: 'get',
	params: query
  })
}
//绑定学生
export function bindStudents(data) {
  return request({
    url: '/parent/parent/bindingStudent',
	method: 'post',
	data: data
  })
}


















