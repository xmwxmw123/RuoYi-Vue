// 应用全局配置
module.exports = {
	//刘颖超环境
	// baseUrl: 'http://192.168.0.53:48080/admin-api',
	
  // baseUrl: 'http://localhost:8080',
  // baseUrl: 'http://192.168.0.47/dev-api',
  
  //罗奥
 // baseUrl: 'http://192.168.0.17/dev-api',
 //本地
  // baseUrl: 'http://localhost:8089/dev-api',
  
  //线上环境
  baseUrl:'http://101.132.100.172/prod-api',
  // 应用信息
  appInfo: {
    // 应用名称
    name: "yudao-app",
    // 应用版本
    version: "1.0.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "https://iocoder.cn",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "https://iocoder.cn"
      },
      {
        title: "用户服务协议",
        url: "https://iocoder.cn"
      }
    ]
  }
}
